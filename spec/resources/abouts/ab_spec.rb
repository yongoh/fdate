require 'spec_helper.rb'

describe FDate::About.collection["ab"] do
  let(:about){ described_class }

  describe "#*" do
    context "両端が同じ数値の範囲を渡した場合" do
      it{ expect(about * (5..5)).to eq(5..5) }
    end

    context "1桁の範囲を渡した場合" do
      it{ expect(about * (0..9)).to eq(0..9) }
    end

    context "3桁の範囲を渡した場合" do
      it{ expect(about * (0..999)).to eq(0..999) }
    end

    context "負数の範囲を渡した場合" do
      it{ expect(about * (-9..0)).to eq(-9..0) }
    end

    context "中途半端な範囲を渡した場合" do
      it{ expect(about * (12..25)).to eq(12..25) }
    end
  end
end
