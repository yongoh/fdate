require 'spec_helper.rb'
require 'calendar_system_spec_helper'

describe FDate::Calendar.collection["Russia"] do
  extend CalendarSystemSpecHelper

  describe "::jd" do
    jd_test(2049782, "adjl", {year: {num: 899}, month: 12, day: 31})
    jd_test(2049783, "bzjl", {year: {num: 6408}, month: 1, day: 1})
    jd_test(2341982, "bzjl", {year: {num: 7207}, month: 12, day: 31})
    jd_test(2341983, "adjl", {year: {num: 1700}, month: 1, day: 1})
    jd_test(2421638, "adjl", {year: {num: 1918}, month: 1, day: 31})
    jd_test(2421639, "adgr", {year: {num: 1918}, month: 2, day: 14})
    jd_test(2425885, "adgr", {year: {num: 1929}, month: 9, day: 30})
    jd_test(2425886, "USSR", {year: {num: 1929}, month: 10, day: 1})
    jd_test(2429807, "USSR", {year: {num: 1940}, month: 6, day: 26})
    jd_test(2429808, "adgr", {year: {num: 1940}, month: 6, day: 27})
  end

  describe "::default_calendar" do
    it{ expect(described_class.default_calendar.key).to eq("adjl") }
  end
end
