require 'spec_helper.rb'

describe FDate::Calendar.collection["JulianCalendar"] do
  let(:jdy){ local_year + 4713 }

  describe "::leap_year?" do
    context "キリスト紀元年が4で割り切れないユリウス通年を渡した場合" do
      let(:local_year){ 1995 }
      it{ expect(described_class).not_to be_leap_year(jdy) }
    end

    context "キリスト紀元年が4で割り切れるユリウス通年を渡した場合" do
      let(:local_year){ 1996 }
      it{ expect(described_class).to be_leap_year(jdy) }
    end

    context "キリスト紀元年が100で割り切れるユリウス通年を渡した場合" do
      let(:local_year){ 1900 }
      it{ expect(described_class).to be_leap_year(jdy) }
    end

    context "キリスト紀元年が400で割り切れるユリウス通年を渡した場合" do
      let(:local_year){ 2000 }
      it{ expect(described_class).to be_leap_year(jdy) }
    end

    context "紀元前1年の場合" do
      let(:local_year){ 0 }
      it{ expect(described_class).to be_leap_year(jdy) }
    end

    context "紀元前2年の場合" do
      let(:local_year){ -1 }
      it{ expect(described_class).not_to be_leap_year(jdy) }
    end

    context "紀元前5年の場合" do
      let(:local_year){ -4 }
      it{ expect(described_class).to be_leap_year(jdy) }
    end
  end

  describe "::mdays" do
    subject{ described_class.mdays(jdy, month) }

    shared_examples_for "return long month days" do
      it "は、大の月の日数を返すこと" do
        is_expected.to eq(31)
      end
    end

    shared_examples_for "return short month days" do
      it "は、小の月の日数を返すこと" do
        is_expected.to eq(30)
      end
    end

    shared_examples_for "other monthes" do
      {
        1 => "long",
        3 => "long",
        4 => "short",
        5 => "long",
        6 => "short",
        7 => "long",
        8 => "long",
        9 => "short",
        10 => "long",
        11 => "short",
        12 => "long",
      }.each do |m, xxx|
        context "#{m}月を表す数を渡した場合" do
          let(:month){ m }
          it_behaves_like "return #{xxx} month days"
        end
      end
    end


    context "`::leap_year? == true`となるユリウス通年と" do
      let(:local_year){ 1995 }

      it_behaves_like "other monthes"

      context "2月を表す数を渡した場合" do
        let(:month){ 2 }
        it{ is_expected.to eq(28) }
      end
    end

    context "`::leap_year? == false`となるユリウス通年と" do
      let(:local_year){ 1996 }

      it_behaves_like "other monthes"

      context "2月を表す数を渡した場合" do
        let(:month){ 2 }
        it{ is_expected.to eq(29) }
      end
    end
  end

  describe "::solar?,::lunar?,::lunisolar?" do
    it "は、太陽暦であることを示すこと" do
      expect(described_class).to be_solar
      expect(described_class).not_to be_lunar
      expect(described_class).not_to be_lunisolar
    end
  end

  describe "日付・ユリウス通日変換メソッド群" do
    shared_examples_for "jd methods" do
      describe "::from_jd" do
        it "は、渡したユリウス通日を日付インスタンスを生成するためのハッシュに変換して返すこと" do
          expect(described_class.from_jd(jd)).to eq(date_hash)
        end
      end

      describe "::to_jd" do
        it "は、渡した日付ハッシュをユリウス通日に変換して返すこと" do
          expect(described_class.to_jd(date_hash[:year], date_hash[:month], date_hash[:day])).to eq(jd)
        end
      end
    end


    context "日付コード'adjl_B=44-03-15_j'相当（ユリウス・カエサル暗殺）" do
      let(:jd){ 1705426 }
      let(:date_hash){ {year: jdy, month: 3, day: 15} }
      let(:local_year){ -43 }
      it_behaves_like "jd methods"
    end

    context "日付コード'adjl_800-12-25_j'相当（カールの戴冠）" do
      let(:jd){ 2013617 }
      let(:date_hash){ {year: jdy, month: 12, day: 25} }
      let(:local_year){ 800 }
      it_behaves_like "jd methods"
    end

    context "日付コード'adjl_1582-10-04_j'相当（イタリアにおいてユリウス暦が使われた最後の日）" do
      let(:jd){ 2299160 }
      let(:date_hash){ {year: jdy, month: 10, day: 4} }
      let(:local_year){ 1582 }
      it_behaves_like "jd methods"
    end

    context "日付コード'adgr_1868-01-27_j'相当（鳥羽伏見の戦い開戦日）" do
      let(:jd){ 2403359 }
      let(:date_hash){ {year: jdy, month: 1, day: 15} }
      let(:local_year){ 1868 }
      it_behaves_like "jd methods"
    end

    context "日付コード'adgr_2008-09-15_j'相当（リーマン・ブラザーズ破綻）" do
      let(:jd){ 2454725 }
      let(:date_hash){ {year: jdy, month: 9, day: 2} }
      let(:local_year){ 2008 }
      it_behaves_like "jd methods"
    end
  end
end
