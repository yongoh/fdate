require 'spec_helper.rb'
require 'calendar_system_spec_helper'

describe FDate::Calendar.collection["England"] do
  extend CalendarSystemSpecHelper

  describe "::jd" do
    jd_test(1723979, "adgr", {year: {num: 7}, month: 12, day: 29})
    jd_test(1723980, "adjl", {year: {num: 8}, month: 1, day: 1})
    jd_test(2361221, "adjl", {year: {num: 1752}, month: 9, day: 2})
    jd_test(2361222, "adgr", {year: {num: 1752}, month: 9, day: 14})
  end

  describe "::default_calendar" do
    it{ expect(described_class.default_calendar.key).to eq("adgr") }
  end
end
