require 'spec_helper.rb'

describe FDate::Calendar.collection["adgr"] do
  describe "#convert_to" do
    subject{ fdate.convert_to("adjl") }
    let(:adjl){ FDate::Calendar.collection["adjl"] }

    context "fixedの場合" do
      let(:fdate){ described_class.new(year: {num: 2017}, month: 1, day: 17) }

      it "は、指定した暦の日付オブジェクトに変換すること" do
        is_expected.to eql(adjl.new(year: {num: 2017}, month: 1, day: 4))
      end
    end

    context "fuzzyの場合" do
      let(:fdate){ described_class.new(year: {num: 2017}, month: 1) }

      it "は、指定した暦の期間オブジェクトに変換すること" do
        is_expected.to eql(FDate::Term.new(
          first: adjl.new(year: {num: 2016}, month: 12, day: 19),
          last: adjl.new(year: {num: 2017}, month: 1, day: 18),
        ))
      end
    end
  end
end
