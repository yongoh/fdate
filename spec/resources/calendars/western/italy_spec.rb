require 'spec_helper.rb'
require 'calendar_system_spec_helper'

describe FDate::Calendar.collection["Italy"] do
  extend CalendarSystemSpecHelper

  describe "::jd" do
    jd_test(1723979, "adgr", {year: {num: 7}, month: 12, day: 29})
    jd_test(1723980, "adjl", {year: {num: 8}, month: 1, day: 1})
    jd_test(2299160, "adjl", {year: {num: 1582}, month: 10, day: 4})
    jd_test(2299161, "adgr", {year: {num: 1582}, month: 10, day: 15})
  end

  describe "::default_calendar" do
    it{ expect(described_class.default_calendar.key).to eq("adgr") }
  end
end
