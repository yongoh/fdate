describe FDate::Calendar.modules["NarrowDownReigns"] do
  let(:calendar){
    mod = described_class
    y = year_class

    Class.new(adgr) do
      include mod

      self.key = "cal"
      self.year_class = y
    end
  }

  let(:year_class){
    raigns = [reign_1, reign_2]

    Class.new(FDate::Part.collection["RegnalYearSet"]) do
      self.reign_collection = raigns
    end
  }

  let(:reign_1){
    term = FDate::Term.new(
      first: adgr.new(year: 10, month: 6, day: 15),
      last: adgr.new(year: 20, month: 6, day: 15),
    )

    Class.new(FDate::Part.collection["RegnalYear"]) do
      self.key = "Foo"
      self.term = term
    end
  }

  let(:reign_2){
    term = FDate::Term.new(
      first: adgr.new(year: 20, month: 6, day: 15),
      last: adgr.new(year: 30, month: 6, day: 15),
    )

    Class.new(FDate::Part.collection["RegnalYear"]) do
      self.key = "Bar"
      self.term = term
    end
  }

  let(:adgr){ FDate::Calendar.collection["adgr"] }


  describe "#narrow_down_reigns" do
    context "元号の無い時期とある時期にまたがる年の" do
      context "元号の無い日付の場合" do
        let(:fdate){ calendar.new(year: 10, month: 1, day: 1) }

        it "は、ユリウス通年をラップした元号年セットを再割り当てすること" do
          expect(fdate.local_code).to eq("cal_(10)-1-1_j")
          expect(fdate.year).to be_jdy
        end
      end

      context "元号の無い時期とある時期にまたがる日付の場合" do
        let(:fdate){ calendar.new(year: 10, month: 6) }

        it "は、元号年をラップした元号年セットを再割り当てすること" do
          expect(fdate.local_code).to eq("cal_Foo=1-6-*_j")
          expect(fdate.year.years.map(&:class)).to eq([reign_1])
        end
      end
    end

    context "複数の元号にまたがる年の" do
      context "元号が1つだけある日付の場合" do
        let(:fdate){ calendar.new(year: 20, month: 1, day: 1) }

        it "は、1つの元号年をラップした元号年セットを再割り当てすること" do
          expect(fdate.local_code).to eq("cal_Foo=11-1-1_j")
          expect(fdate.year.years.map(&:class)).to eq([reign_1])
        end
      end

      context "複数の元号にまたがる日付の場合" do
        let(:fdate){ calendar.new(year: 20, month: 6) }

        it "は、複数の元号年をラップした元号年セットを再割り当てすること" do
          expect(fdate.local_code).to eq("cal_Foo=11/Bar=1-6-*_j")
          expect(fdate.year.years.map(&:class)).to eq([reign_1, reign_2])
        end
      end

      context "複数の元号がある日付の場合" do
        let(:fdate){ calendar.new(year: 20, month: 6, day: 15) }

        it "は、複数の元号年をラップした元号年セットを再割り当てすること" do
          expect(fdate.local_code).to eq("cal_Foo=11/Bar=1-6-15_j")
          expect(fdate.year.years.map(&:class)).to eq([reign_1, reign_2])
        end
      end
    end
  end
end
