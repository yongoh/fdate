require 'spec_helper.rb'

describe FDate::Calendar.collection["jpgr"] do
  context "日付コード'adgr_1989-1-7_j'相当（昭和最後の日）" do
    let(:fdate){ described_class.new(year: 4713 + 1989, month: 1, day: 7) }

    describe "local_code" do
      it{ expect(fdate.local_code).to eq("jpgr_昭和=64-1-7_j") }
    end

    it{ expect(fdate.year).to be_single }
    it{ expect(fdate).to be_fixed }
  end

  context "日付コード'adgr_1989-1-8_j'相当（平成最初の日）" do
    let(:fdate){ described_class.new(year: 4713 + 1989, month: 1, day: 8) }

    describe "local_code" do
      it{ expect(fdate.local_code).to eq("jpgr_平成=1-1-8_j") }
    end

    it{ expect(fdate.year).to be_single }
    it{ expect(fdate).to be_fixed }
  end

  context "日付コード'adgr_1989-1-0*_j'相当（昭和と平成にまたがる期間）" do
    let(:fdate){ described_class.new(year: 4713 + 1989, month: 1, day: {num: 0, accuracy: 1}, about: about) }

    context "`about: 'j'(default)`の場合" do
      let(:about){ nil }

      describe "local_code" do
        it{ expect(fdate.local_code).to eq("jpgr_昭和=64/平成=1-1-0*_j") }
      end

      it "は、元号年セットオブジェクトに昭和と平成両方の元号年オブジェクトを格納すること" do
        expect(fdate.year).to be_plural
      end

      it{ expect(fdate).to be_fuzzy }
    end

    context "`about: '1-5'`の場合（昭和しか含まない期間）" do
      let(:about){ '1-5' }

      describe "local_code" do
        it{ expect(fdate.local_code).to eq("jpgr_昭和=64-1-0*_1-5") }
      end

      it{ expect(fdate.year).to be_single }
      it{ expect(fdate).to be_fuzzy }
    end

    context "`about: '5-5'`の場合（平成しか含まない期間）" do
      let(:about){ '5-5' }

      describe "local_code" do
        it{ expect(fdate.local_code).to eq("jpgr_平成=1-1-0*_5-5") }
      end

      it{ expect(fdate.year).to be_single }
      it{ expect(fdate).to be_fuzzy }
    end
  end

  context "日付コード'adgr_686-1-1_j'相当（元号のある年の元号の無い日付）" do
    let(:fdate){ described_class.new(year: 4713 + 686, month: 1, day: 1) }

    describe "local_code" do
      it{ expect(fdate.local_code).to eq("jpgr_天武天皇=14-1-1_j") }
    end

    it{ expect(fdate.year).to be_single }
    it{ expect(fdate).to be_fixed }
  end
end
