require 'spec_helper.rb'
require 'date'

describe FDate::Calendar.collection["EarthCalendar"] do
  describe "#jd" do
    it "は、ユリウス通日を返すこと" do
      structs.each do |str|
        expect(str.fdate.jd).to eq(str.date.jd)
      end
    end
  end

  describe "#yday" do
    it "は、この年に入ってからの日数を返すこと" do
      structs.each do |str|
        expect(str.fdate.yday).to eq(str.date.yday)
      end
    end
  end

  describe "#mday" do
    it "は、この月に入ってからの日数を返すこと" do
      structs.each do |str|
        expect(str.fdate.mday).to eq(str.date.mday)
      end
    end
  end

  describe "#wday" do
    it "は、日曜日を0とする曜日番号を返すこと" do
      structs.each do |str|
        expect(str.fdate.wday).to eq(str.wday).and eq(str.date.wday)
      end
    end
  end

  describe "#cwday" do
    it "は、月曜日を1・日曜日を7とする曜日番号を返すこと" do
      structs.each do |str|
        cwday = str.wday == 0 ? 7 : str.wday
        expect(str.fdate.cwday).to eq(cwday).and eq(str.date.cwday)
      end
    end
  end

  describe "#week" do
    it "は、1月1日から始まる週の週番号を返すこと" do
      structs.each do |str|
        expect(str.fdate.week).to eq(str.week)
      end
    end
  end

  describe "#cweek" do
    it "は、月曜日から始まる週の週番号を返すこと" do
      structs.each do |str|
        expect(str.fdate.cweek).to eq(str.cweek).and eq(str.date.cweek)
      end
    end
  end

  describe "#cwyear" do
    it "は、`#cweek`に対応する年を返すこと" do
      structs.each do |str|
        expect(str.fdate.cwyear - 4713).to eq(str.cwyear).and eq(str.date.cwyear)
      end
    end
  end

  %i(sunday? monday? tuesday? wednesday? thursday? friday? saturday?).each_with_index do |method_name, i|
    describe "##{method_name}" do
      it "は、？曜日であるかどうかを示すこと" do
        structs.each do |str|
          expect(str.fdate.send(method_name)).to eq(str.wday == i).and eq(str.date.send(method_name))
        end
      end
    end
  end


  struct = Struct.new(:date, :wday, :week, :cweek, :cwyear) do
    def fdate
      @fdate ||= FDate::Calendar.collection["adgr"].new(year: {num: date.year}, month: date.month, day: date.day)
    end
  end

  let(:structs){
    [
      # 4（木曜日）に始まる年
      struct.new(Date.new(2008, 12, 28), 0, 53, 52, 2008),
      struct.new(Date.new(2008, 12, 29), 1, 53,  1, 2009),
      struct.new(Date.new(2008, 12, 30), 2, 53,  1, 2009),
      struct.new(Date.new(2008, 12, 31), 3, 53,  1, 2009),
      struct.new(Date.new(2009,  1,  1), 4,  1,  1, 2009),
      struct.new(Date.new(2009,  1,  2), 5,  1,  1, 2009),
      struct.new(Date.new(2009,  1,  3), 6,  1,  1, 2009),
      struct.new(Date.new(2009,  1,  4), 0,  2,  1, 2009),
      struct.new(Date.new(2009,  1,  5), 1,  2,  2, 2009),
      # 5（金曜日）に始まる年
      struct.new(Date.new(2009, 12, 31), 4, 53, 53, 2009),
      struct.new(Date.new(2010,  1,  1), 5,  1, 53, 2009),
      struct.new(Date.new(2010,  1,  2), 6,  1, 53, 2009),
      struct.new(Date.new(2010,  1,  3), 0,  2, 53, 2009),
      struct.new(Date.new(2010,  1,  4), 1,  2,  1, 2010),
      struct.new(Date.new(2010,  1,  5), 2,  2,  1, 2010),
      struct.new(Date.new(2010,  1,  6), 3,  2,  1, 2010),
      struct.new(Date.new(2010,  1,  7), 4,  2,  1, 2010),
      struct.new(Date.new(2010,  1,  8), 5,  2,  1, 2010),
      struct.new(Date.new(2010,  1,  9), 6,  2,  1, 2010),
      struct.new(Date.new(2010,  1, 10), 0,  3,  1, 2010),
      struct.new(Date.new(2010,  1, 11), 1,  3,  2, 2010),
      # 6（土曜日）に始まる年
      struct.new(Date.new(2010, 12, 31), 5, 53, 52, 2010),
      struct.new(Date.new(2011,  1,  1), 6,  1, 52, 2010),
      struct.new(Date.new(2011,  1,  2), 0,  2, 52, 2010),
      struct.new(Date.new(2011,  1,  3), 1,  2,  1, 2011),
      struct.new(Date.new(2011,  1,  4), 2,  2,  1, 2011),
      struct.new(Date.new(2011,  1,  5), 3,  2,  1, 2011),
      struct.new(Date.new(2011,  1,  6), 4,  2,  1, 2011),
      struct.new(Date.new(2011,  1,  7), 5,  2,  1, 2011),
      struct.new(Date.new(2011,  1,  8), 6,  2,  1, 2011),
      struct.new(Date.new(2011,  1,  9), 0,  3,  1, 2011),
      struct.new(Date.new(2011,  1, 10), 1,  3,  2, 2011),
      # 0（日曜日）に始まる年
      struct.new(Date.new(2011, 12, 31), 6, 53, 52, 2011),
      struct.new(Date.new(2012,  1,  1), 0,  1, 52, 2011),
      struct.new(Date.new(2012,  1,  2), 1,  1,  1, 2012),
      struct.new(Date.new(2012,  1,  3), 2,  1,  1, 2012),
      struct.new(Date.new(2012,  1,  4), 3,  1,  1, 2012),
      struct.new(Date.new(2012,  1,  5), 4,  1,  1, 2012),
      struct.new(Date.new(2012,  1,  6), 5,  1,  1, 2012),
      struct.new(Date.new(2012,  1,  7), 6,  1,  1, 2012),
      struct.new(Date.new(2012,  1,  8), 0,  2,  1, 2012),
      struct.new(Date.new(2012,  1,  9), 1,  2,  2, 2012),
      # 1（月曜日）に始まる年
      struct.new(Date.new(2017, 12, 31), 0, 53, 52, 2017),
      struct.new(Date.new(2018,  1,  1), 1,  1,  1, 2018),
      struct.new(Date.new(2018,  1,  2), 2,  1,  1, 2018),
      struct.new(Date.new(2018,  1,  3), 3,  1,  1, 2018),
      struct.new(Date.new(2018,  1,  4), 4,  1,  1, 2018),
      struct.new(Date.new(2018,  1,  5), 5,  1,  1, 2018),
      struct.new(Date.new(2018,  1,  6), 6,  1,  1, 2018),
      struct.new(Date.new(2018,  1,  7), 0,  2,  1, 2018),
      struct.new(Date.new(2018,  1,  8), 1,  2,  2, 2018),
      # 2（火曜日）に始まる年
      struct.new(Date.new(2018, 12, 30), 0, 53, 52, 2018),
      struct.new(Date.new(2018, 12, 31), 1, 53,  1, 2019),
      struct.new(Date.new(2019,  1,  1), 2,  1,  1, 2019),
      struct.new(Date.new(2019,  1,  2), 3,  1,  1, 2019),
      struct.new(Date.new(2019,  1,  3), 4,  1,  1, 2019),
      struct.new(Date.new(2019,  1,  4), 5,  1,  1, 2019),
      struct.new(Date.new(2019,  1,  5), 6,  1,  1, 2019),
      struct.new(Date.new(2019,  1,  6), 0,  2,  1, 2019),
      struct.new(Date.new(2019,  1,  7), 1,  2,  2, 2019),
      # 3（水曜日）に始まる年
      struct.new(Date.new(2019, 12, 29), 0, 53, 52, 2019),
      struct.new(Date.new(2019, 12, 30), 1, 53,  1, 2020),
      struct.new(Date.new(2019, 12, 31), 2, 53,  1, 2020),
      struct.new(Date.new(2020,  1,  1), 3,  1,  1, 2020),
      struct.new(Date.new(2020,  1,  2), 4,  1,  1, 2020),
      struct.new(Date.new(2020,  1,  3), 5,  1,  1, 2020),
      struct.new(Date.new(2020,  1,  4), 6,  1,  1, 2020),
      struct.new(Date.new(2020,  1,  5), 0,  2,  1, 2020),
      struct.new(Date.new(2020,  1,  6), 1,  2,  2, 2020),
      struct.new(Date.new(2020,  1,  7), 2,  2,  2, 2020),
    ]
  }
end
