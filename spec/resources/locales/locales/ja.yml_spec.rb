require 'spec_helper.rb'

describe FDate::Locale.collection["ja"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "ja"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe "#strftime" do
    subject{ described_class.strftime }

    describe FDate::Locale.collection["base"] do
      it{ is_expected.to eq("base") }
    end

    describe FDate::Locale.collection["ja"] do
      it{ is_expected.to eq("日本語") }
    end

    describe FDate::Locale.collection["en"] do
      it{ is_expected.to eq("英語") }
    end
  end
end
