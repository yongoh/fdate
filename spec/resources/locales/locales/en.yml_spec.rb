require 'spec_helper.rb'

describe FDate::Locale.collection["en"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "en"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe "#strftime" do
    subject{ described_class.strftime }

    describe FDate::Locale.collection["base"] do
      it{ is_expected.to eq("base") }
    end

    describe FDate::Locale.collection["ja"] do
      it{ is_expected.to eq("Japanese") }
    end

    describe FDate::Locale.collection["en"] do
      it{ is_expected.to eq("English") }
    end
  end
end
