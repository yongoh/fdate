require 'spec_helper.rb'
require 'date'

describe FDate::Locale.collection["en"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "en"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe FDate::Calendar.collection["adgr"] do
    describe "::strftime" do
      it "は、暦の名前を返すこと" do
        expect(described_class.strftime).to eq("Anno domini with Gregorian calendar")
      end
    end

    describe "#strftime" do
      let(:fixed_fdate){ described_class.new(year: {num: 2018}, month: 8, day: 5) }
      let(:fuzzy_fdate){ described_class.new(year: {num: 2018}, month: 8, day: {num: 20, accuracy: 1}, about: "1-2") }
      let(:date){ Date.new(2018, 8, 5) }

      context "`:long`を渡した場合 (default)" do
        it "は、日付を長く翻訳した文字列を返すこと" do
          expect(fuzzy_fdate.strftime).to eq("first half of 2*'s August 2018")
        end
      end

      context "`:short`を渡した場合" do
        it "は、日付を短く翻訳した文字列を返すこと" do
          expect(fuzzy_fdate.strftime(:short)).to eq("first half of 2*'s Aug. '18")
        end
      end

      context "`:hyphen`を渡した場合" do
        it "は、'-'で日付パートを区切った日付文字列（「頃」つき）を返すこと" do
          expect(fuzzy_fdate.strftime(:hyphen)).to eq("first half of 2*-08-2018")
        end
      end

      context "`:slash`を渡した場合" do
        it "は、'/'で日付パートを区切った日付文字列（「頃」つき）を返すこと" do
          expect(fuzzy_fdate.strftime(:slash)).to eq("first half of 2*/08/2018")
        end
      end

      context "'%b'を渡した場合" do
        it "は、簡略化した月の名前を返すこと" do
          expect(fuzzy_fdate.strftime("%b")).to eq("Aug.")
        end
      end

      context "'%B'を渡した場合" do
        it "は、月の名前を返すこと" do
          expect(fuzzy_fdate.strftime("%B")).to eq("August")
        end
      end

      context "'%x'を渡した場合" do
        it "は、ロケールの適切な日付表現でフォーマットした日付文字列を返すこと" do
          expect(fixed_fdate.strftime("%x")).to eq("08/05/18").and eq(date.strftime("%x"))
        end
      end

      context "'%v'を渡した場合" do
        it "は、VMS形式でフォーマットした日付文字列を返すこと" do
          expect(fuzzy_fdate.strftime("%v")).to eq("2*-Aug.-2018")
        end
      end

      context "'%a'を渡した場合" do
        it "は、簡略化した曜日の名前を返すこと" do
          expect(fixed_fdate.strftime("%a")).to eq("Sun.")
        end
      end

      context "'%A'を渡した場合" do
        it "は、曜日の名前を返すこと" do
          expect(fixed_fdate.strftime("%A")).to eq("Sunday")
        end
      end
    end
  end

  describe FDate::InfinityDate do
    describe "#strftime" do
      context "無限過去の場合" do
        let(:infinity_date){ described_class.negative }
        it{ expect(infinity_date.strftime).to eq("infinite past") }
      end

      context "無限未来の場合" do
        let(:infinity_date){ described_class.positive }
        it{ expect(infinity_date.strftime).to eq("infinite future") }
      end
    end
  end

  describe FDate::Term do
    describe "#strftime" do
      let(:adgr){ FDate::Calendar.collection["adgr"] }

      context "両端が日付の場合" do
        let(:term){
          described_class.new(
            first: adgr.new(year: {num: 2018}, month: 2, day: 15),
            last: adgr.new(year: {num: 2018}, month: 8, day: {num: 20, accuracy: 1}, about: '1-2'),
          )
        }

        it{ expect(term.strftime).to eq("15 February 2018 – first half of 2*'s August 2018") }
      end

      context "始端が無限過去の場合" do
        let(:term){
          described_class.new(
            last: adgr.new(year: {num: 2018}, month: 8, day: {num: 20, accuracy: 1}, about: '1-2'),
          )
        }

        it{ expect(term.strftime).to eq("infinite past – first half of 2*'s August 2018") }
      end

      context "終端が無限未来の場合" do
        let(:term){
          described_class.new(
            first: adgr.new(year: {num: 2018}, month: 2, day: 15),
          )
        }

        it{ expect(term.strftime).to eq("15 February 2018 – infinite future") }
      end
    end
  end
end
