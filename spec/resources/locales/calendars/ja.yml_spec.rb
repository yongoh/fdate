require 'spec_helper.rb'

describe FDate::Locale.collection["ja"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "ja"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe FDate::Calendar.collection["adgr"] do
    describe "::strftime" do
      it "は、暦の名前を返すこと" do
        expect(described_class.strftime).to eq("西暦（グレゴリオ暦）")
      end
    end

    describe "#strftime" do
      let(:fixed_fdate){ described_class.new(year: {num: 2018}, month: 8, day: 5) }
      let(:fuzzy_fdate){ described_class.new(year: {num: 2018}, month: 8, day: {num: 20, accuracy: 1}, about: "1-2") }

      context "`:long`を渡した場合 (default)" do
        it "は、日付を長く翻訳した文字列を返すこと" do
          expect(fuzzy_fdate.strftime).to eq("2018年8月2*日台前半")
        end
      end

      context "`:short`を渡した場合" do
        it "は、日付を短く翻訳した文字列を返すこと" do
          expect(fuzzy_fdate.strftime(:short)).to eq("18/08/2*前半")
        end
      end

      context "`:hyphen`を渡した場合" do
        it "は、'-'で日付パートを区切った日付文字列（「頃」つき）を返すこと" do
          expect(fuzzy_fdate.strftime(:hyphen)).to eq("2018-08-2*前半")
        end
      end

      context "`:slash`を渡した場合" do
        it "は、'/'で日付パートを区切った日付文字列（「頃」つき）を返すこと" do
          expect(fuzzy_fdate.strftime(:slash)).to eq("2018/08/2*前半")
        end
      end

      context "'%x'を渡した場合" do
        it "は、ロケールの適切な日付表現でフォーマットした日付文字列を返すこと" do
          expect(fuzzy_fdate.strftime("%x")).to eq("18/08/2*")
        end
      end

      context "'%a'を渡した場合" do
        it "は、簡略化した曜日の名前を返すこと" do
          expect(fixed_fdate.strftime("%a")).to eq("日")
        end
      end

      context "'%A'を渡した場合" do
        it "は、曜日の名前を返すこと" do
          expect(fixed_fdate.strftime("%A")).to eq("日曜日")
        end
      end
    end
  end

  describe FDate::InfinityDate do
    describe "#strftime" do
      context "無限過去の場合" do
        let(:infinity_date){ described_class.negative }
        it{ expect(infinity_date.strftime).to eq("無限過去") }
      end

      context "無限未来の場合" do
        let(:infinity_date){ described_class.positive }
        it{ expect(infinity_date.strftime).to eq("無限未来") }
      end
    end
  end

  describe FDate::Term do
    describe "#strftime" do
      let(:adgr){ FDate::Calendar.collection["adgr"] }

      context "両端が日付の場合" do
        let(:term){
          described_class.new(
            first: adgr.new(year: {num: 2018}, month: 2, day: 15),
            last: adgr.new(year: {num: 2018}, month: 8, day: {num: 20, accuracy: 1}, about: '1-2'),
          )
        }

        it{ expect(term.strftime).to eq("2018年2月15日〜2018年8月2*日台前半") }
      end

      context "始端が無限過去の場合" do
        let(:term){
          described_class.new(
            last: adgr.new(year: {num: 2018}, month: 8, day: {num: 20, accuracy: 1}, about: '1-2'),
          )
        }

        it{ expect(term.strftime).to eq("無限過去〜2018年8月2*日台前半") }
      end

      context "終端が無限未来の場合" do
        let(:term){
          described_class.new(
            first: adgr.new(year: {num: 2018}, month: 2, day: 15),
          )
        }

        it{ expect(term.strftime).to eq("2018年2月15日〜無限未来") }
      end
    end
  end
end
