require 'spec_helper.rb'
require 'date'

describe FDate::Locale.collection["base"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "base"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe FDate::Calendar.collection["adgr"] do
    describe "::strftime" do
      it "は、暦の名前を返すこと" do
        expect(described_class.strftime).to eq("adgr")
      end
    end

    describe "#strftime" do
      let(:fixed_fdate){ described_class.new(year: {num: 2018}, month: 3, day: 1) }
      let(:fuzzy_fdate){ described_class.new(year: {num: 2018}, month: 8, day: {num: 20, accuracy: 1}, about: "1-2") }
      let(:date){ Date.new(2018, 3, 1) }

      context "`:long`を渡した場合 (default)" do
        it "は、日付を長く翻訳した文字列を返すこと" do
          expect(fuzzy_fdate.strftime).to eq("2018/8/2*(1/2)")
        end
      end

      context "`:short`を渡した場合" do
        it "は、日付を短く翻訳した文字列を返すこと" do
          expect(fuzzy_fdate.strftime(:short)).to eq("18/08/2*(1/2)")
        end
      end

      context "`:hyphen`を渡した場合" do
        it "は、'-'で日付パートを区切った日付文字列（「頃」つき）を返すこと" do
          expect(fuzzy_fdate.strftime(:hyphen)).to eq("2018-08-2*(1/2)")
        end
      end

      context "`:slash`を渡した場合" do
        it "は、'/'で日付パートを区切った日付文字列（「頃」つき）を返すこと" do
          expect(fuzzy_fdate.strftime(:slash)).to eq("2018/08/2*(1/2)")
        end
      end

      context "'%y'を渡した場合" do
        it "は、年の末尾2桁を返すこと" do
          expect(fuzzy_fdate.strftime("%y")).to eq("18").and eq(date.strftime("%y"))
        end
      end

      context "'%Y'を渡した場合" do
        context "紀元後の場合" do
          it "は、年の数値文字列を返すこと" do
            expect(fuzzy_fdate.strftime("%Y")).to eq("2018").and eq(date.strftime("%Y"))
          end
        end

        context "紀元前の場合" do
          let(:fdate){ described_class.new(year: {num: -1}) }
          let(:date){ Date.new(-1) }

          it "は、0または負数の年の数値文字列（符号を除き4桁）を返すこと" do
            expect(fdate.strftime("%Y")).to eq("-0001").and eq(date.strftime("%Y"))
          end
        end

        context "fuzzyな年の場合" do
          let(:fdate){ described_class.new(year: {num: -100, accuracy: 2}) }
          it{ expect(fdate.strftime("%Y")).to eq("0000") }
        end
      end

      context "'%m'を渡した場合" do
        it "は、月の数値（0埋め2桁）を返すこと" do
          expect(fixed_fdate.strftime("%m")).to eq("03").and eq(date.strftime("%m"))
        end
      end

      context "'%d'を渡した場合" do
        it "は、日の数値（0埋め2桁）を返すこと" do
          expect(fixed_fdate.strftime("%d")).to eq("01").and eq(date.strftime("%d"))
        end
      end

      context "'%e'を渡した場合" do
        it "は、日の数値（空白埋め2桁）を返すこと" do
          expect(fixed_fdate.strftime("%e")).to eq(" 1").and eq(date.strftime("%e"))
        end
      end

      context "'%D'を渡した場合" do
        it "は、米国式でフォーマットした日付文字列を返すこと" do
          expect(fixed_fdate.strftime("%D")).to eq("03/01/18").and eq(date.strftime("%D"))
        end
      end

      context "'%F'を渡した場合" do
        it "は、ISO 8601式でフォーマットした日付文字列を返すこと" do
          expect(fixed_fdate.strftime("%F")).to eq("2018-03-01").and eq(date.strftime("%F"))
        end
      end

      context "'%x'を渡した場合" do
        it "は、ロケールの適切な日付表現でフォーマットした日付文字列を返すこと" do
          expect(fixed_fdate.strftime("%x")).to eq("18/03/01")
        end
      end

      context "'%w'を渡した場合" do
        it "は、日曜日を0とした曜日の数を返すこと" do
          expect(fixed_fdate.strftime("%w")).to eq("4").and eq(date.strftime("%w"))
        end
      end

      context "'%V'を渡した場合" do
        it "は、暦週（0埋め2桁）を返すこと" do
          expect(fixed_fdate.strftime("%V")).to eq("09").and eq(date.strftime("%V"))
        end
      end

      context "'%j'を渡した場合" do
        it "は、この年に入ってから経過した日数（0埋め3桁）を返すこと" do
          expect(fixed_fdate.strftime("%j")).to eq("060").and eq(date.strftime("%j"))
        end
      end
    end
  end

  describe FDate::CalendarSystem do
    describe "#strftime" do
      let(:fdate){ FDate::Calendar.collection["Italy"].jd(2458361) }
      it{ expect(fdate.strftime).to eq("2018/8/30") }
    end
  end

  describe FDate::InfinityDate do
    describe "#strftime" do
      context "無限過去の場合" do
        let(:infinity_date){ described_class.negative }
        it{ expect(infinity_date.strftime).to eq("(-Infinity)") }
      end

      context "無限未来の場合" do
        let(:infinity_date){ described_class.positive }
        it{ expect(infinity_date.strftime).to eq("(Infinity)") }
      end
    end
  end

  describe FDate::Term do
    describe "#strftime" do
      let(:adgr){ FDate::Calendar.collection["adgr"] }

      let(:term_1){
        described_class.new(
          first: adgr.new(year: {num: 2018}, month: 2, day: 15),
          last: adgr.new(year: {num: 2018}, month: 8, day: {num: 20, accuracy: 1}, about: '1-2'),
        )
      }

      let(:term_2){
        described_class.new(
          last: adgr.new(year: {num: 2018}, month: 8, day: {num: 20, accuracy: 1}, about: '1-2'),
        )
      }

      let(:term_3){
        described_class.new(
          first: adgr.new(year: {num: 2018}, month: 2, day: 15),
        )
      }

      context "訳文キー`:long`を渡した場合 (default)" do
        context "両端が日付の場合" do
          it{ expect(term_1.strftime).to eq("2018/2/15–2018/8/2*(1/2)") }
        end

        context "始端が無限過去の場合" do
          it{ expect(term_2.strftime).to eq("(-Infinity)–2018/8/2*(1/2)") }
        end

        context "終端が無限未来の場合" do
          it{ expect(term_3.strftime).to eq("2018/2/15–(Infinity)") }
        end
      end

      context "訳文キー`:short`を渡した場合" do
        context "両端が日付の場合" do
          it{ expect(term_1.strftime(:short)).to eq("18/02/15–18/08/2*(1/2)") }
        end

        context "始端が無限過去の場合" do
          it{ expect(term_2.strftime(:short)).to eq("(-Infinity)–18/08/2*(1/2)") }
        end

        context "終端が無限未来の場合" do
          it{ expect(term_3.strftime(:short)).to eq("18/02/15–(Infinity)") }
        end
      end
    end
  end
end
