require 'spec_helper.rb'

describe FDate::Locale.collection["en"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "en"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe FDate::Part.collection["EarthMonth"] do
    let(:klass){
      Class.new(described_class) do
        self.key = "hoge"
      end
    }

    describe "::strftime" do
      context "引数なしの場合 (default)" do
        it "は、日付パートの名前を返すこと" do
          expect(described_class.strftime).to eq("Month")
        end
      end
    end

    describe "#strftime" do
      context "fixedな日付パートの場合" do
        let(:obj){ klass.new(num: 12) }

        context "`:full`を渡した場合 (default)" do
          it{ expect(obj.strftime).to eq("12") }
        end

        context "`:name`を渡した場合" do
          it{ expect(obj.strftime(:name)).to eq("December") }
        end
      end

      context "fuzzyな日付パートの場合" do
        let(:obj){ klass.new(num: 10, accuracy: 1) }

        context "`:full`を渡した場合 (default)" do
          it{ expect(obj.strftime).to eq("1*'s") }
        end

        context "`:name`を渡した場合" do
          it{ expect(obj.strftime(:name)).to eq("") }
        end
      end

      context "NilDateな日付パートの場合" do
        let(:obj){ klass.nildate }

        context "`:full`を渡した場合 (default)" do
          it{ expect(obj.strftime).to eq("") }
        end

        context "`:name`を渡した場合" do
          it{ expect(obj.strftime(:name)).to eq("") }
        end
      end

      context "「頃」を持つ日付パートの場合" do
        let(:obj){ klass.new(num: 0, accuracy: 1, about: "2-2") }

        context "`:full`を渡した場合 (default)" do
          it{ expect(obj.strftime).to eq("latter half of 0*'s") }
        end

        context "`:name`を渡した場合" do
          it{ expect(obj.strftime(:name)).to eq("") }
        end
      end
    end
  end
end
