require 'spec_helper.rb'

describe FDate::Locale.collection["base"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "base"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe FDate::Part.collection["RegnalYear"] do
    let(:klass){
      Class.new(described_class) do
        self.key = "平成"
        self.term = FDate::Term.new(first: "adgr_1989-01-08_", last: "adgr_2019-04-30_")
      end
    }

    describe "::strftime" do
      context "引数なしの場合 (default)" do
        it "は、日付パートのキーを返すこと" do
          expect(described_class.strftime).to eq("RegnalYear")
        end

        it "は、日付パートのキーを返すこと" do
          expect(klass.strftime).to eq("平成")
        end
      end
    end

    describe "#strftime" do
      context "fixedな日付パートの場合" do
        let(:obj){ klass.new(num: 15) }
        it{ expect(obj.strftime).to eq("平成15") }
      end

      context "NilDateな日付パートの場合" do
        let(:obj){ klass.nildate }
        it{ expect(obj.strftime).to eq("平成") }
      end
    end
  end
end
