require 'spec_helper.rb'

describe FDate::Locale.collection["ja"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "ja"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe FDate::Part.collection["RegnalYear"] do
    let(:klass){
      Class.new(described_class) do
        self.key = "平成"
        self.term = FDate::Term.new(first: "adgr_1989-01-08_", last: "adgr_2019-04-30_")
      end
    }

    describe "::strftime" do
      context "引数なしの場合 (default)" do
        it "は、日付パートの名前を返すこと" do
          expect(described_class.strftime).to eq("元号年")
        end
      end
    end

    describe "#strftime" do
      context "fixedな日付パートの場合" do
        let(:obj){ klass.new(num: 15) }
        it{ expect(obj.strftime).to eq("平成15年") }
      end

      context "NilDateな日付パートの場合" do
        let(:obj){ klass.nildate }
        it{ expect(obj.strftime).to eq("平成年間") }
      end

      context "1年の場合" do
        let(:obj){ klass.new(num: 1) }
        it{ expect(obj.strftime).to eq("平成元年") }
      end
    end
  end
end
