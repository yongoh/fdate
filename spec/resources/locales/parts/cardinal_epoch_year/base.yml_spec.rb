require 'spec_helper.rb'

describe FDate::Locale.collection["base"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "base"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe FDate::Part.collection["CardinalEpochYear"] do
    let(:klass){
      Class.new(described_class) do
        self.key = "hoge"
        self.max_range = -100..100
      end
    }

    describe "::strftime" do
      context "引数なしの場合 (default)" do
        it "は、日付パートのキーを返すこと" do
          expect(described_class.strftime).to eq("CardinalEpochYear")
        end

        it "は、日付パートのキーを返すこと" do
          expect(klass.strftime).to eq("hoge")
        end
      end
    end

    describe "#strftime" do
      context "紀元後の年の場合" do
        let(:obj){ klass.new(num: 15) }

        context "':full'を渡した場合 (default)" do
          it{ expect(obj.strftime).to eq("A15") }
        end

        context "':simple'を渡した場合" do
          it{ expect(obj.strftime(:simple)).to eq("15") }
        end
      end

      context "紀元前の年の場合" do
        let(:obj){ klass.new(num: -15) }

        context "':full'を渡した場合 (default)" do
          it{ expect(obj.strftime).to eq("B15") }
        end

        context "':simple'を渡した場合" do
          it{ expect(obj.strftime(:simple)).to eq("B15") }
        end
      end
    end
  end
end
