require 'spec_helper.rb'

describe FDate::Locale.collection["base"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "base"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe FDate::Part.collection["Day"] do
    let(:klass){
      Class.new(described_class) do
        self.key = "hoge"
        self.max_range = 1..31
      end
    }

    describe "::strftime" do
      context "引数なしの場合 (default)" do
        it "は、日付パートのキーを返すこと" do
          expect(described_class.strftime).to eq("Day")
        end

        it "は、日付パートのキーを返すこと" do
          expect(klass.strftime).to eq("hoge")
        end
      end
    end

    describe "#strftime" do
      let(:obj){ klass.new(num: 15) }

      context "メソッド置換パターンを渡した場合" do
        it "は、置換後の文字列を返すこと" do
          expect(obj.strftime("$local_i()")).to eq("15")
        end
      end

      context "fixedな日付パートの場合" do
        context ":numを渡した場合" do
          it "は、日付数値コードを返すこと" do
            expect(obj.strftime(:num)).to eq("15")
          end
        end

        context "引数なしの場合 (default)" do
          it "は、翻訳した日付パートを返すこと" do
            expect(obj.strftime).to eq("15")
          end
        end
      end

      context "fuzzyな日付パートの場合" do
        let(:obj){ klass.new(num: 10, accuracy: 1) }

        context ":numを渡した場合" do
          it "は、日付数値コードを返すこと" do
            expect(obj.strftime(:num)).to eq("1*")
          end
        end

        context "引数なしの場合 (default)" do
          it "は、翻訳した日付パートを返すこと" do
            expect(obj.strftime).to eq("1*")
          end
        end
      end

      context "NilDateな日付パートの場合" do
        let(:obj){ klass.nildate }

        context "引数なしの場合 (default)" do
          it "は、空文字を返すこと" do
            expect(obj.strftime).to eq("")
          end
        end
      end

      context "「頃」を持つ日付パートの場合" do
        let(:obj){ klass.new(num: 10, accuracy: 1, about: "2-2") }

        context ":aboutを渡した場合" do
          it "は、翻訳した「頃」を返すこと" do
            expect(obj.strftime(:about)).to eq("(2/2)")
          end
        end

        context "引数なしの場合 (default)" do
          it "は、翻訳した日付パートを返すこと" do
            expect(obj.strftime).to eq("1*(2/2)")
          end
        end
      end
    end
  end
end
