require 'spec_helper.rb'

describe FDate::Locale.collection["en"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "en"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe FDate::Part.modules["Leapable"] do
    let(:klass){
      mod = described_class

      Class.new(FDate::Part.collection["EarthMonth"]) do
        include mod
        self.key = "hoge"
        self.leap = 6
      end
    }

    describe "#strftime" do
      context "設定した閏より前の日付の場合" do
        let(:obj){ klass.new(num: 3) }

        context "`:full`を渡した場合 (default)" do
          it{ expect(obj.strftime).to eq("3") }
        end

        context "`:name`を渡した場合" do
          it{ expect(obj.strftime(:name)).to eq("March") }
        end
      end

      context "設定した閏と同じ日付の場合" do
        let(:obj){ klass.new(num: 6) }

        context "`:full`を渡した場合 (default)" do
          it{ expect(obj.strftime).to eq("Leap 5") }
        end

        context "`:name`を渡した場合" do
          it{ expect(obj.strftime(:name)).to eq("May") }
        end
      end

      context "設定した閏より後の日付の場合" do
        let(:obj){ klass.new(num: 9) }

        context "`:full`を渡した場合 (default)" do
          it{ expect(obj.strftime).to eq("8") }
        end

        context "`:name`を渡した場合" do
          it{ expect(obj.strftime(:name)).to eq("August") }
        end
      end
    end
  end
end
