require 'spec_helper.rb'

describe FDate::Locale.collection["base"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "base"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe FDate::Part.modules["Leapable"] do
    let(:klass){
      mod = described_class

      Class.new(FDate::Part.collection["EarthMonth"]) do
        include mod
        self.key = "hoge"
      end
    }

    describe "#strftime" do
      context "閏を設定していない場合" do
        context "fixedな日付パートの場合" do
          let(:obj){ klass.new(num: 6) }
          it{ expect(obj.strftime).to eq("6") }
        end
  
        context "fuzzyな日付パートの場合" do
          let(:obj){ klass.new(num: 10, accuracy: 1) }
          it{ expect(obj.strftime).to eq("1*") }
        end
  
        context "NilDateな日付パートの場合" do
          let(:obj){ klass.nildate }
          it{ expect(obj.strftime).to eq("") }
        end
      end

      context "閏を設定してる場合" do
        before do
          klass.class_eval do
            self.leap = 6
          end
        end

        context "fixedな日付パートの場合" do
          context "設定した閏より前の日付の場合" do
            let(:obj){ klass.new(num: 3) }
            it{ expect(obj.strftime).to eq("3") }
          end

          context "設定した閏と同じ日付の場合" do
            let(:obj){ klass.new(num: 6) }
            it{ expect(obj.strftime).to eq("L5") }
          end

          context "設定した閏より後の日付の場合" do
            let(:obj){ klass.new(num: 9) }
            it{ expect(obj.strftime).to eq("8") }
          end
        end

        context "fuzzyな日付パートの場合" do
          let(:obj){ klass.new(num: 10, accuracy: 1) }
          it{ expect(obj.strftime).to eq("1*") }
        end

        context "NilDateな日付パートの場合" do
          let(:obj){ klass.nildate }
          it{ expect(obj.strftime).to eq("") }
        end
      end
    end
  end
end
