require 'spec_helper.rb'

describe FDate::Locale.collection["ja"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "ja"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe FDate::Part.modules["Leapable"] do
    let(:klass){
      mod = described_class

      Class.new(FDate::Part.collection["EarthMonth"]) do
        include mod
        self.key = "hoge"
        self.leap = 6
      end
    }

    describe "#strftime" do
      context "設定した閏より前の日付の場合" do
        let(:obj){ klass.new(num: 3) }

        context "`:full`を渡した場合 (default)" do
          it{ expect(obj.strftime).to eq("3月") }
        end

        context "`:name`を渡した場合" do
          it{ expect(obj.strftime(:name)).to eq("弥生") }
        end
      end

      context "設定した閏と同じ日付の場合" do
        let(:obj){ klass.new(num: 6) }

        context "`:full`を渡した場合 (default)" do
          it{ expect(obj.strftime).to eq("閏5月") }
        end

        context "`:name`を渡した場合" do
          it{ expect(obj.strftime(:name)).to eq("皐月") }
        end
      end

      context "設定した閏より後の日付の場合" do
        let(:obj){ klass.new(num: 9) }

        context "`:full`を渡した場合 (default)" do
          it{ expect(obj.strftime).to eq("8月") }
        end

        context "`:name`を渡した場合" do
          it{ expect(obj.strftime(:name)).to eq("葉月") }
        end
      end
    end
  end
end
