require 'spec_helper.rb'

describe FDate::Locale.collection["base"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "base"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe FDate::Part.collection["RegnalYearSet"] do
    let(:klass){
      Class.new(described_class) do
        self.key = "japanese/Gengou"

        reign_collection.inherit "昭和" do
          self.term = FDate::Term.new(first: "adgr_1926-12-25_", last: "adgr_1989-01-07_")
        end

        reign_collection.inherit "平成" do
          self.term = FDate::Term.new(first: "adgr_1989-01-08_", last: "adgr_2019-04-30_")
        end
      end
    }

    describe "::strftime" do
      context "引数なしの場合 (default)" do
        it "は、日付パートのキーを返すこと" do
          expect(described_class.strftime).to eq("RegnalYearSet")
        end

        it "は、日付パートのキーを返すこと" do
          expect(klass.strftime).to eq("japanese/Gengou")
        end
      end
    end

    describe "#strftime" do
      context "元号が無い年の場合" do
        let(:obj){ klass.jdy(5000) }
        it{ expect(obj.strftime).to eq("(JDY 5000)") }
      end

      context "元号が1つある年の場合" do
        let(:obj){ klass.jdy(6716) }
        it{ expect(obj.strftime).to eq("平成15") }
      end

      context "元号が複数ある年の場合" do
        let(:obj){ klass.jdy(6702) }
        it{ expect(obj.strftime).to eq("昭和64/平成1") }
      end
    end
  end
end
