require 'spec_helper.rb'

describe FDate::Locale.collection["en"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "en"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe FDate::Part.collection["RegnalYearSet"] do
    let(:klass){
      Class.new(described_class) do
        self.key = "japanese/Gengou"

        reign_collection.inherit "昭和" do
          self.term = FDate::Term.new(first: "adgr_1926-12-25_", last: "adgr_1989-01-07_")
        end

        reign_collection.inherit "平成" do
          self.term = FDate::Term.new(first: "adgr_1989-01-08_", last: "adgr_2019-04-30_")
        end
      end
    }

    describe "::strftime" do
      context "引数なしの場合 (default)" do
        it "は、日付パートの名前を返すこと" do
          expect(described_class.strftime).to eq("Regnal year set")
        end
      end
    end

    describe "#strftime" do
      context "元号が無い年の場合" do
        let(:obj){ klass.jdy(5000) }
        it{ expect(obj.strftime).to eq("(JDY 5000)") }
      end

      context "元号が1つある年の場合" do
        let(:obj){ klass.jdy(6716) }
        it{ expect(obj.strftime).to eq("Heisei 15") }
      end

      context "元号が複数ある年の場合" do
        let(:obj){ klass.jdy(6702) }
        it{ expect(obj.strftime).to eq("Shouwa 64/Heisei 1") }
      end
    end
  end
end
