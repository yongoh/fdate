require 'spec_helper.rb'

describe FDate::Locale.collection["en"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "en"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe FDate::Part.collection["OrdinalEpochYear"] do
    let(:klass){
      Class.new(described_class) do
        self.key = "hoge"
        self.max_range = -100..100
      end
    }

    describe "::strftime" do
      context "引数なしの場合 (default)" do
        it "は、日付パートの名前を返すこと" do
          expect(described_class.strftime).to eq("Epoch year")
        end
      end
    end

    describe "#strftime" do
      context "紀元後の年の場合" do
        let(:obj){ klass.new(num: 15) }

        context "':full'を渡した場合 (default)" do
          it{ expect(obj.strftime).to eq("A.D. 15") }
        end

        context "':simple'を渡した場合" do
          it{ expect(obj.strftime(:simple)).to eq("15") }
        end
      end

      context "紀元前の年の場合" do
        let(:obj){ klass.new(num: -15) }

        context "':full'を渡した場合 (default)" do
          it{ expect(obj.strftime).to eq("16 B.C.") }
        end

        context "':simple'を渡した場合" do
          it{ expect(obj.strftime(:simple)).to eq("16 BC") }
        end
      end
    end
  end
end
