require 'spec_helper.rb'

describe FDate::Locale.collection["base"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "base"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe FDate::Part.collection["Year"] do
    let(:klass){
      Class.new(described_class) do
        self.key = "hoge"
        self.max_range = 1..100
      end
    }

    describe "::strftime" do
      context "引数なしの場合 (default)" do
        it "は、日付パートのキーを返すこと" do
          expect(described_class.strftime).to eq("Year")
        end

        it "は、日付パートのキーを返すこと" do
          expect(klass.strftime).to eq("hoge")
        end
      end
    end

    describe "#strftime" do
      context "fixedな日付パートの場合" do
        let(:obj){ klass.new(num: 15) }
        it{ expect(obj.strftime).to eq("15") }
      end

      context "fuzzyな日付パートの場合" do
        let(:obj){ klass.new(num: 10, accuracy: 1) }
        it{ expect(obj.strftime).to eq("1*") }
      end

      context "NilDateな日付パートの場合" do
        let(:obj){ klass.nildate }
        it{ expect(obj.strftime).to eq("") }
      end

      context "「頃」を持つ日付パートの場合" do
        let(:obj){ klass.new(num: 10, accuracy: 1, about: "2-2") }
        it{ expect(obj.strftime).to eq("1*(2/2)") }
      end
    end
  end
end
