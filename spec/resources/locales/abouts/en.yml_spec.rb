require 'spec_helper.rb'

describe FDate::Locale.collection["en"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "en"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe "#strftime" do
    subject{ described_class.strftime }

    describe FDate::About.collection["j"] do
      it{ is_expected.to eq("") }
    end

    describe FDate::About.collection["ab"] do
      it{ is_expected.to eq("about ") }
    end

    describe FDate::About.collection["1-2"] do
      it{ is_expected.to eq("first half of ") }
    end

    describe FDate::About.collection["2-2"] do
      it{ is_expected.to eq("latter half of ") }
    end

    describe FDate::About.collection["1-5"] do
      it{ is_expected.to eq("first of ") }
    end

    describe FDate::About.collection["2-5"] do
      it{ is_expected.to eq("early ") }
    end

    describe FDate::About.collection["3-5"] do
      it{ is_expected.to eq("middle ") }
    end

    describe FDate::About.collection["4-5"] do
      it{ is_expected.to eq("late ") }
    end

    describe FDate::About.collection["5-5"] do
      it{ is_expected.to eq("end of ") }
    end
  end
end
