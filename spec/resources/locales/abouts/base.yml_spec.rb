require 'spec_helper.rb'

describe FDate::Locale.collection["base"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "base"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe "#strftime" do
    subject{ described_class.strftime }

    describe FDate::About.collection["j"] do
      it{ is_expected.to eq("") }
    end

    describe FDate::About.collection["ab"] do
      it{ is_expected.to eq("ab") }
    end

    describe FDate::About.collection["1-2"] do
      it{ is_expected.to eq("(1/2)") }
    end

    describe FDate::About.collection["2-2"] do
      it{ is_expected.to eq("(2/2)") }
    end

    describe FDate::About.collection["1-5"] do
      it{ is_expected.to eq("(1/5)") }
    end

    describe FDate::About.collection["2-5"] do
      it{ is_expected.to eq("(2/5)") }
    end

    describe FDate::About.collection["3-5"] do
      it{ is_expected.to eq("(3/5)") }
    end

    describe FDate::About.collection["4-5"] do
      it{ is_expected.to eq("(4/5)") }
    end

    describe FDate::About.collection["5-5"] do
      it{ is_expected.to eq("(5/5)") }
    end
  end
end
