require 'spec_helper.rb'

describe FDate::Locale.collection["ja"] do
  before do
    @default = FDate::Locale.collection.default
    FDate::Locale.collection.default = "ja"
  end

  after do
    FDate::Locale.collection.default = @default
  end


  describe "#strftime" do
    subject{ described_class.strftime }

    describe FDate::About.collection["j"] do
      it{ is_expected.to eq("") }
    end

    describe FDate::About.collection["ab"] do
      it{ is_expected.to eq("頃") }
    end

    describe FDate::About.collection["1-2"] do
      it{ is_expected.to eq("前半") }
    end

    describe FDate::About.collection["2-2"] do
      it{ is_expected.to eq("後半") }
    end

    describe FDate::About.collection["1-5"] do
      it{ is_expected.to eq("初頭") }
    end

    describe FDate::About.collection["2-5"] do
      it{ is_expected.to eq("前期") }
    end

    describe FDate::About.collection["3-5"] do
      it{ is_expected.to eq("中期") }
    end

    describe FDate::About.collection["4-5"] do
      it{ is_expected.to eq("後期") }
    end

    describe FDate::About.collection["5-5"] do
      it{ is_expected.to eq("末") }
    end
  end
end
