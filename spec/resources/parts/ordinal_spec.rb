require 'spec_helper.rb'

describe FDate::Part.modules["Ordinal"] do
  let(:klass){
    mod = described_class

    Class.new(FDate::Part) do
      include mod

      self.last = 25
    end
  }


  describe "::max_range" do
    it "は、`1..klass.last`を返すこと" do
      expect(klass.max_range).to eq(1..25)
    end
  end

  describe "#initialize" do
    shared_examples_for "raise RangeError" do
      it "は、範囲エラーを発生させること" do
        expect{ klass.new(num: num) }.to raise_error(FDate::Part::RangeError, "#{num}..#{num} deviates from 1..25")
      end
    end


    context "numに`klass.max_range`の範囲内の正数を渡した場合" do
      context "`accuracy: 0`を渡した場合 (default)" do
        let(:part){ klass.new(num: 5) }

        it "は、渡した数値を範囲とすること" do
          expect(part.global_range).to eq(5..5)
        end
      end

      context "accuracyに1以上numの桁数以下の数を渡した場合" do
        let(:part){ klass.new(num: 10, accuracy: 1) }

        it "は、誤差accuracy桁を範囲とすること" do
          expect(part.global_range).to eq(10..19)
        end
      end

      context "accuracyに1以上numの桁数以下の数を渡した場合" do
        let(:part){ klass.new(num: 20, accuracy: 1) }

        it "は、`num..klass.max_range.last`の範囲とすること" do
          expect(part.global_range).to eq(20..25)
        end
      end
    end

    context "numに`klass.max_range`の範囲外の数を渡した場合" do
      let(:num){ 50 }
      it_behaves_like "raise RangeError"
    end

    context "numに0を渡した場合" do
      context "`accuracy: 0`を渡した場合 (default)" do
        let(:num){ 0 }
        it_behaves_like "raise RangeError"
      end

      context "accuracyに1以上numの桁数以下の数を渡した場合" do
        let(:part){ klass.new(num: 0, accuracy: 1) }

        it "は、1から始まる範囲とすること（0を含まない）" do
          expect(part.global_range).to eq(1..9)
        end
      end
    end
  end
end
