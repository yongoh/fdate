require 'spec_helper.rb'

describe FDate::Part.collection["ByzantineYear"] do
  describe "::cardinal?,::ordinal?" do
    it "は、序数紀年法であることを示すこと" do
      expect(described_class).to be_ordinal
      expect(described_class).not_to be_cardinal
    end
  end


  context "ユリウス通年計算メソッド群" do
    shared_examples_for "jdy methods" do
      describe "::to_jdy" do
        it "は、渡した暦特有の年数をユリウス通年に変換して返すこと" do
          expect(described_class.to_jdy(local_year)).to eq(jdy)
        end
      end

      describe "::from_jdy" do
        it "は、渡したユリウス通年を暦特有の年数に変換して返すこと" do
          expect(described_class.from_jdy(jdy)).to eq(local_year)
        end
      end

      describe "::jdy" do
        it "は、渡したユリウス通年のキリスト紀元年インスタンスを生成すること" do
          expect(described_class.jdy(jdy)).to have_attributes(
            local_range: local_year..local_year,
            jdy_range: jdy..jdy,
            accuracy: 0,
          )
        end
      end
    end


    context "B.C.6508相当" do
      let(:local_year){ -1000 }
      let(:jdy){ -1795 }
      it_behaves_like "jdy methods"
    end

    context "B.C.5508相当（東方正教会が算出した世界創造の年）" do
      let(:local_year){ 0 }
      let(:jdy){ -795 }
      it_behaves_like "jdy methods"
    end

    context "B.C.4713相当" do
      let(:local_year){ 795 }
      let(:jdy){ 0 }
      it_behaves_like "jdy methods"
    end

    context "A.D.1相当" do
      let(:local_year){ 5509 }
      let(:jdy){ 4714 }
      it_behaves_like "jdy methods"
    end

    context "A.D.395相当（テオドシウス1世によるローマ帝国の東西分割）" do
      let(:local_year){ 5903 }
      let(:jdy){ 5108 }
      it_behaves_like "jdy methods"
    end

    context "A.D.988相当（皇帝バシレイオス2世による世界創造紀元の採用）" do
      let(:local_year){ 6496}
      let(:jdy){ 5701 }
      it_behaves_like "jdy methods"
    end

    context "A.D.1453相当（コンスタンティノープル陥落）" do
      let(:local_year){ 6961 }
      let(:jdy){ 6166 }
      it_behaves_like "jdy methods"
    end
  end
end
