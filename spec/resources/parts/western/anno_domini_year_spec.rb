require 'spec_helper.rb'

describe FDate::Part.collection["AnnoDominiYear"] do
  describe "::cardinal?,::ordinal?" do
    it "は、序数紀年法であることを示すこと" do
      expect(described_class).to be_ordinal
      expect(described_class).not_to be_cardinal
    end
  end

  context "ユリウス通年計算メソッド群" do
    shared_examples_for "jdy methods" do
      describe "::to_jdy" do
        it "は、渡した暦特有の年数をユリウス通年に変換して返すこと" do
          expect(described_class.to_jdy(local_year)).to eq(jdy)
        end
      end

      describe "::from_jdy" do
        it "は、渡したユリウス通年を暦特有の年数に変換して返すこと" do
          expect(described_class.from_jdy(jdy)).to eq(local_year)
        end
      end

      describe "::jdy" do
        it "は、渡したユリウス通年のキリスト紀元年インスタンスを生成すること" do
          expect(described_class.jdy(jdy)).to have_attributes(
            local_range: local_year..local_year,
            jdy_range: jdy..jdy,
            accuracy: 0,
          )
        end
      end
    end


    context "キリスト紀元-4713年（紀元前4713年）の場合" do
      let(:local_year){ -4713 }
      let(:jdy){ 0 }
      it_behaves_like "jdy methods"
    end

    context "キリスト紀元-2000年（紀元前2001年）の場合" do
      let(:local_year){ -2000 }
      let(:jdy){ 2713 }
      it_behaves_like "jdy methods"
    end

    context "キリスト紀元0年（紀元前1年）の場合" do
      let(:local_year){ 0 }
      let(:jdy){ 4713 }
      it_behaves_like "jdy methods"
    end

    context "キリスト紀元2000年の場合" do
      let(:local_year){ 2000 }
      let(:jdy){ 6713 }
      it_behaves_like "jdy methods"
    end
  end

  #*-> Year
  describe "#convert_to" do
    let(:klass){ FDate::Part.collection["JimmuEpochYear"] }

    context "fixedの場合" do
      let(:year){ described_class.new(num: 1940) }

      shared_examples_for "convert" do
        it "は、指定した年クラスのインスタンスに変換すること" do
          is_expected.to be === klass.new(num: 2600)
        end
      end


      context "年クラスを渡した場合" do
        subject{ year.convert_to(klass) }
        it_behaves_like "convert"
      end

      context "年クラスのキーを渡した場合" do
        subject{ year.convert_to("JimmuEpochYear") }
        it_behaves_like "convert"
      end
    end

    context "fuzzyの場合" do
      let(:year){ described_class.new(num: 1940, accuracy: 1) }

      it "は、メソッド未定義エラーを返すこと" do
        expect{ year.convert_to(klass) }.to raise_error(NoMethodError)
      end
    end
  end
end
