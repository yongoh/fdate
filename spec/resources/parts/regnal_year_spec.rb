require 'spec_helper.rb'

describe FDate::Part.collection["RegnalYear"] do
  let(:klass){
    k = key
    r = term

    Class.new(described_class) do
      self.key = k
      self.term = r
    end
  }

  let(:key){ "G" }
  let(:local_years){ 24 }
  let(:first_jdy){ 6713 }
  let(:term){
    cal = FDate::Calendar.collection["adgr"]

    FDate::Term.build(
      first: cal.new(year: first_jdy),
      last: cal.new(year: first_jdy + local_years - 1)
    )
  }


  describe "#eql?" do
    let(:year){ klass.new(num: 15) }

    context "同じクラスと値を持つ元号オブジェクトと比べた場合" do
      it{ expect(year).to eql(klass.new(num: 15)) }
    end

    context "keyと値は同じだがクラスが違う元号オブジェクトと比べた場合" do
      let(:other_class){
        k = key
        r = term

        Class.new(described_class) do
          self.key = k
          self.term = r
        end
      }
      it{ expect(year).not_to eql(other_class.new(num: 15)) }
    end
  end

  describe "日付計算メソッド群" do
    shared_examples_for "jdy methods" do
      describe "::to_jdy" do
        it "は、この暦特有の年数をユリウス通年に変換すること" do
          expect(klass.to_jdy(local_year)).to eq(jdy)
        end
      end

      describe "::from_jdy" do
        it "は、ユリウス通年をこの暦特有の年数に変換すること" do
          expect(klass.from_jdy(jdy)).to eq(local_year)
        end
      end
    end

    context "日付パートコード'げんごう=0'相当の場合" do
      let(:local_year){ 0 }
      let(:jdy){ 6712 }
      it_behaves_like "jdy methods"
    end

    context "日付パートコード'げんごう=1'相当の場合" do
      let(:local_year){ 1 }
      let(:jdy){ 6713 }
      it_behaves_like "jdy methods"
    end

    context "日付パートコード'げんごう=24'相当の場合" do
      let(:local_year){ 24 }
      let(:jdy){ 6736 }
      it_behaves_like "jdy methods"
    end

    context "日付パートコード'げんごう=25'相当の場合" do
      let(:local_year){ 25 }
      let(:jdy){ 6737 }
      it_behaves_like "jdy methods"
    end
  end

  describe "#initialize" do
    shared_examples_for "range methods" do
      describe "#jdy_range" do
        it "は、ユリウス通年の範囲を返すこと" do
          expect(year.jdy_range).to eq(expected_jdy_range)
        end
      end

      describe "#local_range" do
        it "は、この暦特有の年数の範囲を返すこと" do
          expect(year.local_range).to eq(expected_local_range)
        end
      end
    end

    shared_examples_for "raise RangeError" do
      it "は、範囲エラーを発生させること" do
        expect{ year }.to raise_error(FDate::Part::RangeError, "#{expected_local_range} deviates from 1..24")
      end
    end


    context "`accuracy: 0(default)`" do
      context "と`num: (`klass.max_range`の範囲内の正数)`でインスタンス化した場合" do
        let(:year){ klass.new(num: 15) }

        let(:expected_jdy_range){ 6727..6727 }
        let(:expected_local_range){ 15..15 }

        it_behaves_like "range methods"
      end

      context "と`num: (`klass.max_range`の範囲外の正数)`でインスタンス化した場合" do
        let(:year){ klass.new(num: 30) }
        let(:expected_local_range){ 30..30 }
        it_behaves_like "raise RangeError"
      end
    end

    context "`num: 0`でインスタンス化した場合" do
      let(:year){ klass.new(num: 0) }
      let(:expected_local_range){ 0..0 }
      it_behaves_like "raise RangeError"
    end

    context "`num: (負数)`でインスタンス化した場合" do
      let(:year){ klass.new(num: -5) }
      let(:expected_local_range){ -5..-5 }
      it_behaves_like "raise RangeError"
    end

    context "`num: nil`でインスタンス化した場合" do
      let(:year){ klass.new(num: nil) }

      let(:expected_jdy_range){ 6713..6736 }
      let(:expected_local_range){ 1..24 }

      it_behaves_like "range methods"
    end
  end

  describe "#local_code" do
    subject{ year.local_code }

    context "fixedの場合" do
      let(:year){ klass.new(num: 15) }

      it "は、元号キーと年数を含む日付パートコードを返すこと" do
        expect(year.local_code).to eq("G=15")
      end
    end

    context "fuzzyの場合" do
      let(:year){ klass.new(num: 15, accuracy: 1) }

      it "は、元号キーと'*'付き年数を含む日付パートコードを返すこと" do
        expect(year.local_code).to eq("G=1*")
      end
    end

    context "NilDateの場合" do
      let(:year){ klass.new }

      it "は、元号キーと'*'のみの日付パートコードを返すこと" do
        expect(year.local_code).to eq("G=*")
      end
    end
  end


  describe "::_local_parse" do
    subject{ klass._local_parse(code) }

    context "fixedの場合" do
      let(:code){ "G=10" }

      it "は、インスタンス化用ハッシュを返すこと" do
        is_expected.to eq({num: 10, accuracy: 0, reign: "G"})
      end
    end

    context "fuzzyの場合" do
      let(:code){ "G=1*" }

      it "は、インスタンス化用ハッシュを返すこと" do
        is_expected.to eq({num: 10, accuracy: 1, reign: "G"})
      end
    end

    context "NilDateの場合" do
      let(:code){ "G=*" }

      it "は、インスタンス化用ハッシュを返すこと" do
        is_expected.to eq({num: nil, accuracy: 1, reign: "G"})
      end
    end
  end

  describe "::_jdy" do
    context "範囲内のユリウス通年を渡した場合" do
      it "は、この暦の年数に変換しインスタンス用ハッシュを返すこと" do
        expect(klass._jdy(6727)).to eq({num: 15})
      end
    end

    context "範囲外のユリウス通年を渡した場合" do
      it "は、範囲エラーを発生させること" do
        expect{ klass._jdy(6737) }.to raise_error(FDate::Part::RangeError, "25..25 deviates from 1..24")
      end
    end
  end
end
