require 'spec_helper.rb'

describe FDate::Part.modules["Leapable"] do
  describe "閏判定系メソッド群" do
    let(:klass){
      mod = described_class

      Struct.new(:global_i) do
        include mod

        def fixed?; true end
      end
    }

    context "`::leap == nil`（閏設定なし）の場合" do
      let(:owner){ klass.new(2) }

      describe "#compare_with_leap" do
        it{ expect(owner.compare_with_leap).to be_nil }
      end

      describe "#leap?" do
        it{ expect(owner).not_to be_leap }
      end
    end

    context "閏設定ありの場合" do
      before do
        klass.class_eval do
          self.leap = 3
        end
      end

      context "`#global_i < ::leap`の場合" do
        let(:owner){ klass.new(2) }

        describe "#compare_with_leap" do
          it "は、設定された閏より前であることを示す結果を返すこと" do
            expect(owner.compare_with_leap).to eq(-1)
          end
        end

        describe "#leap?" do
          it{ expect(owner).not_to be_leap }
        end
      end

      context "`#global_i == ::leap`の場合" do
        let(:owner){ klass.new(3) }

        describe "#compare_with_leap" do
          it "は、設定された閏と同じ日付であることを示す結果を返すこと" do
            expect(owner.compare_with_leap).to eq(0)
          end
        end

        describe "#leap?" do
          it{ expect(owner).to be_leap }
        end
      end

      context "`#global_i > ::leap`の場合" do
        let(:owner){ klass.new(4) }

        describe "#compare_with_leap" do
          it "は、設定された閏より後であることを示す結果を返すこと" do
            expect(owner.compare_with_leap).to eq(1)
          end
        end

        describe "#leap?" do
          it{ expect(owner).not_to be_leap }
        end
      end

      context "fuzzyの場合" do
        let(:owner){ klass.new(nil) }

        describe "#compare_with_leap" do
          it{ expect(owner.compare_with_leap).to be_nil }
        end

        describe "#leap?" do
          it{ expect(owner).not_to be_leap }
        end
      end
    end
  end

  describe "#local_range" do
    subject{ owner.local_range }

    let(:klass){
      mod = described_class

      Struct.new(:global_range) do
        include mod
      end
    }

    context "閏が設定されていない場合" do
      context "fixedの場合" do
        let(:owner){ klass.new(3..3) }

        it "は、`#global_range`と同じ範囲を返すこと" do
          is_expected.to eq(3..3).and eq(owner.global_range)
        end
      end

      context "fuzzyの場合" do
        let(:owner){ klass.new(1..12) }

        it "は、`#global_range`と同じ範囲を返すこと" do
          is_expected.to eq(1..12).and eq(owner.global_range)
        end
      end
    end

    context "閏が設定されている場合" do
      before do
        klass.class_eval do
          self.leap = 6
        end
      end

      context "fixedの場合" do
        context "設定された閏より前の日付の場合" do
          let(:owner){ klass.new(3..3) }

          it "は、`#global_range`と同じ範囲を返すこと" do
            is_expected.to eq(3..3).and eq(owner.global_range)
          end
        end

        context "設定された閏と同じ日付の場合" do
          let(:owner){ klass.new(6..6) }

          it "は、`#global_range`から両端を1引いた範囲を返すこと" do
            is_expected.to eq(5..5)
          end
        end

        context "設定された閏より後の日付の場合" do
          let(:owner){ klass.new(9..9) }

          it "は、`#global_range`から両端を1引いた範囲を返すこと" do
            is_expected.to eq(8..8)
          end
        end
      end

      context "fuzzyの場合" do
        context "設定された閏より前の日付の場合" do
          let(:owner){ klass.new(1..5) }

          it "は、`#global_range`と同じ範囲を返すこと" do
            is_expected.to eq(1..5).and eq(owner.global_range)
          end
        end

        context "設定された閏を含む日付の場合" do
          let(:owner){ klass.new(3..9) }

          it "は、`#global_range`から終端を1引いた範囲を返すこと" do
            is_expected.to eq(3..8)
          end
        end

        context "設定された閏より後の日付の場合" do
          let(:owner){ klass.new(7..10) }

          it "は、`#global_range`から両端を1引いた範囲を返すこと" do
            is_expected.to eq(6..9)
          end
        end
      end
    end
  end

  describe "#local_code" do
    subject{ owner.local_code }

    let(:klass){
      mod = described_class

      Class.new(FDate::Part) do
        include mod

        self.max_range = 1..30
      end
    }

    context "閏が設定されていない場合" do
      context "fixedの場合" do
        let(:owner){ klass.new(num: 3) }
        it{ is_expected.to eq("3") }
      end

      context "fuzzyの場合" do
        let(:owner){ klass.new(num: 10, accuracy: 1) }
        it{ is_expected.to eq("1*") }
      end

      context "NilDateの場合" do
        let(:owner){ klass.nildate }
        it{ is_expected.to eq("*") }
      end
    end

    context "閏が設定されている場合" do
      before do
        klass.class_eval do
          self.leap = 15
        end
      end

      context "fixedの場合" do
        context "設定された閏より前の日付の場合" do
          let(:owner){ klass.new(num: 3) }

          it "は、`#global_range.first`と同じ数値のコードを返すこと" do
            is_expected.to eq("3")
          end
        end

        context "設定された閏と同じ日付の場合" do
          let(:owner){ klass.new(num: 15) }

          it "は、暦キーと`#global_range.first`から1引いた数値のコードを返すこと" do
            is_expected.to eq("L14")
          end
        end

        context "設定された閏より後の日付の場合" do
          let(:owner){ klass.new(num: 20) }

          it "は、`#global_range.first`から1引いた数値のコードを返すこと" do
            is_expected.to eq("19")
          end
        end
      end

      context "fuzzyの場合" do
        context "設定された閏より前の日付の場合" do
          let(:owner){ klass.new(num: 0, accuracy: 1) }
          it{ is_expected.to eq("0*") }
        end

        context "設定された閏を含む日付の場合" do
          let(:owner){ klass.new(num: 10, accuracy: 1) }
          it{ is_expected.to eq("1*") }
        end

        context "設定された閏より後の日付の場合" do
          let(:owner){ klass.new(num: 20, accuracy: 1) }
          it{ is_expected.to eq("2*") }
        end
      end

      context "NilDateの場合" do
        let(:owner){ double("owner", nildate?: true, global_range: 1..30) }
        let(:owner){ klass.nildate }
        it{ is_expected.to eq("*") }
      end
    end
  end

  describe "::_local_parse" do
    let(:klass){
      mod = described_class

      Class.new do
        extend FDate::Part::Parseable
        include mod

        def self._from_i(int)
          {num: int, accuracy: 0}
        end
      end
    }

    context "`::leap == nil`（閏設定なし）の場合" do
      before do
        allow(klass).to receive(:leap)
      end

      context "閏キーなしの場合" do
        it "は、インスタンス化用ハッシュを返すこと" do
          expect(klass._local_parse("2")).to eq({num: 2, accuracy: 0})
        end
      end

      context "閏キーありの場合" do
        it "は、日付エラーを発生させること" do
          expect{ klass._local_parse("L2") }.to raise_error(FDate::DateError, "There is no leap on this class. \"L2\"")
        end
      end
    end

    context "閏設定ありの場合" do
      before do
        klass.class_eval do
          self.leap = 3
        end
      end

      shared_examples_for "raise DateLeapError" do
        it "は、日付エラーを発生させること" do
          expect{ klass._local_parse(code) }.to raise_error(FDate::DateError, "The leap of this class is 3. #{code.inspect}")
        end
      end


      context "設定された閏より前の数値のコードを渡した場合" do
        context "閏キーなしの場合" do
          it "は、`num: 数値`のインスタンス化用ハッシュを返すこと" do
            expect(klass._local_parse("1")).to eq({num: 1, accuracy: 0})
          end
        end

        context "閏キーありの場合" do
          let(:code){ "L1" }
          it_behaves_like "raise DateLeapError"
        end
      end

      context "設定された閏より1つ前の数値のコードを渡した場合" do
        context "閏キーなしの場合" do
          it "は、`num: 数値`のインスタンス化用ハッシュを返すこと" do
            expect(klass._local_parse("2")).to eq({num: 2, accuracy: 0})
          end
        end

        context "閏キーありの場合" do
          it "は、`num: 数値 + 1`のインスタンス化用ハッシュを返すこと" do
            expect(klass._local_parse("L2")).to eq({num: 3, accuracy: 0})
          end
        end
      end

      context "設定された閏と同じ数値のコードを渡した場合" do
        context "閏キーなしの場合" do
          it "は、`num: 数値 + 1`のインスタンス化用ハッシュを返すこと" do
            expect(klass._local_parse("3")).to eq({num: 4, accuracy: 0})
          end
        end

        context "閏キーありの場合" do
          let(:code){ "L3" }
          it_behaves_like "raise DateLeapError"
        end
      end

      context "設定された閏より後の数値のコードを渡した場合" do
        context "閏キーなしの場合" do
          it "は、`num: 数値 + 1`のインスタンス化用ハッシュを返すこと" do
            expect(klass._local_parse("4")).to eq({num: 5, accuracy: 0})
          end
        end

        context "閏キーありの場合" do
          let(:code){ "L4" }
          it_behaves_like "raise DateLeapError"
        end
      end

      context "fuzzyなコードを渡した場合" do
        context "閏キーなしの場合" do
          it "は、インスタンス化用ハッシュを返すこと" do
            expect(klass._local_parse("*")).to eq({num: nil, accuracy: 1})
          end
        end

        context "閏キーありの場合" do
          it "は、パースエラーを発生させること" do
            expect{
              klass._local_parse("L*")
            }.to raise_error(FDate::ParseError, "It's wrong grammar. \"L*\"")
          end
        end
      end
    end
  end
end
