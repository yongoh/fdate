require 'spec_helper.rb'

describe FDate::Part.collection["japanese/Tennou"] do
  let(:year){ described_class.jdy(4713 + ad_year) }

  context "天皇がいない年" do
    let(:ad_year){ 1 }

    describe "#local_code" do
      it{ expect(year.local_code).to eq("(4714)") }
    end

    it{ expect(year).to be_jdy }
  end

  context "天皇が一人いる年" do
    let(:ad_year){ 1985 }

    describe "#local_code" do
      it{ expect(year.local_code).to eq("昭和天皇=60") }
    end

    it{ expect(year).to be_single }
  end

  context "2人の天皇にまたがる年" do
    let(:ad_year){ 1989 }

    describe "#local_code" do
      it{ expect(year.local_code).to eq("昭和天皇=64/今上天皇=1") }
    end

    it{ expect(year).to be_plural }
  end

  context "南朝北朝それぞれ一人づつ天皇がいる年" do
    let(:ad_year){ 1340 }

    describe "#local_code" do
      it{ expect(year.local_code).to eq("後村上天皇=2/光明天皇=5") }
    end

    it{ expect(year).to be_plural }
  end
end
