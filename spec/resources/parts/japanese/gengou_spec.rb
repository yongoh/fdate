require 'spec_helper.rb'

describe FDate::Part.collection["japanese/Gengou"] do
  let(:year){ described_class.jdy(4713 + ad_year) }

  context "元号の無い年" do
    context "天皇がいない年" do
      let(:ad_year){ 1 }

      describe "#local_code" do
        it{ expect(year.local_code).to eq("(4714)") }
      end

      it{ expect(year).to be_jdy }
    end

    context "天皇がいる年" do
      let(:ad_year){ 510 }

      describe "#local_code" do
        it{ expect(year.local_code).to eq("継体天皇=4") }
      end

      it{ expect(year).to be_single }
    end
  end

  context "元号と天皇がある年" do
    context "1つの元号があり2人の天皇にまたがる年" do
      let(:ad_year){ 645 }

      describe "#local_code" do
        it{ expect(year.local_code).to eq("大化=1/皇極天皇=4/孝徳天皇=1") }
      end

      it{ expect(year).to be_plural }
    end

    context "大宝以前" do
      let(:ad_year){ 648 }

      describe "#local_code" do
        it{ expect(year.local_code).to eq("大化=4/孝徳天皇=4") }
      end

      it{ expect(year).to be_plural }
    end

    context "大宝元年" do
      let(:ad_year){ 701 }

      describe "#local_code" do
        it{ expect(year.local_code).to eq("大宝=1/文武天皇=5") }
      end

      it{ expect(year).to be_plural }
    end
  end

  context "元号が1つしかない年" do
    let(:ad_year){ 1985 }

    describe "#local_code" do
      it{ expect(year.local_code).to eq("昭和=60") }
    end

    it{ expect(year).to be_single }
  end

  context "2つの元号にまたがる年" do
    let(:ad_year){ 1989 }

    describe "#local_code" do
      it{ expect(year.local_code).to eq("昭和=64/平成=1") }
    end

    it{ expect(year).to be_plural }
  end

  context "3つの元号にまたがる年" do
    let(:ad_year){ 749 }

    describe "#local_code" do
      it{ expect(year.local_code).to eq("天平=21/天平感宝=1/天平勝宝=1") }
    end

    it{ expect(year).to be_plural }
  end

  context "南朝北朝それぞれ1つづつ元号がある年" do
    let(:ad_year){ 1339 }

    describe "#local_code" do
      it{ expect(year.local_code).to eq("延元=4/暦応=2") }
    end

    it{ expect(year).to be_plural }
  end

  context "南朝北朝・前後合わせて4つの元号がある年" do
    let(:ad_year){ 1375 }

    describe "#local_code" do
      it{ expect(year.local_code).to eq("文中=4/天授=1/応安=8/永和=1") }
    end

    it{ expect(year).to be_plural }
  end
end
