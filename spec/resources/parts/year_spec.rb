require 'spec_helper.rb'

describe FDate::Part.collection["Year"] do
  let(:klass){
    Class.new(described_class) do
      self.max_range = 1000..2000
    end
  }

  describe "#jdy" do
    context "fixedの場合" do
      let(:year){ klass.new(num: 1955) }

      it "は、この日付のユリウス通日を返すこと" do
        expect(year.jdy).to eq(1955)
      end
    end

    context "fuzzyの場合" do
      let(:year){ klass.new(num: 1955, accuracy: 1) }

      it "は、ユリウス通日の範囲を返すこと" do
        expect(year.jdy).to eq(1950..1959)
      end
    end
  end
end
