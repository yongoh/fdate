require 'spec_helper.rb'

describe FDate::Part.collection["CardinalEpochYear"] do
  let(:klass){
    Class.new(described_class) do
      self.diff_from_jdy = 500
    end
  }

  let(:year){ klass.new(arg) }


  describe "::cardinal?" do
    it{ expect(klass).to be_cardinal }
  end

  describe "::ordinal?" do
    it{ expect(klass).not_to be_ordinal }
  end

  describe "#initialize" do
    context "`num: nil`でインスタンス化しようとした場合" do
      it "は、日付エラーを発生させること" do
        expect{ klass.new(num: nil) }.to raise_error(FDate::DateError, "NilDate is not allowed.")
      end
    end
  end

  describe "#ad?,#bc?" do
    shared_examples_for "is A.D." do
      it "は、紀元後であることを示すこと" do
        is_expected.to be_ad
        is_expected.not_to be_bc
      end
    end

    shared_examples_for "is B.C." do
      it "は、紀元前であることを示すこと" do
        is_expected.not_to be_ad
        is_expected.to be_bc
      end
    end


    context "`num: 1`でインスタンス化した場合" do
      subject{ klass.new(num: 1) }
      it_behaves_like "is A.D."
    end

    context "`num: -1`でインスタンス化した場合" do
      subject{ klass.new(num: -1) }
      it_behaves_like "is B.C."
    end

    context "`num: 0`でインスタンス化した場合" do
      subject{ klass.new(num: 0) }
      it_behaves_like "is A.D."
    end
  end

  describe "範囲系メソッド群" do
    shared_examples_for "range methods" do
      describe "#local_range" do
        it "は、この暦特有の年数の範囲を返すこと" do
          expect(year.local_range).to eq(expected_local_range)
        end
      end

      describe "#jdy_range" do
        it "は、ユリウス通年の範囲を返すこと" do
          expect(year.jdy_range).to eq(expected_jdy_range)
        end
      end
    end

    context "紀元後の場合" do
      let(:year){ klass.new(num: 100, accuracy: 2) }

      let(:expected_local_range){ 100..199 }
      let(:expected_jdy_range){ 600..699 }

      it_behaves_like "range methods"
    end

    context "紀元前の場合" do
      let(:year){ klass.new(num: -100, accuracy: 2) }

      let(:expected_local_range){ -199..-100 }
      let(:expected_jdy_range){ 301..400 }

      it_behaves_like "range methods"
    end

    context "`num: 0`でインスタンス化した場合" do
      let(:year){ klass.new(num: 0) }

      let(:expected_local_range){ 0..0 }
      let(:expected_jdy_range){ 500..500 }

      it_behaves_like "range methods"
    end
  end

  describe "#local_code" do
    subject{ year.local_code }

    context "`num: 0, accuracy: 0`の場合" do
      let(:year){ klass.new(num: 0, accuracy: 0) }

      it "は、日付パートコードを返すこと" do
        is_expected.to eq("A=0")
      end
    end

    context "`num: 0, accuracy: 1`の場合" do
      let(:year){ klass.new(num: 0, accuracy: 1) }

      it "は、日付パートコードを返すこと" do
        is_expected.to eq("A=0*")
      end
    end

    context "`num: 10, accuracy: 0`の場合" do
      let(:year){ klass.new(num: 10, accuracy: 0) }

      it "は、日付パートコードを返すこと" do
        is_expected.to eq("A=10")
      end
    end

    context "`num: 10, accuracy: 1`の場合" do
      let(:year){ klass.new(num: 10, accuracy: 1) }

      it "は、日付パートコードを返すこと" do
        is_expected.to eq("A=1*")
      end
    end

    context "`num: -1, accuracy: 0`の場合" do
      let(:year){ klass.new(num: -1, accuracy: 0) }

      it "は、日付パートコードを返すこと" do
        is_expected.to eq("B=1")
      end
    end

    context "`num: -1, accuracy: 1`の場合" do
      let(:year){ klass.new(num: -1, accuracy: 1) }

      it "は、日付パートコードを返すこと" do
        is_expected.to eq("B=0*")
      end
    end

    context "`num: -10, accuracy: 0`の場合" do
      let(:year){ klass.new(num: -10, accuracy: 0) }

      it "は、日付パートコードを返すこと" do
        is_expected.to eq("B=10")
      end
    end

    context "`num: -10, accuracy: 1`の場合" do
      let(:year){ klass.new(num: -10, accuracy: 1) }

      it "は、日付パートコードを返すこと" do
        is_expected.to eq("B=1*")
      end
    end
  end

  describe "パース系メソッド群" do
    let(:year){ klass.local_parse(code) }

    shared_examples_for "local_parse" do
      describe "::_local_parse" do
        it "は、インスタンス化用ハッシュを返すこと" do
          expect(klass._local_parse(code)).to eq(expected_hash)
        end
      end

      describe "::local_parse" do
        it "は、パースしてインスタンスを生成すること" do
          expect(year).to be_instance_of(klass)
          expect(year).to have_attributes(
            local_range: expected_local_range,
            accuracy: expected_hash[:accuracy],
          )
        end

        it{ expect(year.local_code).to eq(expected_code) }
      end
    end

    shared_examples_for "raise ParseError" do
      it "は、パースエラーを発生させること" do
        expect{ klass._local_parse(code) }.to raise_error(FDate::ParseError, "It's wrong grammar. #{code.inspect}")
      end
    end

    let(:expected_code){ code }


    context "日付パートコード'A=0'を渡した場合" do
      let(:code){ "A=0" }

      let(:expected_hash){ {num: 0, accuracy: 0} }
      let(:expected_local_range){ 0..0 }

      it_behaves_like "local_parse"
    end

    context "日付パートコード'A=1'を渡した場合" do
      let(:code){ "A=1" }

      let(:expected_hash){ {num: 1, accuracy: 0} }
      let(:expected_local_range){ 1..1 }

      it_behaves_like "local_parse"
    end

    context "日付パートコード'A=10'を渡した場合" do
      let(:code){ "A=10" }

      let(:expected_hash){ {num: 10, accuracy: 0} }
      let(:expected_local_range){ 10..10 }

      it_behaves_like "local_parse"
    end

    context "日付パートコード'A=0*'を渡した場合" do
      let(:code){ "A=0*" }

      let(:expected_hash){ {num: 0, accuracy: 1} }
      let(:expected_local_range){ 0..9 }

      it_behaves_like "local_parse"
    end

    context "日付パートコード'A=1*'を渡した場合" do
      let(:code){ "A=1*" }

      let(:expected_hash){ {num: 10, accuracy: 1} }
      let(:expected_local_range){ 10..19 }

      it_behaves_like "local_parse"
    end

    context "日付パートコード'B=0'を渡した場合" do
      let(:code){ "B=0" }

      let(:expected_hash){ {num: 0, accuracy: 0} }
      let(:expected_local_range){ 0..0 }
      let(:expected_code){ "A=0" }

      it_behaves_like "local_parse"
    end

    context "日付パートコード'B=1'を渡した場合" do
      let(:code){ "B=1" }

      let(:expected_hash){ {num: -1, accuracy: 0} }
      let(:expected_local_range){ -1..-1 }

      it_behaves_like "local_parse"
    end

    context "日付パートコード'B=10'を渡した場合" do
      let(:code){ "B=10" }

      let(:expected_hash){ {num: -10, accuracy: 0} }
      let(:expected_local_range){ -10..-10 }

      it_behaves_like "local_parse"
    end

    context "日付パートコード'B=0*'を渡した場合" do
      let(:code){ "B=0*" }

      let(:expected_hash){ {num: -1, accuracy: 1} }
      let(:expected_local_range){ -9..0 }

      it_behaves_like "local_parse"
    end

    context "日付パートコード'B=1*'を渡した場合" do
      let(:code){ "B=1*" }

      let(:expected_hash){ {num: -10, accuracy: 1} }
      let(:expected_local_range){ -19..-10 }

      it_behaves_like "local_parse"
    end

    context "'A='を省略したコードを渡した場合" do
      let(:code){ "0" }

      let(:expected_hash){ {num: 0, accuracy: 0} }
      let(:expected_local_range){ 0..0 }
      let(:expected_code){ "A=0" }

      it_behaves_like "local_parse"
    end

    context "'A='を省略したコードを渡した場合" do
      let(:code){ "1" }

      let(:expected_hash){ {num: 1, accuracy: 0} }
      let(:expected_local_range){ 1..1 }
      let(:expected_code){ "A=1" }

      it_behaves_like "local_parse"
    end

    context "マイナスの数値のコードを渡した場合" do
      let(:code){ "-1" }
      it_behaves_like "raise ParseError"
    end

    context "'*'から始まるコードを渡した場合" do
      let(:code){ "*" }

      describe "::_local_parse" do
        it "は、不正なインスタンス化用ハッシュを返すこと" do
          expect(klass._local_parse(code)).to eq({num: nil, accuracy: 1})
        end
      end

      describe "::local_parse" do
        it "は、日付エラーを発生させること" do
          expect{ klass.local_parse(code) }.to raise_error(FDate::DateError, "NilDate is not allowed.")
        end
      end
    end

    context "不正なAD/BCキーを持つコードを渡した場合" do
      let(:code){ "C=1" }
      it_behaves_like "raise ParseError"
    end
  end

end
