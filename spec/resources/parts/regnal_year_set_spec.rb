require 'spec_helper.rb'

describe FDate::Part.collection["RegnalYearSet"] do
  let(:klass){
    Class.new(described_class) do
      self.key = "key"
    end
  }

  before do
    klass.reign_collection.clear

    [
      ["G0", {first: adgr.new(year: 5001), last: adgr.new(year: 5003)}],
      ["G1", {first: adgr.new(year: 5003), last: adgr.new(year: 5008)}],
      ["G2", {first: adgr.new(year: 5006), last: adgr.new(year: 5029)}],
    ].each do |key, _term|
      klass.reign_collection.inherit key do
        self.term = FDate::Term.new(_term)
      end
    end
  end

  let(:collection){ klass.reign_collection }
  let(:adgr){ FDate::Calendar.collection["adgr"] }
  let(:jdy_class){ FDate::Part.collection["JDY"] }


  describe "#initialize" do
    context "元号年モード" do
      shared_examples_for "store regnal_years" do
        it "は、渡した元号年の配列を`@years`に格納すること" do
          expect(klass.new(years).years).to eql(years)
        end
      end


      context "1つの元号年オブジェクトを渡した場合" do
        context "fixedな年を渡した場合" do
          let(:years){ [collection["G0"].new(num: 2)] }
          it_behaves_like "store regnal_years"
        end

        context "fuzzyな年を渡した場合" do
          let(:years){ [collection["G2"].new(num: 10, accuracy: 1)] }
          it_behaves_like "store regnal_years"
        end

        context "NilDateな年を渡した場合" do
          let(:years){ [collection["G0"].new(num: nil)] }
          it_behaves_like "store regnal_years"
        end
      end

      context "複数の元号年を渡した場合" do
        context "fixedかつユリウス通年が全て同じ場合" do
          let(:years){ [collection["G1"].new(num: 5), collection["G2"].new(num: 2)] }
          it_behaves_like "store regnal_years"
        end

        context "fixedかつユリウス通年が異なる場合" do
          let(:years){ [collection["G1"].new(num: 6), collection["G2"].new(num: 2)] }

          it "は、引数エラーを発生させること" do
            expect{ klass.new(years) }.to raise_error(ArgumentError, 'include different JDY. [5008, 5007]')
          end
        end

        context "fuzzyかつ範囲が全て同じ場合" do
          let(:reign){
            term = collection["G1"].term

            klass.reign_collection.inherit "Gx" do
              self.term = term
            end
          }
          let(:years){ [collection["G1"].new(num: 0, accuracy: 1), reign.new(num: 0, accuracy: 1)] }
          it_behaves_like "store regnal_years"
        end

        context "fuzzyかつ範囲が異なる場合" do
          let(:years){ [collection["G1"].new(num: 0, accuracy: 1), collection["G2"].new(num: 0, accuracy: 1)] }

          it "は、引数エラーを発生させること" do
            expect{ klass.new(years) }.to raise_error(ArgumentError, 'include different JDY. [5003..5008, 5006..5014]')
          end
        end
      end

      context "元号コレクションに含まれない元号を渡した場合" do
        let(:other_reign_class){
          term = FDate::Term.new(first: adgr.new(year: 5002), last: adgr.new(year: 5003))
          Class.new(FDate::Part.collection["RegnalYear"]) do
            self.key = "OthenClass"
            self.term = term
          end
        }
        let(:years){ [collection["G0"].new(num: 2), other_reign_class.new(num: 1)] }

        it{ expect(years.map(&:jdy)).to all(eq(5002)) }

        it "は、引数エラーを発生させること" do
          expect{ klass.new(years) }.to raise_error(ArgumentError, "`FDate::Part[key]::reign_collection` not include OthenClass")
        end
      end
    end

    context "ユリウス通年モード" do
      shared_examples_for "store single jdy" do
        it "は、1つのユリウス通年だけを`@years`に格納すること" do
          expect(klass.new(years).years).to eql([years.first])
          expect(klass.new(years)).to be_jdy
          expect(klass.new(years)).to be_single
        end
      end


      context "1つのユリウス通年オブジェクトを渡した場合" do
        context "fixedな年を渡した場合" do
          let(:years){ [jdy_class.new(num: 4713)] }
          it_behaves_like "store single jdy"
        end

        context "fuzzyな年を渡した場合" do
          let(:years){ [jdy_class.new(num: 4700, accuracy: 2)] }
          it_behaves_like "store single jdy"
        end
      end

      context "複数のユリウス通年オブジェクトを渡した場合" do
        context "fixedかつ同じ年を渡した場合" do
          let(:years){ [jdy_class.new(num: 4713), jdy_class.new(num: 4713)] }
          it_behaves_like "store single jdy"
        end

        context "fixedかつ違う年を渡した場合" do
          let(:years){ [jdy_class.new(num: 4713), jdy_class.new(num: 4716)] }

          it "は、引数エラーを発生させること" do
            expect{ klass.new(years) }.to raise_error(ArgumentError, 'include different JDY. [4713, 4716]')
          end
        end

        context "fuzzyかつ範囲が同じ年を渡した場合" do
          let(:years){ [jdy_class.new(num: 4700, accuracy: 2), jdy_class.new(num: 4710, accuracy: 2)] }
          it_behaves_like "store single jdy"
        end

        context "fuzzyかつ範囲の異なる年を渡した場合" do
          let(:years){ [jdy_class.new(num: 4700, accuracy: 2), jdy_class.new(num: 4710, accuracy: 1)] }

          it "は、引数エラーを発生させること" do
            expect{ klass.new(years) }.to raise_error(ArgumentError, 'include different JDY. [4700..4799, 4710..4719]')
          end
        end
      end

      context "ユリウス通年オブジェクトと元号オブジェクトを渡した場合" do
        context "fixedかつ同じ年を渡した場合" do
          let(:years){ [jdy_class.new(num: 5007), collection["G1"].new(num: 5), collection["G2"].new(num: 2)] }
          it_behaves_like "store single jdy"
        end

        context "fixedかつ違う年を渡した場合" do
          let(:years){ [jdy_class.new(num: 5007), collection["G1"].new(num: 4), collection["G2"].new(num: 3)] }

          it "は、引数エラーを発生させること" do
            expect{ klass.new(years) }.to raise_error(ArgumentError, 'include different JDY. [5007, 5006, 5008]')
          end
        end

        context "fuzzyかつ範囲が同じ年を渡した場合" do
          let(:reign){
            term = FDate::Term.new(first: adgr.new(year: 4701), last: adgr.new(year: 5000))

            klass.reign_collection._inherit do
              self.term = term
            end
          }
          let(:years){ [jdy_class.new(num: 4710, accuracy: 1), reign.new(num: 10, accuracy: 1)] }
          it_behaves_like "store single jdy"
        end

        context "fuzzyかつ範囲の異なる年を渡した場合" do
          let(:years){ [jdy_class.new(num: 5000, accuracy: 1), collection["G1"].new(num: 0, accuracy: 1)] }

          it "は、引数エラーを発生させること" do
            expect{ klass.new(years) }.to raise_error(ArgumentError, 'include different JDY. [5000..5009, 5003..5008]')
          end
        end
      end
    end

    context "他の種類の年オブジェクトを渡した場合" do
      let(:years){ [FDate::Part.collection["AnnoDominiYear"].new(num: 2018)] }

      it "は、引数エラーを発生させること" do
        expect{ klass.new(years) }.to raise_error(ArgumentError, "`FDate::Part[key]::reign_collection` not include AnnoDominiYear")
      end
    end

    context "空配列を渡した場合" do
      let(:years){ [] }

      it "は、引数エラーを発生させること" do
        expect{ klass.new(years) }.to raise_error(ArgumentError, "years are empty")
      end
    end
  end

  describe "#fixed?,#fuzzy?,#nildate?" do
    subject{ klass.new(years) }

    context "`#years`が全てfixedである場合" do
      let(:years){ [collection["G0"].new(num: 2)] }

      it "は、fixedであることを示すこと" do
        is_expected.to be_fixed
        is_expected.not_to be_fuzzy
        is_expected.not_to be_nildate
      end
    end

    context "`#years`にfuzzyが1つでも含まれている場合" do
      let(:years){ [collection["G2"].new(num: 10, accuracy: 1)] }

      it "は、fuzzyであることを示すこと" do
        is_expected.not_to be_fixed
        is_expected.to be_fuzzy
        is_expected.not_to be_nildate
      end
    end

    context "`#years`にNilDateが1つでも含まれている場合" do
      let(:years){ [collection["G0"].new(num: nil)] }

      it "は、NilDateであることを示すこと" do
        is_expected.not_to be_fixed
        is_expected.to be_fuzzy
        is_expected.to be_nildate
      end
    end
  end

  describe "#single?,#plural?" do
    subject{ klass.new(years) }

    context "年オブジェクトが単数の場合" do
      let(:years){ [collection["G0"].new(num: 2)] }

      it "は、単数であることを示すこと" do
        is_expected.to be_single
        is_expected.not_to be_plural
      end
    end

    context "年オブジェクトが複数の場合" do
      let(:years){ [collection["G1"].new(num: 5), collection["G2"].new(num: 2)] }

      it "は、複数であることを示すこと" do
        is_expected.not_to be_single
        is_expected.to be_plural
      end
    end
  end

  describe "#jdy?" do
    subject{ klass.new(years) }

    context "元号年オブジェクトが格納されている場合" do
      let(:years){ [collection["G0"].new(num: 2)] }
      it{ is_expected.not_to be_jdy }
    end

    context "ユリウス通年オブジェクトが格納されている場合" do
      let(:years){ [jdy_class.new(num: 4713)] }
      it{ is_expected.to be_jdy }
    end
  end

  describe "年数系メソッド群" do
    let(:year){ klass.new(years) }

    context "fixedかつ年オブジェクトが単数の場合" do
      let(:years){ [collection["G0"].new(num: 2)] }

      describe "#local_range" do
        it "は、唯一の年オブジェクトの紀年法特有の年数の範囲を返すこと" do
          expect(year.local_range).to eq(2..2)
        end
      end

      describe "#jdy_range" do
        it "は、唯一の年オブジェクトのユリウス通年の範囲を返すこと" do
          expect(year.jdy_range).to eq(5002..5002)
        end
      end

      describe "#jdy" do
        it "は、ユリウス通年（数値）を返すこと" do
          expect(year.jdy).to eq(5002)
        end
      end
    end

    context "fixedかつ年オブジェクトが複数の場合" do
      let(:years){ [collection["G1"].new(num: 5), collection["G2"].new(num: 2)] }

      describe "#local_range" do
        it{ expect(year.local_range).to be_nil }
      end

      describe "#jdy_range" do
        it "は、各年オブジェクト共通のユリウス通年の範囲を返すこと" do
          expect(year.jdy_range).to eq(5007..5007)
        end
      end

      describe "#jdy" do
        it "は、ユリウス通年（数値）を返すこと" do
          expect(year.jdy).to eq(5007)
        end
      end
    end

    context "fuzzyの場合" do
      let(:years){ [collection["G2"].new(num: 10, accuracy: 1)] }

      describe "#local_range" do
        it "は、唯一の年オブジェクトの紀年法特有の年数の範囲を返すこと" do
          expect(year.local_range).to eq(10..19)
        end
      end

      describe "#jdy_range" do
        it "は、ユリウス通年の範囲を返すこと" do
          expect(year.jdy_range).to eq(5015..5024)
        end
      end

      describe "#jdy" do
        it{ expect(year.jdy).to eq(5015..5024) }
      end
    end
  end

  describe "#eql?" do
    subject{ klass.new([collection["G1"].new(num: 5), collection["G2"].new(num: 2)]) }

    context "元号年セットオブジェクトが同じ場合" do
      context "格納された年オブジェクトが全て同じ場合" do
        let(:other){ klass.new([collection["G1"].new(num: 5), collection["G2"].new(num: 2)]) }
        it{ is_expected.to eql(other) }
      end

      context "格納された年オブジェクトが異なる場合" do
        let(:other){ klass.new([collection["G1"].new(num: 5)]) }
        it{ is_expected.not_to eql(other) }
      end
    end

    context "元号年セットオブジェクトが異なる場合" do
      let(:other_class){
        Class.new(described_class) do
          self.key = "other"
        end
      }

      before do
        collection.each do |reign|
          other_class.reign_collection << reign
        end
      end

      context "格納された年オブジェクトが全て同じ場合" do
        let(:other){ other_class.new([collection["G1"].new(num: 5), collection["G2"].new(num: 2)]) }
        it{ is_expected.not_to eql(other) }
      end

      context "格納された年オブジェクトが異なる場合" do
        let(:other){ other_class.new([collection["G1"].new(num: 5)]) }
        it{ is_expected.not_to eql(other) }
      end
    end
  end

  describe "#narrow_down" do
    subject{ year.narrow_down(arg) }

    context "単数の元号年オブジェクトが格納されている場合" do
      let(:year){ klass.new([collection["G0"].new(num: 2)]) }

      context "唯一の元号年オブジェクトの期間に含まれる日付を渡した場合" do
        let(:arg){ 1827000 }

        it{ expect(collection["G0"].term).to be == arg }

        it "は、同じクラスと内容の別インスタンスを返すこと" do
          is_expected.to eql(year)
          is_expected.not_to equal(year)
        end
      end

      context "唯一の元号年オブジェクトの期間に含まれない日付を渡した場合" do
        let(:arg){ 1828000 }

        it{ expect(collection["G0"].term).to be < arg }

        it "は、同じ年のユリウス通年オブジェクトを格納した元号年セットオブジェクトを返すこと" do
          is_expected.to eql(klass.new([jdy_class.new(num: 5002)]))
        end
      end
    end

    context "複数の元号年オブジェクトが格納されている場合" do
      let(:year){ klass.new([collection["G1"].new(num: 5), collection["G2"].new(num: 2), collection["G3"].new(num: 1)]) }

      before do
        term = FDate::Term.new(first: adgr.new(year: 5007, month: 12, day: 31), last: adgr.new(year: 5009))
        collection.inherit "G3" do
          self.term = term
        end
      end

      context "渡した日付を期間に含む元号年オブジェクトがある場合" do
        let(:arg){ 1827000 }

        example do
          expect(collection["G1"].term).to be == arg
          expect(collection["G2"].term).to be > arg
          expect(collection["G3"].term).to be > arg
        end

        it "は、年オブジェクトを絞り込んだ元号年セットオブジェクトを返すこと" do
          is_expected.to eql(klass.new([collection["G1"].new(num: 5)]))
        end
      end

      context "渡した日付を期間に含む元号年オブジェクトがある場合" do
        let(:arg){ 1828500 }

        example do
          expect(collection["G1"].term).to be == arg
          expect(collection["G2"].term).to be == arg
          expect(collection["G3"].term).to be > arg
        end

        it "は、年オブジェクトを絞り込んだ元号年セットオブジェクトを返すこと" do
          is_expected.to eql(klass.new([collection["G1"].new(num: 5), collection["G2"].new(num: 2)]))
        end
      end

      context "渡した日付を期間に含む元号年オブジェクトがない場合" do
        let(:arg){ 1826000 }

        example do
          expect(collection["G1"].term).to be > arg
          expect(collection["G2"].term).to be > arg
          expect(collection["G3"].term).to be > arg
        end

        it "は、同じ年のユリウス通年オブジェクトを格納した元号年セットオブジェクトを返すこと" do
          is_expected.to eql(klass.new([jdy_class.new(num: 5007)]))
        end
      end
    end

    context "ユリウス通年オブジェクトが格納されている場合" do
      let(:year){ klass.new([jdy_class.new(num: 5001)]) }

      let(:arg){ nil } # 渡すものは何でもいい

      it "は、同じクラスと内容の別インスタンスを返すこと" do
        is_expected.to eql(year)
        is_expected.not_to equal(year)
      end
    end
  end

  describe "#local_code" do
    subject{ year.local_code }

    context "年オブジェクトが単数の場合" do
      context "fixedの場合" do
        let(:year){ klass.new([collection["G0"].new(num: 2)]) }
        it{ is_expected.to eq("G0=2") }
      end

      context "fuzzyの場合" do
        let(:year){ klass.new([collection["G2"].new(num: 10, accuracy: 1)]) }
        it{ is_expected.to eq("G2=1*") }
      end

      context "NilDateの場合" do
        context "fuzzyの場合" do
          let(:year){ klass.new([collection["G0"].new(num: nil)]) }
          it{ is_expected.to eq("G0=*") }
        end
      end
    end

    context "年オブジェクトが複数の場合" do
      let(:year){ klass.new([collection["G1"].new(num: 5), collection["G2"].new(num: 2)]) }

      it "は、各元号年を連結した日付コードを返すこと" do
        is_expected.to eq("G1=5/G2=2")
      end
    end

    context "年オブジェクトがユリウス通年オブジェクトの場合" do
      let(:year){ klass.new([jdy_class.new(num: 5000)]) }

      it "は、グローバルコードを返すこと" do
        is_expected.to eq("(5000)")
      end
    end
  end

  describe "::reign_collection.inherit" do
    %w(_ - = ~ ^ & |).each do |char|
      context "禁則文字'#{char}'を含むキーを渡した場合" do
        let(:key){ "G#{char}1" }

        it "は、引数エラーを発生させること" do
          expect{
            klass.reign_collection.inherit(key)
          }.to raise_error(ArgumentError, "key #{key.inspect} includes illegal character [#{char.inspect}]")
        end
      end
    end
  end

  describe "::_local_parse" do
    context "1つのfixedな元号の日付パートコードを渡した場合" do
      it "は、1つのfixedな元号年オブジェクトを含む配列を返すこと" do
        expect(klass._local_parse("G1=2")).to eq([collection["G1"].new(num: 2)])
      end
    end

    context "単一の元号かつfuzzyの日付パートコードを渡した場合" do
      it "は、1つのfuzzyな元号年オブジェクトを含む配列を返すこと" do
        expect(klass._local_parse("G2=1*")).to eq([collection["G2"].new(num: 10, accuracy: 1)])
      end
    end

    context "複数の元号かつfixedの日付パートコードを場合" do
      it "は、複数の元号年オブジェクトを含む配列を返すこと" do
        expect(klass._local_parse("G0=3/G1=1")).to eq([collection["G0"].new(num: 3), collection["G1"].new(num: 1)])
      end
    end

    context "'='の無い日付パートコードを渡した場合" do
      it "は、パースエラーを発生させること" do
        expect{ klass._local_parse("G1") }.to raise_error(FDate::ParseError, "It's wrong grammar. nil")
      end
    end

    context "存在しない元号キーを含む日付パートコードを渡した場合" do
      it "は、キーエラーを発生させること" do
        expect{ klass.local_parse("G9=1") }.to raise_error(KeyError, "key not found: \"G9\"")
      end
    end

    context "グローバルコードを渡した場合" do
      it "は、パースエラーを発生させること" do
        expect{ klass._local_parse("(5000)") }.to raise_error(FDate::ParseError, "It's wrong grammar. nil")
      end
    end
  end

  describe "::_jdy" do
    context "ユリウス通年のみを渡した場合" do
      context "渡したユリウス通年を期間に含む元号が無い場合" do
        it "は、ユリウス通年オブジェクトを1つだけ含んだ配列を返すこと" do
          expect(klass._jdy(5000)).to eq([jdy_class.new(num: 5000)])
        end
      end

      context "渡したユリウス通年を期間に含む元号が1つある場合" do
        it "は、その元号のインスタンスを1つ含んだ配列を返すこと" do
          expect(klass._jdy(5010)).to eq([collection['G2'].new(num: 5)])
        end
      end

      context "渡したユリウス通年を期間に含む元号が複数ある場合" do
        it "は、それら全ての元号のインスタンスを含んだ配列を返すこと" do
          expect(klass._jdy(5007)).to eq([collection["G1"].new(num: 5), collection["G2"].new(num: 2)])
        end
      end
    end

    context "ユリウス通年と日付を渡した場合" do
      subject{ klass._jdy(6731, date) }

      before do
        klass.reign_collection.clear

        [
          ["G0", adgr.jd(2458100)..(adgr.jd(2458200))],
          ["G1", adgr.jd(2458150)..(adgr.jd(2458250))],
        ].each do |key, _term|
          klass.reign_collection.inherit key do
            self.term = FDate::Term.build(_term)
          end
        end
      end

      context "fixedな日付を渡した場合" do
        context "渡した日付を期間に含む元号が無い場合" do
          let(:date){ 2458095 }

          it "は、ユリウス通年オブジェクトを1つ含む配列を返すこと" do
            is_expected.to eq([jdy_class.jdy(6731)])
          end
        end

        context "渡した日付を期間に含む元号が1つある場合" do
          let(:date){ 2458105 }

          it "は、その元号のインスタンスを1つ含む配列を返すこと" do
            is_expected.to eq([collection["G0"].jdy(6731)])
          end
        end

        context "渡した日付を期間に含む元号が複数ある場合" do
          let(:date){ 2458155 }

          it "は、それらの元号のインスタンスを含む配列を返すこと" do
            is_expected.to eq([collection["G0"].jdy(6731), collection["G1"].jdy(6731)])
          end
        end
      end

      context "fuzzyな日付を渡した場合" do
        context "渡した日付が期間に引っかかる元号が無い場合" do
          let(:date){ 2458085..2458095 }

          it "は、ユリウス通年オブジェクトを1つ含む配列を返すこと" do
            is_expected.to eq([jdy_class.jdy(6731)])
          end
        end

        context "渡した日付が期間に引っかかる元号が1つある場合" do
          let(:date){ 2458095..2458105 }

          it "は、その元号のインスタンスを1つ含む配列を返すこと" do
            is_expected.to eq([collection["G0"].jdy(6731)])
          end
        end

        context "渡した日付が期間に引っかかる元号が複数ある場合" do
          let(:date){ 2458125..2458200 }

          it "は、それらの元号のインスタンスを含む配列を返すこと" do
            is_expected.to eq([collection["G0"].jdy(6731), collection["G1"].jdy(6731)])
          end
        end
      end
    end
  end
end
