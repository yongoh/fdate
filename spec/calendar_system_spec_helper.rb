module CalendarSystemSpecHelper
  def jd_test(jd, cal_key, arg)
    context "ユリウス通日`#{jd}`を渡した場合" do
      let(:calendar){ FDate::Calendar.collection[cal_key] }

      it "は、暦'#{cal_key}'の日付オブジェクトを作成すること" do
        expect(described_class.jd(jd)).to eql(calendar.new(arg))
      end
    end
  end
end
