require 'spec_helper.rb'

describe FDate::Part::RangeMethods do
  let(:owner){ double("Owner").extend described_class }

  before do
    owner.send(:max_range=, -25..25)
  end

  describe "#valid_range?" do
    # [max_range] <range>

    context "`     [  ] <  >`となる範囲を渡した場合" do
      it{ expect(owner).not_to be_valid_range(30..50) }
    end

    context "`     [  X  >  `となる範囲を渡した場合" do
      it{ expect(owner).to be_valid_range(25..30) }
    end

    context "`     [ <] >   `となる範囲を渡した場合" do
      it{ expect(owner).to be_valid_range(15..30) }
    end

    context "`    [< >]     `となる範囲を渡した場合" do
      it{ expect(owner).to be_valid_range(-15..15) }
    end

    context "`     {  }     `となる範囲を渡した場合" do
      it{ expect(owner).to be_valid_range(-25..25) }
    end

    context "`    <[ ]>     `となる範囲を渡した場合" do
      it{ expect(owner).to be_valid_range(-30..30) }
    end

    context "`   < [> ]     `となる範囲を渡した場合" do
      it{ expect(owner).to be_valid_range(-30..-15) }
    end

    context "`  <  X  ]     `となる範囲を渡した場合" do
      it{ expect(owner).to be_valid_range(-30..-25) }
    end

    context "`<  > [  ]     `となる範囲を渡した場合" do
      it{ expect(owner).not_to be_valid_range(-50..-30) }
    end
  end

  describe "#trim_range" do
    context "`     [  ] <  >`となる範囲を渡した場合" do
      it "は、`RangeError`を発生させること" do
        expect{ owner.trim_range(30..50) }.to raise_error(FDate::Part::RangeError, "30..50 deviates from -25..25")
      end
    end

    context "`     [  X  >  `となる範囲を渡した場合" do
      it{ expect(owner.trim_range(25..30)).to eq(25..25) }
    end

    context "`     [ <] >   `となる範囲を渡した場合" do
      it{ expect(owner.trim_range(15..30)).to eq(15..25) }
    end

    context "`    [< >]     `となる範囲を渡した場合" do
      it{ expect(owner.trim_range(-15..15)).to eq(-15..15) }
    end

    context "`     {  }     `となる範囲を渡した場合" do
      it{ expect(owner.trim_range(-25..25)).to eq(-25..25) }
    end

    context "`    <[ ]>     `となる範囲を渡した場合" do
      it{ expect(owner.trim_range(-30..30)).to eq(-25..25) }
    end

    context "`   < [> ]     `となる範囲を渡した場合" do
      it{ expect(owner.trim_range(-30..-15)).to eq(-25..-15) }
    end

    context "`  <  X  ]     `となる範囲を渡した場合" do
      it{ expect(owner.trim_range(-30..-25)).to eq(-25..-25) }
    end

    context "`<  > [  ]     `となる範囲を渡した場合" do
      it "は、`RangeError`を発生させること" do
        expect{ owner.trim_range(-50..-30) }.to raise_error(FDate::Part::RangeError, "-50..-30 deviates from -25..25")
      end
    end
  end
end
