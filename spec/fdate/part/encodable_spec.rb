require 'spec_helper.rb'

describe FDate::Part::Encodable do
  before do
    part.extend described_class
  end


  describe "#global_code" do
    context "fixedの場合" do
      context "正数の場合" do
        let(:part){ double("Part", fixed?: true, global_range: 10..10) }
        it{ expect(part.global_code).to eq("(10)") }
      end

      context "負数の場合" do
        let(:part){ double("Part", fixed?: true, global_range: -10..-10) }
        it{ expect(part.global_code).to eq("(-10)") }
      end
    end

    context "fuzzyの場合" do
      context "正数の場合" do
        let(:part){ double("Part", fixed?: false, global_range: 10..19) }
        it{ expect(part.global_code).to eq("(10..19)") }
      end

      context "負数の場合" do
        let(:part){ double("Part", fixed?: false, global_range: -19..-10) }
        it{ expect(part.global_code).to eq("(-19..-10)") }
      end
    end

    context "NilDateの場合" do
      let(:part){ double("Part", fixed?: false, global_range: 30..35) }
      it{ expect(part.global_code).to eq("(30..35)") }
    end
  end

  describe "#local_code" do
    context "fixedの場合" do
      context "正数の場合" do
        let(:part){ double("Part", nildate?: false, local_range: 10..10, accuracy: 0) }
        it{ expect(part.local_code).to eq("10") }
      end

      context "負数の場合" do
        let(:part){ double("Part", nildate?: false, local_range: -10..-10, accuracy: 0) }
        it{ expect(part.local_code).to eq("10") }
      end
    end

    context "fuzzyの場合" do
      context "正数・誤差1桁の場合" do
        let(:part){ double("Part", nildate?: false, local_range: 10..19, accuracy: 1) }
        it{ expect(part.local_code).to eq("1*") }
      end

      context "正数・誤差2桁の場合" do
        let(:part){ double("Part", nildate?: false, local_range: 100..199, accuracy: 2) }
        it{ expect(part.local_code).to eq("1**") }
      end

      context "正数・誤差1桁・中途半端な範囲の場合" do
        let(:part){ double("Part", nildate?: false, local_range: 20..25, accuracy: 1) }
        it{ expect(part.local_code).to eq("2*") }
      end

      context "負数・誤差1桁の場合" do
        let(:part){ double("Part", nildate?: false, local_range: -19..-10, accuracy: 1) }
        it{ expect(part.local_code).to eq("1*") }
      end

      context "負数・誤差2桁の場合" do
        let(:part){ double("Part", nildate?: false, local_range: -100..-199, accuracy: 2) }
        it{ expect(part.local_code).to eq("1**") }
      end

      context "負数・誤差1桁・中途半端な範囲の場合" do
        let(:part){ double("Part", nildate?: false, local_range: -25..-20, accuracy: 1) }
        it{ expect(part.local_code).to eq("2*") }
      end
    end

    context "NilDateの場合" do
      let(:part){ double("Part", nildate?: true, local_range: 30..35) }
      it{ expect(part.local_code).to eq("*") }
    end
  end
end
