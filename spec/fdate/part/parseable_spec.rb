require 'spec_helper.rb'

describe FDate::Part::Parseable do
  let(:owner){ double("Owner").extend described_class }

  before do
    allow(owner).to receive(:_from_i){|int| {num: int + 5} }
  end

  shared_examples_for "return hash for nildate" do
    it "は、NilDate用インスタンス化用ハッシュを返すこと" do
      is_expected.to eq({num: nil, accuracy: 1})
    end
  end

  shared_examples_for "raise ParseError" do
    it "は、パースエラーを発生させること" do
      expect{ subject }.to raise_error(FDate::ParseError, "It's wrong grammar. #{code.inspect}")
    end
  end

  shared_examples_for "raise error" do
    context "文字列以外を渡した場合" do
      let(:code){ 1234 }

      it "は、何らかの例外を発生させること" do
        expect{ subject }.to raise_error(StandardError)
      end
    end
  end


  describe "#_local_parse" do
    subject do
      owner._local_parse(code)
    end

    context "数字のみの相対値コードを渡した場合" do
      let(:code){ "1234" }

      it "は、`accuracy: 0`を含むインスタンス化用ハッシュを返すこと" do
        is_expected.to eq({num: 1234, accuracy: 0})
      end
    end

    context "1桁目に*を置いた相対値コードを渡した場合" do
      let(:code){ "123*" }

      it "は、`accuracy: 1`を含むインスタンス化用ハッシュを返すこと" do
        is_expected.to eq({num: 1230, accuracy: 1})
      end
    end

    context "2桁目から*を置いた相対値コードを渡した場合" do
      let(:code){ "12**" }

      it "は、`accuracy: 2`を含むインスタンス化用ハッシュを返すこと" do
        is_expected.to eq({num: 1200, accuracy: 2})
      end
    end

    context "3桁目に*を置きその後に数字が続く相対値コードを渡した場合" do
      let(:code){ "1*34" }

      it "は、`accuracy: 3`を含むインスタンス化用ハッシュを返すこと" do
        is_expected.to eq({num: 1000, accuracy: 3})
      end
    end

    context "0の次に*がくる相対値コードを渡した場合" do
      let(:code){ "0*" }

      it "は、インスタンス化用ハッシュを返すこと" do
        is_expected.to eq({num: 0, accuracy: 1})
      end
    end

    context "複数の0の次に複数の*がくる相対値コードを渡した場合" do
      let(:code){ "00***" }

      it "は、インスタンス化用ハッシュを返すこと" do
        is_expected.to eq({num: 0, accuracy: 3})
      end
    end

    context "*のみの相対値コードを渡した場合" do
      let(:code){ "*" }
      it_behaves_like "return hash for nildate"
    end

    context "先頭が*の相対値コードを渡した場合" do
      let(:code){ "*234" }
      it_behaves_like "return hash for nildate"
    end

    context "不正なコードを渡した場合" do
      let(:code){ "123x" }
      it_behaves_like "raise ParseError"
    end

    it_behaves_like "raise error"
  end

  describe "#_global_parse" do
    subject do
      owner._global_parse(code)
    end

    context "数字のみの絶対値コードを渡した場合" do
      let(:code){ "(1234)" }

      it "は、インスタンス化用ハッシュを返すこと" do
        is_expected.to eq({num: 1239})
      end
    end

    context "正符号つきの絶対値コードを渡した場合（数字のみと同じ）" do
      let(:code){ "(+1234)" }

      it "は、インスタンス化用ハッシュを返すこと" do
        is_expected.to eq({num: 1239})
      end
    end

    context "負符号つきの絶対値コードを渡した場合" do
      let(:code){ "(-1234)" }

      it "は、インスタンス化用ハッシュを返すこと" do
        is_expected.to eq({num: -1229})
      end
    end

    context "不正なコード（*を含む）を与えた場合" do
      let(:code){ "(123*)" }
      it_behaves_like "raise ParseError"
    end

    context "不正なコード（括弧なし）を渡した場合" do
      let(:code){ "1234" }
      it_behaves_like "raise ParseError"
    end

    it_behaves_like "raise error"
  end

  describe "#_parse" do
    subject do
      owner._parse(code)
    end

    context "相対値コードを渡した場合" do
      let(:code){ "123*" }

      it "は、インスタンス化用ハッシュを返すこと" do
        is_expected.to eq({num: 1230, accuracy: 1})
      end
    end

    context "絶対値コードを渡した場合" do
      let(:code){ "(-1234)" }

      it "は、インスタンス化用ハッシュを返すこと" do
        is_expected.to eq({num: -1229})
      end
    end

    context "不正なコードを渡した場合" do
      let(:code){ "(-1234" }
      it_behaves_like "raise ParseError"
    end

    it_behaves_like "raise error"
  end
end
