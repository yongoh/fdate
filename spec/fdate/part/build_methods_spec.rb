require 'spec_helper.rb'

describe FDate::Part::BuildMethods do
  let(:klass) do
    mod = described_class
    rng = max_range

    Class.new(FDate::Part) do
      extend mod
      self.max_range = rng
    end
  end

  let(:max_range){ 1..31 }

  shared_examples_for "fixed" do
    it{ expect(part.accuracy).to be == 0 }
  end

  shared_examples_for "fuzzy" do
    it{ expect(part.accuracy).to be > 0 }
  end

  shared_examples_for "nildate" do
    it{ expect(part.accuracy).to be(Float::INFINITY) }
  end


  describe "#from_i" do
    context "`self.class.max_range`の範囲内の数値を与えた場合" do
      let(:part){ klass.from_i(5) }

      it{ expect(part.global_range).to eq(5..5) }
      it_behaves_like "fixed"
    end

    context "`self.class.max_range`の範囲外の数値を与えた場合" do
      it "は、範囲エラーを発生させること" do
        expect{ klass.from_i(0) }.to raise_error(FDate::Part::RangeError, "0..0 deviates from 1..31")
      end
    end
  end

  describe "#nildate" do
    let(:part){ klass.nildate }

    it{ expect(part.global_range).to eq(max_range) }
    it_behaves_like "nildate"
  end

  describe "#first" do
    let(:part){ klass.first }

    it{ expect(part.global_range).to eq((max_range.first)..(max_range.first)) }
    it_behaves_like "fixed"
  end

  describe "#last" do
    let(:part){ klass.last }

    it{ expect(part.global_range).to eq((max_range.last)..(max_range.last)) }
    it_behaves_like "fixed"
  end

  describe "#parse" do
    context "fixedなコードを与えた場合" do
      let(:part){ klass.parse("25") }

      it{ expect(part.global_range).to eq(25..25) }
      it_behaves_like "fixed"
    end

    context "fuzzyなコードを与えた場合" do
      let(:part){ klass.parse("2*") }

      it{ expect(part.global_range).to eq(20..29) }
      it_behaves_like "fuzzy"
    end

    context "NilDateなコードを与えた場合" do
      let(:part){ klass.parse("*") }

      it{ expect(part.global_range).to eq(1..31) }
      it_behaves_like "nildate"
    end

    context "不正なコードを与えた場合" do
      it "は、パースエラーを発生させること" do
        expect{ klass.parse("abcdefg") }.to raise_error(FDate::ParseError)
      end
    end
  end

  describe "#_build" do
    context "インスタンス化できないオブジェクトを渡した場合" do
      it "は、型エラーを発生させること" do
        expect{ klass.build(Time.now) }.to raise_error(TypeError, "can not convert Time into Hash.")
      end
    end
  end
end
