require 'spec_helper.rb'

describe FDate::Calendar do
  let(:calendar){
    prts = class_parts

    Class.new(described_class) do
      self.key = :key
      self.parts = prts
    end
  }

  let(:class_parts) do
    {
      year: -Float::INFINITY..Float::INFINITY,
      month: 1..12,
      day: 1..31,
    }.map{|name, range|
      [
        name,
        Class.new(FDate::Part) do
          self.max_range = range
        end,
      ]
    }.to_h
  end

  let(:adgr){ FDate::Calendar.collection["adgr"] }
  let(:adjl){ FDate::Calendar.collection["adjl"] }


  describe "#initialize" do
    %i(year month day).each do |name|
      shared_examples_for "fixed #{name}" do
        describe "##{name}" do
          it "は、fixedの日付パートを返すこと" do
            part = fdate.parts[name]

            expect(part.global_range).to eq(send("expected_#{name}_range"))
            expect(part).to be_fixed
            expect(part.about).to be_nil
          end
        end
      end

      shared_examples_for "fuzzy #{name}" do
        describe "##{name}" do
          it "は、fuzzyの日付パートを返すこと" do
            part = fdate.parts[name]

            expect(part.global_range).to eq(send("expected_#{name}_range"))
            expect(part).to be_fuzzy
            expect(part.about).to equal(about)
          end
        end
      end

      shared_examples_for "nildate #{name}" do
        describe "##{name}" do
          it "は、NilDateの日付パートを返すこと" do
            part = fdate.parts[name]

            expect(part.global_range).to eq(send("expected_#{name}_range"))
            expect(part).to be_nildate
            expect(part.about).to equal(about)
          end
        end
      end
    end

    let(:about){ FDate::About.collection['j'] }


    context "日付コード'_1955-10-23_'相当のパラメータでインスタンス化した場合" do
      let(:fdate){ calendar.new(year: 1955, month: 10, day: 23) }

      let(:expected_year_range){ 1955..1955 }
      let(:expected_month_range){ 10..10 }
      let(:expected_day_range){ 23..23 }

      it_behaves_like "fixed year"
      it_behaves_like "fixed month"
      it_behaves_like "fixed day"
    end

    context "日付コード'_1955-10-2*_'相当のパラメータでインスタンス化した場合" do
      let(:fdate){ calendar.new(year: 1955, month: 10, day: {num: 20, accuracy: 1}) }

      let(:expected_year_range){ 1955..1955 }
      let(:expected_month_range){ 10..10 }
      let(:expected_day_range){ 20..29 }

      it_behaves_like "fixed year"
      it_behaves_like "fixed month"
      it_behaves_like "fuzzy day"
    end

    context "日付コード'_1955-10-**_'相当のパラメータでインスタンス化した場合" do
      let(:fdate){ calendar.new(year: 1955, month: 10, day: nil) }

      let(:expected_year_range){ 1955..1955 }
      let(:expected_month_range){ 10..10 }
      let(:expected_day_range){ 1..31 }

      it_behaves_like "fixed year"
      it_behaves_like "fixed month"
      it_behaves_like "nildate day"
    end

    context "日付コード'_1955-1*-**_'相当のパラメータでインスタンス化した場合" do
      let(:fdate){ calendar.new(year: 1955, month: {num: 10, accuracy: 1}, day: nil) }

      let(:expected_year_range){ 1955..1955 }
      let(:expected_month_range){ 10..12 }

      it_behaves_like "fixed year"
      it_behaves_like "fuzzy month"
      it{ expect(fdate.day).to be_nil }
    end

    context "日付コード'_1955-**-**_'相当のパラメータでインスタンス化した場合" do
      let(:fdate){ calendar.new(year: 1955, month: nil, day: nil) }

      let(:expected_year_range){ 1955..1955 }
      let(:expected_month_range){ 1..12 }

      it_behaves_like "fixed year"
      it_behaves_like "nildate month"
      it{ expect(fdate.day).to be_nil }
    end

    context "日付コード'_195*-**-**_'相当のパラメータでインスタンス化した場合" do
      let(:fdate){ calendar.new(year: {num: 1950, accuracy: 1}, month: nil, day: nil) }

      let(:expected_year_range){ 1950..1959 }

      it_behaves_like "fuzzy year"
      it{ expect(fdate.month).to be_nil }
      it{ expect(fdate.day).to be_nil }
    end

    context "日付コード'_195*-10-23_'相当のパラメータでインスタンス化した場合" do
      let(:fdate){ calendar.new(year: {num: 1950, accuracy: 1}, month: 10, day: 23) }

      let(:expected_year_range){ 1950..1959 }

      it_behaves_like "fuzzy year"
      it{ expect(fdate.month).to be_nil }
      it{ expect(fdate.day).to be_nil }
    end

    context "日付コード'_195*-**-**_1-2'相当のパラメータでインスタンス化した場合" do
      let(:fdate){ calendar.new(year: {num: 1950, accuracy: 1}, month: nil, day: nil, about: '1-2') }

      let(:expected_year_range){ 1950..1954 }
      let(:about){ FDate::About.collection['1-2'] }

      it_behaves_like "fuzzy year"
      it{ expect(fdate.month).to be_nil }
      it{ expect(fdate.day).to be_nil }
    end
  end

  describe "#eql?" do
    subject{ calendar.new(year: 1955, month: 10, day: 23, about: "1-2") }

    context "暦と各パートとAboutが同じ場合" do
      let(:other){ calendar.new(year: 1955, month: 10, day: 23, about: "1-2") }
      it{ is_expected.to eql(other) }
    end

    context "Aboutが違う場合" do
      let(:other){ calendar.new(year: 1955, month: 10, day: 23, about: "2-2") }
      it{ is_expected.not_to eql(other) }
    end

    context "パートの一部が違う場合" do
      let(:other){ calendar.new(year: 1955, month: 11, day: 23, about: "1-2") }
      it{ is_expected.not_to eql(other) }
    end

    context "暦（クラス）が違う場合" do
      let(:other){ FDate::Calendar.collection["adjl"].new(year: 1955, month: 10, day: 23, about: "1-2") }
      it{ is_expected.not_to eql(other) }
    end
  end

  describe "#fixed?,#fuzzy?" do
    shared_examples_for "fixed date" do
      example do
        is_expected.to be_fixed
        is_expected.not_to be_fuzzy
      end
    end

    shared_examples_for "fuzzy date" do
      example do
        is_expected.not_to be_fixed
        is_expected.to be_fuzzy
      end
    end


    context "日付コード'_1234-12-25_'相当のパラメータでインスタンス化した場合" do
      subject{ calendar.new(year: {num: 1234}, month: 12, day: 25) }
      it_behaves_like "fixed date"
    end

    context "日付コード'_1234-12-2*_'相当のパラメータでインスタンス化した場合" do
      subject{ calendar.new(year: {num: 1234}, month: 12, day: {num: 20, accuracy: 1}) }
      it_behaves_like "fuzzy date"
    end

    context "日付コード'_1234-1*-**_'相当のパラメータでインスタンス化した場合" do
      subject{ calendar.new(year: {num: 1234}, month: {num: 10, accuracy: 1}) }
      it_behaves_like "fuzzy date"
    end

    context "日付コード'_123*-**-**_'相当のパラメータでインスタンス化した場合" do
      subject{ calendar.new(year: {num: 1230, accuracy: 1}) }
      it_behaves_like "fuzzy date"
    end
  end

  describe "#convert_to" do
    let(:result){ fdate.convert_to("adjl") }

    context "fixedの場合" do
      let(:fdate){ adgr.new(year: 1955, month: 10, day: 23) }

      it "は、指定した暦に変換すること" do
        expect(result.calendar).to equal(adjl)
        expect(result.calendar).not_to equal(fdate.calendar)
      end

      it "は、同じ日を返すこと" do
        expect(result.jd).to eq(fdate.jd)
      end

      it{ expect(result).not_to eql(fdate) }
    end

    context "fuzzyの場合" do
      let(:fdate){ adgr.new(year: 1955, month: 10, day: {num: 20, accuracy: 1}) }

      it "は、指定した暦の期間オブジェクトに変換すること" do
        expect(result).to be_a(FDate::Term)

        %i(first last).each do |direction|
          calendar = result.send(direction).calendar

          expect(calendar).to equal(adjl)
          expect(calendar).not_to equal(fdate.calendar)
        end
      end

      it "は、同じ範囲の期間を返すこと" do
        expect(result.jd_range).to eq(fdate.jd_range)
      end

      it{ expect(result).not_to eql(fdate) }
    end
  end
end
