require 'spec_helper.rb'

describe FDate::Parseable do
  let(:owner){ double("Owner").extend described_class }

  before do
    allow(owner).to receive(:_from_i){|int| {num: int + 5} }
    allow(owner).to receive(:_local_parse).and_return({num: 87654321})
  end

  shared_examples_for "raise ParseError" do
    it "は、パースエラーを発生させること" do
      expect{ subject }.to raise_error(FDate::ParseError, "It's wrong grammar. #{code.inspect}")
    end
  end

  shared_examples_for "raise error" do
    context "文字列以外を渡した場合" do
      let(:code){ 1234 }

      it "は、何らかの例外を発生させること" do
        expect{ subject }.to raise_error(StandardError)
      end
    end
  end


  describe "::global_parse" do
    subject do
      described_class.global_parse(code)
    end

    context "相対値コードを渡した場合" do
      let(:code){ "25" }
      it_behaves_like "raise ParseError"
    end

    context "絶対値コードを渡した場合" do
      let(:code){ "(12345678)" }

      it "は、コードから得られた数値を返すこと" do
        is_expected.to eq(12345678)
      end
    end

    context "不正なコードを渡した場合" do
      let(:code){ "1a345xf00" }
      it_behaves_like "raise ParseError"
    end
  end

  describe "#_global_parse" do
    subject do
      owner._global_parse(code)
    end

    context "相対値コードを渡した場合" do
      let(:code){ "25" }
      it_behaves_like "raise ParseError"
    end

    context "絶対値コードを渡した場合" do
      let(:code){ "(12345678)" }

      it "は、インスタンス化用ハッシュを返すこと" do
        is_expected.to eq({num: 12345683})
      end
    end

    context "不正なコードを渡した場合" do
      let(:code){ "1a345xf00" }
      it_behaves_like "raise ParseError"
    end

    it_behaves_like "raise error"
  end

  describe "#_parse" do
    subject do
      owner._parse(code)
    end

    context "相対値コードを渡した場合" do
      let(:code){ "25" }

      it "は、インスタンス化用ハッシュを返すこと" do
        is_expected.to eq({num: 87654321})
      end
    end

    context "絶対値コードを渡した場合" do
      let(:code){ "(12345678)" }

      it "は、インスタンス化用ハッシュを返すこと" do
        is_expected.to eq({num: 12345683})
      end
    end

    it_behaves_like "raise error"
  end
end
