require 'spec_helper.rb'

describe FDate::Part::BuildMethods do
  let(:owner){ double("Owner").extend described_class }

  describe "#_build" do
    context "インスタンス化できないオブジェクトを渡した場合" do
      it "は、型エラーを発生させること" do
        expect{ owner.build(Time.now) }.to raise_error(TypeError, "can not convert Time into Hash.")
      end
    end
  end
end
