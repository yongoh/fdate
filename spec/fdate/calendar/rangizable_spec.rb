require 'spec_helper.rb'

describe FDate::Calendar::Rangizable do
  let(:adjl){ FDate::Calendar.collection["adjl"] }
  let(:adgr){ FDate::Calendar.collection["adgr"] }

  shared_examples_for "range methods" do
    ja_directions = {
      "first" => "最初",
      "last" => "最後",
      "begin" => "最初",
      "end" => "最後",
      "min" => "最初",
      "max" => "最後",
    }

    describe "#jd_range" do
      it "は、ユリウス通日の範囲を返すこと" do
        expect(fdate.jd_range).to eq(expected_jd_range)
      end
    end

    %w(first last begin end min max).each do |direction|
      describe "#_#{direction}" do
        it "は、期間のうち#{ja_directions[direction]}の日付のインスタンス化用Hashを返すこと" do
          expect(fdate.send("_#{direction}")).to eq(send("expected__#{direction}"))
        end
      end if direction.match(/first|last/)

      describe "##{direction}" do
        let(:drc){ fdate.send(direction) }

        it "は、期間のうち#{ja_directions[direction]}の日付オブジェクトを返すこと" do
          expect(drc.jd).to eq(expected_jd_range.send(direction))
          expect(drc).to be_instance_of(adjl)
          expect(drc).to be_fixed
        end
      end

      describe "##{direction}_jd" do
        it "は、期間のうち#{ja_directions[direction]}のユリウス通日を返すこと" do
          expect(fdate.send("#{direction}_jd")).to eq(expected_jd_range.send(direction))
        end
      end
    end
  end


  context "日付コード'adjl_1234-12-25_'相当のインスタンスの場合" do
    let(:fdate){ adjl.new({year: {num: 1234}, month: 12, day: 25}) }

    let(:expected__first){ {year: 5947, month: 12, day: 25} }
    let(:expected__last){ {year: 5947, month: 12, day: 25} }
    let(:expected_jd_range){ 2172135..2172135 }

    it_behaves_like "range methods"
  end

  context "日付コード'adjl_1234-12-2*_'相当のインスタンスの場合" do
    let(:fdate){ adjl.new({year: {num: 1234}, month: 12, day: {num: 25, accuracy: 1}}) }

    let(:expected__first){ {year: 5947, month: 12, day: 20} }
    let(:expected__last){ {year: 5947, month: 12, day: 29} }
    let(:expected_jd_range){ 2172130..2172139 }

    it_behaves_like "range methods"
  end

  context "日付コード'adjl_1234-12-**_'相当のインスタンスの場合" do
    let(:fdate){ adjl.new({year: {num: 1234}, month: 12}) }

    let(:expected__first){ {year: 5947, month: 12, day: 1} }
    let(:expected__last){ {year: 5947, month: 12, day: 31} }
    let(:expected_jd_range){ 2172111..2172141 }

    it_behaves_like "range methods"
  end

  context "日付コード'adjl_1234-1*-**_'相当のインスタンスの場合" do
    let(:fdate){ adjl.new({year: {num: 1234}, month: {num: 10, accuracy: 1}}) }

    let(:expected__first){ {year: 5947, month: 10, day: 1} }
    let(:expected__last){ {year: 5947, month: 12, day: 31} }
    let(:expected_jd_range){ 2172050..2172141 }

    it_behaves_like "range methods"
  end

  context "日付コード'adjl_1234-**-**_'相当のインスタンスの場合" do
    let(:fdate){ adjl.new({year: {num: 1234}}) }

    let(:expected__first){ {year: 5947, month: 1, day: 1} }
    let(:expected__last){ {year: 5947, month: 12, day: 31} }
    let(:expected_jd_range){ 2171777..2172141 }

    it_behaves_like "range methods"
  end

  context "日付コード'adjl_123*-**-**_'相当のインスタンスの場合" do
    let(:fdate){ adjl.new({year: {num: 1230, accuracy: 1}}) }

    let(:expected__first){ {year: 5943, month: 1, day: 1} }
    let(:expected__last){ {year: 5952, month: 12, day: 31} }
    let(:expected_jd_range){ 2170316..2173967 }

    it_behaves_like "range methods"
  end

  context "日付コード'adjl_12**-**-**_'相当のインスタンスの場合" do
    let(:fdate){ adjl.new({year: {num: 1200, accuracy: 2}}) }

    let(:expected__first){ {year: 5913, month: 1, day: 1} }
    let(:expected__last){ {year: 6012, month: 12, day: 31} }
    let(:expected_jd_range){ 2159358..2195882 }

    it_behaves_like "range methods"
  end



  describe "#jd_range" do
    context "fixedの場合" do
      let(:fdate){ adgr.new(year: {num: 1955}, month: 10, day: 23) }

      it "は、ユリウス通日の範囲を返すこと" do
        expect(fdate.jd_range).to eq(2435404..2435404)
      end
    end

    context "fuzzyの場合" do
      let(:fdate){ adgr.new(year: {num: 1955}, month: 10, day: {num: 20, accuracy: 1}) }

      it "は、ユリウス通日の範囲を返すこと" do
        expect(fdate.jd_range).to eq(2435401..2435410)
      end
    end
  end

  describe "#jd" do
    context "fixedの場合" do
      let(:fdate){ adgr.new(year: {num: 1955}, month: 10, day: 23) }

      it "は、この日付のユリウス通日を返すこと" do
        expect(fdate.jd).to eq(2435404)
      end
    end

    context "fuzzyの場合" do
      let(:fdate){ adgr.new(year: {num: 1955}, month: 10, day: {num: 20, accuracy: 1}) }

      it "は、ユリウス通日の範囲を返すこと" do
        expect(fdate.jd).to eq(2435401..2435410)
      end
    end
  end

  describe "#center_jd" do
    context "fixedの場合" do
      let(:fdate){ adgr.new(year: {num: 1955}, month: 10, day: 23) }

      it "は、この日付のユリウス通日を返すこと" do
        expect(fdate.center_jd).to eq(2435404)
      end
    end

    context "fuzzyの場合" do
      let(:fdate){ adgr.new(year: {num: 1955}, month: 10, day: {num: 20, accuracy: 1}) }

      it "は、日付範囲のうち真ん中の日のユリウス通日を返すこと" do
        expect(fdate.center_jd).to eq(2435405)
      end
    end
  end
end
