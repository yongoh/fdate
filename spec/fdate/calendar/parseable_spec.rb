require 'spec_helper.rb'

describe FDate::Calendar::Parseable do
  let(:owner){ double("Owner").extend described_class }

  before do
    allow(owner).to receive(:parts).and_return({year: nil, month: nil, day: nil})
    allow(owner).to receive(:_from_i){|int| {year: int / 10000, month: (int - 12340000) / 100, day: int - 12345600} }
  end


  describe "#parseable?" do
    context "ローカルコードを渡した場合" do
      it{ expect(owner.parseable?("1234-1*-**")).to be_truthy }
    end

    context "ユリウス通日コードを渡した場合" do
      it{ expect(owner.parseable?("(12345678)")).to be_truthy }
    end

    context "不正なコードを渡した場合" do
      it{ expect(owner.parseable?("1a345xf00")).to be_truthy }
    end
  end

  describe "#_global_parse" do
    context "ローカルコードを渡した場合" do
      it "は、パースエラーを発生させること" do
        expect{
          owner._global_parse("1234-1*-**")
        }.to raise_error(FDate::ParseError, "It's wrong grammar. \"1234-1*-**\"")
      end
    end

    context "ユリウス通日コードを渡した場合" do
      it "は、インスタンス化用ハッシュを返すこと" do
        expect(owner._global_parse("(12345678)")).to eq({year: 1234, month: 56, day: 78})
      end
    end

    context "不正なコードを渡した場合" do
      it "は、パースエラーを発生させること" do
        expect{
          owner._global_parse("1a345xf00")
        }.to raise_error(FDate::ParseError, "It's wrong grammar. \"1a345xf00\"")
      end
    end

    context "文字列以外を渡した場合" do
      it "は、何らかの例外を発生させること" do
        expect{ owner._global_parse(1234) }.to raise_error(StandardError)
      end
    end
  end

  describe "#_local_parse" do
    context "パート数が`#parts.size`と同じ日付コードを渡した場合" do
      it "は、インスタンス化用ハッシュを返すこと" do
        expect(owner._local_parse("1234-12-25")).to eq({year: "1234", month: "12", day: "25"})
      end
    end

    context "パート数が`#parts.size`より少ない日付コードを渡した場合" do
      it "は、一部パートがnilのインスタンス化用ハッシュを返すこと" do
        expect(owner._local_parse("1234-12")).to eq({year: "1234", month: "12", day: nil})
      end
    end

    context "パート数が`#parts.size`より多い日付コードを渡した場合" do
      it "は、不正な日付パートコードを含むインスタンス化用ハッシュを返すこと" do
        expect(owner._local_parse("1234-12-25-31")).to eq({year: "1234", month: "12", day: "25-31"})
      end
    end

    context "ユリウス通日コードを渡した場合" do
      it "は、コードをトップの日付パートコードとみなしたインスタンス化用ハッシュを返すこと" do
        expect(owner._local_parse("(12345678)")).to eq({year: "(12345678)", month: nil, day: nil})
      end
    end

    context "ユリウス通日コード（負数）を渡した場合" do
      it "は、不正なインスタンス化用ハッシュを返すこと" do
        expect(owner._local_parse("(-12345678)")).to eq({year: "(", month: "12345678)", day: nil})
      end
    end

    context "不正なコードを渡した場合" do
      it "は、不正なインスタンス化用ハッシュを返すこと" do
        expect(owner._local_parse("1a345xf00")).to eq({year: "1a345xf00", month: nil, day: nil})
      end
    end

    context "文字列以外を渡した場合" do
      it "は、何らかの例外を発生させること" do
        expect{ owner._local_parse(1234) }.to raise_error(StandardError)
      end
    end
  end

  describe "#_parse" do
    context "ユリウス通日コードを渡した場合" do
      it "は、インスタンス化用ハッシュを返すこと" do
        expect(owner._parse("(12345678)")).to eq({year: 1234, month: 56, day: 78})
      end
    end

    context "ローカルコードを渡した場合" do
      it "は、インスタンス化用ハッシュを返すこと" do
        expect(owner._parse("1234-1*-**")).to eq({year: "1234", month: "1*", day: "**"})
      end
    end

    context "不正なコードを渡した場合" do
      it "は、不正なインスタンス化用ハッシュを返すこと" do
        expect(owner._parse("1a345xf00")).to eq({year: "1a345xf00", month: nil, day: nil})
      end
    end

    context "文字列以外を渡した場合" do
      it "は、何らかの例外を発生させること" do
        expect{ owner._parse(1234) }.to raise_error(StandardError)
      end
    end
  end

  describe "#full_parse" do
    let(:adgr){ FDate::Calendar.collection["adgr"] }

    before do
      allow(owner).to receive(:collection).and_return(FDate::Calendar.collection)
    end

    shared_examples_for "raise ParseError" do
      it "は、パースエラーを発生させること" do
        expect{ owner.full_parse(code) }.to raise_error(FDate::ParseError, "It's wrong grammar. #{code.inspect}")
      end
    end


    context "暦キーを含むFDateコードを渡した場合" do
      it "は、日付オブジェクトを生成すること" do
        expect(owner.full_parse("adgr_1955-11-05_2-2")).to eql(adgr.new(year: {num: 1955}, month: 11, day: 5, about: "2-2"))
      end
    end

    context "暦体系キーを含むFDateコードを渡した場合" do
      let(:result){ owner.full_parse("Italy_1955-11-05_2-2") }

      it "は、日付オブジェクトをラップした暦体系オブジェクト（「頃」は消えてしまう）を生成すること" do
        expect(result).to eql(adgr.new(year: {num: 1955}, month: 11, day: 5, about: "j"))
        expect(result.calendar).to equal(FDate::Calendar.collection["Italy"])
        expect(result.object.calendar).to equal(adgr)
      end
    end

    context "暦体系付きの暦キーを含むFDateコードを渡した場合" do
      let(:result){ owner.full_parse("adgr(Italy)_2018-07-30_2-2") }

      it "は、日付オブジェクトをラップした暦体系オブジェクトを生成すること" do
        expect(result).to eql(adgr.new(year: {num: 2018}, month: 7, day: 30, about: "2-2"))
        expect(result.calendar).to equal(FDate::Calendar.collection["Italy"])
        expect(result.object.calendar).to equal(adgr)
      end
    end

    context "暦キーと「頃」キーを省略したFDateコードを渡した場合" do
      it "は、デフォルト暦の日付オブジェクトを生成すること" do
        expect(owner.full_parse("_195*_")).to eql(adgr.new(year: {num: 1950, accuracy: 1}))
      end
    end

    context "FDateコード（日付部分はユリウス通日）を渡した場合" do
      it "は、日付オブジェクトを生成すること" do
        expect(owner.full_parse("adgr_(2435417)_2-2")).to eql(adgr.new(year: {num: 1955}, month: 11, day: 5, about: "2-2"))
      end
    end

    context "不正なコード（期間コード）を渡した場合" do
      it "は、キーエラーを発生させること" do
        expect{
          owner.full_parse("adgr_1925-12-25_~adgr_1989-01-07_")
        }.to raise_error(LoadError, "cannot found file by such key -- ~adgr_1989-01-07_")
      end
    end

    context "不正なコード（無限日付コード）を渡した場合" do
      let(:code){ "(+Infinity)" }
      it_behaves_like "raise ParseError"
    end

    context "不正なコード（'_'の数が足りないコード）を渡した場合" do
      let(:code){ "_1955-10-23" }
      it_behaves_like "raise ParseError"
    end

    context "不正なコード（'_'の数が多いコード）を渡した場合" do
      it "は、パースエラーを発生させること" do
        expect{ owner.full_parse("__1955-10-23_") }.to raise_error(FDate::ParseError, "It's wrong grammar. \"_1955\"")
      end
    end

    context "不正なコードを渡した場合" do
      let(:code){ "1a345xf00" }
      it_behaves_like "raise ParseError"
    end
  end

  describe "#parse_calendar_key" do
    before do
      allow(owner).to receive(:collection).and_return(FDate::Calendar.collection)
    end

    context "暦キーを渡した場合" do
      it "は、暦オブジェクトとnilを返すこと" do
        expect(owner.parse_calendar_key("adgr")).to match [
          equal(FDate::Calendar.collection["adgr"]),
          be_nil,
        ]
      end
    end

    context "暦体系キーを渡した場合" do
      it "は、nilと暦体系オブジェクトを返すこと" do
        expect(owner.parse_calendar_key("Italy")).to match [
          be_nil,
          equal(FDate::Calendar.collection["Italy"]),
        ]
      end
    end

    context "暦体系付きの暦キーを渡した場合" do
      it "は、nilと暦体系オブジェクトを返すこと" do
        expect(owner.parse_calendar_key("adgr(Italy)")).to match [
          equal(FDate::Calendar.collection["adgr"]),
          equal(FDate::Calendar.collection["Italy"]),
        ]
      end
    end

    context "空文字を渡した場合" do
      subject{ owner.parse_calendar_key("") }

      context "デフォルト暦が暦体系ではない場合" do
        before do
          FDate::Calendar.collection.default = "bzjl"
        end

        it "は、デフォルト暦（暦）とnilを返すこと" do
          is_expected.to match [
            equal(FDate::Calendar.collection["bzjl"]),
            be_nil,
          ]
        end
      end

      context "デフォルト暦が暦体系の場合" do
        before do
          FDate::Calendar.collection.default = "Italy"
        end

        it "は、nilとデフォルト暦（暦体系）を返すこと" do
          is_expected.to match [
            be_nil,
            equal(FDate::Calendar.collection["Italy"]),
          ]
        end
      end
    end
  end

  describe "#parse_about_key" do
    context "「頃」キーを渡した場合" do
      it "は、渡したキーを持つ「頃」を返すこと" do
        expect(owner.parse_about_key("1-2")).to equal(FDate::About.collection["1-2"])
      end
    end

    context "空文字を渡した場合" do
      it "は、デフォルト「頃」を返すこと" do
        expect(owner.parse_about_key("")).to equal(FDate::About.collection.default)
      end
    end
  end
end
