require 'spec_helper.rb'

describe FDate::Calendar::Encodable do
  let(:calendar){ double("Calendar", key: :key) }
  let(:about){ double("About", key: "j") }

  describe "#global_code" do
    subject{ fdate.global_code }

    let(:fdate){ double("FDate", calendar: calendar, global_range: global_range, about: about).extend described_class }

    before do
      allow(fdate).to receive(:fixed?).and_return(global_range.first == global_range.last)
    end


    context "fixedの場合" do
      let(:global_range){ 2435404..2435404 }

      it "は、ユリウス通日コードを返すこと" do
        is_expected.to eq("(2435404)")
      end
    end

    context "fuzzyの場合" do
      let(:global_range){ 2435109..2435473 }

      it "は、ユリウス通日コードを返すこと" do
        is_expected.to eq("(2435109..2435473)")
      end
    end
  end

  describe "#local_code" do
    subject{ fdate.local_code }

    let(:fdate){ double("FDate", calendar: calendar, parts: parts, about: about).extend described_class }
    let(:year){ double("Year", local_code: "1955") }
    let(:month){ double("Month", local_code: "10") }
    let(:day){ double("Day", local_code: "23") }


    context "1955-10-23 の場合" do
      let(:parts){ {year: year, month: month, day: day} }

      it "は、日付コードを返すこと" do
        is_expected.to eq("key_1955-10-23_j")
      end
    end

    context "1955-10-** の場合" do
      let(:parts){ {year: year, month: month, day: nil} }

      it "は、日付コードを返すこと" do
        is_expected.to eq("key_1955-10-*_j")
      end
    end

    context "1955-**-** の場合" do
      let(:parts){ {year: year, month: nil, day: nil} }

      it "は、日付コードを返すこと" do
        is_expected.to eq("key_1955-*-*_j")
      end
    end
  end
end
