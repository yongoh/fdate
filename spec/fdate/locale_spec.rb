require 'spec_helper.rb'

describe FDate::Locale do
  let(:locale){
    described_class.new("key", {
      klass => {"hoge" => "ほげ"},
      object => {"piyo" => "ぴよ"},
    })
  }

  let(:super_class){
    Class.new do
      def self.name
        "SuperClass"
      end
    end
  }

  let(:mod){
    Module.new do
      def self.name
        "IncludedModule"
      end
    end
  }

  let(:klass){
    m = mod
    Class.new(super_class) do
      include m

      def self.name
        "Klass"
      end

      def self.name_with_key
        "Klass[key]"
      end

      def class_name_with_key
        "FDate::About[1-2]"
      end
    end
  }

  let(:object){ klass.new }
  let(:translations){ double("TranslationHash") }


  %i(dup clone).each do |method_name|
    describe "##{method_name}" do
      let(:result){ locale.send(method_name) }

      it "は、自身をコピーすること" do
        expect(result).to be_a(described_class)
        expect(result).not_to equal(locale)
      end

      it "は、`@data`もコピーすること" do
        expect(result.data).to eql(locale.data)
        expect(result.data).not_to equal(locale.data)
      end
    end
  end

  describe "#taint" do
    it "は、`@data`と訳文ハッシュも汚染状態にすること" do
      locale.taint

      expect(locale).to be_tainted
      expect(locale.data).to be_tainted
      expect(locale.data.values).to all(be_tainted)
    end
  end

  describe "#freeze" do
    it "は、`@data`と訳文ハッシュも凍結状態にすること" do
      locale.freeze

      expect(locale).to be_frozen
      expect(locale.data).to be_frozen
      expect(locale.data.keys).to all(be_frozen)
      expect(locale.data.values).to all(be_frozen)
    end
  end

  describe "#[]" do
    context "第1引数にオブジェクトを渡した場合" do
      shared_examples_for "return translations" do
        it "は、渡したオブジェクトに関係する訳文ハッシュを返すこと" do
          expect(locale[object]).to equal(translations)
        end
      end


      context "渡したオブジェクトをキーに持つロケールの場合" do
        let(:locale){ described_class.new("key", {object => translations}) }
        it_behaves_like "return translations"
      end

      context "'#<クラス名[インスタンスのキー]>'となる文字列をキーに持つロケールの場合" do
        let(:locale){ described_class.new("key", {"#<FDate::About[1-2]>" => translations}) }
        it_behaves_like "return translations"
      end

      context "`Klass[key]`のインスタンスであることを表す文字列をキーに持つロケールの場合" do
        let(:locale){ described_class.new("key", {"#<Klass[key]>" => translations}) }
        it_behaves_like "return translations"
      end

      context "`Klass`のインスタンスであることを表す文字列をキーに持つロケールの場合" do
        let(:locale){ described_class.new("key", {"#<Klass>" => translations}) }
        it_behaves_like "return translations"
      end

      context "`IncludedModule`をインクルードしたクラスのインスタンスであることを表す文字列をキーに持つロケールの場合" do
        let(:locale){ described_class.new("key", {"#<IncludedModule>" => translations}) }
        it_behaves_like "return translations"
      end

      context "`SuperClass`のサブクラスのインスタンスであることを表す文字列をキーに持つロケールの場合" do
        let(:locale){ described_class.new("key", {"#<SuperClass>" => translations}) }
        it_behaves_like "return translations"
      end

      context "`BasicObject`のサブクラスのインスタンスであることを表す文字列をキーに持つロケールの場合" do
        let(:locale){ described_class.new("key", {"#<BasicObject>" => translations}) }
        it_behaves_like "return translations"
      end

      context "対応するキーが無い場合" do
        let(:locale){ described_class.new("key", {}) }

        it "は、キーエラーを発生させること" do
          expect{ locale[object] }.to raise_error(KeyError, "key not found: #{object.inspect}")
        end
      end
    end

    context "第2引数に訳文キーを渡した場合" do
      let(:trs_key){ "trs_key" }
      let(:translation){ "やくぶん" }

      shared_examples_for "return translation" do
        it "は、訳文を返すこと" do
          expect(locale[object, trs_key]).to equal(translation)
        end
      end


      context "オブジェクトの訳文ハッシュに訳文キーがある場合" do
        let(:locale){
          described_class.new("key", {
            object => {trs_key => translation},
          })
        }

        it_behaves_like "return translation"
      end

      context "オブジェクトの先祖クラスの名前の訳文ハッシュに訳文キーがある場合" do
        let(:locale){
          described_class.new("key", {
            object => {},
            "#<SuperClass>" => {trs_key => translation},
          })
        }

        it_behaves_like "return translation"
      end

      context "遡っても訳文キーが無い場合" do
        let(:locale){ described_class.new("key", Time => {trs_key => translation}) }

        it "は、キーエラーを発生させること" do
          expect{ locale[object, trs_key] }.to raise_error(KeyError, "key not found: #{object.inspect}, #{trs_key.inspect}")
        end
      end
    end
  end

  describe "#[]=" do
    let(:locale){
      described_class.new("key", {
        klass => {"hoge" => "ほげ"},
      })
    }

    it "は、追加した訳文を返すこと" do
      expect(locale[object, "trs_key"] = "やくぶん").to eq("やくぶん")
    end

    context "第1引数のオブジェクトキーが存在する場合" do
      let(:locale){
        described_class.new("key", {
          klass => {"hoge" => "ほげ"},
          object => {"piyo" => "ぴよ"},
        })
      }

      before do
        locale[object, "trs_key"] = "やくぶん"
      end

      it "は、オブジェクトの訳文ハッシュに訳文を追加すること" do
        expect(locale.data).to eq({
          klass => {"hoge" => "ほげ"},
          object => {
            "piyo" => "ぴよ",
            "trs_key" => "やくぶん"
          },
        })
      end
    end

    context "第1引数のオブジェクトキーが存在しない場合" do
      before do
        locale[object, "trs_key"] = "やくぶん"
      end

      it "は、オブジェクトの訳文ハッシュを追加すること" do
        expect(locale.data).to eq({
          klass => {"hoge" => "ほげ"},
          object => {
            "trs_key" => "やくぶん"
          },
        })
      end
    end

    context "第2引数にシンボルを渡した場合" do
      before do
        locale[object, :trs_key] = "やくぶん"
      end

      it "は、文字列化した訳文キーで訳文を追加すること" do
        expect(locale.data).to eq({
          klass => {"hoge" => "ほげ"},
          object => {
            "trs_key" => "やくぶん"
          },
        })
      end
    end

    context "第1引数とイコールの右辺しか渡さない場合" do
      it "は、引数エラーを発生させること" do
        expect{ locale[object] = "やくぶん" }.to raise_error(ArgumentError, "wrong number of arguments (2 for 3)")
      end
    end

    context "右辺にハッシュを渡した場合" do
      context "キー'names'を持つハッシュの場合" do
        before do
          locale[object, "trs_key"] = {
            "names" => {
              0 => "あああ",
              1 => "いいい",
            },
            "key_proc" => :hoge,
          }
        end

        let(:object){ double("Object", hoge: 1) }

        it "は、名前セットオブジェクトに変換して格納すること" do
          name_set = locale[object, "trs_key"]

          expect(name_set).to be_a(FDate::NameSet)
          expect(name_set.names).to eq({
            0 => "あああ",
            1 => "いいい",
          })
          expect(name_set[object]).to eq("いいい")
        end
      end
    end
  end

  describe "マージ系メソッド群" do
    let(:locale){
      described_class.new("key", {
        "SuperClass" => {"foobar" => "ふーばー"},
        object => {"shobon" => "(´･ω･｀)"},
        klass => {"hoge" => "ほげ"},
      })
    }

    let(:other_hash){
      {
        klass => {
          "hoge" => "HOGE",
          "piyo" => "ぴよ",
        },
        object => {"trs_key" => "やくぶん"},
      }
    }

    let(:result_hash){
      {
        klass => {
          "hoge" => "HOGE",
          "piyo" => "ぴよ",
        },
        object => {
          "trs_key" => "やくぶん",
          "shobon" => "(´･ω･｀)",
        },
        "SuperClass" => {
          "foobar" => "ふーばー",
        },
      }
    }

    shared_examples_for "merge!" do
      describe "#merge!" do
        it "は、マージ後の自身を返すこと" do
          expect(locale.merge!(other)).to equal(locale)
        end

        it "は、引数を自身にマージすること" do
          locale.merge!(other)

          expect(locale.data).to eq(result_hash)
        end
      end
    end

    shared_examples_for "merge" do
      describe "#merge" do
        it "は、新しいロケールオブジェクトを返すこと" do
          result = locale.merge(other)

          expect(result).to be_a(described_class)
          expect(result).not_to equal(locale)
          expect(result).not_to equal(other)
        end

        it "は、自身と引数をマージしたロケールオブジェクトを返すこと" do
          result = locale.merge(other)

          expect(result.data).to eq(result_hash)
        end
      end
    end


    context "ロケールオブジェクトを渡した場合" do
      let(:other){ described_class.new("key", other_hash) }
      it_behaves_like "merge!"
      it_behaves_like "merge"
    end

    context "ハッシュを渡した場合" do
      let(:other){ other_hash }
      it_behaves_like "merge!"
      it_behaves_like "merge"
    end

    describe "#reverse_merge!" do
      it "は、引数より自身の値を優先してマージすること" do
        result = locale.reverse_merge!(other_hash)

        expect(result).to equal(locale)
        expect(locale.data).to eq({
          "SuperClass" => {"foobar" => "ふーばー"},
          object => {
            "shobon" => "(´･ω･｀)",
            "trs_key" => "やくぶん",
          },
          klass => {
            "hoge" => "ほげ",
            "piyo" => "ぴよ",
          },
        })
      end
    end
  end

  describe "::each_keys_by" do
    shared_examples_for "each_keys_by" do
      context "ブロックを渡した場合" do
        it "は、引数のキーとみなせるオブジェクトをブロック引数に優先順に渡すこと" do
          ary = []
          described_class.each_keys_by(arg){|obj_key| ary << obj_key }

          expect(ary).to match(expected)
        end
      end

      context "ブロックを渡さない場合" do
        it "は、引数のキーとみなせるオブジェクトを優先順に格納した配列を返すこと" do
          expect(described_class.each_keys_by(arg)).to match(expected)
        end
      end
    end


    context "インスタンスを渡した場合" do
      let(:arg){ object }

      let(:expected){
        [
          equal(object),
          eq("#<FDate::About[1-2]>"),
          eq("#<Klass[key]>"),
          eq("#<Klass>"),
          eq("#<IncludedModule>"),
          eq("#<SuperClass>"),
          eq("#<Object>"),
          eq("#<Kernel>"),
          eq("#<BasicObject>"),
        ]
      }

      it_behaves_like "each_keys_by"
    end

    context "クラスを渡した場合" do
      let(:arg){ klass }

      let(:expected){
        [
          equal(klass),
          eq("Klass[key]"),
          eq("Klass"),
          equal(mod),
          eq("IncludedModule"),
          equal(super_class),
          eq("SuperClass"),
          equal(Object),
          eq("Object"),
          equal(Kernel),
          eq("Kernel"),
          equal(BasicObject),
          eq("BasicObject"),
        ]
      }

      it_behaves_like "each_keys_by"
    end
  end
end
