require 'spec_helper.rb'

describe FDate::Term do
  # FDate(fixed)~FDate(fixed)
  let(:term_1){ described_class.new(first: fixed_first, last: fixed_last) }
  # FDate(fixed)~FDate(fuzzy)
  let(:term_2){ described_class.new(first: fixed_first, last: fuzzy_last) }
  # FDate(fixed)~Term(fuzzy)（丸めるバージョン）
  let(:term_3){
    described_class.new(
      first: fuzzy_first,
      last: described_class.new(
        first: calendar.new(year: {num: 1930, accuracy: 1}),
        last: fuzzy_last,
      )
    )
  }
  # FDate(fixed)~Infinity
  let(:term_4){ described_class.new(first: fixed_first) }

  let(:calendar){ FDate::Calendar.collection["adgr"] }

  let(:fixed_first){ calendar.new(year: {num: 1926}, month: 12, day: 25) }
  let(:fixed_last){ calendar.new(year: {num: 1989}, month: 1, day: 7) }
  let(:fuzzy_first){ calendar.new(year: {num: 1926}, month: 12, day: {num: 20, accuracy: 1}) }
  let(:fuzzy_last){ calendar.new(year: {num: 1980, accuracy: 1}) }


  describe "#fixed?,#fuzzy?" do
    context "`FDate(fixed)~FDate(fixed)`の場合" do
      example do
        expect(term_1).to be_fixed
        expect(term_1).not_to be_fuzzy
      end
    end

    context "`FDate(fixed)~FDate(fuzzy)`の場合" do
      example do
        expect(term_2).not_to be_fixed
        expect(term_2).to be_fuzzy
      end
    end

    context "`FDate(fixed)~Term(fuzzy)`の場合" do
      example do
        expect(term_3).not_to be_fixed
        expect(term_3).to be_fuzzy
      end
    end

    context "`FDate(fixed)~Infinity`の場合" do
      example do
        expect(term_4).not_to be_fixed
        expect(term_4).to be_fuzzy
      end
    end
  end

  describe "#jd_range" do
    context "`FDate(fixed)~FDate(fixed)`の場合" do
      subject{ term_1.jd_range }

      it "は、ユリウス通日の範囲を返すこと" do
        is_expected.to eq(2424875..2447534)
        is_expected.to eq((fixed_first.first_jd)..(fixed_last.last_jd))
      end
    end

    context "`FDate(fixed)~FDate(fuzzy)`の場合" do
      subject{ term_2.jd_range }

      it "は、ユリウス通日の範囲を返すこと" do
        is_expected.to eq(2424875..2447892)
        is_expected.to eq((fixed_first.first_jd)..(fuzzy_last.last_jd))
      end
    end

    context "`FDate(fixed)~Term(fuzzy)`の場合" do
      subject{ term_3.jd_range }

      it "は、ユリウス通日の範囲を返すこと" do
        is_expected.to eq(2424870..2447892)
        is_expected.to eq((fuzzy_first.first_jd)..(fuzzy_last.last_jd))
      end
    end

    context "`FDate(fixed)~Infinity`の場合" do
      subject{ term_4.jd_range }

      it "は、ユリウス通日の範囲を返すこと" do
        is_expected.to eq(2424875..Float::INFINITY)
        is_expected.to eq((fixed_first.first_jd)..Float::INFINITY)
      end
    end
  end

  describe "#center_jd" do
    context "`FDate(fixed)~FDate(fixed)`の場合" do
      it "は、ユリウス通日の範囲を返すこと" do
        expect(term_1.center_jd).to eq(2436204)
      end
    end

    context "`FDate(fixed)~FDate(fuzzy)`の場合" do
      it "は、ユリウス通日の範囲を返すこと" do
        expect(term_2.center_jd).to eq(2436383)
      end
    end

    context "`FDate(fixed)~Term(fuzzy)`の場合" do
      it "は、ユリウス通日の範囲を返すこと" do
        expect(term_3.center_jd).to eq(2436381)
      end
    end

    context "`FDate(fixed)~Infinity`の場合" do
      it "は、ユリウス通日の範囲を返すこと" do
        expect(term_4.center_jd).to eq(Float::INFINITY)
      end
    end
  end

  describe "direction系メソッド群" do
    describe "#last" do
      it "は、終端の日付オブジェクト（fuzzyの可能性あり）を返すこと" do
        expect(term_3.last).to eql(fuzzy_last)
      end
    end

    describe "#last_jd" do
      it "は、とりうる期間のうち最も最後のユリウス通日を返すこと" do
        expect(term_3.last_jd).to eq(2447892)
      end
    end

    describe "#fixed_last" do
      it "は、とりうる期間のうち最も最後の日付オブジェクト（必ずfixed）を返すこと" do
        expect(term_3.fixed_last).to eq(calendar.new(year: {num: 1989}, month: 12, day: 31))
      end
    end
  end

  describe "#global_code,#local_code" do
    context "`FDate(fixed)~FDate(fixed)`の場合" do
      it{ expect(term_1.global_code).to eq("(2424875..2447534)") }
      it{ expect(term_1.local_code).to eq("adgr_A=1926-12-25_j~adgr_A=1989-1-7_j") }
    end

    context "`FDate(fixed)~FDate(fuzzy)`の場合" do
      it{ expect(term_2.global_code).to eq("(2424875..2447892)") }
      it{ expect(term_2.local_code).to eq("adgr_A=1926-12-25_j~adgr_A=198*-*-*_j") }
    end

    context "`FDate(fixed)~Term(fuzzy)`の場合" do
      it{ expect(term_3.global_code).to eq("(2424870..2447892)") }
      it{ expect(term_3.local_code).to eq("adgr_A=1926-12-2*_j~adgr_A=198*-*-*_j") }
    end

    context "`FDate(fixed)~Infinity`の場合" do
      it{ expect(term_4.global_code).to eq("(2424875..Infinity)") }
      it{ expect(term_4.local_code).to eq("adgr_A=1926-12-25_j~(Infinity)") }
    end

    context "終端を含まない場合" do
      let(:term){ described_class.new(first: fixed_first, last: fixed_last, exclude_end: true) }
      it{ expect(term.global_code).to eq("(2424875...2447534)") }
      it{ expect(term.local_code).to eq("adgr_A=1926-12-25_j~adgr_A=1989-1-7_j") }
    end
  end

  describe "#eql?" do
    let(:other){ described_class.new(first: fixed_first, last: fixed_last) }

    context "真の場合" do
      it{ expect(term_1).to eql(other) }
    end

    context "偽の場合" do
      it{ expect(term_2).not_to eql(other) }
    end
  end

  describe "#to_dr" do
    let(:term_5){ described_class.new(first: fuzzy_first, last: fuzzy_last) }

    shared_examples_for "eq options" do
      it "は、オプションの値が同じであること" do
        expect(result.rng.exclude_end?).to eq(expected.rng.exclude_end?)
        expect(result.between?).to eq(expected.between?)
      end
    end


    context "`fixed: false`を渡した場合 (defalut)" do
      let(:result){ term_5.to_dr }
      let(:expected){ described_class.new(first: fuzzy_first, last: fuzzy_last) }

      it "は、firstとlastを同じくする期間オブジェクトを生成すること" do
        expect(result).to eql(expected)
        expect(result).not_to equal(term_5)

        expect(result).to be_fuzzy

        expect(result.first).to equal(expected.first)
        expect(result.last).to equal(expected.last)
      end

      it_behaves_like "eq options"
    end

    context "`fixed: true`を渡した場合" do
      let(:result){ term_5.to_dr(fixed: true) }
      let(:expected) do
        FDate::Term.new(
          first: calendar.new(year: {num: 1926}, month: 12, day: 20),
          last: calendar.new(year: {num: 1989}, month: 12, day: 31)
        )
      end

      it "は、fixedな期間オブジェクトを生成すること" do
        expect(result).to eql(expected)
        expect(result).to be_fixed
      end

      it_behaves_like "eq options"
    end
  end

  describe "Range再現メソッド群" do
    describe "オプション`exclude_end`について" do
      context "falseを渡した場合 (defalut)" do
        subject{ term_1 }

        describe "#exclude_end?" do
          it{ is_expected.not_to be_exclude_end }
        end

        describe "#jd_range" do
          subject{ term_2.jd_range }

          it "は、終端を含むユリウス通日範囲を返すこと" do
            is_expected.to eql(2424875..2447892)
            is_expected.not_to eql(2424875...2447892)
          end
        end

        describe "#<=>" do
          it "は、両端を含む結果を返すこと" do
            is_expected.to be == fixed_first
            is_expected.to be == fixed_first.jd
            is_expected.to be == fixed_last
            is_expected.not_to be < fixed_last
          end
        end
      end

      context "trueを渡した場合" do
        let(:term){ described_class.new(first: fixed_first, last: fixed_last, exclude_end: true) }
        subject{ term }

        describe "#exclude_end?" do
          it{ is_expected.to be_exclude_end }
        end

        describe "#jd_range" do
          subject{ term.jd_range }

          it "は、終端を含まないユリウス通日範囲を返すこと" do
            is_expected.to eql(2424875...2447534)
            is_expected.not_to eql(2424875..2447534)
          end
        end

        describe "#<=>" do
          it "は、終端を含まない結果を返すこと" do
            is_expected.to be == fixed_first
            is_expected.to be == fixed_first.jd
            is_expected.to be < fixed_last
            is_expected.to be < fixed_last.jd
          end
        end
      end
    end
  end
end
