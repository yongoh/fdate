require 'spec_helper.rb'

describe FDate::Period::Set do
  let(:adjl){ FDate::Calendar.collection["adjl"] }

  describe "#[]" do
    let(:period_set){ described_class.new(periods) }

    let(:periods){
      [
        double("Period", key: "あああ", term: FDate::Term.new(first: adjl.new(year: {num: 1324}), last: adjl.new(year: {num: 1332}))),
        double("Period", key: "いいい", term: FDate::Term.new(first: adjl.new(year: {num: 1330}), last: adjl.new(year: {num: 1336}))),
        double("Period", key: "ううう", term: FDate::Term.new(first: adjl.new(year: {num: 1337}), last: adjl.new(year: {num: 1340}))),
        double("Period", key: "いいい", term: FDate::Term.new(first: adjl.new(year: {num: 1341}), last: adjl.new(year: {num: 1345}))),
      ]
    }

    context "キー（文字列）を渡した場合" do
      context "渡したキーを持つ時代がある場合" do
        let(:key){ "いいい" }

        it "は、その時代を全て返すこと" do
          expect(period_set[key]).to eq([periods[1], periods[3]]).and(be_all{|p| p.key == key })
        end
      end

      context "渡したキーを持つ時代がない場合" do
        it{ expect(period_set["ほげ"]).to eq([]) }
      end
    end

    context "ユリウス通日（数値）を渡した場合" do
      context "渡したユリウス通日を期間に含む時代がある場合" do
        let(:jd){ 2207206 }

        it "は、その時代を全て返すこと" do
          expect(period_set[jd]).to eq([periods[0], periods[1]]).and(be_all{|p| p.term.cover?(jd) })
        end
      end

      context "渡したユリウス通日を期間に含む時代が無い場合" do
        it{ expect(period_set[2204284]).to eq([]) }
      end
    end

    context "ユリウス通日の範囲を渡した場合" do
      context "渡したユリウス通日の範囲を期間に含む時代がある場合" do
        let(:jd_range){ 2204524..2207206 }

        it "は、その時代を全て返すこと" do
          expect(period_set[jd_range]).to eq([periods[0], periods[1]]).and(be_all{|p| p.term.cover?(jd_range) })
        end
      end

      context "渡したユリウス通日の範囲を期間に含む時代がない場合" do
        it{ expect(period_set[2204284..2204524]).to eq([]) }
      end
    end

    context "日付オブジェクトを渡した場合" do
      context "渡した日付を期間に含む時代がある場合" do
        let(:fdate){ adjl.new(year: {num: 1331}, month: 1, day: 1) }

        it "は、その時代を全て返すこと" do
          expect(period_set[fdate]).to eq([periods[0], periods[1]]).and(be_all{|p| p.term.cover?(fdate) })
        end
      end

      context "渡した日付を期間に含む時代がない場合" do
        let(:fdate){ adjl.new(year: {num: 1320}, month: 1, day: 1) }
        it{ expect(period_set[fdate]).to eq([]) }
      end
    end

    context "期間オブジェクトを渡した場合" do
      context "渡した期間を期間に含む時代がある場合" do
        let(:term){
          FDate::Term.new(
            first: adjl.new(year: {num: 1323}, month: 1, day: 1),
            last: adjl.new(year: {num: 1330}, month: 1, day: 1),
          )
        }

        it "は、その時代を全て返すこと" do
          expect(period_set[term]).to eq([periods[0], periods[1]]).and(be_all{|p| p.term.cover?(term) })
        end
      end

      context "渡した期間を期間に含む時代がない場合" do
        let(:term){
          FDate::Term.new(
            first: adjl.new(year: {num: 1310}, month: 1, day: 1),
            last: adjl.new(year: {num: 1320}, month: 1, day: 1),
          )
        }

        it{ expect(period_set[term]).to eq([]) }
      end
    end
  end

  describe "::fill_last" do
    context "期間に変換しうるオブジェクトの配列を渡した場合" do
      context "期間ハッシュの配列を渡した場合" do
        subject do
          described_class.fill_last([
            {first: adjl.new(year: {num: 1324})},
            {first: adjl.new(year: {num: 1330}), last: adjl.new(year: {num: 1335}), exclude_end: false},
            {first: adjl.new(year: {num: 1337})},
          ])
        end

        it "は、`last`を補った期間オブジェクトの配列を返すこと" do
          is_expected.to eql([
            FDate::Term.new(first: adjl.new(year: {num: 1324}), last: adjl.new(year: {num: 1330}), exclude_end: true),
            FDate::Term.new(first: adjl.new(year: {num: 1330}), last: adjl.new(year: {num: 1335}), exclude_end: false),
            FDate::Term.new(first: adjl.new(year: {num: 1337}), last: +FDate::InfinityDate),
          ])
        end
      end

      context "期間オブジェクトを含む配列を渡した場合" do
        subject do
          described_class.fill_last([
            {first: adjl.new(year: {num: 1324})},
            FDate::Term.new(first: adjl.new(year: {num: 1330}), last: adjl.new(year: {num: 1335}), exclude_end: false),
            {first: adjl.new(year: {num: 1337})},
          ])
        end

        it "は、`last`を補った期間オブジェクトの配列を返すこと" do
          is_expected.to eql([
            FDate::Term.new(first: adjl.new(year: {num: 1324}), last: adjl.new(year: {num: 1330}), exclude_end: true),
            FDate::Term.new(first: adjl.new(year: {num: 1330}), last: adjl.new(year: {num: 1335}), exclude_end: false),
            FDate::Term.new(first: adjl.new(year: {num: 1337}), last: +FDate::InfinityDate),
          ])
        end
      end
    end

    context "期間の生データを含む配列の配列とブロックを渡した場合" do
      let(:klass){ Struct.new(:key, :term, :option) }
  
      subject do
        described_class.fill_last([
          ["key1", {first: adjl.new(year: {num: 1324})}, 123],
          ["key2", {first: adjl.new(year: {num: 1330}), last: adjl.new(year: {num: 1335}), exclude_end: false}, 223],
          ["key3", {first: adjl.new(year: {num: 1337})}, 323],
        ]) do |key, _term, option, &to_term|
          klass.new(key, to_term.call(_term), option)
        end
      end
  
      it "は、`last`を補った期間オブジェクトを含むブロックの結果の配列を返すこと" do
        is_expected.to eql([
          klass.new("key1", FDate::Term.new(first: adjl.new(year: {num: 1324}), last: adjl.new(year: {num: 1330}), exclude_end: true), 123),
          klass.new("key2", FDate::Term.new(first: adjl.new(year: {num: 1330}), last: adjl.new(year: {num: 1335}), exclude_end: false), 223),
          klass.new("key3", FDate::Term.new(first: adjl.new(year: {num: 1337}), last: +FDate::InfinityDate), 323),
        ])
      end
    end
  end
end
