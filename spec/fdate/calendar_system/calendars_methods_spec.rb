require 'spec_helper.rb'

describe FDate::CalendarSystem::CalendarsMethods do
  let(:owner){ double("CalendarSystemClass").extend described_class }

  before do
    owner.calendars = [
      FDate::Period.new("bzjl", FDate::Term.new(first: "adgr_A=1-1-1_", last: "adjl_A=1000_")),
      FDate::Period.new("adjl", FDate::Term.new(first: "adjl_A=800-1-1_", last: "adjl_A=1582-10-04_")),
      FDate::Period.new("adgr", FDate::Term.new(first: "adgr_A=1582-10-15_")),
    ]
  end

  describe "#valid_fdate?" do
    shared_examples_for "return true" do
      it{ expect(owner).to be_valid_fdate(fdate) }
    end

    shared_examples_for "return false" do
      it{ expect(owner).not_to be_valid_fdate(fdate) }
    end


    context "使われていた暦が無い期間の日付をデフォルト暦で渡した場合" do
      let(:fdate){ owner.default_calendar.new(year: 1) }
      it_behaves_like "return true"
    end

    context "使われていた暦が1つある期間の日付をその暦で渡した場合" do
      let(:fdate){ FDate::Calendar.collection["adgr"].new(year: 6713) }
      it_behaves_like "return true"
    end

    context "使われていた暦が1つある期間の日付をそれ以外の暦で渡した場合" do
      let(:fdate){ FDate::Calendar.collection["adjl"].new(year: 6713) }
      it_behaves_like "return false"
    end

    context "使われていた暦が複数ある期間の日付をそれに含まれる暦で渡した場合" do
      let(:fdate){ FDate::Calendar.collection["bzjl"].new(year: 5613) }
      it_behaves_like "return true"
    end

    context "使われていた暦が複数ある期間の日付をそれに含まれる暦で渡した場合" do
      let(:fdate){ FDate::Calendar.collection["adjl"].new(year: 5613) }
      it_behaves_like "return true"
    end

    context "使われていた暦が複数ある期間の日付をそれに含まれない暦で渡した場合" do
      let(:fdate){ FDate::Calendar.collection["adgr"].new(year: 5613) }
      it_behaves_like "return false"
    end

    context "暦体系に含まれない暦の日付オブジェクトを渡した場合" do
      let(:fdate){ FDate::Calendar.collection["jpgr"].new(year: 5613) }
      it_behaves_like "return false"
    end
  end

  describe "#calendar_get" do
    context "使われていた暦が無い期間の日付とみなせるオブジェクトを渡した場合" do
      it "は、デフォルト暦を返すこと" do
        expect(owner.calendar_get(1234567)).to equal(owner.default_calendar)
      end
    end

    context "使われていた暦が1つある期間の日付とみなせるオブジェクトを渡した場合" do
      it "は、その暦を返すこと" do
        expect(owner.calendar_get(1903683)).to equal(FDate::Calendar.collection["bzjl"])
      end
    end

    context "使われていた暦が複数ある期間の日付とみなせるオブジェクトを渡した場合" do
      it "は、そのうち使用開始日が最も遅い暦を返すこと" do
        expect(owner.calendar_get(2049783)).to equal(FDate::Calendar.collection["adjl"])
      end
    end
  end

  describe "#default_calendar" do
    subject{ owner.default_calendar }

    let(:last_calendar){ FDate::Calendar.collection[owner.calendars.periods.last.context[:calendar]] }

    context "`#default_calendar=`でセットしていない場合" do
      it "は、`#calendars`のうち使用開始日が最も遅い暦を返すこと" do
        is_expected.to equal(last_calendar)
        is_expected.to equal(FDate::Calendar.collection["adgr"])
      end
    end

    context "`#default_calendar=`でセットしてる場合" do
      before do
        owner.default_calendar = "bzjl"
      end

      it "は、その暦を返すこと" do
        is_expected.not_to equal(last_calendar)
        is_expected.to equal(FDate::Calendar.collection["bzjl"])
      end
    end
  end
end
