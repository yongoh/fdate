require 'spec_helper.rb'

describe FDate::CalendarSystem::BuildMethods do
  let(:owner){ double("CalendarSystemClass").extend described_class }

  before do
    allow(owner).to receive(:calendar_get).and_return(object_calendar)
  end

  let(:object_calendar){ FDate::Calendar.collection["adgr"] }


  describe "#from_jd" do
    it "は、`#calendar_get`から返された暦の日付オブジェクトを返すこと" do
      expect(owner.from_jd(1721425)).to eql(object_calendar.new(year: {num: 0}, month: 12, day: 31))
    end
  end

  describe "#_local_parse" do
    before do
      allow(owner).to receive(:default_calendar).and_return(object_calendar)
    end

    it "は、`#calendar_get`から返された暦の日付オブジェクトを返すこと" do
      expect(owner._local_parse("2018-07-31")).to eql(object_calendar.new(year: {num: 2018}, month: 7, day: 31))
    end
  end
end
