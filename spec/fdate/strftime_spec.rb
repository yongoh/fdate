require 'spec_helper.rb'

describe FDate::Strftime do
  before do
    owner.extend(described_class)

    allow(FDate::Locale.collection).to receive(:[]).and_return(locale)
  end

  let(:owner){ double("Owner") }

  let(:locale){
    x = double("Locale")
    allow(x).to receive(:[]){|obj, key| locale_hash.fetch(obj).fetch(key.to_s) }
    x
  }


  describe "#strftime" do
    context "引数なしの場合" do
      let(:locale_hash){ {owner => {"default" => "でふぉると"}} }

      it "は、'default'をキーとしてロケールから訳文を取得して返すこと" do
        expect(owner.strftime).to eq("でふぉると")
      end
    end

    context "文字列を渡した場合" do
      it "は、`#strftime_parse`を使ってフォーマット変換すること" do
        expect(owner.strftime("FooBar")).to eq("FooBar")
      end
    end

    context "シンボルを渡した場合" do
      let(:locale_hash){ {owner => {"foobar" => "FooBar"}} }

      it "は、それをキーとしてロケールから訳文を取得して返すこと" do
        expect(owner.strftime(:foobar)).to eq("FooBar")
      end
    end

    context "名前セットオブジェクトを渡した場合" do
      let(:owner){ double("Owner", day_of_week: 3, youbi: "曜日") }
      let(:name_set){
        FDate::NameSet.new({
          0 => "日$youbi()",
          1 => "月$youbi()",
          2 => "火$youbi()",
          3 => "水$youbi()",
          4 => "木$youbi()",
          5 => "金$youbi()",
          6 => "土$youbi()",
        })
      }

      before do
        name_set.key_proc = :day_of_week.to_proc
      end

      it "は、得られた名前を置換して返すこと" do
        expect(owner.strftime(name_set)).to eq("水曜日")
      end
    end

    context "それ以外を渡した場合" do
      it "は、`#to_s`を使い文字列に変換して返すこと" do
        expect(owner.strftime(1234)).to eq("1234")
      end
    end

    context "`#to_s`の結果に置換用文字列を含む場合" do
      let(:owner){ double("Owner", foobar: "ふーばー") }
      let(:arg){ double(to_s: "[[[$foobar()]]]") }

      it "は、`#to_s`の結果を`#strftime_parse`で置換して返すこと" do
        expect(owner.strftime(arg)).to eq("[[[ふーばー]]]")
      end
    end
  end

  describe "#strftime_parse" do
    context "'$xxx()'を含む文字列を渡した場合" do
      subject do
        owner.strftime_parse("[[[$foobar()]]]")
      end

      context "メソッド`#xxx`が無い場合" do
        let(:owner){ Class.new.new }

        it "は、メソッド未定義エラーを発生させること" do
          expect{ subject }.to raise_error(NoMethodError, "undefined method `foobar' for #{owner.inspect}")
        end
      end

      context "メソッド`#xxx`がある場合" do
        let(:owner){ double("Owner", foobar: Time.new(2018, 8, 14)) }

        it "は、'$xxx()'部分を`#xxx`の戻り値を文字列化して置換すること" do
          is_expected.to eq("[[[2018-08-14 00:00:00 +0900]]]")
        end
      end

      context "メソッド`#xxx`が'$yyy()'を含む文字列を返す場合" do
        let(:owner){ double("Owner", foobar: "{{{$aaa()}}}", aaa: "あああ") }

        it "は、再帰的に置換した文字列を返すこと" do
          is_expected.to eq("[[[{{{あああ}}}]]]")
        end
      end

      context "メソッド`#xxx`が'$xxx()'を含む文字列を返す場合" do
        let(:owner){ double("Owner", foobar: "{{{$foobar()}}}") }

        it "は、ループエラーを発生させること" do
          expect{ subject }.to raise_error(SystemStackError, "stack level too deep")
        end
      end

      context "括弧内に文字列を入れた場合" do
        subject do
          owner.strftime_parse("[[[$foobar('ふーばー')]]]")
        end

        before do
          allow(owner).to receive(:foobar){|str| str * 3 }
        end

        it "は、'@xxx()'部分を`#foobar(str))`の結果で置換すること" do
          is_expected.to eq("[[[ふーばーふーばーふーばー]]]")
        end
      end

      context "入れ子括弧を含む文字列を渡した場合" do
        before do
          allow(owner).to receive(:foobar){|str| str.gsub(/\(/, "{").gsub(/\)/, "}") }
        end

        context "括弧の数が合っている場合" do
          it "は、置換しないこと" do
            expect(owner.strftime_parse("[[[$foobar('(ふー)((ばー))(')]]]")).to eq("[[[$foobar('(ふー)((ばー))(')]]]")
          end
        end

        context "括弧の数が合わない場合" do
          it "は、正確に括弧内の文字列を処理して置換すること" do
            expect(owner.strftime_parse("[[[$foobar('(ふー)((ばー))()')]]]")).to eq("[[[{ふー}{{ばー}}{}]]]")
          end
        end
      end

      context "置換パターンの前にフォーマットパターンを入れた場合" do
        let(:owner){ double("Owner", foobar: Time.new(2018, 8, 14)) }

        it "は、置換後にフォーマットした文字列を返すこと" do
          expect(owner.strftime_parse("[[[%15.-10$foobar()]]]")).to eq("[[[     2018-08-14]]]")
        end
      end
    end

    context "'@xxx()'を含む文字列を渡した場合" do
      subject do
        owner.strftime_parse("[[[@foobar()]]]")
      end

      let(:owner){ double("Owner", foobar: foobar) }

      context "メソッド`#xxx`が無い場合" do
        let(:owner){ Class.new.new }

        it "は、メソッド未定義エラーを発生させること" do
          expect{ subject }.to raise_error(NoMethodError, "undefined method `foobar' for #{owner.inspect}")
        end
      end

      context "メソッド`#xxx`の結果が`#strftime`を持たない場合" do
        let(:foobar){ Class.new.new }

        it "は、メソッド未定義エラーを発生させること" do
          expect{ subject }.to raise_error(NoMethodError, "undefined method `strftime' for #{foobar.inspect}")
        end
      end

      context "メソッド`#xxx`が`#strftime`を持つ場合" do
        let(:foobar){ double("Foobar", strftime: "ふーばー") }

        it "は、'@xxx()'部分を`#foobar.strftime`（引数なし）の結果で置換すること" do
          is_expected.to eq("[[[ふーばー]]]")
        end
      end

      context "メソッド`#xxx`の結果が`nil`の場合" do
        let(:foobar){ nil }

        it "は、'@xxx()'部分を空文字で置換すること" do
          is_expected.to eq("[[[]]]")
        end
      end

      context "括弧内にフォーマット文字列を入れた場合" do
        let(:foobar){ double("Foobar", hoge: "Hoge").extend(described_class) }

        let(:locale_hash){
          {
            owner => {
              "hoge" => "ほげ",
              "a" => "あああ",
            },
            foobar => {
              "hoge" => "HOGE",
              "h" => :hoge,
            },
          }
        }

        context "メソッド置換パターンを入れた場合" do
          it "は、'@xxx()'部分を`#foobar.strftime(format))`の結果で置換すること" do
            expect(owner.strftime_parse("[[[@foobar('$hoge()')]]]")).to eq("[[[Hoge]]]")
          end
        end

        context "訳文キー（シンボル）を入れた場合" do
          it "は、'@xxx()'部分を`#foobar.strftime(format))`の結果で置換すること" do
            expect(owner.strftime("[[[@foobar(:hoge)]]]")).to eq("[[[HOGE]]]")
          end
        end

        context "訳文置換パターンを入れた場合" do
          it "は、'@xxx()'部分を`#foobar.strftime(format))`の結果で置換すること" do
            expect(owner.strftime("[[[@foobar(':hoge()')]]]")).to eq("[[[HOGE]]]")
          end
        end

        context "フォーマット可能訳文置換パターンを入れた場合" do
          it "は、'@xxx()'部分を`#foobar.strftime(format))`の結果で置換すること" do
            expect(owner.strftime("%a[[[@foobar('%h')]]]")).to eq("あああ[[[HOGE]]]")
          end
        end

        context "参照置換パターンを入れた場合" do
          let(:foobar){ double("Foobar", baz: baz).extend(described_class) }
          let(:baz){ double("Baz").extend(described_class) }

          before do
            locale_hash[baz] = {"spam" => "すぱむ"}
          end

          it "は、'@xxx()'部分を`#foobar.strftime(format))`の結果で置換すること" do
            expect(owner.strftime("[[[@foobar('@baz(:spam)')]]]")).to eq("[[[すぱむ]]]")
          end
        end
      end
    end

    context "':xxx()'を含む文字列を渡した場合" do
      subject do
        owner.strftime_parse("[[[:foobar()]]]")
      end

      context "ロケールにキー'xxx'が無い場合" do
        let(:locale_hash){ {owner => {}} }

        it "は、キーエラーを発生させること" do
          expect{ subject }.to raise_error(KeyError, 'key not found: "foobar"')
        end
      end

      context "ロケールにキー'xxx'がある場合" do
        let(:locale_hash){ {owner => {"foobar" => "ふーばー"}} }

        it "は、':xxx()'をその値で置換すること" do
          is_expected.to eq("[[[ふーばー]]]")
        end
      end
    end

    context "'%x'を含む文字列を渡した場合" do
      let(:locale_hash){ {owner => {"n" => "123*"}} }

      context "オプション無しの場合" do
        it "は、'%x'をロケールの訳文キー'n'の値で置換すること" do
          expect(owner.strftime_parse("[[[%n]]]")).to eq("[[[123*]]]")
        end
      end

      context "'%'と訳文キーの間に数値を入れた場合" do
        context "正数を入れた場合" do
          it "は、左にスペースを埋めてその数値の長さにした文字列を返すこと" do
            expect(owner.strftime_parse("[[[%8n]]]")).to eq("[[[    123*]]]")
          end
        end

        context "符号付き正数を入れた場合" do
          it "は、左にスペースを埋めてその数値の長さにした文字列を返すこと" do
            expect(owner.strftime_parse("[[[%+8n]]]")).to eq("[[[    123*]]]")
          end
        end

        context "負数を入れた場合" do
          it "は、右にスペースを埋めてその数値の長さにした文字列を返すこと" do
            expect(owner.strftime_parse("[[[%-8n]]]")).to eq("[[[123*    ]]]")
          end
        end

        context "数値の前に'0'を入れた場合" do
          it "は、左に'0'を埋めてその数値の長さにした文字列を返すこと" do
            expect(owner.strftime_parse("[[[%08n]]]")).to eq("[[[0000123*]]]")
          end
        end
      end

      context "'%'と訳文キーの間に'.'（ドット）と数値を入れた場合" do
        context "正数を入れた場合" do
          it "は、左を切り捨てその数値の長さにした文字列を返すこと" do
            expect(owner.strftime_parse("[[[%.2n]]]")).to eq("[[[3*]]]")
          end
        end

        context "負数を入れた場合" do
          it "は、右を切り捨てその数値の長さにした文字列を返すこと" do
            expect(owner.strftime_parse("[[[%.-2n]]]")).to eq("[[[12]]]")
          end
        end
      end

      context "全てのオプションをつけたパターンを渡した場合" do
        it "は、全ての整形を行った文字列を返すこと" do
          expect(owner.strftime_parse("[[[%#8.2n]]]")).to eq("[[[######3*]]]")
        end
      end

      context "ロケールにキー'x'が無い場合" do
        it "は、キーエラーを発生させること" do
          expect{ owner.strftime_parse("[[[%a]]]") }.to raise_error(KeyError, 'key not found: "a"')
        end
      end
    end

    context "エスケープした置換用文字列を含む文字列を渡した場合" do
      context "記号をエスケープした場合" do
        it "は、規定の置換を行わず記号を文字参照に置換すること" do
          expect(owner.strftime_parse('[[[\$foobar()]]]')).to eq('[[[&#36;foobar()]]]')
        end
      end

      context "左括弧をエスケープした場合" do
        it "は、規定の置換を行わず左括弧を文字参照に置換すること" do
          expect(owner.strftime_parse('[[[$foobar\()]]]')).to eq('[[[$foobar&#40;)]]]')
        end
      end

      context "右括弧をエスケープした場合" do
        it "は、規定の置換を行わず右括弧を文字参照に置換すること" do
          expect(owner.strftime_parse('[[[$foobar(\)]]]')).to eq('[[[$foobar(&#41;]]]')
        end
      end
    end
  end

  describe "::strftime_args_parse" do
    context "全体を括弧で囲んでいない文字列を渡した場合" do
      it "は、配列に入れずに文字列を返すこと" do
        expect(described_class.strftime_args_parse("'hoge'")).to eq("hoge")
      end
    end

    context "クォートで囲んだ文字列を渡した場合" do
      it "は、文字列を含んだ配列を返すこと" do
        expect(described_class.strftime_args_parse("('hoge')")).to eq(["hoge"])
      end
    end

    context "クォートで囲まない数字文字列を渡した場合" do
      it "は、数値を含んだ配列を返すこと" do
        expect(described_class.strftime_args_parse("(1234)")).to eq([1234])
      end
    end

    context "複数の引数を含む括弧を渡した場合" do
      it "は、パースしたオブジェクト群を含む配列を返すこと" do
        expect(described_class.strftime_args_parse("('hoge', 1234, Time.new(2018, 8, 14))")).to eql(["hoge", 1234, Time.new(2018, 8, 14)])
      end
    end

    context "オプション引数を含む括弧を渡した場合" do
      it "は、オプションをハッシュとして入れた配列を返すこと" do
        expect(described_class.strftime_args_parse("('hoge', locale: 'en')")).to eql(["hoge", {locale: "en"}])
      end
    end
  end

  describe "::align" do
    context "第2引数に文字列の長さと等しい数値を渡した場合" do
      it "は、渡したままの文字列を返すこと" do
        expect(described_class.align("123*", 4)).to eq("123*")
      end
    end

    context "第2引数に文字列の長さより大きい正数を、" do
      context "第3引数にスペース(default)を渡した場合" do
        it "は、左にスペースを埋めて第2引数の長さにした文字列を返すこと" do
          expect(described_class.align("123*", 8)).to eq("    123*")
        end
      end
    end

    context "第2引数に絶対値が文字列の長さより大きい負数を渡した場合" do
      it "は、右にスペースを埋めて第2引数の絶対値の長さにした文字列を返すこと" do
        expect(described_class.align("123*", -8)).to eq("123*    ")
      end
    end

    context "第2引数に文字列の長さより小さい正数を渡した場合" do
      it "は、引数エラーを発生させること" do
        expect{ described_class.align("123*", 3) }.to raise_error(ArgumentError, "negative argument")
      end
    end

    context "第3引数に'0'、" do
      context "第1引数に正負符号から始まらない文字列を渡した場合" do
        it "は、左に'0'を埋めて全体が第2引数の長さにした文字列を返すこと" do
          expect(described_class.align("123*", 8, "0")).to eq("0000123*")
        end
      end

      context "第1引数に'-'から始まる文字列、第2引数に文字列の長さより大きい正数を渡した場合" do
        it "は、符号と次以降の間に'0'を埋めて、符号を除いた部分が第2引数の長さとなる文字列を返すこと" do
          expect(described_class.align("-123*", 8, "0")).to eq("-0000123*")
        end
      end

      context "第1引数が'-'から始まる文字列、第2引数に絶対値が文字列の長さより大きい負数を渡した場合" do
        it "は、右に'0'を埋めて全体が第2引数の長さとなる文字列を返すこと" do
          expect(described_class.align("-123*", -8, "0")).to eq("-123*000")
        end
      end
    end
  end

  describe "::omit" do
    context "第2引数に文字列の長さと等しい数値を渡した場合" do
      it "は、渡したままの文字列を返すこと" do
        expect(described_class.omit("123*", 4)).to eq("123*")
      end
    end

    context "第2引数に文字列の長さより小さい正数を渡した場合" do
      it "は、右端から渡した数値の絶対値分の文字列を返すこと" do
        expect(described_class.omit("123*", 2)).to eq("3*")
      end
    end

    context "第2引数に絶対値が文字列の長さより小さい負数を渡した場合" do
      it "は、左端から渡した数値分の文字列を返すこと" do
        expect(described_class.omit("123*", -2)).to eq("12")
      end
    end

    context "第2引数に文字列の長さより大きい正数を渡した場合" do
      it "は、渡したままの文字列を返すこと" do
        expect(described_class.omit("123*", 8)).to eq("123*")
      end
    end

    context "第2引数に絶対値が文字列の長さより大きい負数を渡した場合" do
      it "は、渡したままの文字列を返すこと" do
        expect(described_class.omit("123*", -8)).to eq("123*")
      end
    end

    context "第2引数に0を渡した場合" do
      it "は、空文字を返すこと" do
        expect(described_class.omit("123*", 0)).to eq("")
      end
    end
  end
end
