require 'spec_helper.rb'

describe FDate::FullDate do
  # adjl_1234-06-**_
  let(:owner){ double("Owner", global_range: 2171928..2171957).extend described_class }

  describe "#<=>" do
    shared_examples_for "owner == other" do
      it{ expect(owner <=> other).to eq(0) }
    end

    shared_examples_for "owner < other" do
      it{ expect(owner <=> other).to eq(-1) }
    end

    shared_examples_for "owner > other" do
      it{ expect(owner <=> other).to eq(1) }
    end

    shared_examples_for "not comparable" do
      it{ expect(owner <=> other).to be_nil }
    end

    # [self] <other>
    context "`FullDate`と比較した場合" do
      let(:other){ double("Other", global_range: other_range).extend described_class }

      context "     [  ] <  >" do
        let(:other_range){ 2171989..2172019 } # adjl_1235-08-**_
        it_behaves_like "owner < other"
      end

      context "     [  X  >  " do
        let(:other_range){ 2171928..2171972 } # adjl_1234-06-30_~_1234-07-15_
        it_behaves_like "owner == other"
      end

      context "     [ <] >   " do
        let(:other_range){ 2171942..2171972 } # adjl_1234-06-15_~_1234-07-15_
        it_behaves_like "owner == other"
      end

      context "    [< >]     " do
        let(:other_range){ 2171947..2171956 } # adjl_1234-06-2*_
        it_behaves_like "owner == other"
      end

      context "     {  }     " do
        let(:other_range){ 2171928..2171928 } # adjl_1234-06-**_
        it_behaves_like "owner == other"
      end

      context "    <[ ]>     " do
        let(:other_range){ 2171911..2171972 } # adjl_1234-05-15_~_1234-07-15_
        it_behaves_like "owner == other"
      end

      context "   < [> ]     " do
        let(:other_range){ 2171911..2171942 } # adjl_1234-05-15_~_1234-06-15_
        it_behaves_like "owner == other"
      end

      context "  <  X  ]     " do
        let(:other_range){ 2171911..2171928 } # adjl_1234-05-15~_1234-06-01_
        it_behaves_like "owner == other"
      end

      context "<  > [  ]     " do
        let(:other_range){ 2171867..2171896 } # adjl_1234-04-**_
        it_behaves_like "owner > other"
      end
    end
  end
end
