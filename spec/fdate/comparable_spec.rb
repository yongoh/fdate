require 'spec_helper.rb'

describe FDate::Comparable do
  let(:owner){ double("Owner", global_range: 6..12).extend described_class }

  describe "#<=>" do
    shared_examples_for "owner == other" do
      it{ expect(owner <=> other).to eq(0) }
    end

    shared_examples_for "owner < other" do
      it{ expect(owner <=> other).to eq(-1) }
    end

    shared_examples_for "owner > other" do
      it{ expect(owner <=> other).to eq(1) }
    end

    # [self] <other>
    context "Rangeオブジェクトと比較した場合" do
      context "     [  ] <  >" do
        let(:other){ 13..15 }
        it_behaves_like "owner < other"
      end

      context "     [  X  >  " do
        let(:other){ 12..15 }
        it_behaves_like "owner == other"
      end

      context "     [ <] >   " do
        let(:other){ 10..15 }
        it_behaves_like "owner == other"
      end

      context "    [< >]     " do
        let(:other){ 8..10 }
        it_behaves_like "owner == other"
      end

      context "     {  }     " do
        let(:other){ 6..12 }
        it_behaves_like "owner == other"
      end

      context "    <[ ]>     " do
        let(:other){ 3..15 }
        it_behaves_like "owner == other"
      end

      context "   < [> ]     " do
        let(:other){ 0..8 }
        it_behaves_like "owner == other"
      end

      context "  <  X  ]     " do
        let(:other){ 0..6 }
        it_behaves_like "owner == other"
      end

      context "<  > [  ]     " do
        let(:other){ 0..3 }
        it_behaves_like "owner > other"
      end
    end

    # [self] *other
    context "数値と比較した場合" do
      context "*[ ] " do
        let(:other){ 3 }
        it_behaves_like "owner > other"
      end

      context " { ] " do
        let(:other){ 6 }
        it_behaves_like "owner == other"
      end

      context " [*] " do
        let(:other){ 8 }
        it_behaves_like "owner == other"
      end

      context " [ } " do
        let(:other){ 12 }
        it_behaves_like "owner == other"
      end

      context " [ ]*" do
        let(:other){ 15 }
        it_behaves_like "owner < other"
      end
    end

    context "`Range`,`Numeric`以外と比較しようとした場合" do
      let(:other){ "Other" }
      it{ expect(owner <=> other).to be_nil }
    end
  end
end
