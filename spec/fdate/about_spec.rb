require 'spec_helper.rb'

describe FDate::About do
  describe "#validate_order" do
    context "昇順の範囲を渡した場合" do
      it{ expect{ described_class.new("key", 0.3..0.8) }.not_to raise_error }
    end

    context "降順の範囲を渡した場合" do
      it "は、引数エラーを発生させること" do
        expect{ described_class.new("key", 0.8..0.3) }.to raise_error(ArgumentError, "0.8..0.3 is descending order")
      end
    end

    context "両端が同じ数値の範囲を渡した場合" do
      it{ expect{ described_class.new("key", 0.3..0.3) }.not_to raise_error }
    end
  end

  describe "#validate_range" do
    context "始端が0未満の範囲を渡した場合" do
      it "は、引数エラーを発生させること" do
        expect{ described_class.new("key", -0.5..0.5) }.to raise_error(ArgumentError, "-0.5..0.5 is not included 0..1")
      end
    end

    context "終端が1より上の範囲を渡した場合" do
      it "は、引数エラーを発生させること" do
        expect{ described_class.new("key", 0.5..1.5) }.to raise_error(ArgumentError, "0.5..1.5 is not included 0..1")
      end
    end

    context "0から1に収まる昇順の範囲を渡した場合" do
      it{ expect{ described_class.new("key", 0..1) }.not_to raise_error }
    end
  end

  describe "#*" do
    let(:about){ described_class.new("key", 0/2r..1/2r) }

    context "1桁の範囲を渡した場合" do
      it{ expect(about * (0..10)).to eq(0..5) }
    end

    context "3桁の範囲を渡した場合" do
      it{ expect(about * (0..1000)).to eq(0..500) }
    end

    context "負数の範囲を渡した場合" do
      it{ expect(about * (-10..0)).to eq(-10..-5) }
    end

    context "中途半端な範囲を渡した場合" do
      it{ expect(about * (12..25)).to eq(12..18) }
    end
  end
end
