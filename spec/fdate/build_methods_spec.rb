require 'spec_helper.rb'

describe FDate::BuildMethods do
  let(:owner){ double("FDate").extend described_class }

  before do
    FDate::Calendar.collection.default = "adgr"
  end

  let(:adgr){ FDate::Calendar.collection["adgr"] }
  let(:adjl){ FDate::Calendar.collection["adjl"] }
  let(:default_calendar){ FDate::Calendar.collection.default }


  describe "#parseable?" do
    context "ユリウス通日コードを渡した場合" do
      it{ expect(owner.parseable?("(12345678)")).to be_truthy }
    end

    context "期間コードを渡した場合" do
      it{ expect(owner.parseable?("hoge~piyo")).to be_truthy }
    end

    context "ユリウス通日範囲コードを渡した場合" do
      it{ expect(owner.parseable?("(2424875...2447892)")).to be_truthy }
    end

    context "+符号付き無限日付コードを渡した場合" do
      it{ expect(owner.parseable?("(+Infinity)")).to be_truthy }
    end

    context "-符号付き無限日付コードを渡した場合" do
      it{ expect(owner.parseable?("(-Infinity)")).to be_truthy }
    end

    context "符号なし無限日付コードを渡した場合" do
      it{ expect(owner.parseable?("(Infinity)")).to be_truthy }
    end

    context "FDateコード（日付部がローカル）を渡した場合" do
      it{ expect(owner.parseable?("adgr_1955-11-05_2-2")).to be_truthy }
    end

    context "FDateコード（日付部がグローバル）を渡した場合" do
      it{ expect(owner.parseable?("adgr_(2424875)_2-2")).to be_truthy }
    end

    context "不正なコードを渡した場合" do
      it{ expect(owner.parseable?("1a345xf00")).to be_falsey }
    end
  end

  describe "#parse" do
    context "ユリウス通日コードを渡した場合" do
      context "オプション無しの場合" do
        it "は、デフォルト暦の日付オブジェクトを生成すること" do
          expect(owner.parse("(2424875)")).to eql(default_calendar.new(year: {num: 1926}, month: 12, day: 25))
        end
      end

      context "オプションに暦キーを渡した場合" do
        it "は、渡した暦のオブジェクトを生成すること" do
          expect(owner.parse("(2424875)", calendar: adjl)).to eql(adjl.new(year: {num: 1926}, month: 12, day: 12))
        end
      end
    end

    context "ユリウス通日範囲コードを渡した場合" do
      it "は、デフォルト暦の期間オブジェクトを生成すること" do
        expect(owner.parse("(2424875..2447534)")).to eql(FDate::Term.new(
          first: default_calendar.new(year: {num: 1926}, month: 12, day: 25),
          last: default_calendar.new(year: {num: 1989}, month: 1, day: 7),
        ))
      end
    end

    context "無限日付コードを渡した場合" do
      it "は、無限日付オブジェクトを生成すること" do
        expect(owner.parse("(-Infinity)")).to eql(FDate::InfinityDate.negative)
      end
    end

    context "FDateコードを渡した場合" do
      context "日付部がローカルコードの場合" do
        it "は、日付オブジェクトを生成すること" do
          expect(owner.parse("adgr_1955-11-05_2-2")).to eql(adgr.new(year: {num: 1955}, month: 11, day: 5, about: "2-2"))
        end
      end

      context "日付部がグローバルコードの場合" do
        it "は、日付オブジェクトを生成すること" do
          expect(owner.parse("adgr_(2424875)_2-2")).to eql(adgr.new(year: {num: 1926}, month: 12, day: 25, about: "2-2"))
        end
      end
    end

    context "期間コードを渡した場合" do
      it "は、期間オブジェクトを生成すること" do
        expect(owner.parse("adgr_1926-12-25_2-2~adgr_1989-01-07_")).to eql(FDate::Term.new(
          first: adgr.new(year: {num: 1926}, month: 12, day: 25, about: "2-2"),
          last: adgr.new(year: {num: 1989}, month: 1, day: 7),
        ))
      end
    end

    context "不正なコードを渡した場合" do
      it "は、パースエラーを発生させること" do
        expect{ owner.parse("1a345xf00") }.to raise_error(FDate::ParseError, "It's wrong grammar. \"1a345xf00\"")
      end
    end
  end

  describe "#from_i" do
    context "ユリウス通日を渡した場合" do
      context "オプション無しの場合" do
        it "は、デフォルト暦の日付オブジェクトを生成すること" do
          expect(owner.from_i(2435417)).to eql(default_calendar.new(year: {num: 1955}, month: 11, day: 5))
        end
      end

      context "オプションに暦キーを渡した場合" do
        it "は、指定した暦の日付オブジェクトを生成すること" do
          expect(owner.from_i(2435417, calendar: 'adjl')).to eql(adjl.new(year: {num: 1955}, month: 10, day: 23))
        end
      end

      context "オプションに「頃」キーを渡した場合" do
        it "は、指定した「頃」を持つ日付オブジェクトを生成すること" do
          expect(owner.from_i(2435417, about: '2-2')).to eql(default_calendar.new(year: {num: 1955}, month: 11, day: 5, about: '2-2'))
        end
      end
    end

    context "ユリウス通日範囲を渡した場合" do
      context "オプション無しの場合" do
        it "は、デフォルト暦の期間オブジェクトを生成すること" do
          expect(owner.from_i(2424875..2447534)).to eql(FDate::Term.new(
            first: default_calendar.new(year: {num: 1926}, month: 12, day: 25),
            last: default_calendar.new(year: {num: 1989}, month: 1, day: 7),
          ))
        end
      end

      context "オプションに暦キーを渡した場合" do
        it "は、指定した暦の期間オブジェクトを生成すること" do
          expect(owner.from_i(2424875..2447534, calendar: 'adjl')).to eql(FDate::Term.new(
            first: adjl.new(year: {num: 1926}, month: 12, day: 12),
            last: adjl.new(year: {num: 1988}, month: 12, day: 25),
          ))
        end
      end
    end

    context "無限数値を渡した場合" do
      context "正の無限数値を渡した場合" do
        it "は、無限未来オブジェクトを生成すること" do
          expect(owner.from_i(Float::INFINITY)).to eql(FDate::InfinityDate.positive)
        end
      end

      context "負の無限数値を渡した場合" do
        it "は、無限過去オブジェクトを生成すること" do
          expect(owner.from_i(-Float::INFINITY)).to eql(FDate::InfinityDate.negative)
        end
      end
    end

    context "数値・範囲以外のオブジェクトを渡した場合" do
      it "は、引数エラーを発生させること" do
        expect{ owner.from_i("ほげ") }.to raise_error(ArgumentError)
      end
    end
  end

  describe "#new" do
    context "キー`:first`を含むハッシュを渡した場合" do
      it "は、期間オブジェクトを生成すること" do
        expect(owner.new({first: 2424875})).to eql(FDate::Term.new(
          first: default_calendar.new(year: {num: 1926}, month: 12, day: 25),
        ))
      end
    end

    context "キー`:last`を含むハッシュを渡した場合" do
      it "は、期間オブジェクトを生成すること" do
        expect(owner.new({last: 2447534})).to eql(FDate::Term.new(
          last: default_calendar.new(year: {num: 1989}, month: 1, day: 7),
        ))
      end
    end

    context "どちらも含まないハッシュを渡した場合" do
      subject do
        owner.new(year: {num: 1955}, month: 11, day: 5)
      end

      it "は、デフォルト暦の日付オブジェクトを生成すること" do
        is_expected.to eql(default_calendar.new(year: {num: 1955}, month: 11, day: 5))
      end
    end

    context "オプションに暦キーを渡した場合" do
      subject do
        owner.new({year: {num: 1955}, month: 11, day: 5}, calendar: "adjl")
      end

      it "は、指定した暦の日付オブジェクトを生成すること" do
        is_expected.to eql(adjl.new(year: {num: 1955}, month: 11, day: 5))
      end
    end
  end

  describe "#build" do
    context "FDate::Full" do
      shared_examples_for "return arg" do
        it "は、渡したオブジェクトをそのまま返すこと" do
          expect(owner.build(arg)).to equal(arg)
        end
      end


      context "日付オブジェクトを渡した場合" do
        let(:arg){ default_calendar.new(year: {num: 1955}, month: 11, day: 5, about: '2-2') }
        it_behaves_like "return arg"
      end

      context "期間オブジェクトを渡した場合" do
        let(:arg){ FDate::Term.new(first: "adgr_1925-12-25_", last: "adgr_1989-01-07_") }
        it_behaves_like "return arg"
      end

      context "無限日付オブジェクトを渡した場合" do
        let(:arg){ FDate::InfinityDate.positive }
        it_behaves_like "return arg"
      end
    end

    context "ハッシュを渡した場合" do
      let(:arg){ {year: {num: 1955}, month: 11, day: 5} }

      it "は、`#new`を使って日付オブジェクトを生成すること" do
        expect(owner.build(year: {num: 1955}, month: 11, day: 5)).to eql(default_calendar.new(year: {num: 1955}, month: 11, day: 5))
      end
    end

    context "String" do
      context "日付コードを渡した場合" do
        it "は、`#parse`を使って日付オブジェクトを生成すること" do
          expect(owner.build("_1955-11-5_2-2")).to eql(default_calendar.new(year: {num: 1955}, month: 11, day: 5, about: "2-2"))
        end
      end

      context "期間コードを渡した場合" do
        subject{ owner.build("adgr_1926-12-25_~adgr_1989-01-07_") }

        it "は、`#parse`を使って期間オブジェクトを生成すること" do
          is_expected.to eql(FDate::Term.new(
            first: default_calendar.new(year: {num: 1926}, month: 12, day: 25),
            last: default_calendar.new(year: {num: 1989}, month: 1, day: 7),
          ))
        end
      end

      context "無限日付コードを渡した場合" do
        it "は、`#parse`を使って無限日付オブジェクトを生成すること" do
          expect(owner.build("(Infinity)")).to eql(FDate::InfinityDate.positive)
        end
      end
    end

    context "ユリウス通日を渡した場合" do
      it "は、`#jd`を使って日付オブジェクトを生成すること" do
        expect(owner.build(2435417)).to eql(default_calendar.new(year: {num: 1955}, month: 11, day: 5))
      end
    end

    context "Numeric, Range" do
      context "ユリウス通日の範囲を渡した場合" do
        it "は、`#jd`を使って期間オブジェクトを生成すること" do
          expect(owner.build(2424875..2447534)).to eql(FDate::Term.new(
            first: default_calendar.new(year: {num: 1926}, month: 12, day: 25),
            last: default_calendar.new(year: {num: 1989}, month: 1, day: 7),
          ))
        end
      end

      context "無限大数値を渡した場合" do
        it "は、`#jd`を使って無限未来日付オブジェクトを生成すること" do
          expect(owner.build(Float::INFINITY)).to eql(FDate::InfinityDate.positive)
        end
      end

      context "無限小数値を渡した場合" do
        it "は、`#jd`を使って無限過去日付オブジェクトを生成すること" do
          expect(owner.build(-Float::INFINITY)).to eql(FDate::InfinityDate.negative)
        end
      end
    end

    context "それ以外のオブジェクトを渡した場合" do
      it "は、ビルドエラーを発生させること" do
        expect{ owner.build(Time.now) }.to raise_error(TypeError, "can not instantiate from Time.")
      end
    end
  end
end
