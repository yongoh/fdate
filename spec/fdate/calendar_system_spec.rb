require 'spec_helper.rb'

describe FDate::CalendarSystem do
  let(:calendar){
    Class.new(described_class) do
      self.key = "key"

      self.calendars = [
        FDate::Period.new("bzjl", FDate::Term.new(first: "adgr_A=1-1-1_", last: "adjl_A=1000_")),
        FDate::Period.new("adjl", FDate::Term.new(first: "adjl_A=800-1-1_", last: "adgr_A=1582-10-15_")),
        FDate::Period.new("adgr", FDate::Term.new(first: "adgr_A=1582-10-15_")),
      ]
    end
  }

  let(:fdate){ calendar.jd(2458330) }


  %i(year month day leap_year?).each do |method_name|
    describe "##{method_name}" do
      it "は、デリゲートすること" do
        expect(fdate.public_send(method_name)).to eql(fdate.object.public_send(method_name))
      end
    end
  end

  %i(object_id hash).each do |method_name|
    describe "##{method_name}" do
      it "は、デリゲートしないこと" do
        expect(fdate.public_send(method_name)).not_to eq(fdate.object.public_send(method_name))
      end
    end
  end

  describe "#eql?" do
    it "は、`#object`と同一とみなすこと" do
      expect(fdate).to eql(FDate::Calendar.collection["adgr"].jd(2458330))
    end
  end

  describe "#equal?" do
    it "は、`#object`と同一とみなさないこと" do
      expect(fdate).not_to equal(FDate::Calendar.collection["adgr"].jd(2458330))
    end
  end

  %i(dup clone).each do |method_name|
    describe "##{method_name}" do
      let(:result){ fdate.send(method_name) }

      it "は、自身をコピーすること" do
        expect(result).to be_a(described_class)
        expect(result).not_to equal(fdate)
      end

      it "は、`#object`もコピーすること" do
        expect(result.object).to eql(fdate.object)
        expect(result.object).not_to equal(fdate.object)
      end
    end
  end

  describe "#taint" do
    it "は、`#object`も汚染状態にすること" do
      fdate.taint
      expect(fdate).to be_tainted
      expect(fdate.object).to be_tainted
    end
  end

  describe "#freeze" do
    it "は、`#object`も凍結状態にすること" do
      fdate.freeze
      expect(fdate).to be_frozen
      expect(fdate.object).to be_frozen
    end
  end

  describe "#calendar" do
    subject{ fdate.calendar }

    it "は、暦体系クラスを返すこと" do
      is_expected.to equal(calendar)
    end

    it "は、`#object`にデリゲートしないこと" do
      is_expected.not_to equal(fdate.object.calendar)
    end
  end

  describe "#validate_calendar" do
    shared_examples_for "raise DateError" do
      it "は、日付エラーを発生させること" do
        expect{ calendar.new(fdate) }.to raise_error(FDate::DateError, "#{fdate.calendar} is not used in FDate::Calendar[key] on #{fdate}")
      end
    end


    context "暦がある期間の日付で、それに含まれる暦の日付オブジェクトをラップした場合" do
      let(:fdate){ FDate::Calendar.collection["adjl"].new(year: 5613) }
      it{ expect{ calendar.new(fdate) }.not_to raise_error }
    end

    context "暦体系に含まれない暦の日付オブジェクトをラップした場合" do
      let(:fdate){ FDate::Calendar.collection["USSR"].new(year: 5613) }
      it_behaves_like "raise DateError"
    end

    context "暦がある期間の日付だが、それに含まれない暦の日付オブジェクトをラップした場合" do
      let(:fdate){ FDate::Calendar.collection["adgr"].new(year: 5613) }
      it_behaves_like "raise DateError"
    end
  end

  %i(local_code code encode to_s inspect).each do |method_name|
    describe "##{method_name}" do
      it "は、暦キーと暦体系のキーを含むFDateコードを返すこと" do
        expect(fdate.public_send(method_name)).to eq("adgr(key)_A=2018-7-30_j".freeze)
      end
    end
  end

  describe "#compound_calendar_key" do
    it "は、複合暦キーを返すこと" do
      expect(fdate.compound_calendar_key).to eq("adgr(key)")
    end
  end
end
