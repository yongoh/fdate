require 'spec_helper.rb'

describe FDate::InfinityDate do
  let(:negative){ described_class.new(false) }
  let(:positive){ described_class.new(true) }


  describe "#local_code" do
    context "無限過去の場合" do
      it "は、無限過去を示す日付コードを返すこと" do
        expect(negative.local_code).to eq("(-Infinity)")
      end
    end

    context "無限未来の場合" do
      it "は、無限未来を示す日付コードを返すこと" do
        expect(positive.local_code).to eq("(Infinity)")
      end
    end
  end

  describe "#jd" do
    context "無限過去の場合" do
      it{ expect(negative.jd).to eq(-Float::INFINITY) }
    end

    context "無限未来の場合" do
      it{ expect(positive.jd).to eq(Float::INFINITY) }
    end
  end

  describe "direction系メソッド群" do
    describe "#first" do
      context "無限過去の場合" do
        it{ expect(negative.first).to equal(negative) }
      end

      context "無限未来の場合" do
        it{ expect(positive.first).to equal(positive) }
      end
    end

    describe "#first_jd" do
      context "無限過去の場合" do
        it{ expect(negative.first_jd).to eq(-Float::INFINITY) }
      end

      context "無限未来の場合" do
        it{ expect(positive.first_jd).to eq(Float::INFINITY) }
      end
    end

    describe "#last" do
      context "無限過去の場合" do
        it{ expect(negative.last).to equal(negative) }
      end

      context "無限未来の場合" do
        it{ expect(positive.last).to equal(positive) }
      end
    end

    describe "#last_jd" do
      context "無限過去の場合" do
        it{ expect(negative.last_jd).to eq(-Float::INFINITY) }
      end

      context "無限未来の場合" do
        it{ expect(positive.last_jd).to eq(Float::INFINITY) }
      end
    end
  end

  describe "#jd_range" do
    context "無限過去の場合" do
      it{ expect(negative.jd_range).to eq(-Float::INFINITY..-Float::INFINITY) }
    end

    context "無限未来の場合" do
      it{ expect(positive.jd_range).to eq(Float::INFINITY..Float::INFINITY) }
    end
  end

  describe "#jd" do
    context "無限過去の場合" do
      it{ expect(negative.jd).to eq(-Float::INFINITY) }
    end

    context "無限未来の場合" do
      it{ expect(positive.jd).to eq(Float::INFINITY) }
    end
  end

  describe "#center_jd" do
    context "無限過去の場合" do
      it{ expect(negative.center_jd).to eq(-Float::INFINITY) }
    end

    context "無限未来の場合" do
      it{ expect(positive.center_jd).to eq(Float::INFINITY) }
    end
  end

  describe "#fixed?,#fuzzy?" do
    context "無限過去の場合" do
      example do
        expect(negative).not_to be_fixed
        expect(negative).to be_fuzzy
      end
    end

    context "無限未来の場合" do
      example do
        expect(positive).not_to be_fixed
        expect(positive).to be_fuzzy
      end
    end
  end

  describe "#negative?,#positive?" do
    context "無限過去の場合" do
      example do
        expect(negative).to be_negative
        expect(negative).not_to be_positive
      end
    end

    context "無限未来の場合" do
      example do
        expect(positive).not_to be_negative
        expect(positive).to be_positive
      end
    end
  end

  describe "#eql?" do
    context "無限過去の場合" do
      example do
        expect(negative).to eql(negative)
        expect(negative).not_to eql(positive)
      end
    end

    context "無限過去の場合" do
      example do
        expect(positive).not_to eql(negative)
        expect(positive).to eql(positive)
      end
    end
  end

  describe "#<=>" do
    context "無限過去の場合" do
      context "無限小の数値と比較" do
        it{ expect(negative).to be == -Float::INFINITY }
      end

      context "無限大の数値と比較" do
        it{ expect(negative).to be < Float::INFINITY }
      end

      context "無限過去日付オブジェクトと比較" do
        it{ expect(negative).to be == negative }
      end

      context "無限未来日付オブジェクトと比較" do
        it{ expect(negative).to be < positive }
      end
    end

    context "無限未来の場合" do
      context "無限小の数値と比較" do
        it{ expect(positive).to be > -Float::INFINITY }
      end

      context "無限大の数値と比較" do
        it{ expect(positive).to be == Float::INFINITY }
      end

      context "無限過去日付オブジェクトと比較" do
        it{ expect(positive).to be > negative }
      end

      context "無限未来日付オブジェクトと比較" do
        it{ expect(positive).to be == positive }
      end
    end
  end
end
