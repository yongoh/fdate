require 'spec_helper.rb'

describe FDate::Term::BuildMethods do
  let(:owner){ double("Owner").extend described_class }

  describe "#_build" do
    subject{ owner._build(arg) }

    context "Rangeオブジェクトを渡した場合" do
      let(:arg){ 1234567..7654321 }

      it "は、期間クラスのインスタンス化用ハッシュを返すこと" do
        is_expected.to eq({first: 1234567, last: 7654321, exclude_end: false})
      end
    end

    context "日付オブジェクトを渡した場合" do
      let(:calendar){ FDate::Calendar.collection["adgr"] }
      let(:arg){ calendar.new(year: {num: 1955}, month: 5, day: {num: 20, accuracy: 1}) }

      it "は、期間クラスのインスタンス化用ハッシュを返すこと" do
        is_expected.to match(
          first: an_object_eql_to(calendar.new(year: {num: 1955}, month: 5, day: 20)),
          last: an_object_eql_to(calendar.new(year: {num: 1955}, month: 5, day: 29)),
        )
      end
    end

    context "インスタンス化できないオブジェクトを渡した場合" do
      it "は、型エラーを発生させること" do
        expect{ owner.build(Time.now) }.to raise_error(TypeError, "can not convert Time into Hash.")
      end
    end
  end

  describe "#parseable?" do
    context "ユリウス通日コードを渡した場合" do
      it{ expect(owner.parseable?("(12345678)")).to be_truthy }
    end

    context "ユリウス通日範囲コードを渡した場合" do
      it{ expect(owner.parseable?("(2424875...2447892)")).to be_truthy }
    end

    context "ローカルコードを渡した場合" do
      it{ expect(owner.parseable?("hoge~piyo")).to be_truthy }
    end

    context "不正なコードを渡した場合" do
      it{ expect(owner.parseable?("1a345xf00")).to be_falsey }
    end
  end

  describe "#_global_parse" do
    let(:calendar){ FDate::Calendar.collection["adgr"] }

    context "ユリウス通日コードを渡した場合" do
      it "は、両端が同じ日付の期間クラスのインスタンス化用ハッシュを返すこと" do
        expect(owner._global_parse("(2424875)")).to match(
          first: an_object_eql_to(calendar.jd(2424875)),
          last: an_object_eql_to(calendar.jd(2424875)),
        )
      end
    end

    context "ユリウス通日範囲コードを渡した場合" do
      it "は、渡した範囲の期間クラスのインスタンス化用ハッシュを返すこと" do
        expect(owner._global_parse("(2424875..2447892)")).to match(
          first: an_object_eql_to(calendar.jd(2424875)),
          last: an_object_eql_to(calendar.jd(2447892)),
          exclude_end: false,
        )
      end
    end

    context "ユリウス通日範囲コード（終端を含まない）を渡した場合" do
      it "は、渡した範囲の期間クラスのインスタンス化用ハッシュを返すこと" do
        expect(owner._global_parse("(2424875...2447892)")).to match(
          first: an_object_eql_to(calendar.jd(2424875)),
          last: an_object_eql_to(calendar.jd(2447892)),
          exclude_end: true,
        )
      end
    end

    context "無限日付コードを含むユリウス通日範囲コードを渡した場合" do
      it "は、無限日付オブジェクトを含む期間クラスのインスタンス化用ハッシュを返すこと" do
        expect(owner._global_parse("(2424875..+Infinity)")).to match(
          first: an_object_eql_to(calendar.jd(2424875)),
          last: an_object_eql_to(FDate::InfinityDate.positive),
          exclude_end: false,
        )
      end
    end

    context "オプションに暦キーを渡した場合" do
      let(:calendar){ FDate::Calendar.collection["adjl"] }

      it "は、渡した暦の日付オブジェクトを含む期間クラスのインスタンス化用ハッシュを返すこと" do
        expect(owner._global_parse("(2424875...2447892)", calendar: "adjl")).to match(
          first: an_object_eql_to(calendar.jd(2424875)),
          last: an_object_eql_to(calendar.jd(2447892)),
          exclude_end: true,
        )
      end
    end
  end

  describe "#_local_parse" do
    subject{ owner._local_parse(code) }

    shared_examples_for "parse through" do
      it "は、期間クラスのインスタンス化用ハッシュを返すこと" do
        is_expected.to eq({first: "hoge", last: "piyo", between: false})
      end
    end

    shared_examples_for "parse between" do
      it "は、期間クラスのインスタンス化用ハッシュを返すこと" do
        is_expected.to eq({first: "hoge", last: "piyo", between: true})
      end
    end


    context "through(~)の場合" do
      let(:code){ "hoge~piyo" }
      it_behaves_like "parse through"
    end

    context "through(&)の場合" do
      let(:code){ "hoge&piyo" }
      it_behaves_like "parse through"
    end

    context "between(^)の場合" do
      let(:code){ "hoge^piyo" }
      it_behaves_like "parse between"
    end

    context "between(|)の場合" do
      let(:code){ "hoge|piyo" }
      it_behaves_like "parse between"
    end

    context "日付グローバルコードを含むコードの場合" do
      let(:code){ "(-1234567)~(7654321)" }

      it "は、期間クラスのインスタンス化用ハッシュを返すこと" do
        is_expected.to eq({first: "(-1234567)", last: "(7654321)", between: false})
      end
    end

    context "波括弧つきの場合" do
      let(:code){ "{hoge}~{piyo}" }
      it_behaves_like "parse through"
    end

    context "first,last部が空文字の場合（nil -> infinity）" do
      let(:code){ "~" }

      it "は、期間クラスのインスタンス化用ハッシュを返すこと" do
        is_expected.to match(
          first: an_object_eql_to(FDate::InfinityDate.negative),
          last: an_object_eql_to(FDate::InfinityDate.positive),
          between: false,
        )
      end
    end

    context "デリミタを間違えた場合" do
      let(:code){ "hoge..piyo" }

      it "は、パースエラーを発生させること" do
        expect{ subject }.to raise_error(FDate::ParseError, "It's wrong grammar. \"hoge..piyo\"")
      end
    end
  end

  describe "#_jd" do
    let(:calendar){ FDate::Calendar.collection["adgr"] }

    context "ユリウス通日を渡した場合" do
      context "オプション無しの場合" do
        it "は、両端ともその日付のインスタンス化用ハッシュを返すこと" do
          expect(owner._jd(2424875)).to match(
            first: an_object_eql_to(calendar.jd(2424875)),
            last: an_object_eql_to(calendar.jd(2424875)),
          )
        end
      end

      context "オプションに暦キーを渡した場合" do
        let(:adjl){ FDate::Calendar.collection["adjl"] }

        it "は、両端とも指定した暦で渡した日付のインスタンス化用ハッシュを返すこと" do
          expect(owner._jd(2424875, calendar: "adjl")).to match(
            first: an_object_eql_to(adjl.jd(2424875)),
            last: an_object_eql_to(adjl.jd(2424875)),
          )
        end
      end
    end

    context "ユリウス通日範囲を渡した場合" do
      context "終端を含む範囲を渡した場合" do
        it "は、両端の日付オブジェクトを持つインスタンス化用ハッシュを返すこと" do
          expect(owner._jd(2424875..2447892)).to match(
            first: an_object_eql_to(calendar.jd(2424875)),
            last: an_object_eql_to(calendar.jd(2447892)),
            exclude_end: false,
          )
        end
      end

      context "終端を含まない範囲を渡した場合" do
        it "は、両端の日付オブジェクトを持つインスタンス化用ハッシュを返すこと" do
          expect(owner._jd(2424875...2447892)).to match(
            first: an_object_eql_to(calendar.jd(2424875)),
            last: an_object_eql_to(calendar.jd(2447892)),
            exclude_end: true,
          )
        end
      end
    end
  end
end
