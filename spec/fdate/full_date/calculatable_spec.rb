require 'spec_helper.rb'

describe FDate::FullDate::Calculatable do
  let(:calendar){ FDate::Calendar.collection["adgr"] }
  let(:fdate){ calendar.new(year: {num: 2017}, month: 1, day: 17) }


  describe "#+" do
    context "自身がfixedの場合" do
      context "数値`5`を渡した場合" do
        it "は、5日後の日付オブジェクトを返すこと" do
          expect(fdate + 5).to eql(calendar.new(year: {num: 2017}, month: 1, day: 22))
        end
      end

      context "数値`60`を渡した場合" do
        it "は、60日後の日付オブジェクトを返すこと" do
          expect(fdate + 60).to eql(calendar.new(year: {num: 2017}, month: 3, day: 18))
        end
      end

      context "数値`-60`を渡した場合" do
        it "は、60日前の日付オブジェクトを返すこと" do
          expect(fdate + -60).to eql(calendar.new(year: {num: 2016}, month: 11, day: 18))
        end
      end

      context "日付オブジェクトを渡した場合" do
        let(:other){ calendar.new(year: {num: 2017}, month: 3, day: 18) }

        it "は、型エラーを発生させること" do
          expect{ fdate + other }.to raise_error(TypeError, "FDate::Calendar[adgr] can't be coerced into Fixnum")
        end
      end
    end

    context "自身がfuzzyの場合" do
      let(:fdate){ calendar.new(year: {num: 2017}, month: 3, day: {num: 20, accuracy: 1}) }

      context "数値を渡した場合" do
        it "は、渡した数値分未来の期間オブジェクトを返すこと" do
          expect(fdate + 5).to eql(FDate::Term.new(
            first: calendar.new(year: {num: 2017}, month: 3, day: 25),
            last: calendar.new(year: {num: 2017}, month: 4, day: 3),
          ))
        end
      end

      context "日付オブジェクトを渡した場合" do
        let(:other){ calendar.new(year: {num: 2017}, month: 3, day: 18) }

        it "は、型エラーを発生させること" do
          expect{ fdate + other }.to raise_error(TypeError, "FDate::Calendar[adgr] can't be coerced into Fixnum")
        end
      end
    end
  end

  describe "#-" do
    context "自身がfixedの場合" do
      context "数値を渡した場合" do
        context "数値`5`を渡した場合" do
          it "は、5日前の日付オブジェクトを返すこと" do
            expect(fdate - 5).to eql(calendar.new(year: {num: 2017}, month: 1, day: 12))
          end
        end

        context "数値`60`を渡した場合" do
          it "は、60日前の日付オブジェクトを返すこと" do
            expect(fdate - 60).to eql(calendar.new(year: {num: 2016}, month: 11, day: 18))
          end
        end

        context "数値`-60`を渡した場合" do
          it "は、60日後の日付オブジェクトを返すこと" do
            expect(fdate - -60).to eql(calendar.new(year: {num: 2017}, month: 3, day: 18))
          end
        end
      end

      context "日付オブジェクトを渡した場合" do
        context "自身より前の日付を渡した場合" do
          let(:other){ calendar.new(year: {num: 2017}, month: 1, day: 3) }

          it "は、引数から自身までの日数を返すこと" do
            expect(fdate - other).to equal(14)
          end
        end

        context "自身より後の日付を渡した場合" do
          let(:other){ calendar.new(year: {num: 2017}, month: 2, day: 5) }

          it "は、引数から自身までの日数（負数）を返すこと" do
            expect(fdate - other).to equal(-19)
          end
        end

        context "自身とは違う暦の日付オブジェクトを渡した場合" do
          let(:other){ FDate::Calendar.collection["adjl"].new(year: {num: 2017}, month: 1, day: 1) }

          it "は、引数から自身までの日数を返すこと" do
            expect(fdate - other).to equal(3)
          end
        end

        context "fuzzyな日付を渡した場合" do
          let(:other){ calendar.new(year: {num: 2017}, month: 1, day: {num: 0, accuracy: 1}) }

          it "は、引数から自身までの日数の範囲を返すこと" do
            expect(fdate - other).to eq(8..16)
          end
        end
      end
    end

    context "自身がfuzzyの場合" do
      let(:fdate){ calendar.new(year: {num: 2017}, month: 3, day: {num: 20, accuracy: 1}) }

      context "数値を渡した場合" do
        it "は、渡した数値分過去の期間オブジェクトを返すこと" do
          expect(fdate - 5).to eql(FDate::Term.new(
            first: calendar.new(year: {num: 2017}, month: 3, day: 15),
            last: calendar.new(year: {num: 2017}, month: 3, day: 24),
          ))
        end
      end

      context "日付オブジェクトを渡した場合" do
        context "自身より前の日付を渡した場合" do
          let(:other){ calendar.new(year: {num: 2017}, month: 3, day: 18) }

          it "は、引数から自身までの日数の範囲を返すこと" do
            expect(fdate - other).to eq(2..11)
          end
        end

        context "自身とは違う暦の日付オブジェクトを渡した場合" do
          let(:other){ FDate::Calendar.collection["adjl"].new(year: {num: 2017}, month: 3, day: 1) }

          it "は、引数から自身までの日数の範囲を返すこと" do
            expect(fdate - other).to eq(6..15)
          end
        end

        context "fuzzyな日付を渡した場合" do
          let(:other){ calendar.new(year: {num: 2017}, month: 3, day: {num: 0, accuracy: 1}) }

          it "は、引数から自身までの日数の範囲を返すこと" do
            expect(fdate - other).to eq(19..20)
          end
        end
      end
    end
  end
end
