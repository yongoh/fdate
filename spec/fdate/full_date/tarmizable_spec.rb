require 'spec_helper.rb'

describe FDate::FullDate::Tarmizable do
  let(:calendar){ FDate::Calendar.collection["adjl"] }
  let(:first){ calendar.new(year: {num: 1925}, month: 12, day: 25) }
  let(:last){ calendar.new(year: {num: 1989}, month: 1, day: 7) }


  describe "#to_dr" do
    shared_examples_for "return Term" do
      it "は、期間を期間オブジェクトとして返すこと" do
        expect(fdate.to_dr).to eql(expected)
      end
    end

    subject{ fdate.to_dr }

    context "fixedの場合" do
      let(:fdate){ first }
      let(:expected){ FDate::Term.new(first: first, last: first) }
      it_behaves_like "return Term"
    end

    context "fuzzyの場合" do
      let(:fdate){ calendar.new(year: {num: 1920, accuracy: 1}) }
      let(:expected){
        FDate::Term.new(
          first: calendar.new(year: {num: 1920}, month: 1, day: 1),
          last: calendar.new(year: {num: 1929}, month: 12, day: 31)
        )
      }
      it_behaves_like "return Term"
    end
  end


  describe "DateRange生成メソッド群" do
    shared_examples_for "return Term from self to other" do
      it "は、自身から引数の日付までの期間オブジェクトを返すこと" do
        is_expected.to eql(FDate::Term.new(first: first, last: last))
        is_expected.not_to be_exclude_end
        is_expected.to be_through
      end
    end

    shared_examples_for "return Term from other to self" do
      it "は、引数の日付から自身までの期間オブジェクトを返すこと" do
        is_expected.to eql(FDate::Term.new(first: first, last: last))
        is_expected.not_to be_exclude_end
        is_expected.to be_through
      end
    end

    shared_examples_for "return Term with options" do
      it "は、オプション付きで生成した期間オブジェクトを返すこと" do
        is_expected.to eql(FDate::Term.new(first: first, last: last, exclude_end: true, between: true))
        is_expected.to be_exclude_end
        is_expected.not_to be_through
      end
    end


    describe "#to" do
      context "オプションを渡さない場合" do
        subject{ first.to(last) }
        it_behaves_like "return Term from self to other"
      end

      context "オプションを渡した場合" do
        subject{ first.to(last, exclude_end: true, between: true) }
        it_behaves_like "return Term with options"
      end
    end

    describe "#>>" do
      subject{ first >> last }
      it_behaves_like "return Term from self to other"
    end

    describe "#from" do
      context "オプションを渡さない場合" do
        subject{ last.from(first) }
        it_behaves_like "return Term from other to self"
      end

      context "オプションを渡した場合" do
        subject{ last.from(first, exclude_end: true, between: true) }
        it_behaves_like "return Term with options"
      end
    end

    describe "#<<" do
      subject{ last << first }
      it_behaves_like "return Term from other to self"
    end

    describe "#&" do
      subject{ first & last }

      it "は、自身から引数の日付まで（ずっと）の期間オブジェクトを返すこと" do
        is_expected.to eql(FDate::Term.new(first: first, last: last))
        is_expected.not_to be_exclude_end
        is_expected.to be_through
      end
    end

    describe "#|" do
      subject{ first | last }

      it "は、自身から引数の日付まで（いずれか）の期間オブジェクトを返すこと" do
        is_expected.to eql(FDate::Term.new(first: first, last: last, between: true))
        is_expected.not_to be_exclude_end
        is_expected.not_to be_through
      end
    end
  end
end
