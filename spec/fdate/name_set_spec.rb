require 'spec_helper.rb'

describe FDate::NameSet do
  let(:name_set){
    described_class.new({
      0 => "日曜日",
      1 => "月曜日",
      2 => "火曜日",
      3 => "水曜日",
      4 => "木曜日",
      5 => "金曜日",
      6 => "土曜日",
    })
  }

  let(:object){ double("Calendar", day_of_week: 3) }

  describe "#key_of" do
    context "`@key_proc`を設定していない場合" do
      it "は、メソッド未定義エラーを発生させること" do
        expect{ name_set.key_of(object) }.to raise_error(NoMethodError, "undefined method `call' for nil:NilClass")
      end
    end

    context "`@key_proc`にProcが設定されている場合" do
      before do
        name_set.key_proc = :day_of_week.to_proc
      end

      it "は、`#key_proc`に引数を渡して名前のキーを返すこと" do
        expect(name_set.key_of(object)).to eq(3)
      end
    end
  end

  describe "#[]" do
    before do
      name_set.key_proc = :day_of_week.to_proc
    end

    context "`#key_of`が返したキーが存在する場合" do
      it "は、`#key_of`から得られたキーで名前を取得すること" do
        expect(name_set[object]).to eq("水曜日")
      end
    end

    context "`#key_of`が返したキーが存在しない場合" do
      let(:object){ double("Calendar", day_of_week: 10) }
      it{ expect(name_set[object]).to be_nil }
    end

    context "`#key_of`が`@names`に存在しないキーを返した場合" do
      let(:object){ double("Calendar", day_of_week: nil) }

      context "デフォルト値を設定していない場合" do
        it "は、nilを返すこと" do
          expect(name_set[object]).to eq(name_set.names.default).and be_nil
        end
      end

      context "デフォルト値を設定している場合" do
        before do
          name_set.default = "ほげ"
        end

        it "は、デフォルト値を返すこと" do
          expect(name_set[object]).to eq(name_set.names.default).and eq("ほげ")
        end
      end
    end
  end

  describe "::build" do
    context "キー'names'に" do
      let(:hash){
        {
          0 => "日曜日",
          1 => "月曜日",
          2 => "火曜日",
          3 => "水曜日",
          4 => "木曜日",
          5 => "金曜日",
          6 => "土曜日",
        }
      }

      context "ハッシュを渡した場合" do
        let(:name_set){ described_class.build("names" => hash) }

        it "は、`@names`に渡したハッシュを割り当てること" do
          expect(name_set.names).to eq(hash)
        end
      end

      context "配列を渡した場合" do
        let(:name_set){
          described_class.build({
            "names" => [
              "日曜日",
              [1, "月曜日"],
              "火曜日",
              [3, "水曜日"],
              "木曜日",
              [5, "金曜日"],
              "土曜日",
            ],
          })
        }

        it "は、`@names`にインデックスをキーにハッシュ化したものを割り当てること" do
          expect(name_set.names).to eq(hash)
        end
      end
    end

    context "キー'default'を渡した場合" do
      before do
        name_set.key_proc = ->(obj){ obj }
      end

      let(:name_set){ described_class.build("default" => "でふぉると") }

      it "は、デフォルト値を設定すること" do
        expect(name_set.default).to eq("でふぉると")
        expect(name_set[object]).to eq("でふぉると")
      end
    end

    context "キー'default_proc'を渡した場合" do
      before do
        name_set.key_proc = ->(obj){ obj }
      end

      context "Procを渡した場合" do
        let(:name_set){ described_class.build("default_proc" => ->(key, hash){ "でふぉると" }) }

        it "は、デフォルトProcを設定すること" do
          expect(name_set.default_proc).to be_a(Proc)
          expect(name_set[object]).to eq("でふぉると")
        end
      end

      context "シンボルを渡した場合" do
        let(:name_set){ described_class.build("default_proc" => :day_of_week) }

        it "は、渡したシンボルの名前のメソッドをデフォルトProcに設定すること" do
          expect(name_set.default_proc).to be_a(Proc)
          expect(name_set[object]).to eq(3).and(eq(object.day_of_week))
        end
      end

      xcontext "文字列を渡した場合" do
        let(:name_set){ described_class.build("default_proc" => "->(obj){ obj.day_of_week + 2 }") }

        it "は、渡した文字列を`Kernel#eval`して得られたProcをデフォルトProcに設定すること" do
          expect(name_set.default_proc).to be_a(Proc)
          expect(name_set[object]).to eq(5)
        end
      end
    end

    context "キー'key_proc'に" do
      context "Procを渡した場合" do
        let(:name_set){ described_class.build("key_proc" => ->(obj){ 5 }) }

        it "は、渡したProcをキー作成Procに設定すること" do
          expect(name_set.key_proc).to be_a(Proc)
          expect(name_set.key_of(object)).to eq(5)
        end
      end

      context "シンボルを渡した場合" do
        let(:name_set){ described_class.build("key_proc" => :day_of_week) }

        it "は、渡したシンボルの名前のメソッドをキー作成Procに設定すること" do
          expect(name_set.key_proc).to be_a(Proc)
          expect(name_set.key_of(object)).to eq(3).and(eq(object.day_of_week))
        end
      end

      xcontext "文字列を渡した場合" do
        let(:name_set){ described_class.build("key_proc" => "->(obj){ obj.day_of_week + 2 }") }

        it "は、渡した文字列を`Kernel#eval`して得られたProcをキー作成Procに設定すること" do
          expect(name_set.key_proc).to be_a(Proc)
          expect(name_set.key_of(object)).to eq(5)
        end
      end
    end
  end
end
