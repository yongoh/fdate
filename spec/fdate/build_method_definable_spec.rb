require 'spec_helper.rb'

describe FDate::BuildMethodDefinable do
  let(:klass){ Struct.new(:arg) }

  describe "#build_methods" do
    context "クラスメソッド用モジュールにextendした場合" do
      before do
        klass.extend class_methods_module
      end

      let(:class_methods_module){
        mod = described_class

        Module.new do
          extend mod

          build_methods :hoge

          def _hoge(arg)
            "ほ#{arg}げ"
          end
        end
      }

      it "は、渡した名前のメソッドを定義すること" do
        expect(class_methods_module).to be_method_defined(:hoge)
        expect(klass).to be_respond_to(:hoge)
      end

      it "は、引数を`::_{name}`に渡してその結果でインスタンスを作成するメソッドを定義すること" do
        result = klass.hoge("(´･ω･｀)")

        expect(result).to be_instance_of(klass)
        expect(result.arg).to eq("ほ(´･ω･｀)げ")
      end
    end
  end

  context "クラスに直接extendした場合" do
    before do
      mod = described_class

      klass.class_eval do
        extend mod

        build_methods :piyo

        def _piyo(arg)
          "ぴ#{arg}よ"
        end
      end
    end

    let(:instance){ klass.new("(・∀・)") }

    it "は、渡した名前のメソッドを定義すること" do
      expect(klass).to be_method_defined(:piyo)
      expect(instance).to be_respond_to(:piyo)
    end

    it "は、引数を`#_{name}`に渡してその結果で同じクラスのインスタンスを作成するメソッドを定義すること" do
      result = instance.piyo("(´･ω･｀)")

      expect(result).to be_instance_of(klass)
      expect(result.arg).to eq("ぴ(´･ω･｀)よ")
    end
  end
end
