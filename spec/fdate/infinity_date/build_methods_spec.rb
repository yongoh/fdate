require 'spec_helper.rb'

describe FDate::InfinityDate::BuildMethods do
  let(:owner){ double("Owner").extend described_class }

  let(:positive){ double("InfinityDate(Positive)") }
  let(:negative){ double("InfinityDate(Negative)") }

  before do
    allow(owner).to receive(:new){|x| x ? positive : negative }
  end

  shared_examples_for "return positive" do
    it{ should equal(positive) }
  end

  shared_examples_for "return negative" do
    it{ should equal(negative) }
  end


  describe "#parseable?" do
    context "+符号付きコードを渡した場合" do
      it{ expect(owner.parseable?("(+Infinity)")).to be_truthy }
    end

    context "-符号付きコードを渡した場合" do
      it{ expect(owner.parseable?("(-Infinity)")).to be_truthy }
    end

    context "符号なしコードを渡した場合" do
      it{ expect(owner.parseable?("(Infinity)")).to be_truthy }
    end

    context "不正なコードを渡した場合" do
      it{ expect(owner.parseable?("1a345xf00")).to be_falsey }
    end
  end

  describe "#_parse" do
    subject{ owner._parse(code) }

    context "+符号付きコードを渡した場合" do
      it{ expect(owner._parse("(+Infinity)")).to be_truthy }
    end

    context "-符号付きコードを渡した場合" do
      it{ expect(owner._parse("(-Infinity)")).to be_falsey }
    end

    context "符号なしコードを渡した場合" do
      it{ expect(owner._parse("(Infinity)")).to be_truthy }
    end
  end

  describe "#positive" do
    subject{ owner.positive }
    it_behaves_like "return positive"
  end

  describe "#negative" do
    subject{ owner.negative }
    it_behaves_like "return negative"
  end

  describe "#+@" do
    subject{ +owner }
    it_behaves_like "return positive"
  end

  describe "#-@" do
    subject{ -owner }
    it_behaves_like "return negative"
  end
end
