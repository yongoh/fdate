require 'spec_helper.rb'

describe FDate::Part do
  let(:klass){
    r = max_range
    Class.new(described_class) do
      self.key = :key
      self.max_range = r
    end
  }
  let(:max_range){ -25..25 }


  describe "#initialize" do
    shared_examples_for "fixed" do
      it "は、誤差無しであること" do
        expect(part).to be_fixed
        expect(part).not_to be_fuzzy
        expect(part).not_to be_nildate
        expect(part.accuracy).to eq(0)
        expect(part.about).to be_nil
      end
    end

    shared_examples_for "fuzzy" do
      it "は、誤差ありであること" do
        expect(part).not_to be_fixed
        expect(part).to be_fuzzy
        expect(part).not_to be_nildate
      end
    end

    shared_examples_for "nildate" do
      it "は、NilDateであること" do
        expect(part).not_to be_fixed
        expect(part).to be_fuzzy
        expect(part).to be_nildate
      end
    end

    shared_examples_for "about is 'j'" do
      it{ expect(part.about).to equal(FDate::About.collection['j']) }
    end


    context "`accuracy: 0(default)`" do
      context "と`num: (許容範囲内の正数)`でインスタンス化した場合" do
        let(:part){ klass.new(num: 10) }

        it{ expect(part.global_range).to eq(10..10) }
        it_behaves_like "fixed"
      end

      context "と`num: (許容範囲内の負数)`でインスタンス化した場合" do
        let(:part){ klass.new(num: -10) }

        it{ expect(part.global_range).to eq(-10..-10) }
        it_behaves_like "fixed"
      end

      context "と`num: 0`でインスタンス化した場合" do
        let(:part){ klass.new(num: 0) }

        it{ expect(part.global_range).to eq(0..0) }
        it_behaves_like "fixed"
      end

      context "と`num: (許容範囲外の正数)`でインスタンス化した場合" do
        it "は、`RangeError`を発生させること" do
          expect{
            klass.new(num: 30)
          }.to raise_error(FDate::Part::RangeError, "30..30 deviates from -25..25")
        end
      end

      context "と`num: (許容範囲外の負数)`でインスタンス化した場合" do
        it "は、`RangeError`を発生させること" do
          expect{
            klass.new(num: -30)
          }.to raise_error(FDate::Part::RangeError, "-30..-30 deviates from -25..25")
        end
      end
    end

    context "`accuracy: 1`" do
      shared_examples_for "accuracy is 1" do
        it{ expect(part.accuracy).to eq(1) }
      end

      context "と`num: (許容範囲内の正数)`でインスタンス化した場合" do
        let(:part){ klass.new(num: 10, accuracy: 1) }

        it{ expect(part.global_range).to eq(10..19) }
        it_behaves_like "accuracy is 1"
        it_behaves_like "about is 'j'"
        it_behaves_like "fuzzy"
      end

      context "と`num: (許容範囲内外にまたがる数)`" do
        context "と`about: nil(default)`でインスタンス化した場合" do
          let(:part){ klass.new(num: 20, accuracy: 1) }

          it{ expect(part.global_range).to eq(20..25) }
          it_behaves_like "accuracy is 1"
          it_behaves_like "about is 'j'"
          it_behaves_like "fuzzy"
        end

        context "と`about: '1-5'`でインスタンス化した場合" do
          let(:part){ klass.new(num: 20, accuracy: 1, about: '1-5') }

          it{ expect(part.global_range).to eq(20..21) }
          it_behaves_like "accuracy is 1"
          it{ expect(part.about).to equal(FDate::About.collection['1-5']) }
          it_behaves_like "fuzzy"
        end

        context "と`about: '5-5'`でインスタンス化した場合" do
          it "は、`RangeError`を発生させること" do
            expect{
              klass.new(num: 20, accuracy: 1, about: '5-5')
            }.to raise_error(FDate::Part::RangeError, "28..29 deviates from -25..25")
          end
        end

        context "と`about: '2-2'`でインスタンス化した場合" do
          let(:part){ klass.new(num: 20, accuracy: 1, about: '2-2') }

          it{ expect(part.global_range).to eq(25..25) }
          it_behaves_like "accuracy is 1"
          it{ expect(part.about).to equal(FDate::About.collection['2-2']) }
          it_behaves_like "fuzzy"
        end
      end

      context "と`num: (許容範囲外の正数)`でインスタンス化した場合" do
        it "は、`RangeError`を発生させること" do
          expect{
            klass.new(num: 30, accuracy: 1)
          }.to raise_error(FDate::Part::RangeError, "30..39 deviates from -25..25")
        end
      end

      context "と`num: -1`でインスタンス化した場合" do
        let(:part){ klass.new(num: -1, accuracy: 1) }

        it{ expect(part.global_range).to eq(-9..0) }
        it_behaves_like "accuracy is 1"
        it_behaves_like "about is 'j'"
        it_behaves_like "fuzzy"
      end

      context "と`num: -10`でインスタンス化した場合" do
        let(:part){ klass.new(num: -10, accuracy: 1) }

        it{ expect(part.global_range).to eq(-19..-10) }
        it_behaves_like "accuracy is 1"
        it_behaves_like "about is 'j'"
        it_behaves_like "fuzzy"
      end

      context "と`num: (許容範囲内外にまたがる数)`" do
        context "と`about: nil(default)`でインスタンス化した場合" do
          let(:part){ klass.new(num: -20, accuracy: 1) }

          it{ expect(part.global_range).to eq(-25..-20) }
          it_behaves_like "accuracy is 1"
          it_behaves_like "about is 'j'"
          it_behaves_like "fuzzy"
        end

        context "と`about: '5-5'`でインスタンス化した場合" do
          let(:part){ klass.new(num: -20, accuracy: 1, about: '5-5') }

          it{ expect(part.global_range).to eq(-21..-20) }
          it_behaves_like "accuracy is 1"
          it{ expect(part.about).to equal(FDate::About.collection['5-5']) }
          it_behaves_like "fuzzy"
        end

        context "と`about: '1-5'`でインスタンス化した場合" do
          it "は、`RangeError`を発生させること" do
            expect{
              klass.new(num: -20, accuracy: 1, about: '1-5')
            }.to raise_error(FDate::Part::RangeError, "-29..-28 deviates from -25..25")
          end
        end

        context "と`about: '1-2'`でインスタンス化した場合" do
          let(:part){ klass.new(num: -20, accuracy: 1, about: '1-2') }

          it{ expect(part.global_range).to eq(-25..-25) }
          it_behaves_like "accuracy is 1"
          it{ expect(part.about).to equal(FDate::About.collection['1-2']) }
          it_behaves_like "fuzzy"
        end
      end

      context "と`num: (許容範囲外の負数)`でインスタンス化した場合" do
        it "は、`RangeError`を発生させること" do
          expect{
            klass.new(num: -30, accuracy: 1)
          }.to raise_error(FDate::Part::RangeError, "-39..-30 deviates from -25..25")
        end
      end

      context "と`num: 0`でインスタンス化した場合" do
        let(:part){ klass.new(num: 0, accuracy: 1) }

        it{ expect(part.global_range).to eq(0..9) }
        it_behaves_like "accuracy is 1"
        it_behaves_like "about is 'j'"
        it_behaves_like "fuzzy"
      end
    end

    context "と`num: nil`" do
      context "と`about: nil(default)`でインスタンス化した場合" do
        let(:part){ klass.new(num: nil) }

        it{ expect(part.global_range).to eq(-25..25) }
        it{ expect(part.accuracy).to eq(Float::INFINITY) }
        it_behaves_like "about is 'j'"
        it_behaves_like "nildate"
      end

      context "と`about: '1-2'`でインスタンス化した場合" do
        let(:part){ klass.new(num: nil, about: '1-2') }

        it{ expect(part.global_range).to eq(-25..0) }
        it{ expect(part.accuracy).to eq(Float::INFINITY) }
        it{ expect(part.about).to equal(FDate::About.collection['1-2']) }
        it_behaves_like "nildate"
      end

      context "と`about: '5-5'`でインスタンス化した場合" do
        let(:part){ klass.new(num: nil, about: '5-5') }

        it{ expect(part.global_range).to eq(15..25) }
        it{ expect(part.accuracy).to eq(Float::INFINITY) }
        it{ expect(part.about).to equal(FDate::About.collection['5-5']) }
        it_behaves_like "nildate"
      end
    end

    describe "#about" do
      let(:about_key){ '1-2' }

      shared_examples_for "about is '1-2'" do
        it{ expect(part.about).to equal(FDate::About.collection['1-2']) }
      end

      context "fixedの場合" do
        let(:part){ klass.new(num: 15, accuracy: 0, about: about_key) }

        it{ expect(part.global_range).to eq(15..15) }
        it_behaves_like "fixed"
      end

      context "fuzzyの場合" do
        let(:part){ klass.new(num: 20, accuracy: 1, about: about_key) }

        context "aboutを掛けたrange（'2*'前半）が許容範囲内の場合" do
          it{ expect(part.global_range).to eq(20..24) }
          it{ expect(part.accuracy).to eq(1) }
          it_behaves_like "about is '1-2'"
          it_behaves_like "fuzzy"
        end

        context "aboutを掛けたrange（'2*'後期）が許容範囲外の場合" do
          let(:about_key){ "5-5" }
          it{ expect{ part }.to raise_error(FDate::Part::RangeError) }
        end
      end

      context "Nildateの場合" do
        let(:max_range){ 1..12 }
        let(:part){ klass.new(num: nil, about: about_key) }

        it{ expect(part.global_range).to eq(1..6) }
        it{ expect(part.accuracy).to eq(Float::INFINITY) }
        it_behaves_like "about is '1-2'"
        it_behaves_like "nildate"
      end
    end
  end

  describe "#eql?" do
    let(:part){ klass.new(num: 10) }

    shared_examples_for "eql" do
      it{ expect(part).to eql(other) }
    end

    shared_examples_for "not eql" do
      it{ expect(part).not_to eql(other) }
    end

    context "自身と比較対象のクラス,`#global_range`,`#accuracy`,`#about`が同じ場合" do
      let(:other){ klass.new(num: 10) }
      it_behaves_like "eql"
    end

    context "自身と比較対象の`#global_range`が異なる場合" do
      let(:other){ klass.new(num: 12) }
      it_behaves_like "not eql"
    end

    context "自身と比較対象の`#accuracy`が異なる場合" do
      let(:other){ klass.new(num: 10, accuracy: 1) }
      it_behaves_like "not eql"
    end

    context "自身と比較対象の`#about`が異なる場合" do
      let(:part){ klass.new(num: 0, accuracy: 1, about: "1-2") }
      let(:other){ klass.new(num: 0, accuracy: 1, about: "2-2") }
      it_behaves_like "not eql"
    end

    context "比較対象が自身のクラスのサブクラスのインスタンスである場合" do
      let(:sub_class){ Class.new(klass) }
      let(:other){ sub_class.new(num: 10) }
      it_behaves_like "eql"
    end

    context "異なるキーを持つクラスのインスタンス同士の場合" do
      let(:other){ klass.inherits_to(:piyo).new(num: 10) }
      it_behaves_like "not eql"
    end
  end

  describe "range系メソッド群" do
    shared_examples_for "direction methods" do
      ja_directions = {
        "first" => "最初",
        "last" => "最後",
        "begin" => "最初",
        "end" => "最後",
        "min" => "最初",
        "max" => "最後",
      }

      let(:n){ 5 }
      let(:block){ ->(a, b){ a <=> b } }

      %w(first last begin end min max).each do |direction|
        describe "##{direction}" do
          context "引数なしの場合" do
            subject{ part.send(direction) }

            it "は、とりうる期間のうち#{ja_directions[direction]}の日付パートオブジェクトを返すこと" do
              is_expected.to eql(klass.new(num: part.global_range.send(direction)))
              is_expected.to be_instance_of(klass)
              is_expected.to be_fixed
            end
          end

          context "引数で取得する数を指定した場合" do
            subject{ part.send(direction, n) }

            it "は、とりうる数値範囲のうち#{ja_directions[direction]}からn番目までの日付パートオブジェクトの配列を返すこと" do
              is_expected.to eql(part.global_range.send(direction, n).map{|i| klass.new(num: i) })
              is_expected.to all(be_instance_of(klass))
              is_expected.to all(be_fixed)
            end
          end if direction.match(/first|last/)

          context "ブロックで比較した場合" do
            subject{ part.send(direction, &block) }

            it "は、ブロックの条件でソートした期間の内#{ja_directions[direction]}の日付パートオブジェクトを返すこと" do
              is_expected.to eql(klass.new(num: part.global_range.send(direction)))
              is_expected.to be_instance_of(klass)
              is_expected.to be_fixed
            end
          end if direction.match(/min|end/)
        end

        describe "##{direction}_i" do
          context "引数なしの場合" do
            subject{ part.send("#{direction}_i") }

            it "は、とりうる数値範囲のうち#{ja_directions[direction]}の整数を返すこと" do
              is_expected.to eql(part.global_range.send(direction))
              is_expected.to be_a(Integer)
            end
          end

          context "引数で取得する数を指定した場合" do
            subject{ part.send("#{direction}_i", n) }

            it "は、とりうる数値範囲のうち#{ja_directions[direction]}からn番目までの整数の配列を返すこと" do
              is_expected.to eql(part.global_range.send(direction, n))
              is_expected.to all(be_a(Integer))
            end
          end if direction.match(/first|last/)

          context "ブロックで比較した場合" do
            subject{ part.send("#{direction}_i") }

            it "は、ブロックの条件でソートした数値範囲の内#{ja_directions[direction]}の整数を返すこと" do
              is_expected.to eql(part.global_range.send(direction, &block))
              is_expected.to be_a(Integer)
            end
          end if direction.match(/min|end/)
        end
      end
    end

    context "fixedの場合" do
      let(:part){ klass.new(num: 10) }
      it_behaves_like "direction methods"
    end

    context "fuzzy, 正の値の場合" do
      let(:part){ klass.new(num: 0, accuracy: 1) }
      it_behaves_like "direction methods"
    end

    context "fuzzy, 負の値の場合" do
      let(:part){ klass.new(num: -1, accuracy: 1) }
      it_behaves_like "direction methods"
    end
  end

  describe "#<=>" do
    let(:part){ klass.new(num: 10, accuracy: 1) }

    shared_examples_for "part == other" do
      it{ expect(part <=> other).to eq(0) }
    end

    shared_examples_for "part < other" do
      it{ expect(part <=> other).to eq(-1) }
    end

    shared_examples_for "part > other" do
      it{ expect(part <=> other).to eq(1) }
    end


    # [self] <other>
    context "fuzzyな日付パートと比較した場合" do
      let(:other){ klass.new }

      context "     [  ] <  >" do
        let(:other){ klass.new(num: 20, accuracy: 1) }
        it_behaves_like "part < other"
      end

      context "     [  X  >  " do
        before{ allow(other).to receive(:global_range).and_return(19..25) }
        it_behaves_like "part == other"
      end

      context "     [ <] >   " do
        before{ allow(other).to receive(:global_range).and_return(15..25) }
        it_behaves_like "part == other"
      end

      context "    [< >]     " do
        before{ allow(other).to receive(:global_range).and_return(11..18) }
        it_behaves_like "part == other"
      end

      context "     {  }     " do
        let(:other){ klass.new(num: 10, accuracy: 1) }
        it_behaves_like "part == other"
      end

      context "    <[ ]>     " do
        before{ allow(other).to receive(:global_range).and_return(5..25) }
        it_behaves_like "part == other"
      end

      context "   < [> ]     " do
        before{ allow(other).to receive(:global_range).and_return(5..15) }
        it_behaves_like "part == other"
      end

      context "  <  X  ]     " do
        before{ allow(other).to receive(:global_range).and_return(1..10) }
        it_behaves_like "part == other"
      end

      context "<  > [  ]     " do
        let(:other){ klass.new(num: 0, accuracy: 1) }
        it_behaves_like "part > other"
      end
    end

    # [self] *other
    context "fixidな日付パートと比較した場合" do
      context "*[ ] " do
        let(:other){ klass.new(num: 5) }
        it_behaves_like "part > other"
      end

      context " { ] " do
        let(:other){ klass.new(num: 10) }
        it_behaves_like "part == other"
      end

      context " [*] " do
        let(:other){ klass.new(num: 15) }
        it_behaves_like "part == other"
      end

      context " [ } " do
        let(:other){ klass.new(num: 19) }
        it_behaves_like "part == other"
      end

      context " [ ]*" do
        let(:other){ klass.new(num: 25) }
        it_behaves_like "part < other"
      end
    end

    context "クラスの違う日付パートと比較した場合" do
      let(:klass_2){
        Class.new(described_class) do
          self.max_range = 1..30
        end
      }

      let(:other){ klass_2.new(num: 15) }

      it{ expect(part <=> other).to be_nil }
    end
  end
end
