# 紀元・基数年（周年）
# * 0年があり得る
FDate::Part.collection.inherit "CardinalEpochYear", from: "Year" do
  include modules["Infinity"]

  # 紀元後を表すプレフィックス
  self::AD_KEYS = %w(A AD 後 紀元後).map(&:freeze).freeze

  # 紀元前を表すプレフィックス
  self::BC_KEYS = %w(B BC 前 紀元前).map(&:freeze).freeze

  class_methods "BuildMethods" do

    # パース
    # @param [String] code 年コード
    # @return [Hash] インスタンス化用ハッシュ
    # @example 紀元後のコードを渡した場合
    #   Year._local_parse("A=12*") #=> {num: 120, accuracy: 1}
    # @example 紀元前のコードを渡した場合
    #   Year._local_parse("B=12*") #=> {num: -120, accuracy: 1}
    # @example プレフィックスを省略した場合
    #   Year._local_parse("12*") #=> {num: 120, accuracy: 1}
    def _local_parse(code)
      code = [self::AD_KEYS.first, code].join(self::SEPARATOR) unless code.include?(self::SEPARATOR)
      key, fuzzy_num_code = code.split(self::SEPARATOR, 2)
      ad_bool = ad_key?(key)
      raise FDate::ParseError.new(code) if ad_bool.nil?
      hash = _parse_fuzzy_num(fuzzy_num_code)
      hash.merge(num: modify_num(hash[:num], ad_bool, hash[:accuracy] == 0))
    end

    # プレフィックスを判定
    # @param [String] key 判定するプレフィックス
    # @return [true] 紀元後の場合
    # @return [false] 紀元前の場合
    # @return [nil] どちらとも評価できない場合
    def ad_key?(key)
      if self::AD_KEYS.include?(key)
        true
      elsif self::BC_KEYS.include?(key)
        false
      else
        nil
      end
    end

    private

    # @param [Integer] num 計算前の数値
    # @param [Boolean] ad_bool 紀元後かどうか
    # @param [Boolean] fixed_bool fixedかどうか
    # @return [Integer] インスタンス化用ハッシュのnum用数値
    def modify_num(num, ad_bool, fixed_bool)
      if ad_bool
        num
      else
        if !fixed_bool && num == 0
          -1
        else
          -num
        end
      end
    end
  end

  class_methods "boolean methods" do

    # @raturn [Boolean] 基数年（周年）かどうか
    def cardinal?
      true
    end

    # @raturn [Boolean] 序数年（数え年）かどうか
    def ordinal?
      false
    end
  end

  class_methods "JdyMethods" do

    # @!attribute [rw] diff_from_jdy
    #   @return [Integer] ユリウス通年との差
    attr_accessor :diff_from_jdy
    private :diff_from_jdy=

    # @param [Integer] local_year この紀年法の年
    # @return [Integer] ユリウス通年
    def to_jdy(local_year)
      local_year + diff_from_jdy
    end

    # @param [Integer] jdy ユリウス通年
    # @return [Integer] この紀年法の年
    def from_jdy(jdy)
      jdy - diff_from_jdy
    end
  end

  # @return [String] プレフィックス付き年コード
  def local_code
    [prefix_key, fuzzy_num_code].join(self.class::SEPARATOR)
  end

  # @return [Boolean] 紀元後かどうか（0年は紀元後とする）
  def ad?
    local_range.first >= 0
  end

  # @return [Boolean] 紀元前かどうか
  def bc?
    local_range.first < 0
  end

  private

  # @return [String] 紀元前 or 後を示すプレフィックス
  def prefix_key
    bc? ? self.class::BC_KEYS.first : self.class::AD_KEYS.first
  end
end
