# 元号年セット
FDate::Part.collection.inherit "RegnalYearSet", from: "Year" do
  extend FDate::BuildMethodDefinable

  # 複数の元号年コードを連結する際のセパレータ
  self::RY_SEPARATOR = "/".freeze

  # ユリウス通年クラス
  self::JDY_CLASS = FDate::Part.collection["JDY"]

  class_methods "SetingMethods" do

    # @return [InheritableCollection::ClassCollection<Class<FDate::Part[RegnalYear]>>] 元号オブジェクト（実態は元号年クラス）のコレクション
    def reign_collection
      @reign_collection ||= InheritableCollection::ClassCollection.new(FDate::Part.collection["RegnalYear"]).configure do |config|
        # 元号キーの禁則文字をセット
        config.illegal_characters += FDate::ILLEGAL_CHARACTERS + [FDate::Calendar::SEPARATOR, self::SEPARATOR, self::RY_SEPARATOR]
      end
    end

    # 元号のコレクションをセット
    # @param [Array<Class<FDate::Part[RegnalYear]>>] reigns 元号年クラスの配列
    # @note このメソッドを使うたびに古いコレクションは破棄され新しいコレクションが作られる
    def reign_collection=(reigns)
      reign_collection.replace(reigns)
    end
  end

  class_methods "BuildMethods" do

    # @param [code] 元号年コード（`self::RY_SEPARATOR`で区切って複数可）
    # @return [Array<FDate::Part[RegnalYear]>] 元号年オブジェクトの配列
    def _local_parse(code)
      code.split(self::RY_SEPARATOR).map do |c|
        h = reign_collection.klass._local_parse(c)
        reign_collection[h.delete(:reign)].new(h)
      end
    end

    # @param [Integer] jdy ユリウス通年
    # @param [Object] date 日付もしくは期間とみなせるオブジェクト
    # @return [Array<FDate::Part[RegnalYear]>] 渡した年でインスタンス化した元号年オブジェクトの配列
    # @return [Array<FDate::Part[jdy]>] 渡した年を範囲に含む元号オブジェクト（元号年クラス）が無い場合
    # @note `date`を渡した場合は`date`で、渡さない場合は`jdy`で元号を絞り込む
    def _jdy(jdy, date = nil)
      reigns = date ? reigns_by_date(date) : reigns_by_jdy(jdy)
      reigns << self::JDY_CLASS if reigns.empty?
      reigns.map{|reign| reign.jdy(jdy) }
    end

    # @param [Integer] jdy ユリウス通年
    # @return [Array<Class<FDate::Part[RegnalYear]>>] 渡した年を範囲に含む元号オブジェクト（元号年クラス）の配列
    # @todo メソッド名が気に入らない
    def reigns_by_jdy(jdy)
      reign_collection.select{|reign| reign.jdy_range.cover?(jdy) }
    end

    # @param [Object] date 日付もしくは期間とみなせるオブジェクト
    # @return [Array<Class<FDate::Part[RegnalYear]>>] 渡した年を範囲に含む元号オブジェクト（元号年クラス）の配列
    # @todo メソッド名が気に入らない
    def reigns_by_date(date)
      reign_collection.select{|reign| reign.term.cover?(date) }
    end
  end

  # @!attribute [r] years
  #   @return [Array<FDate::Part[RegnalYear]>] 元号年オブジェクトの配列
  #   @return [Array<FDate::Part[jdy]>] ユリウス通年オブジェクトを1つ含む配列
  attr_reader :years

  def initialize(years)
    @years = years

    validate_years
    validate_jdy
    singlize_jdy
    validate_reign unless jdy?
  end

  # @return [Range<Integer>] ユリウス通年の範囲
  def jdy_range
    years.first.jdy_range
  end

  # @return [Range<Integer>] 唯一の元号年オブジェクトの年数の範囲
  # @reutrn [nil] 複数の年オブジェクトを格納しているかユリウス通年モードの場合
  def local_range
    years.first.local_range if single?
  end

  # @param [Object] date 日付や期間とみなせるオブジェクト
  # @return [Array<FDate::Part[Year]>] 渡した日付で絞り込んだ年オブジェクトの配列
  def _narrow_down(date)
    if jdy?
      years
    else
      narrow_years = years.select{|year| year.reign.term.cover?(date) }
      if narrow_years.empty?
        [self.class::JDY_CLASS.new(num: jdy)]
      else
        narrow_years
      end
    end
  end

  build_methods :narrow_down

  # @param [FDate::Part[RegnalYearSet]] other 比較対象の元号年セットオブジェクト
  # @return [Boolean] 同じクラスかつ格納した年オブジェクトが全て同じかどうか
  def eql?(other)
    self.class.equal?(other.class) && years.eql?(other.years)
  end

  # @return [String] 日付パートコード
  # @example 元号年オブジェクトが1つ含まれている場合
  #   year.single? #=> true
  #   year.local_code #=> "平成=1"
  # @example 元号年オブジェクトが複数含まれている場合
  #   year.plural? #=> true
  #   year.local_code #=> "昭和=64/平成=1"
  # @example ユリウス通年オブジェクトが1つ含まれている場合
  #   year.jdy? #=> true
  #   year.local_code #=> "(6702)"
  def local_code
    years.map(&:local_code).join(self.class::RY_SEPARATOR)
  end

  # 翻訳
  # @todo メソッドではなく訳文だけで処理できるようにしたい
  def strftime(arg = FDate::Strftime::DEFAULT_TRANSLATION_KEY, locale: nil)
    locale = FDate::Locale.collection[locale]

    if jdy?
      "(#{self.class::JDY_CLASS.key} #{years.first.strftime(:simple, locale: locale)})"
    else
      years.map{|y| y.strftime(arg, locale: locale) }.join(locale[self, "separator"])
    end
  end

  # @return [Boolean] 誤差無しかどうか
  def fixed?
    years.all?(&:fixed?)
  end

  # @return [Boolean] 誤差ありかどうか
  def fuzzy?
    years.any?(&:fuzzy?)
  end

  # @return [Boolean] NilDateかどうか
  def nildate?
    years.any?(&:nildate?)
  end

  # @return [Boolean] 格納された年オブジェクトが単数かどうか
  def single?
    years.size == 1
  end

  # @return [Boolean] 格納された年オブジェクトが複数かどうか
  def plural?
    years.size > 1
  end

  # @return [Boolean] ユリウス通年モードかどうか
  def jdy?
    years.any?{|year| year.is_a?(self.class::JDY_CLASS) }
  end

  private

  # @raise [ArgumentError] `@years`が空の場合
  def validate_years
    if years.empty?
      raise ArgumentError, "years are empty"
    end
  end

  # @raise [ArgumentError] コレクションに含まれない年オブジェクトが含まれている場合
  def validate_reign
    collection = self.class.reign_collection
    years.each do |year|
      unless collection.include?(year.class)
        raise ArgumentError, "`#{self.class}::reign_collection` not include #{year.class.key}"
      end
    end
  end

  # @raise [ArgumentError] 複数の年オブジェクトの中にユリウス通年か違うユリウス通年の年が含まれている場合
  def validate_jdy
    unless years.all?{|year| jdy == year.jdy }
      raise ArgumentError, "include different JDY. #{years.map(&:jdy)}"
    end
  end

  # ユリウス通年オブジェクトが1つでも含まれている場合は、それのみを格納する
  def singlize_jdy
    if jdy = years.find{|year| year.is_a?(self.class::JDY_CLASS) }
      @years = [jdy]
    end
  end
end
