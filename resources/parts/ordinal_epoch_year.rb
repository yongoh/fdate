# 紀元・序数年（数え年）
# * 0年は無く1年から始まる
FDate::Part.collection.inherit "OrdinalEpochYear", from: "CardinalEpochYear" do
  class_methods "BuildMethods" do

    private

    # @param [Integer] num 計算前の数値
    # @param [Boolean] ad_bool 紀元後かどうか
    # @param [Boolean] fixed_bool fixedかどうか
    # @return [Integer] インスタンス化用ハッシュのnum用数値
    def modify_num(num, ad_bool, fixed_bool)
      if ad_bool
        num == 0 && !fixed_bool ? 1 : num
      else
        num > 0 ? -(num - 1) : num
      end
    end
  end

  class_methods "boolean methods" do

    # @raturn [Boolean] 基数年（周年）かどうか
    def cardinal?
      false
    end

    # @raturn [Boolean] 序数年（数え年）かどうか
    def ordinal?
      true
    end
  end

  # @return [Boolean] 紀元後かどうか
  def ad?
    local_range.first > 0
  end

  # @return [Boolean] 紀元前かどうか（0年は紀元前とする）
  def bc?
    local_range.first <= 0
  end

  private

  # @see FDate::Part#raw_range
  def raw_range(num)
    if num > 0
      rng = super
      rng.first == 0 ? 1..rng.last : rng
    else
      num -= 1
      rng = super(num)
      d = rng.first + 1
      e = rng.last < 0 ? rng.last + 1 : rng.last
      d..e
    end
  end

  # @see FDate::Part#fuzzy_num_code_i
  def fuzzy_num_code_i
    fixed? && bc? ? super + 1 : super
  end
end
