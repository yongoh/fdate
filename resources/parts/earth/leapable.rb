# 閏
FDate::Part.modules.build "Leapable" do

  # 閏キーの配列
  self::LEAP_KEYS = %w(L Leap 閏).map(&:freeze).freeze

  # 閏付き日付パートコードの正規表現
  self::LOCAL_CODE_PATTERN = /\A(#{Regexp.union(self::LEAP_KEYS)})(\d+)\z/


  class_methods "LeapingMethods" do

    # @!attribute [rw] leap
    #   @return [Integer] 閏日付（何番目か）
    attr_accessor :leap
    private :leap=

    # @param [Integer] global_i 比較対象の日付数値
    # @return [Boolean] 渡した日付数値が閏かどうか
    def leap?(global_i)
      !global_i.nil? && global_i == leap
    end

    # @param [Integer] global_i 比較対象の日付数値
    # @return [-1] 渡した日付数値が閏より前の場合
    # @return [0] 渡した日付数値が閏の場合
    # @return [1] 渡した日付数値が閏より後の場合
    # @return [nil] 閏設定無しもしくはnilを渡した場合
    def compare_with_leap(global_i)
      global_i <=> leap unless global_i.nil?
    end

    # @overload to_local(global_i)
    #   @param [Integer] global_i グローバル日付数値
    #   @return [Integer] 「閏」をつけない日付数値
    # @overload to_local(global_range)
    #   @param [Range<Integer>] global_range グローバル日付数値の範囲
    #   @return [Range<Integer>] 「閏」をつけない日付数値の範囲
    def to_local(global)
      if global.is_a?(Range)
        to_local(global.first)..to_local(global.last)
      else
        leap && global >= leap ? global - 1 : global
      end
    end
  end

  class_methods "BuildMethods" do

    # パース
    # @param [String] code 日付パートコード
    # @return [Hash] インスタンス化用ハッシュ
    def _local_parse(code)
      if leap
        if matched = code.match(self::LOCAL_CODE_PATTERN)
          int = matched[2].to_i + 1
          raise FDate::DateError.new(self, "The leap of this class is #{leap}. #{code.inspect}") if int != leap
          _from_i(int)
        elsif code.match(/\A\d+\z/) && code.to_i >= leap
          _from_i(code.to_i + 1)
        else
          _parse_fuzzy_num(code)
        end
      elsif code.match(self::LOCAL_CODE_PATTERN)
        raise FDate::DateError.new(self, "There is no leap on this class. #{code.inspect}")
      else
        _parse_fuzzy_num(code)
      end
    end
  end


  # @return [Range<Integer>] 「閏」をつけない日付数値の配列
  def local_range
    self.class.to_local(global_range)
  end

  # @return [Boolean] この日付が閏かどうか
  def leap?
    self.class.leap?(global_i)
  end

  # この日付と設定された閏とを比べる
  # @return [-1] この日付が閏より前の場合
  # @return [0] この日付が閏の場合
  # @return [1] この日付が閏より後の場合
  # @return [nil] fuzzyもしくは閏設定なしの場合
  def compare_with_leap
    self.class.compare_with_leap(global_i)
  end

  # @return [String] 日付パートコード
  def local_code
    leap_key + fuzzy_num_code
  end

  # @return [String] 閏キー
  def leap_key
    leap? ? self.class::LEAP_KEYS.first : ""
  end

  private

  # @return [Integer] `#fuzzy_num_code`の作成に使う数値
  def fuzzy_num_code_i
    (fixed? ? local_range : global_range).first.abs
  end
end
