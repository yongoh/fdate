# 地球の月（地月公転回数）
FDate::Part.collection.inherit "EarthMonth" do
  include modules["Ordinal"]

  # 一年の月数
  self.last = 12
end
