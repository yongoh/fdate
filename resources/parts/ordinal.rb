# 序数
# * 上位の日付パートの中で何番目であるかを数える
# * 0以下は許可しない
# @note 日付パートクラスにインクルードする
# @todo `OrdinalYear`とは名前は被るが継承関係にないし機能が全く異なるという問題がある
FDate::Part.modules.build "Ordinal" do
  class_methods "SettingMethods" do

    private

    # `self.max_range`をセット
    # @param [Integer] n 許容する範囲の最後
    def last=(n)
      self.max_range = 1..n
    end
  end

  # @return [Integer] 順序
  def ordinal
    global_i
  end
end
