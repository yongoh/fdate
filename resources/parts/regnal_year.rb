# 元号年
FDate::Part.collection.inherit "RegnalYear", from: "Year" do
  include modules["Ordinal"]

  class_methods "SetingMethods" do

    # @!attribute [rw] term
    #   @return [FDate::Term] この元号を使用する期間
    attr_accessor :term
    private :term=
  end

  class_methods "ReignMethods" do
    alias_method :eql?, :equal?

    # @return [Range<Integer>] ローカル年の範囲
    def local_range
      @local_range ||= 1..years
    end

    alias_method :max_range, :local_range

    # @return [Range<Integer>] ユリウス通年の範囲
    def jdy_range
      @jdy_range ||= (term.first.infinity? ? term.first.jd : term.first.year.first_i)..(term.last.infinity? ? term.last.jd : term.last.year.last_i)
    end

    # @return [Integer] この元号を使用した年数
    def years
      if term.first.infinity? || term.last.infinity?
        Float::INFINITY
      else
        jdy_range.last - jdy_range.first + 1
      end
    end

    # @return [Integer] この元号を使用した日数
    # @todo `Term`へ移動
    def days
      if term.first.infinity? || term.last.infinity?
        Float::INFINITY
      else
        jd_range.last - jd_range.first + 1
      end
    end
  end

  class_methods "BuildMethods" do

    # @param [String] code 元号年コード
    # @return [Hash] `:reign`に元号キーを持つインスタンス化用ハッシュ
    # @todo このクラス（元号）でインスタンス化することは明確なので、`:reign`は必ずしも必要ないんだが
    # @example
    #   RegnalYear._local_parse("平成=1*") #=> {num: 10, accuracy: 1, reign: "平成"}
    def _local_parse(code)
      splited = code.split(self::SEPARATOR, 2)
      h = _parse_fuzzy_num(splited[1])
      h.merge(reign: splited[0])
    end

    # @param [Integer] jdy ユリウス通年
    # @return [Hash] インスタンス化用ハッシュ
    # @example
    #   RegnalYear._jdy(6731) #=> {num: 30}
    # @raise [FDate::RangeError] この元号の期間に含まれない年を渡した場合に発生
    def _jdy(jdy)
      local_year = from_jdy(jdy)
      validate_range(local_year..local_year)
      {num: local_year}
    end
  end

  class_methods "DateMethods" do

    # @param [FDate::Part[Year]] local_year この紀年法の年オブジェクト
    # @return [Integer] ユリウス通年
    def to_jdy(local_year)
      jdy_range.first + local_year - 1
    end

    # @param [Integer] jdy ユリウス通年
    # @return [FDate::Part[Year]] この紀年法の年オブジェクト
    def from_jdy(jdy)
      jdy - jdy_range.first + 1
    end
  end

  # @return [Class<FDate::Part[RegnalYear]>] 元号オブジェクト（実態は元号年クラス）
  def reign
    self.class
  end

  # @return [String] 元号キーを含む年コード
  def local_code
    [reign.key, fuzzy_num_code].join(self.class::SEPARATOR)
  end
end
