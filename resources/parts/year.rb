# 年（日地公転回数）
FDate::Part.collection.inherit "Year" do

  # プレフィックスと数値部のセパレータ
  self::SEPARATOR = "=".freeze

  class_methods "BuildMethods" do
    extend FDate::BuildMethodDefinable

    # @param [Integer] jdy ユリウス通年
    # @return [Hash] インスタンス化用ハッシュ
    def _jdy(jdy)
      {num: from_jdy(jdy)}
    end

    # エイリアス
    %i(_from_i).each do |method_name|
      define_method(method_name){|*args, &block| _jdy(*args, &block) }
    end

    build_methods :jdy, :from_i
  end

  class_methods "DateMethods" do

    # @param [FDate::Part[Year]] local_year この紀年法の年オブジェクト
    # @return [Integer] ユリウス通年
    def to_jdy(local_year)
      local_year
      # オーバーライド
    end

    # @param [Integer] jdy ユリウス通年
    # @return [FDate::Part[Year]] この紀年法の年オブジェクト
    def from_jdy(jdy)
      jdy
      # オーバーライド
    end
  end


  # @return [Range<Integer>] ユリウス通年の範囲
  def jdy_range
    @jdy_range ||= self.class.to_jdy(local_range.first)..self.class.to_jdy(local_range.last)
  end

  # @return [Integer] fixedの場合はこの年のユリウス通年
  # @return [Range<Integer>] fuzzyの場合はユリウス通年の範囲
  def jdy
    fixed? ? jdy_range.first : jdy_range
  end

  # エイリアス
  [
    %i(global_range jdy_range),
  ].each do |new_name, original_name|
    define_method(new_name){|*args, &block| send(original_name, *args, &block) }
  end

  # 他の紀年法に変換
  # @param [Class<FDate::Part[Year]>,String] key 年クラスかそのキー
  # @return [FDate::Part[Year]] 変換済みの年オブジェクト
  # @note fuzzyは考慮されていない
  def convert_to(key)
    FDate::Part.collection[key].jdy(jdy)
  end
end