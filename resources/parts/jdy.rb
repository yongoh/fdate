# ユリウス通年（仮名）
#
# ユリウス通日0日目を含む年を基準とする基数年
FDate::Part.collection.inherit "JDY", from: "CardinalEpochYear" do
  self.diff_from_jdy = 0

  # @return [String] 日付パートコード（グローバルコードのみ）
  def local_code
    global_code
  end
end
