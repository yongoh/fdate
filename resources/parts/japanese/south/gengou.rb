# 日本の元号（大覚寺統・南朝）
FDate::Part.collection.inherit "japanese/south/Gengou", from: "RegnalYearSet" do
  self.reign_collection = FDate::Period::Set.fill_last([

    # 鎌倉時代（大覚寺統）
    ["s元徳", {first: "adjl_1329-09-22_"}],
    ["元弘", {first: "adjl_1331-09-11_"}],

    # 建武の新政
    ["s建武", {first: "adjl_1334-03-05_"}],

    # 南北朝時代（南朝）
    ["延元", {first: "adjl_1336-04-11_"}],
    ["興国", {first: "adjl_1340-05-25_"}],
    ["正平", {first: "adjl_1347-01-20_"}],
    ["建徳", {first: "adjl_1370-08-16_"}],
    ["文中", {first: "adjl_1372-05-04_"}],
    ["天授", {first: "adjl_1375-06-26_"}],
    ["弘和", {first: "adjl_1381-03-06_"}],
    ["元中", {first: "adjl_1384-05-18_", last: "adjl_1392-11-19_"}]

  ]){|key, _term, &to_term|
    self.reign_collection._inherit(key) do
      self.term = to_term.call(_term)
    end
  }
end
