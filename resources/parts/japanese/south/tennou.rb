# 天皇（大覚寺統・南朝）
FDate::Part.collection.inherit "japanese/south/Tennou", from: "RegnalYearSet" do
  [
    # 鎌倉時代（大覚寺統）
    ["亀山天皇", {first: "adjl_1260-01-09_", last: "adjl_1274-03-06_"}],
    ["後宇多天皇", {first: "adjl_1274-03-06_", last: "adjl_1287-11-27_"}],
    ["後二条天皇", {first: "adjl_1301-03-02_", last: "adjl_1308-09-10_"}],

    # 建武の新政
    ["後醍醐天皇", {first: "adjl_1318-03-29_", last: "adjl_1339-09-18_"}],

    # 南北朝時代（南朝）
    ["後村上天皇", {first: "adjl_1339-09-18_", last: "adjl_1368-03-29_"}],
    ["長慶天皇", {first: "adjl_1368-03-29_", last: "adjl_1383-10-**_"}],
    ["後亀山天皇", {first: "adjl_1383-10-**_", last: "adjl_1392-11-19_"}],

  ].map do |key, _term|
    self.reign_collection.inherit(key) do
      self.term = FDate::Term.new(_term)
    end
  end
end
