# 天皇（持明院統・北朝）
FDate::Part.collection.inherit "japanese/north/Tennou", from: "RegnalYearSet" do
  [
    # 鎌倉時代（持明院統）
    ["後深草天皇", {first: "adjl_1246-02-16_", last: "adjl_1260-01-09_"}],
    ["伏見天皇", {first: "adjl_1287-11-27_", last: "adjl_1298-08-30_"}],
    ["後伏見天皇", {first: "adjl_1298-08-30_", last: "adjl_1301-03-02_"}],
    ["花園天皇", {first: "adjl_1308-12-28_", last: "adjl_1318-03-29_"}],

    # 南北朝時代（北朝）
    ["光厳天皇", {first: "adjl_1331-10-22_", last: "adjl_1333-07-07_"}],
    ["光明天皇", {first: "adjl_1336-09-20_", last: "adjl_1348-11-18_"}],
    ["崇光天皇", {first: "adjl_1348-11-18_", last: "adjl_1351-11-26_"}],
    ["後光厳天皇", {first: "adjl_1352-09-25_", last: "adjl_1371-04-09_"}],
    ["後円融天皇", {first: "adjl_1371-04-09_", last: "adjl_1382-05-24_"}],
    ["後小松天皇", {first: "adjl_1382-05-24_", last: "adjl_1412-10-05_"}],

    # 南北合一後
    ["称光天皇", {first: "adjl_1412-10-05_", last: "adjl_1428-08-30_"}],

  ].map do |key, _term|
    self.reign_collection.inherit(key) do
      self.term = FDate::Term.new(_term)
    end
  end
end
