# 日本の元号（持明院統・北朝）
FDate::Part.collection.inherit "japanese/north/Gengou", from: "RegnalYearSet" do
  self.reign_collection = FDate::Period::Set.fill_last([

    # 鎌倉時代（持明院統）
    ["n元徳", {first: "adjl_1329-09-22_"}],
    ["正慶", {first: "adjl_1332-05-23_"}],

    # 建武の新政
    ["n建武", {first: "adjl_1334-03-05_"}],

    # 南北朝時代（北朝）
    ["暦応", {first: "adjl_1338-10-11_"}],
    ["康永", {first: "adjl_1342-06-01_"}],
    ["貞和", {first: "adjl_1345-11-15_"}],
    ["観応", {first: "adjl_1350-04-04_"}],
    ["文和", {first: "adjl_1352-11-04_"}],
    ["延文", {first: "adjl_1356-04-29_"}],
    ["康安", {first: "adjl_1361-05-04_"}],
    ["貞治", {first: "adjl_1362-10-11_"}],
    ["応安", {first: "adjl_1368-03-07_"}],
    ["永和", {first: "adjl_1375-03-29_"}],
    ["康暦", {first: "adjl_1379-04-09_"}],
    ["永徳", {first: "adjl_1381-03-20_"}],
    ["至徳", {first: "adjl_1384-03-19_"}],
    ["嘉慶", {first: "adjl_1387-10-05_"}],
    ["康応", {first: "adjl_1389-03-07_"}],
    ["明徳", {first: "adjl_1390-04-12_", last: "adjl_1394-08-01_"}],

  ]){|key, _term, &to_term|
    self.reign_collection._inherit(key) do
      self.term = to_term.call(_term)
    end
  }
end
