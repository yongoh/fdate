# 神武天皇即位紀元（皇紀）
FDate::Part.collection.inherit "JimmuEpochYear", from: "OrdinalEpochYear" do

  # ユリウス通年との差
  self.diff_from_jdy = 4053
end
