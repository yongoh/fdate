# 天皇
FDate::Part.collection.inherit "japanese/Tennou", from: "RegnalYearSet" do
  [
    ["継体天皇", {first: "adjl_507-03-03_ab", last: "adjl_531-03-10_ab"}],
    ["安閑天皇", {first: "adjl_531-03-10_ab", last: "adjl_536-01-25_ab"}],
    ["宣化天皇", {first: "adjl_536-01-26_ab", last: "adjl_539-03-15_ab"}],
    ["欽明天皇", {first: "adjl_539-12-30_ab", last: "adjl_571-04-15_ab"}],
    ["敏達天皇", {first: "adjl_572-04-30_ab", last: "adjl_585-09-14_ab"}],
    ["用明天皇", {first: "adjl_585-10-03_ab", last: "adjl_587-05-21_ab"}],
    ["崇峻天皇", {first: "adjl_587-09-09_ab", last: "adjl_592-12-12_ab"}],

    ["推古天皇", {first: "adjl_593-01-15_", last: "adjl_628-04-15_"}],
    ["舒明天皇", {first: "adjl_629-02-02_", last: "adjl_641-11-17_"}],
    ["皇極天皇", {first: "adjl_642-02-19_", last: "adjl_645-07-12_"}],

    ["孝徳天皇", {first: "adjl_645-07-12_", last: "adjl_654-11-24_"}],
    ["斉明天皇", {first: "adjl_655-02-14_", last: "adjl_661-08-24_"}],
    # ["中大兄皇子（称制）", {first: "adjl_661-08-24_", last: "adjl_"}],
    ["天智天皇", {first: "adjl_668-02-20_", last: "adjl_672-01-07_"}],
    ["弘文天皇", {first: "adjl_672-01-09_", last: "adjl_672-08-21_"}],
    ["天武天皇", {first: "adjl_673-03-20_", last: "adjl_686-10-01_"}],
    # ["鵜野讚良（称制）", {first: "adjl_686-10-01_", last: "adjl_690-02-14_"]},
    ["持統天皇", {first: "adjl_690-02-14_", last: "adjl_697-08-22_"}],
    ["文武天皇", {first: "adjl_697-08-22_", last: "adjl_707-07-18_"}],

    ["元明天皇", {first: "adjl_707-08-18_", last: "adjl_715-10-03_"}],
    ["元正天皇", {first: "adjl_715-10-03_", last: "adjl_724-03-03_"}],
    ["聖武天皇", {first: "adjl_724-03-03_", last: "adjl_749-08-19_"}],
    ["孝謙天皇", {first: "adjl_749-08-19_", last: "adjl_758-09-07_"}],
    ["淳仁天皇", {first: "adjl_758-09-07_", last: "adjl_764-11-06_"}],
    ["称徳天皇", {first: "adjl_764-11-06_", last: "adjl_770-08-28_"}],
    ["光仁天皇", {first: "adjl_770-10-23_", last: "adjl_781-04-30_"}],

    ["桓武天皇", {first: "adjl_781-04-30_", last: "adjl_806-04-09_"}],
    ["平城天皇", {first: "adjl_806-04-09_", last: "adjl_809-05-18_"}],
    ["嵯峨天皇", {first: "adjl_809-05-18_", last: "adjl_823-05-29_"}],
    ["淳和天皇", {first: "adjl_823-06-09_", last: "adjl_833-03-22_"}],
    ["仁明天皇", {first: "adjl_833-03-30_", last: "adjl_850-05-04_"}],
    ["文徳天皇", {first: "adjl_850-05-31_", last: "adjl_858-10-07_"}],

    ["清和天皇", {first: "adjl_858-12-15_", last: "adjl_876-12-18_"}],
    ["陽成天皇", {first: "adjl_876-12-18_", last: "adjl_884-03-04_"}],
    ["光孝天皇", {first: "adjl_884-03-23_", last: "adjl_887-09-17_"}],
    ["宇多天皇", {first: "adjl_887-09-17_", last: "adjl_897-08-04_"}],
    ["醍醐天皇", {first: "adjl_897-08-14_", last: "adjl_930-10-16_"}],
    ["朱雀天皇", {first: "adjl_930-12-14_", last: "adjl_946-05-16_"}],
    ["村上天皇", {first: "adjl_946-05-31_", last: "adjl_967-07-05_"}],
    ["冷泉天皇", {first: "adjl_967-11-15_", last: "adjl_969-09-27_"}],
    ["円融天皇", {first: "adjl_969-11-05_", last: "adjl_984-09-24_"}],
    ["花山天皇", {first: "adjl_984-11-05_", last: "adjl_986-08-01_"}],
    ["一条天皇", {first: "adjl_986-07-31_", last: "adjl_1011-07-16_"}],
    ["三条天皇", {first: "adjl_1011-07-16_", last: "adjl_1016-03-10_"}],
    ["後一条天皇", {first: "adjl_1016-03-18_", last: "adjl_1036-05-15_"}],
    ["後朱雀天皇", {first: "adjl_1036-05-15_", last: "adjl_1045-02-05_"}],
    ["後冷泉天皇", {first: "adjl_1045-04-27_", last: "adjl_1068-05-22_"}],
    ["後三条天皇", {first: "adjl_1068-05-22_", last: "adjl_1073-01-18_"}],
    ["白河天皇", {first: "adjl_1073-01-18_", last: "adjl_1087-01-03_"}],

    ["堀河天皇", {first: "adjl_1087-01-03_", last: "adjl_1107-08-09_"}],
    ["鳥羽天皇", {first: "adjl_1107-08-09_", last: "adjl_1123-02-25_"}],
    ["崇徳天皇", {first: "adjl_1123-03-18_", last: "adjl_1142-01-05_"}],
    ["近衛天皇", {first: "adjl_1142-01-05_", last: "adjl_1155-08-22_"}],
    ["後白河天皇", {first: "adjl_1155-08-23_", last: "adjl_1158-09-05_"}],
    ["二条天皇", {first: "adjl_1158-09-05_", last: "adjl_1165-08-03_"}],

    ["六条天皇", {first: "adjl_1165-08-03_", last: "adjl_1168-04-09_"}],
    ["高倉天皇", {first: "adjl_1168-04-09_", last: "adjl_1180-03-18_"}],
    ["安徳天皇", {first: "adjl_1180-05-18_", last: "adjl_1185-04-25_"}],
    ["後鳥羽天皇", {first: "adjl_1183-09-08_", last: "adjl_1198-02-18_"}],

    ["土御門天皇", {first: "adjl_1198-02-18_", last: "adjl_1210-12-12_"}],
    ["順徳天皇", {first: "adjl_1210-12-12_", last: "adjl_1221-05-13_"}],
    ["仲恭天皇", {first: "adjl_1221-05-13_", last: "adjl_1221-07-29_"}],
    ["後堀河天皇", {first: "adjl_1221-07-29_", last: "adjl_1232-11-17_"}],
    ["四条天皇", {first: "adjl_1232-11-17_", last: "adjl_1242-02-10_"}],
    ["後嵯峨天皇", {first: "adjl_1242-02-21_", last: "adjl_1246-02-16_"}],

    # 両統迭立・南北朝時代
    # ※別ファイル

    ["後花園天皇", {first: "adjl_1428-09-07_", last: "adjl_1464-08-21_"}],

    ["後土御門天皇", {first: "adjl_1464-08-21_", last: "adjl_1500-10-21_"}],
    ["後柏原天皇", {first: "adjl_1500-11-16_", last: "adjl_1526-05-18_"}],
    ["後奈良天皇", {first: "adjl_1526-06-09_", last: "adjl_1557-09-27_"}],
    ["正親町天皇", {first: "adjl_1557-11-17_", last: "adgr_1586-12-17_"}],

    ["後陽成天皇", {first: "adgr_1586-12-17_", last: "adgr_1611-05-09_"}],

    ["後水尾天皇", {first: "adgr_1611-05-09_", last: "adgr_1629-12-22_"}],
    ["明正天皇", {first: "adgr_1629-12-22_", last: "adgr_1643-11-14_"}],
    ["後光明天皇", {first: "adgr_1643-11-14_", last: "adgr_1654-10-30_"}],
    ["後西天皇", {first: "adgr_1655-01-05_", last: "adgr_1663-03-05_"}],
    ["霊元天皇", {first: "adgr_1663-03-05_", last: "adgr_1687-05-02_"}],
    ["東山天皇", {first: "adgr_1687-05-06_", last: "adgr_1709-07-27_"}],
    ["中御門天皇", {first: "adgr_1709-07-27_", last: "adgr_1735-04-13_"}],
    ["桜町天皇", {first: "adgr_1735-04-13_", last: "adgr_1747-06-09_"}],
    ["桃園天皇", {first: "adgr_1747-06-09_", last: "adgr_1762-08-31_"}],
    ["後桜町天皇", {first: "adgr_1762-09-15_", last: "adgr_1770-05-23_"}],
    ["後桃園天皇", {first: "adgr_1770-05-23_", last: "adgr_1779-12-16_"}],
    ["光格天皇", {first: "adgr_1780-01-01_", last: "adgr_1817-05-07_"}],
    ["仁孝天皇", {first: "adgr_1817-10-31_", last: "adgr_1846-02-21_"}],
    ["孝明天皇", {first: "adgr_1846-03-10_", last: "adgr_1867-01-30_"}],

    ["明治天皇", {first: "adgr_1867-01-30_", last: "adgr_1912-07-30_"}],
    ["大正天皇", {first: "adgr_1912-07-30_", last: "adgr_1926-12-25_"}],
    ["昭和天皇", {first: "adgr_1926-12-25_", last: "adgr_1989-01-07_"}],
    ["今上天皇", {first: "adgr_1989-01-07_", last: "adgr_2019-04-30_"}],
    ["（新天皇）", {first: "adgr_2019-05-01_", last: FDate::InfinityDate.positive}],
  ].map do |key, _term|
    self.reign_collection.inherit(key) do
      self.term = FDate::Term.new(_term)
    end
  end

  # 両統迭立・南北朝時代
  self.reign_collection.merge!(collection["japanese/south/Tennou"].reign_collection)
  self.reign_collection.merge!(collection["japanese/north/Tennou"].reign_collection)
end
