# 無限に続く日付パート
FDate::Part.modules.build "Infinity" do

  def self.included(klass)
    super(klass)

    klass.class_eval do

      # 許容される範囲に無限過去から無限未来をセット
      self.max_range = -Float::INFINITY..Float::INFINITY
    end
  end

  def initialize(num: nil, **args)
    validate_nildate(num)
    super(num: num, **args)
  end

  private

  # @raise [self::NilDateError] NilDateを作成しようとした場合に発生
  def validate_nildate(num)
    raise FDate::DateError.new(self), "NilDate is not allowed." if num.nil?
  end
end
