# キリスト紀元
FDate::Part.collection.inherit "AnnoDominiYear", from: "OrdinalEpochYear" do

  # ユリウス通年との差
  self.diff_from_jdy = 4713
end
