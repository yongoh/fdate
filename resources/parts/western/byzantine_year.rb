# 世界創造紀元
FDate::Part.collection.inherit "ByzantineYear", from: "OrdinalEpochYear" do

  # ユリウス通年との差
  self.diff_from_jdy = -795
end