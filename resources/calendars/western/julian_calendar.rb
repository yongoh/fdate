# ユリウス暦
FDate::Calendar.collection.inherit "JulianCalendar", from: "SolarCalendar" do

  # この暦における一年の日数
  #
  # `(1461/4) = 365.25`
  self::SOLAR_YEAR_DAYS = 365 + 1/4r

  # 計算用
  # @note 継承先のグレゴリオ暦でも使う
  self::JL_YEAR_DAYS = self::SOLAR_YEAR_DAYS

  class_methods "date methods" do

    # @param [Integer] jdy ユリウス通年
    # @return [Boolean] 渡した年が閏年かどうか
    def leap_year?(jdy)
      (jdy - 1) % 4 == 0
    end

    # @param [Integer] jdy ユリウス通年
    # @param [Integer] month 月
    # @return [Integer] この年この月の日数
    def mdays(jdy, m)
      case m
      when 1,3,5,7,8,10,12 then 31
      when 2 then               leap_year?(jdy) ? 29 : 28
      when 4,6,9,11 then        30
      end
    end

    # ユリウス通日から年月日ハッシュに変換
    # @param [Integer] jd ユリウス通日
    # @return [Hash] インスタンス化用ハッシュ
    def from_jd(jd)
      b = jd + 1524
      c = ((b - 122.1) / self::JL_YEAR_DAYS).floor
      d = b - (self::JL_YEAR_DAYS * c).floor
      e = (d / 30.6001).floor
      {
        year: c - (e < 13.5 ? 3 : 2), # ユリウス通年
        month: e - (e < 13.5 ? 1 : 13),
        day: d - (30.6001 * e).floor,
      }
    end

    # 年月日からユリウス通日に変換
    # @param [Integer] jdy ユリウス通年
    # @param [Integer] month 月
    # @param [Integer] day 日
    # @return [Integer] ユリウス通日
    def to_jd(jdy, month, day)
      if month < 3 # 1月、2月は前年の13月、14月として扱う
        jdy -= 1
        month += 12
      end
      (self::JL_YEAR_DAYS * (jdy - 1)).floor + (30.59 * (month - 2)).floor + day + 29
    end
  end
end
