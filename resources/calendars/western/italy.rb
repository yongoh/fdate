# イタリア（ローマ・カトリック教会）における暦の変遷
FDate::Calendar.collection.inherit "Italy", from: FDate::CalendarSystem do
  self.calendars = [
    FDate::Period.new("adjl", FDate::Term.new(first: "adjl_A=8-01-01_", last: "adjl_A=1582-10-04_")),
    FDate::Period.new("adgr", FDate::Term.new(first: "adgr_A=1582-10-15_")),
  ]
end
