# 世界創造紀元＋ユリウス暦
FDate::Calendar.collection.inherit "bzjl", from: "JulianCalendar" do
  self.year_class = "ByzantineYear"
end
