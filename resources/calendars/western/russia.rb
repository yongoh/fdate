# ロシアにおける暦の変遷
FDate::Calendar.collection.inherit "Russia", from: FDate::CalendarSystem do
  self.calendars = FDate::Period::Set.fill_last([
    ["bzjl", {first: "adjl_A=9**_"}],
    ["adjl", {first: "adjl_1700-01-01_"}],
    ["adgr", {first: "adgr_1918-02-14_"}],
    ["USSR", {first: "adgr_1929-10-01_"}],
    ["adgr", {first: "adgr_1940-06-27_"}],
  ]){|key, _term, &to_term|
    FDate::Period.new(key, to_term.call(_term))
  }

  self.default_calendar = "adjl"
end
