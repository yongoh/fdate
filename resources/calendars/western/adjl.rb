# 西暦（キリスト紀元＋ユリウス暦）
FDate::Calendar.collection.inherit "adjl", from: "JulianCalendar" do
  self.year_class = "AnnoDominiYear"
end
