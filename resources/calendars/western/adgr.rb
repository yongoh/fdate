# 西暦（キリスト紀元＋グレゴリオ暦）
FDate::Calendar.collection.inherit "adgr", from: "GregorianCalendar" do
  self.year_class = "AnnoDominiYear"
end
