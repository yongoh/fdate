# イングランド・大英帝国（植民地含む）における暦の変遷
FDate::Calendar.collection.inherit "England", from: FDate::CalendarSystem do
  self.calendars = [
    FDate::Period.new("adjl", FDate::Term.new(first: "adjl_A=8-01-01_", last: "adjl_A=1752-09-02_")),
    FDate::Period.new("adgr", FDate::Term.new(first: "adgr_A=1752-09-14_")),
  ]
end
