# グレゴリオ暦
FDate::Calendar.collection.inherit "GregorianCalendar", from: "JulianCalendar" do

  # この暦における一年の日数
  #
  # `(146097/400) = 365.2425`
  # @note 計算には使わない
  self::SOLAR_YEAR_DAYS = 365 + 97/400r

  class_methods "date methods" do

    # @param [Integer] jdy ユリウス通年
    # @return [Boolean] 渡した年が閏年かどうか
    def leap_year?(jdy)
      super(jdy) && (jdy - 13) % 100 != 0 || (jdy + 87) % 400 == 0
    end

    # ユリウス通日から年月日ハッシュに変換
    # @param [Integer] jd ユリウス通日
    # @return [Hash] インスタンス化用ハッシュ
    def from_jd(jd)
      a = ((jd - 1867216.25) / 36524.25).floor
      super(jd + 1 + a - a / 4)
    end

    # 年月日からユリウス通日に変換
    # @param [Integer] jdy ユリウス通年
    # @param [Integer] month 月
    # @param [Integer] day 日
    # @return [Integer] ユリウス通日
    def to_jd(jdy, month, day)
      revise400 = ((jdy + 87) / 400).floor # 400年に一度の閏年の調整
      revise100 = ((jdy - 13) / 100).floor # 100年に一度の閏年の調整
      super(jdy, month, day) + revise400 - revise100 + 37
    end
  end
end
