# ソビエト連邦暦
# https://ja.wikipedia.org/wiki/%E3%82%BD%E3%83%93%E3%82%A8%E3%83%88%E9%80%A3%E9%82%A6%E6%9A%A6
FDate::Calendar.collection.inherit "USSR", from: "GregorianCalendar" do
  self.year_class = "AnnoDominiYear"

  # @return [Integer] 週5日制の曜日
  # @todo 単純にユリウス通日から計算できるわけでは無いようなので実装が難しそう
  def day_of_5week
    # 未実装
  end

  # @return [Integer] 週6日制の曜日
  # @todo 単純にユリウス通日から計算できるわけでは無いようなので実装が難しそう
  def day_of_6week
    # 未実装
  end
end
