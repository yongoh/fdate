# 元号年の絞り込み機能
# @node 年に元号年セットを使っている日付クラスにインクルードする
FDate::Calendar.modules.build "NarrowDownReigns" do

  def initialize(*args, &block)
    super(*args, &block)
    narrow_down_reigns
  end

  private

  # 日付に応じて年オブジェクトを作成し直す
  def narrow_down_reigns
    @parts = @parts.merge(year: year.class.jdy(year.jdy, self))
  end
end
