# 地球の暦の基本形
# @note `self.parts = {year: FDate::Part[Year], month: FDate::Part[EarthMonth], day: FDate::Part[Day]}`
FDate::Calendar.collection.inherit "EarthCalendar" do
  class_methods "boolean methods" do

    # @return [Boolean] この暦法が太陽暦であるかどうか
    def solar?
      self <= collection["SolarCalendar"]
    end

    # @return [Boolean] この暦法が太陰暦であるかどうか
    def lunar?
      self <= collection["LunarCalendar"]
    end

    # @return [Boolean] この暦法が太陰太陽暦であるかどうか
    def lunisolar?
      self <= collection["LuniSolarCalendar"]
    end
  end

  class_methods "DateMethods" do

    # @param [Integer] jdy ユリウス通年
    # @return [Integer] 渡した年の日数
    def ydays(jdy)
      parts[:month].max_range.reduce(0){|tmp, m| tmp + mdays(jdy, m) }
    end

    # 年月日ハッシュからユリウス通日に変換
    # @option hash [Integer] year ユリウス通年
    # @option hash [Integer] month 月
    # @option hash [Integer] day 日
    # @return [Integer] ユリウス通日
    def jd_from_hash(hash)
      to_jd(hash[:year], hash[:month], hash[:day])
    end
  end


  # @return [Integer] この年に入って経過した日数
  # @return [nil] fuzzyの場合
  def day_of_year
    jd - self.class.to_jd(year.jdy, 1, 1) + 1 if fixed?
  end

  alias_method :yday, :day_of_year

  # @return [Integer] この月に入って経過した日数
  # @return [nil] fuzzyの場合
  def day_of_month
    day.global_i if fixed?
  end

  alias_method :mday, :day_of_month

  # @return [Integer] この週に入って経過した日数（日曜日を0とする）
  # @return [nil] fuzzyの場合
  def day_of_week
    (jd + 1) % 7 if fixed?
  end

  alias_method :wday, :day_of_week

  # @return [Integer] 1月1日を含む週を第1週とする日曜日から始まる週の週番号（アメリカ式）
  # @return [nil] fuzzyの場合
  def week
    (day_of_year - wday + 12) / 7 if fixed?
  end

  # @return [Integer] 最初の木曜日を含む週を第1週とする月曜日から始まる週の週番号（ヨーロッパ式）
  # @return [nil] fuzzyの場合
  def cweek
    case cwy_diff
    when -1 then ((self.class.ydays(year.jdy - 1) + day_of_year - cwday).to_f / 7).round + 1
    when 0 then ((day_of_year - cwday).to_f / 7).round + 1
    when 1 then 1
    end
  end

  # @return [Integer] 週番号（`#cweek`）に対応する年
  # @return [nil] fuzzyの場合
  def cwyear
    year.jdy + cwy_diff if fixed?
  end

  # @return [Integer] 月曜日を1とした週番号 (1-7)
  # @return [nil] fuzzyの場合
  def cwday
    sunday? ? 7 : wday
  end

  # @return [-1] この週の中間が昨年に位置する場合
  # @return [0] この週の中間が今年に位置する場合
  # @return [1] この週の中間が来年に位置する場合
  # @return [nil] fuzzyの場合
  # @note 2つの年にまたがる週はより日数が多い年に属するものとする
  # @todo メソッド名は仮
  def cwy_diff
    if fixed?
      wm = day_of_year - cwday + 3.5 # この週の中間の日付
      if wm < 0
        -1
      elsif wm > self.class.ydays(year.jdy)
        1
      else
        0
      end
    end
  end

  %i(sunday? monday? tuesday? wednesday? thursday? friday? saturday?).each_with_index do |method_name, i|

    # @return [Boolean] ？曜日であるかどうか
    define_method(method_name){ day_of_week == i }
  end
end
