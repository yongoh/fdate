# 太陽暦
FDate::Calendar.collection.inherit "SolarCalendar", from: "EarthCalendar" do

  class_methods "setting methods" do

    # @param [Integer] year ユリウス通年
    # @param [Integer] month 月
    # @return [Class<FDate::Part[Day]>] この年この月の日数を範囲として持つ日クラス
    def day_class(year: , month: )
      last = mdays(year, month)
      parts[:day].inherits_to do
        self.last = last
      end
    end

    private

    # 年クラスを設定（月・日クラスは既定）
    # @param [Class<FDate::Part[Year]>,String] key 年クラスかそのキー
    def year_class=(key)
      self.parts = {year: key, month: "EarthMonth", day: "Day"}
    end
  end

  # @return [Boolean] この年が閏年かどうか
  def leap_year?
    calendar.leap_year?(year.jdy) if year.fixed?
  end
end
