# 元嘉暦
FDate::Calendar.collection.inherit "GenkaReki", from: "LuniSolarCalendar" do

  # 1太陽年の日数
  #
  # ≒365.2467日
  self::SOLAR_YEAR_DAYS = 365 + 75 / 304

  # 1朔望月の日数
  #
  # ≒29.530585日
  self::SYNODIC_MONTH_DAYS = 29 + 399 / 752

  # @todo 未実装
end
