# 儀鳳暦
FDate::Calendar.collection.inherit "GihouReki", from: "LuniSolarCalendar" do

  # 1太陽年の日数
  #
  # ≒365.24477日
  self::SOLAR_YEAR_DAYS = 365 + 328 / 1340

  # 1朔望月の日数
  #
  # ≒29.530597日
  self::SYNODIC_MONTH_DAYS = 29 + 711 / 1340

  # @todo 未実装
end
