# 日本の暦の変遷
FDate::Calendar.collection.inherit "Japan", from: FDate::CalendarSystem do

  # 紀年法：元号（日本）
  year_class = FDate::Part.collection["japanese/Gengou"]

  self.calendars = FDate::Period::Set.fill_last([
    ["GenkaReki", {first: "adjl_554_ab"}],
    ["GihouReki", {first: "adjl_697_"}],
    ["TaienReki", {first: "adjl_764_"}],
    ["GokiReki", {first: "adjl_858_"}],
    ["SemmyouReki", {first: "adjl_862_"}],
    ["JoukyouReki", {first: "adgr_1685_"}],
    ["HouryakuReki", {first: "adgr_1755_"}],
    ["KanseiReki", {first: "adgr_1798_"}],
    ["TempouReki", {first: 2394615}],
    ["Gregorian", {first: 2405160}],
  ]){|parent_key, _term, &to_term|
    key = [year_class.key, parent_key].join("-")
    calendar = collection[parent_key].inherits_to(key) do
      self.year_class = year_class
    end

    FDate::Period.new(key, to_term.call(_term), context: {calendar: calendar})
  }
end
