# 日本の元号＋グレゴリオ暦
FDate::Calendar.collection.inherit "jpgr", from: "GregorianCalendar" do
  include modules["NarrowDownReigns"]
  self.year_class = "japanese/Gengou"
end
