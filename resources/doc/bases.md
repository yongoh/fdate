# 日付パート
## 年
Year `/resources/parts/year.rb`

主星-自星公転周期
## 日
Day `/resources/parts/day.rb`

自星自転周期
# モジュール（日付パート）
## 序数
Ordinal `/resources/parts/ordinal.rb`
* 上位の日付パートの中で何番目であるかを数える
* 0以下は許可しない
## 無限
Infinity `/resources/parts/infinity.rb`
* 無限過去から無限未来まで表現しうる
* NilDateはありえない

# 日付パート（年クラス・紀年法）
## 紀元・基数年（周年）
CardinalEpochYear `/resources/parts/cardinal_epoch_year.rb`
* 紀元となる特定の年からの経過年数や遡及年数を数える
* 0年があり得る
  * 1：紀元後1年
  * 0：紀元後0年（元年）
  * -1：紀元前1年
### ユリウス通年（仮名）
JDY `/resources/parts/jdy.rb`
* 紀元・基数
* ユリウス通日0日目を含む年を基準とする

## 紀元・序数年（数え年）
OrdinalEpochYear `/resources/parts/ordinal_epoch_year.rb`
* 紀元となる特定の年からの経過年数や遡及年数を数える
* 0年は無く1年から始まる
  * 1：紀元後1年（元年）
  * 0：紀元前1年
  * -1：紀元前1年
## 元号
RegnalYear `/resources/parts/regnal_year.rb`
## 元号セット
RegnalYearSet `/resources/parts/regnal_year_set.rb`
## 循環

# 暦（日付クラス）
## 暦体系
CalendarSystem `/resources/calendars/calendar_system.rb`
# モジュール（暦）
