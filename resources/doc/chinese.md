# 紀年法
### 元号
* 王朝ごとに違っていたはず
* 明朝以降は一世一元の制
* 中華民国は「民国」
* 中華人民共和国では廃止

# 暦法
## 元嘉暦（げんかれき）
GenkaReki `/resources/calendars/chinese/genka_reki.rb`
### 特性
* 太陰太陽暦
  * 1太陽年＝222070(紀日)÷608(紀法)≒365.24671日
  * 1朔望月＝22207(通数)÷752(日法)≒29.530585日
  * 1交点月＝1朔望月×939(会月)÷(939＋80(朔望合数))≒27.212188日
* [平気法](https://ja.wikipedia.org/wiki/平気法)
* [平朔法](https://ja.wikipedia.org/wiki/平朔)
  * 日本で使われた暦法では唯一採用
* 章法（[メトン周期（章法）](https://ja.wikipedia.org/wiki/メトン周期 "メトン周期 - Wikipedia")）
  * 日本の暦法では唯一メトン周期が厳密に成り立つ
  * 19太陽年=235朔望月=6940日
  * 19年に7回の[閏月](https://ja.wikipedia.org/wiki/閏月)）
* 元嘉20年（`AD_A=443_`）より積年5703年前（`AD_B=5261_/JDY_(-547)_`）を基準年とする
* 正月中の雨水を基準とする
  * 中国暦法は普通冬至を基準とする
### 年表
|JD|adjl|ローカル|中国|日本
|-:|-:|-:|-|-|
|||南北朝時代|宋の天文学者・何承天が編纂
||`_A=445_~_A=509_`|`_元嘉=22_~_天監=8_`|南朝の諸王朝（宋・斉・梁）で用いられた
||`_A=554_`|`_欽明天皇=14_`||百済から伝えられる（『日本書紀』）
|1929277|`_A=570-1-27_`|大歳庚寅正月六日庚寅`_庚寅-01-06_`||実証される最古の使用例
||`_A=692_`|`_持統天皇=6_`||**儀鳳暦**と並用し始める（持統天皇4年（`AD_A=690_`）からとの説もある）
||`_A=697_`|`_文武天皇=1_`||元嘉暦を廃止、儀鳳暦のみを使用
### 参照
[Wikipedia](https://ja.wikipedia.org/wiki/元嘉暦 "元嘉暦 - Wikipedia") |
[暦Wiki](http://eco.mtk.nao.ac.jp/koyomi/wiki/B8B5B2C5CEF1.html "暦Wiki/元嘉暦 - 国立天文台暦計算室") |
[天武天皇の年齢研究](http://www7a.biglobe.ne.jp/~kamiya1/mypage-g14.htm "天武天皇の年齢研究－元嘉暦と儀鳳暦")

## 儀鳳暦（ぎほうれき）
GihouReki `/resources/calendars/chinese/gihou_reki.rb`
### 特性
* 麟徳1年の前年11月1日（朔）甲子日（`ch-GihouReki_龍朔=3-11-01_`）（[朔旦冬至](https://ja.wikipedia.org/wiki/朔旦冬至)？）から数えて、上元（積年）269,880年前の同日（`JDY-GihouReki_(-264504)-11-01_`）を基準とする
### 年表
|JD|adjl|ローカル|中国|日本
|-:|-:|-:|-|-|
||`_B=269218-??-??_`|`JDY-GihouReki_(-264504)-11-01_`|この日を基準とする
||`_A=663-??-??_`|`_龍朔=3-11-01_`|この日から数えて269,880年前の同日を基準とする
||`_A=692_`|`_持統天皇=6_`||元嘉暦と並用し始める（持統天皇4年（`AD_A=690_`）からとの説もある）
||`_A=697_`|`_文武天皇=1_`||元嘉暦を廃止、儀鳳暦のみを使用
### 参照
[Wikipedia](https://ja.wikipedia.org/wiki/儀鳳暦 "儀鳳暦 - Wikipedia")

## 大衍暦（たいえんれき/だいえんれき）
TaienReki `/resources/calendars/chinese/taien_reki.rb`
### 参照
[Wikipedia](https://ja.wikipedia.org/wiki/大衍暦 "大衍暦 - Wikipedia")

## 五紀暦（ごきれき）
GokiReki `/resources/calendars/chinese/goki_reki.rb`
### 年表
|JD|adjl|ローカル|中国|日本
|-:|-:|-:|-|-|
||`_A=781_`|||日本に紹介されたが、単独で使われることはなく**大衍暦**と併用された
### 参照
[Wikipedia](https://ja.wikipedia.org/wiki/五紀暦 "五紀暦 - Wikipedia")

## 宣明暦（せんみょうれき）
SemmyouReki `/resources/calendars/chinese/semmyou_reki.rb`
### 参照
[Wikipedia](https://ja.wikipedia.org/wiki/宣明暦 "宣明暦 - Wikipedia")

# 暦体系
