# 日付パート
## 月（朔望月）
EarthMonth `/resources/parts/earth/earth_month.rb`
### 特性
* 1ヶ月は約29日
* およそ1年間に12ヶ月とちょっと
## 閏
Leapable `/resources/parts/earth/leapable.rb`

日付補正のため挿入される余分な期間

# 暦法
## 地球の暦の基本形
EarthCalendar `/resources/calendars/earth/earth_calendar.rb`
### 特性
* 年・月・日の体系
  * 年（太陽-地球公転周期）
  * 月（~~地球-月公転周期~~ 朔望周期）
  * 日（地球の自転周期）

## 太陽暦
SolarCalendar `/resources/calendars/earth/solar_calendar.rb`
### 特性
* 太陽の運行を基準に暦を決定する
  * 月の運行は考慮しない
* 1年の日数の小数点以下を切り捨てることによるズレを補正するため、数年おきに閏日を挿入する
### 参照
[Wikipedia](https://ja.wikipedia.org/wiki/太陽暦 "太陽暦 - Wikipedia")

## 太陰暦
LunarCalendar `/resources/calendars/earth/earth_calendar.rb`
### 特性
* 月の運行を基準に暦を決定する
  * 太陽の運行は考慮しない

## 太陰太陽暦
LuniSolarCalendar `/resources/calendars/earth/earth_calendar.rb`
### 特性
* 月の運行を基準に暦を決定する
* 太陽年と朔望月のズレを補正するため、およそ3年に1度閏月を挿入する
