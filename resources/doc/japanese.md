# 紀年法
## 元号
japanese/Gengou `/resources/parts/japanese/gengou.rb`
### 特性
* 元号
* 元号が設定されていない期間がある
* 南北朝時代など複数の元号体系が併存する期間がある
### 参照
 [Wikipedia](https://ja.wikipedia.org/wiki/%E5%85%83%E5%8F%B7%E4%B8%80%E8%A6%A7_(%E6%97%A5%E6%9C%AC) "元号一覧 (日本) - Wikipedia")

## 天皇在位年
Tennou
### 特性
* 元号
* 天皇がいない期間がある
* 南北朝時代など複数の天皇が併存する期間がある
### 参照
[Wikipedia](https://ja.wikipedia.org/wiki/%E5%A4%A9%E7%9A%87%E3%81%AE%E4%B8%80%E8%A6%A7 "天皇の一覧 - Wikipedia")
## 神武天皇即位紀元
JimmuEpochYear `/resources/parts/japanese/jimmu_epoch_year.rb`

**皇紀**ともいう。
### 特性
* 紀元
### 参照
[Wikipedia](https://ja.wikipedia.org/wiki/神武天皇即位紀元 "神武天皇即位紀元 - Wikipedia")

# 暦法
## 元嘉暦
chinese.md を参照
## 儀鳳暦
chinese.md を参照
## 大衍暦
chinese.md を参照
## 五紀暦
chinese.md を参照
## 宣明暦
chinese.md を参照
## 貞享暦（じょうきょうれき）
JoukyouReki `/resources/calendars/japanese/joukyou_reki.rb`

初めて日本人により編纂された暦
### 参照
[Wikipedia](https://ja.wikipedia.org/wiki/貞享暦 "貞享暦 - Wikipedia")
## 宝暦暦（ほうりゃくれき/ほうれきれき）
HouryakuReki `/resources/calendars/japanese/houryaku_reki.rb`
### 参照
[Wikipedia](https://ja.wikipedia.org/wiki/宝暦暦 "宝暦暦 - Wikipedia")
## 寛政暦（かんせいれき）
KanseiReki `/resources/calendars/japanese/kansei_reki.rb`
### 参照
[Wikipedia](https://ja.wikipedia.org/wiki/寛政暦 "寛政暦 - Wikipedia")
## 天保暦（てんぽうれき）
TempouReki `/resources/calendars/japanese/tempou_reki.rb`

正式には**天保壬寅元暦**（てんぽうじんいんげんれき）。過去の中国暦にも同名の「天保暦」があるため。

### 特性
* 太陰太陽暦
* [定気法](https://ja.wikipedia.org/wiki/定気法)
  * 黄道を*空間*で24等分する。日本では初採用。批判あり。
* [平朔法](https://ja.wikipedia.org/wiki/平朔)？
* 太陽と月の黄径が一致する時を朔とする
* 黄径0°を春分点とし、30°ごとに区切った点を12節気（24節気の中気）とする
* 時刻は京都南中時を採用する。さらに不定時法を用いる
* 朔を含む日を1日とする。
* 冬至11月、春分2月、夏至5月、秋分8月とする
* 閏月は中気を含んではならないが、中気を含まない月が閏月とは限らない
### 年表
|JD|TempouReki|adgr|出来事
|-:|-:|-:|-|
|2394615|`_天保=15-01-01_`|`_A=1844-02-18_`|使用開始
|2405160|`_明治=5-12-03_`|`_A=1873-01-01_`|グレゴリオ暦に移行
### 参照
[Wikipedia](https://ja.wikipedia.org/wiki/天保暦 "天保暦 - Wikipedia") |
[雑多なページ](http://www.tcp-ip.or.jp/~n01/sun/tenporeki.htm "天保暦（てんぽうれき） - 雑多なページ")

### 参照
## グレゴリオ暦
western.md を参照

# 暦体系
## 日本における暦の変遷
japan `/resources/calendars/japanese/japan.rb`
### 年表
|JD|TempouReki|adgr|出来事

暦法|JD|adgr|天保暦|制作地|備考（日本）
-|-:|-:|-:|-|-
元嘉暦||`_A=554-**-**_`||中国|一時儀鳳暦と併用
儀鳳暦||`_A=697-**-**_`||中国|
大衍暦||`_A=764-**-**_`||中国|
五紀暦||`_A=858-**-**_`||中国|単独で使われることはなく大衍暦と併用
宣明暦||`_A=862-**-**_`||中国|
貞享暦||`_A=1685-**-**_`||日本|初の日本人編纂
宝暦暦||`_A=1755-**-**_`||日本|
寛政暦||`_A=1798-**-**_`||日本|
天保暦|2394615|`_A=1844-02-18_`|`_天保=15-01-01_`|日本|初めて定気法を採用
グレゴリオ暦|2405160|`_A=1873-01-01_`|`_明治=6-12-03_`|西洋|太陽暦
