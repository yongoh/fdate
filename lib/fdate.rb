require "fdate/version"

# 外部プラグイン
require "forwardable"
require 'yaml'
require "inheritable_collection"

# *共通部分
require "fdate/build_method_definable"
require "fdate/parseable"
require "fdate/encodable"
require "fdate/comparable"
require "fdate/parse_error"
require "fdate/date_error"

# 自然言語化機能（多言語対応）
require "fdate/strftime"
require "fdate/locale"
require "fdate/name_set"

# 「頃」クラス
require "fdate/about"

# 日付パートクラス
require "fdate/part/build_methods"
require "fdate/part/parseable"
require "fdate/part/range_methods"
require "fdate/part/range_error"
require "fdate/part/encodable"
require "fdate/part"

# *日付クラス互換
require "fdate/full_date/calculatable"
require "fdate/full_date/tarmizable"
require "fdate/full_date"
require "fdate/build_methods"

# 日付クラス
require "fdate/calendar/setting_methods"
require "fdate/calendar/build_methods"
require "fdate/calendar/parseable"
require "fdate/calendar/encodable"
require "fdate/calendar/rangizable"
require "fdate/calendar"

# 期間クラス
require "fdate/term/build_methods"
require "fdate/term"

# 無限過去/未来日付クラス
require "fdate/infinity_date/build_methods"
require "fdate/infinity_date"

# 時代クラス
require "fdate/period/set" #*
#require "fdate/period/lane"
require "fdate/period"

# 暦体系クラス
require "fdate/calendar_system/calendars_methods"
require "fdate/calendar_system/build_methods"
require "fdate/calendar_system"


module FDate

  # FDateコードの禁則文字の配列
  ILLEGAL_CHARACTERS = [SEPARATOR, Term::THROUGH_SEPARATORS, Term::BETWEEN_SEPARATORS].flatten.freeze

  # 「頃」コレクションの設定
  About.collection.configure do |config|
    config.illegal_characters += ILLEGAL_CHARACTERS
    config.autoload_patterns << File.join(Dir.pwd, "resources", "abouts", "%{key}.rb")
  end
  About.collection.default = "j"

  # 日付パートコレクションの設定
  Part.collection.configure do |config|
    config.illegal_characters += ILLEGAL_CHARACTERS
    config.autoload_patterns << File.join(Dir.pwd, "resources", "parts", "**", "%{key}.rb")
  end

  # 暦コレクションの設定
  Calendar.collection.configure do |config|
    config.illegal_characters += ILLEGAL_CHARACTERS
    config.autoload_patterns << File.join(Dir.pwd, "resources", "calendars", "**", "%{key}.rb")
  end
  Calendar.collection.default = "Italy"

  # ロケールコレクションの設定
  Locale.collection.configure do |config|
    config.autoload_patterns << File.join(Dir.pwd, "resources", "locales", "**", "%{key}.yml")
    config.autoload_proc = Locale::AUTOLOAD_PROC
  end
  Locale.collection.default = "ja"
end
