module FDate
  module Encodable

    # @param [Boolean] global trueを渡したらグローバルコード、falseを渡したらローカルコード
    # @return [String] コード
    def code(global = false)
      global ? global_code : local_code
    end

    # エイリアス
    [
      %i(encode code),
      %i(to_s code),
    ].each do |new_name, original_name|
      define_method(new_name){|*args, &block| send(original_name, *args, &block) }
    end

    # グローバルコード
    # @return [String] グローバルコード
    # @note 暦・「頃」などの情報は失われる
    # @example fixidかつ正数の場合
    #   obj.global_code #=> "(1234567)"
    # @example fixidかつ負数の場合
    #   obj.global_code #=> "(-1234567)"
    # @example fuzzyの場合
    #   obj.global_code #=> "(1234500..1234599)"
    def global_code
      "(#{fixed? ? global_range.first : global_range})"
    end

    # コード化
    # @return [String] この暦特有のコード
    def local_code
      # オーバーライド
    end
  end
end
