module FDate
  class Term
    module BuildMethods
      extend BuildMethodDefinable
      include FDate::Parseable

      build_methods :build, :parse, :decode, :jd

      # 引数の型（クラス）ごとに方法を選んでハッシュ化
      # @param [Hash,String,Range,FDate::Calendar] arg 引数
      # @return [Hash] インスタンス化用ハッシュ
      def _build(arg)
        case arg
        when Hash then arg
        when String then _parse(arg)
        when Range then {first: arg.first, last: arg.last, exclude_end: arg.exclude_end?}
        when FDate::Calendar then {first: arg.first, last: arg.last}
        else raise TypeError, "can not convert #{arg.class} into Hash."
        end
      end

      # @param [String] code 判定する文字列
      # @return [Boolean] パース可能なコードかどうか
      def parseable?(code)
        [LOCAL_CODE_PATTERN, GLOBAL_CODE_PATTERN, RANGE_CODE_PATTERN].any?{|pattern| !!pattern.match(code) }
      end

      # @param [String] code ユリウス通日コードまたはユリウス通日範囲コード
      # @param [Class<FDate::Calendar>,String] 暦オブジェクト（日付クラス）もしくはそのキー
      # @return [Hash] インスタンス化用ハッシュ
      def _global_parse(code, calendar: nil)
        calendar = FDate::Calendar.collection[calendar]

        if matched = code.match(RANGE_CODE_PATTERN)
          {
            first: FDate.parse("(#{matched[1]})", calendar: calendar),
            last: FDate.parse("(#{matched[3]})", calendar: calendar),
            exclude_end: matched[2].size == 3,
          }
        else
          fdate = calendar.jd(FDate::Parseable.global_parse(code))
          {first: fdate, last: fdate}
        end
      end

      # @param [String] code 期間コード
      # @return [Hash] インスタンス化用ハッシュ
      def _local_parse(code)
        if matched = code.match(LOCAL_CODE_PATTERN)
          {
            first: matched[:first].empty? ? -FDate::InfinityDate : matched[:first],
            last: matched[:last].empty? ? +FDate::InfinityDate : matched[:last],
            between: BETWEEN_SEPARATORS.include?(matched[:separator]),
          }
        else
          raise FDate::ParseError.new(code)
        end
      end

      # @overload _jd(jd)
      #   @param [Integer] jd ユリウス通日
      #   @return [Hash] 両端が渡した日付のインスタンス化用ハッシュ
      # @overload _jd(jd, calendar: cal)
      #   @param [Integer] jd ユリウス通日
      #   @param [Class<FDate::Calendar>,String] calendar 暦もしくはそのキー
      #   @return [Hash] 両端が渡した日付のインスタンス化用ハッシュ
      # @overload _jd(range)
      #   @param [Range<Integer>] ユリウス通日の範囲
      #   @return [Hash] その範囲のインスタンス化用ハッシュ
      def _jd(jd, calendar: nil)
        calendar = FDate::Calendar.collection[calendar]

        case jd
        when Numeric then {first: calendar.jd(jd), last: calendar.jd(jd)}
        when Range then {first: calendar.jd(jd.first), last: calendar.jd(jd.last), exclude_end: jd.exclude_end?}
        end
      end
    end
  end
end
