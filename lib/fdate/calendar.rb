module FDate

  # 日付クラス/暦オブジェクト
  class Calendar
    extend SettingMethods
    extend BuildMethods
    extend Parseable
    extend Strftime
    include InheritableCollection::ClassMember
    include FullDate
    include Encodable
    include Rangizable
    include Strftime

    # FDateコード日付部分のセパレータ
    SEPARATOR = "-".freeze

    def_class_collection
    def_module_collection config: collection.config

    def self.inherited(klass)
      super

      # クラスの継承時に`@parts`の値も継承
      klass.class_eval do
        instance_variable_set(:@parts, superclass.parts)
      end
    end

    # @return [Boolean] 暦体系かどうか
    def self.calendar_system?
      self <= CalendarSystem
    end


    # @!attribute [r] parts
    #   @return [Hash] `パート名 => FDate::Part`形式のハッシュ
    # @!attribute [r] about
    #   @return [FDate::About] 「頃」オブジェクト
    attr_reader :parts, :about

    # @param [FDate::About,String] about 「頃」オブジェクトかそのキー
    # @param [Hash] parts `パート名 => 日付パートのインスタンス化用引数`となるハッシュ
    def initialize(about: nil, **parts)
      @about = About.collection[about]
      @parts = init_parts(parts).freeze
    end

    # @return [Class<FDate::Calendar>] この日付の暦オブジェクト（実体は日付クラス）
    # @note FDate::Calendar#classと同義
    def calendar
      self.class
    end

    # 他の暦に変換
    # @param [Class<FDate::Calendar>,String] calendar 暦オブジェクトもしくはそのキー
    # @return [FDate::Calendar] fixedの場合は指定した暦の日付オブジェクト
    # @return [FDate::Term] fuzzyの場合は指定した暦の期間オブジェクト
    def convert_to(calendar)
      FDate.jd(jd, calendar: calendar)
    end

    # 同じ暦の同じ日なら真を返す
    # @param [FDate::Calendar] other 日付オブジェクト
    def eql?(other)
      calendar.equal?(other.class) && parts.all?{|name, part| part.eql?(other.parts[name]) } && about.equal?(other.about)
    end

    # @return [Boolean] 誤差無しかどうか
    def fixed?
      !fuzzy?
    end

    # @return [Boolean] 誤差ありかどうか
    def fuzzy?
      parts.values.reverse.any? do |part|
        part && part.fuzzy?
      end
    end

    private

    # @param [Hash] parts `パート名 => 日付パートのインスタンス化用引数`となるハッシュ
    # @return [Hash] `パート名 => 日付パートオブジェクト`となるハッシュ
    def init_parts(parts)
      h = {}
      flag = false
      calendar.parts.map do |name, klass|
        if flag
          [name, nil]
        else
          klass = calendar.send("#{name}_class", h)
          part = klass.build(parts[name], about)
          if part.fixed?
            h[name] = part.global_i
          else
            flag = true
          end
          [name, part]
        end
      end.to_h
    end
  end
end
