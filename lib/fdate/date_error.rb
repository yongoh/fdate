module FDate

  # 日付エラー
  #
  # 暦に存在しない日付を作ろうとした場合に発生
  class DateError < StandardError

    # @!attribute [r] object
    #   @return [Object] 日付オブジェクト・日付パートオブジェクトもしくはそのクラス
    attr_reader :object

    def initialize(object, message = nil)
      @object = object
      super(message)
    end
  end
end
