module FDate
  class Calendar
    module Rangizable

      # @return [Range<Numeric>] ユリウス通日の範囲
      def jd_range
        first_jd..last_jd
      end

      # @return [Integer] fixedの場合はこの日付のユリウス通日
      # @return [Range<Integer>] fuzzyの場合はユリウス通日の範囲
      def jd
        fixed? ? first_jd : jd_range
      end

      %i(first last).each do |direction|
        var = "@_#{direction}"

        # @return [Hash] 最初/最後の日付オブジェクトのインスタンス化用ハッシュ
        define_method("_#{direction}") do
          if instance_variable_defined?(var)
            instance_variable_get(var)
          else
            h = {}
            parts.map do |name, part|
              h[name] =
                if part.nil? # @fixme Year（最初のパート）に対応していない
                  calendar.send("#{name}_class", h).max_range.send(direction)
                else
                  part.send("#{direction}_i")
                end
            end
            instance_variable_set(var, h)
          end
        end
      end

      %i(first begin min).each do |direction|

        # @return [self.class] 最初の日付オブジェクト（fixed）
        define_method(direction) do
          @first ||= calendar.new(_first)
        end

        # @return [Integer] 最初のユリウス通日
        define_method("#{direction}_jd") do
          @first_jd ||= calendar.jd_from_hash(_first)
        end
      end

      %i(last end max).each do |direction|

        # @return [self.class] 最後の日付オブジェクト（fixed）
        define_method(direction) do
          @last ||= calendar.new(_last)
        end

        # @return [Integer] 最後のユリウス通日
        define_method("#{direction}_jd") do
          @last_jd ||= calendar.jd_from_hash(_last)
        end
      end
    end
  end
end
