module FDate
  class Calendar
    module Encodable
      include FDate::Encodable

      # @return [String] FDateコード
      # @example
      #   fdate.local_code #=> "cal_1234-10-2*_ab"
      def local_code
        [calendar.key, local_code_date, about.key].join(FDate::SEPARATOR)
      end

      # @return [String] 日付コード（FDateコードの日付部分）
      # @example
      #   fdate.local_code_date #=> "1234-10-2*"
      def local_code_date
        parts.map do |name, part|
          part ? part.local_code : FDate::Part::FUZZY_CHAR
        end.join(Calendar::SEPARATOR)
      end
    end
  end
end
