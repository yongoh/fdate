module FDate
  class Calendar
    module BuildMethods
      extend BuildMethodDefinable

      build_methods :build, :jd, :xxx

      # 引数の型（クラス）ごとに方法を選んでハッシュ化
      # @param [Object] arg 引数
      # @return [Hash] 日付パート群
      def _build(arg)
        case arg
        when Hash then arg
        when String then _parse(arg)
        when Integer then from_jd(arg)
        else raise TypeError, "can not convert #{arg.class} into Hash."
        end
      end

      # ユリウス通日から日付ハッシュに変換
      # @param [Integer] jd ユリウス通日
      # @return [Hash] 日付パート群
      def from_jd(jd)
        # オーバーライド
      end

      # エイリアス
      [
        %i(_from_i from_jd),
        %i(_jd from_jd),
      ].each do |new_name, original_name|
        define_method(new_name){|*args, &block| send(original_name, *args, &block) }
      end
    end
  end
end
