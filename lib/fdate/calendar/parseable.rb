module FDate
  class Calendar
    module Parseable
      include FDate::Parseable
      extend BuildMethodDefinable

      # @param [String] code 判定する文字列
      # @return [Boolean] パース可能なコードかどうか
      def parseable?(code)
        true
      end

      # 日付コードからハッシュ化
      # @param [String] code 日付コード（FDateコードの日付部）
      # @return [Hash] 日付パートコード群
      # @example
      #   Calendar.parts #=> {:year => FDate::Part[Year], :month => FDate::Part[Month], :day => FDate::Part[Day]}
      #   Calendar._local_parse("1234-12-2*") #=> {:year => "1234", :month => "12", :day => "2*"}
      def _local_parse(code)
        part_codes = code.split(SEPARATOR, parts.size)
        parts.map do |name, klass|
          [name, part_codes.shift]
        end.to_h
      end

      # エイリアス
      [%i(_jd_parse _global_parse)].each do |new_name, original_name|
        define_method(new_name){|*args, &block| send(original_name, *args, &block) }
      end

      build_methods :jd_parse

      # @param [String] code FDateコード（フル）
      # @return [FDate::Calendar,FDate::CalendarSystem] 日付オブジェクト
      def full_parse(code)
        if matched = code.match(CODE_PATTERN)
          calendar, calendar_system = *parse_calendar_key(matched[1])
          if calendar
            fdate = calendar.new(about: parse_about_key(matched[3]), **calendar._parse(matched[2]))
            calendar_system ? calendar_system.new(fdate) : fdate
          elsif calendar_system
            calendar_system.parse(matched[2]) # 「頃」が消えてしまう
          end
        else
          raise ParseError.new(code)
        end
      end

      # @param [String] calendar_key 暦キー（複合 or 単一）
      # @return [Array] 1つめに暦オブジェクト、2つめに暦体系オブジェクトを格納した配列
      #   @note どちらかがnilの可能性もある
      def parse_calendar_key(calendar_key)
        if matched = calendar_key.match(COMPOUND_CALENDAR_KEY_PATTERN)
          [
            collection[matched[1]],
            collection[matched[2]],
          ]
        else
          c = calendar_key == "" ? collection.default : collection[calendar_key]
          c.calendar_system? ? [nil, c] : [c, nil]
        end
      end

      # @param [String] about_key 「頃」キー
      # @return [FDate::About] 「頃」オブジェクト
      def parse_about_key(about_key)
        about_key == "" ? About.collection.default : About.collection[about_key]
      end
    end
  end
end
