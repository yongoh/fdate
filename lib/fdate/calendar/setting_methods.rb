module FDate
  class Calendar

    # 新しい暦を作るときに使うメソッド群
    module SettingMethods

      # @!attribute [r] parts
      #   @return [Hash] 日付パートクラス群
      attr_reader :parts

      private

      # 日付パートクラス群をセット
      # @param [Hash<String => Class<FDate::Part>>] prts 日付パート名をキー、日付パートクラスを値とするハッシュ
      def parts=(prts)
        @parts = prts.map do |name, key|
          define_method(name){ parts[name] }
          method_name = "#{name}_class"
          unless respond_to?(method_name)
            define_singleton_method(method_name){|arg| parts[name] }
          end
          [name, FDate::Part.collection[key]]
        end.to_h.freeze
      end
    end
  end
end
