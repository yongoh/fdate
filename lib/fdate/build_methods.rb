module FDate

  # FDateコードのセクション間のセパレータ
  SEPARATOR = "_".freeze

  # FDateコードの正規表現
  CODE_PATTERN = /\A(.*?)#{SEPARATOR}(.+?)#{SEPARATOR}(.*?)\z/.freeze

  # 複合暦キーの正規表現
  COMPOUND_CALENDAR_KEY_PATTERN = /\A(.+?)\((.+?)\)\z/.freeze

  module BuildMethods

    # @param [String] code 判定する文字列
    # @return [Boolean] パース可能なコードかどうか
    def parseable?(code)
      [
        Parseable::GLOBAL_CODE_PATTERN,
        Term::LOCAL_CODE_PATTERN,
        Term::RANGE_CODE_PATTERN,
        InfinityDate::CODE_PATTERN,
        CODE_PATTERN,
      ].any?{|pattern| !!pattern.match(code) }
    end

    # @param [String] code コード（日付・期間・無限日付）
    # @param [Class<FDate::Calendar>] calendar 暦オブジェクト（日付クラス）もしくはそのキー
    # @return [FDate::Full] 渡したコードの形式に応じた日付系オブジェクト
    def parse(code, calendar: nil)
      case code
      when Parseable::GLOBAL_CODE_PATTERN then FDate::Calendar.collection[calendar].global_parse(code)
      when Term::LOCAL_CODE_PATTERN then Term.local_parse(code)
      when Term::RANGE_CODE_PATTERN then Term.global_parse(code, calendar: calendar)
      when InfinityDate::CODE_PATTERN then InfinityDate.parse(code)
      when CODE_PATTERN then Calendar.full_parse(code)
      when String then raise ParseError.new(code)
      else raise ArgumentError
      end
    end

    # エイリアス
    [%i(decode parse)].each do |new_name, original_name|
      define_method(new_name){|*args, &block| send(original_name, *args, &block) }
    end

    # @overload from_i(jd)
    #   @param [Integer] jd ユリウス通日
    #   @return [FDate::Calendar] デフォルト暦の日付オブジェクト
    # @overload from_i(jd, calendar: cal)
    #   @param [Integer] jd ユリウス通日
    #   @param [Class<FDate::Calendar>,String] calendar 暦もしくはそのキー
    #   @return [FDate::Calendar] 指定した暦の日付オブジェクト
    # @overload from_i(jd, about: ab)
    #   @param [Integer] jd ユリウス通日
    #   @param [FDate::About,String] about 「頃」オブジェクトもしくはそのキー
    #   @return [FDate::Calendar] 渡した「頃」を持つ日付オブジェクト
    # @overload from_i(infinity)
    #   @param [Float::INFINITY] infinity 正または負の無限数値
    #   @return [FDate::InfinityDate] 無限過去/未来オブジェクト
    # @overload from_i(range)
    #   @param [Range<Integer>] ユリウス通日の範囲
    #   @return [FDate::Term] 期間オブジェクト
    def from_i(jd, calendar: nil, about: nil)
      calendar = FDate::Calendar.collection[calendar]

      case jd
      when Float::INFINITY, -Float::INFINITY then InfinityDate.new(0 < jd)
      when Numeric then calendar.new(about: about, **calendar._jd(jd))
      when Range then Term.jd(jd, calendar: calendar)
      else raise ArgumentError
      end
    end

    # # エイリアス
    [%i(jd from_i)].each do |new_name, original_name|
      define_method(new_name){|*args, &block| send(original_name, *args, &block) }
    end

    # 引数の型（クラス）ごとに方法を選んでインスタンス化
    # @param [Object] arg 引数
    # @return [FDate::Calendar] 日付オブジェクト
    # @return [FDate::Term] 期間オブジェクト
    # @return [FDate::InfinityDate] 無限日付オブジェクト
    # @raise [FDate::BuildError] インスタンス化できない種類の引数を与えた場合に発生
    def build(arg)
      case arg
      when FDate::FullDate then arg
      when Hash then new(arg)
      when String then parse(arg)
      when Numeric, Range then from_i(arg)
      else raise TypeError, "can not instantiate from #{arg.class}."
      end
    end

    # @overload new(hash, calendar: cal)
    #   @param [Hash] hash 日付クラスのインスタンス化用ハッシュ
    #   @param [Class<FDate::Calendar>,String] calendar 暦オブジェクトもしくはそのキー
    #   @return [FDate::Calendar] 日付オブジェクト
    # @overload new(hash)
    #   @param [Hash] hash 期間クラスのインスタンス化用ハッシュ
    #   @return [FDate::Term] 期間オブジェクト
    def new(hash, calendar: nil)
      if hash.has_key?(:first) || hash.has_key?(:last)
        Term.new(hash)
      else
        calendar = FDate::Calendar.collection[calendar]
        calendar.new(hash)
      end
    end
  end
  extend BuildMethods
end
