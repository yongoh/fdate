module FDate

  # 日付パートクラス
  class Part
    extend BuildMethods
    extend Parseable
    extend RangeMethods
    extend FDate::Strftime
    include InheritableCollection::ClassMember
    include Encodable
    include Comparable
    include FDate::Strftime

    module ClassMethods

      # @param [Class<FDate::Part>] other 比較対象の日付パートクラス
      # @return [Boolean] 自身と同一もしくは共通祖先を持つ日付パートクラスかどうか
      def eql?(other)
        other.respond_to?(:first_ancestor_with_key) && first_ancestor_with_key.equal?(other.first_ancestor_with_key)
      end
    end
    extend ClassMethods

    # 日付パートコードのfuzzy桁を表す文字
    FUZZY_CHAR = "*".freeze

    # 日付パートコードの数値部の正規表現
    FUZZY_NUM_PATTERN = /\A[\d#{FUZZY_CHAR}]+\z/.freeze

    def_class_collection
    def_module_collection config: collection.config

    def self.inherited(klass)
      super

      # クラスの継承時に`::max_range`の値も継承
      klass.class_eval do
        instance_variable_set(:@max_range, superclass.instance_variable_get(:@max_range))
      end
    end

    # @!attribute [r] global_range
    #   @return [Range<Numeric>] 日付数値の範囲
    # @!attribute [r] accuracy
    #   @return [Integer] 有効桁数
    # @!attribute [r] about
    #   @return [FDate::About,nil] 「頃」オブジェクト
    attr_reader :global_range, :accuracy, :about

    # @!attribute [r] local_range
    #   @return [Range<Numeric>] このクラス特有の日付数値の範囲
    alias_method :local_range, :global_range

    # @param [Integer,nil] num `@global_range`を作るのに使う数値
    # @param [Integer] accuracy 有効桁数（0以上・numの桁数以下の整数）
    # @param [FDate::About,String,nil] about 「頃」オブジェクトかそのキー
    def initialize(num: nil, accuracy: 0, about: nil)
      @accuracy = num.nil? ? Float::INFINITY : accuracy
      @about = About.collection[about] if fuzzy?
      @global_range = init_range(num)
    end

    # @param [FDate::Part] other 比較対象の日付パートオブジェクト
    # @return [Boolean] クラスと値が同一かどうか
    def eql?(other)
      self.class.eql?(other.class) && global_range == other.global_range && accuracy == other.accuracy && about.equal?(other.about)
    end

    alias_method :===, :eql?

    # @param [FDate::Part] other 比較対象の日付パートオブジェクト
    # @return [-1] `self < other`の場合
    # @return [0] `self == other`の場合
    # @return [1] `self > other`の場合
    # @return [nil] otherが比較できないオブジェクトの場合
    def <=>(other)
      self.class.eql?(other.class) ? self <=> other.global_range : super
    end

    # 数値化
    # @return [Integer] 日付数値
    # @note fixedでもfuzzyでも必ず数値を返す
    def to_i
      global_range.first
    end

    # @return [Numeric] 日付数値
    # @return [nil] fuzzyの場合
    def global_i
      global_range.first if fixed?
    end

    # @return [Numeric] このクラス特有の日付数値
    # @return [nil] fuzzyの場合
    # @note `#code`,`#strftime`用
    def local_i
      local_range.first if fixed?
    end

    %i(first last begin end min max).each do |direction|

      # @overload first()
      #   @return [FDate::Part] 最初/最後の日付パートオブジェクト（自身と同じクラスかつfixed）
      # @overload first(n)
      #   @param [Integer] n 取得する個数
      #   @return [FDate::Part] 最初/最後から数えてn個の日付パートオブジェクトの配列
      # @overload first(){}
      #   @yield [x] ソート条件ブロック
      #   @return [FDate::Part] ブロックの条件でソートした上で最初/最後の日付パートオブジェクト（自身と同じクラスかつfixed）
      # @see Range#first
      # @see Range#last
      define_method(direction) do |n = nil, &block|
        if n
          global_range.send(direction, n).map{|i| self.class.new(num: i) }
        else
          self.class.new(num: global_range.send(direction, &block))
        end
      end

      # @return [Integer] 最初/最後の日付数値
      define_method("#{direction}_i") do |*args, &block|
        global_range.send(direction, *args, &block)
      end
    end

    # @return [Boolean] 誤差無しかどうか
    def fixed?
      accuracy == 0
    end

    # @return [Boolean] 誤差ありかどうか
    def fuzzy?
      accuracy > 0
    end

    # @return [Boolean] NilDateかどうか
    def nildate?
      accuracy == Float::INFINITY
    end

    private

    # 純粋に`num`と`#accuracy`だけでRangeを作る
    # @param [Integer] num
    # @return [Range] 日付数値の範囲
    def raw_range(num)
      a = 10 ** accuracy
      b = num.abs / a * a
      c = b + a - 1
      num >= 0 ? b..c : -c..-b
    end

    # `self.class.max_range`と`@about`を加味して日付数値範囲を作る
    # @param [Integer] num
    # @return [Range] 日付数値の範囲
    def init_range(num)
      if fixed?
        self.class.trim_range(num..num)
      elsif nildate?
        about * self.class.max_range
      elsif fuzzy?
        self.class.trim_range(about * raw_range(num))
      end
    end
  end
end
