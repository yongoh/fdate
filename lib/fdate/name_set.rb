module FDate
  class NameSet
    extend Forwardable

    # @!attribute [r] names
    #   @return [Hash] 名前のハッシュ
    attr_reader :names

    def_delegators :names,
      :default, :default=, :default_proc, :default_proc=

    # @!attribute [rw] key_proc
    #   @return [Proc] オブジェクトを引数にとりキーを返す関数
    attr_accessor :key_proc

    def initialize(names)
      @names = names
    end

    # @param [Object] obj レシーバ
    # @return [String] 名前
    def [](obj)
      names[key_of(obj)]
    end

    # @param [Object] obj レシーバ
    # @return [Object] 名前のキー（ハッシュの場合）
    def key_of(obj)
      key_proc.call(obj)
    end

    # @param [Hash] hash ビルド用ハッシュ
    #   @note キーは文字列
    # @return [FDate::NameSet] インスタンス
    def self.build(hash)
      name_set = new(names_build(hash["names"]))
      name_set.default = hash["default"]
      name_set.default_proc = default_proc_build(hash["default_proc"]) if hash.has_key?("default_proc")
      name_set.key_proc = key_proc_build(hash["key_proc"])
      name_set
    end

    # @param [Object] names 名前群
    # @return [Hash] 名前のハッシュ
    def self.names_build(names)
      if names.respond_to?(:each_with_index)
        names.each_with_index.map{|item, i|
          item.is_a?(Enumerable) ? item : [i, item]
        }.to_h
      else
        names.to_h
      end
    end

    # @return [Proc] デフォルト値作成用Proc
    # @todo `String`を渡した場合の処理を作成
    def self.default_proc_build(prc)
      case prc
      when Proc then prc
      when Symbol then ->(hash, key){ key.public_send(prc) }
      end
    end

    # @return [Proc] キー作成用Proc
    # @todo `String`を渡した場合の処理を作成
    def self.key_proc_build(prc)
      case prc
      when Proc then prc
      when Symbol then prc.to_proc
      end
    end
  end
end
