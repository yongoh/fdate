module FDate
  module FullDate

    # @note `Date#<<`（nヶ月前）,`Date#>>`（nヶ月後）は再現していない
    # @todo 日付パートオブジェクトを渡して、n年前/後・nヶ月前/後の計算もできるようにしたいが難しそう
    module Calculatable

      # 数値を渡すとn日後の日付オブジェクトを返す
      # @param [Integer] n 日数
      # @return [FDate::Calendar] 自身がfixedの場合、n日後の日付オブジェクト
      # @return [FDate::Term] 自身がfuzzyの場合、n日後の期間オブジェクト
      def +(n)
        if fixed?
          calendar.jd(jd + n)
        else
          Term.jd((first_jd + n)..(last_jd + n), calendar: calendar)
        end
      end

      # @overload -(n)
      #   @param [Integer] n 日数
      #   @return [self.class] n日前の日付オブジェクト
      # @overload -(date)
      #   @param [FDate::FullDate] date fixedな日付
      #   @return [Integer] nから自身までの日数
      # @overload -(date)
      #   @param [FDate::FullDate] date fuzzyな日付
      #   @return [Range<Integer>] nから自身までの日数の範囲
      def -(n)
        if n.is_a?(FDate::FullDate)
          if fixed? && n.fixed?
            jd - n.jd
          else
            f = first_jd - n.first_jd
            l = last_jd - n.last_jd
            f, l = l, f if f > l
            f..l
          end
        else
          self + -n
        end
      end

      # @return [self.class] 次の日の日付オブジェクト
      # @note 一部イテレータメソッド内部で使う用
      # @optimize これを使うと一部のメソッドがやたらと時間がかかる
      # def succ
      #   self + 1
      # end
      # alias_method :next, :succ
    end
  end
end
