module FDate
  module FullDate

    # 期間オブジェクトへ変換するメソッド群
    # @note 引数`other`は`FDate::FullDate`をインクルードしている必要がある
    # @note 引数`options`は`FDate::Term#initialize`のオプションと同じ
    module Tarmizable

      # `first..last`
      # @return [FDate::Term] 自身の範囲の期間オブジェクト
      def to_dr(**options)
        FDate::Term.new(first: first, last: last, **options)
      end

      # `self..other`
      # @return [FDate::Term] 自身から`other`までの期間オブジェクト
      def to(other, **options)
        FDate::Term.new(first: self, last: other, **options)
      end

      alias_method :>>, :to

      # `other..self`
      # @return [FDate::Term] `other`から自身までの期間オブジェクト
      def from(other, **options)
        FDate::Term.new(first: other, last: self, **options)
      end

      alias_method :<<, :from

      # `self~other`
      # @return [FDate::Term] 自身から`other`までの期間オブジェクト（ずっと）
      def &(other)
        to(other, between: false)
      end

      # @note `#~`は単項式用なので思ったようには使えないらしい。代用として`#&`を割り当てることにする。
      alias_method :~, :&

      # `self^other`
      # @return [FDate::Term] 自身から`other`までの期間オブジェクト（いずれか）
      def |(other)
        to(other, between: true)
      end

      alias_method :^, :|
    end
  end
end
