module FDate

  # 「頃」クラス
  class About
    include InheritableCollection::InstanceMember
    include FDate::Strftime

    def_instance_collection

    # @!attribute [r] range
    #   @return [Range<Numeric>] 範囲
    attr_reader :range

    # @param [String] key キー
    # @param [Range<Numeric>] range 範囲
    def initialize(key, range)
      self.key = key
      @range = range

      validate_order
      validate_range
    end

    # 範囲を加工する
    # @param [Range] rng 加工前Range
    # @return [Range] 加工済みRange
    def *(rng)
      length = rng.max - rng.min
      if length.abs == Float::INFINITY
        rng
      else
        min = ((length + 1) * range.min + rng.min).floor
        max = (length * range.max + rng.min).floor
        min..max
      end
    end

    private

    # @raise [ArgumentError] 範囲が降順の場合
    def validate_order
      if range.first > range.last
        raise ArgumentError, "#{range} is descending order"
      end
    end

    # @raise [ArgumentError] 範囲が0以上1以下に収まらない場合
    def validate_range
      unless 0 <= range.first && 1 >= range.last
        raise ArgumentError, "#{range} is not included 0..1"
      end
    end
  end
end
