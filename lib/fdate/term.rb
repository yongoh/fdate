module FDate

  # 期間クラス
  class Term
    extend Forwardable
    extend BuildMethods
    include FullDate
    include Strftime

    # 期間コードのセパレータ（最初から最後までずっとを表す）
    THROUGH_SEPARATORS = %w(~ &).map(&:freeze).freeze

    # 期間コードのセパレータ（最初から最後までのいずれかの時点を表す）
    BETWEEN_SEPARATORS = %w(^ |).map(&:freeze).freeze

    # 期間コードの正規表現
    separator_pattern = Regexp.union(THROUGH_SEPARATORS + BETWEEN_SEPARATORS)
    LOCAL_CODE_PATTERN = /^(?:\{(?<first>.*?)\}|(?<first>.*?))(?<separator>#{separator_pattern})(?:\{(?<last>.*?)\}|(?<last>.*?))$/.freeze

    # ユリウス通日範囲コードの正規表現
    num_pattern = Regexp.union(Parseable::NUM_PATTERN, Parseable::INFINITY_PATTERN)
    RANGE_CODE_PATTERN = /\A\((#{num_pattern})(\.{2,3})(#{num_pattern})\)\z/.freeze


    def_delegators :rng, *%i(begin first end last max min exclude_end?)

    # 以下のメソッドは内部で`FullDate#succ`を使用
    # def_delegators :rng, *%i(=== include? member? each size step bsearch)

    # @!attribute [r] rng
    #   @return [Range<FullDate>] 日付オブジェクトの範囲
    #   @todo メソッド名が不明瞭
    attr_reader :rng

    # @param [FullDate] first 範囲の最初の日付
    # @param [FullDate] last 範囲の最後の日付
    # @param [Boolean] exclude_end 最後を含むかどうか
    # @param [Boolean] between 真なら「ずっと」、偽なら「いずれかの時点」を表す
    #   @note 今のところ機能的な意味はない
    def initialize(first: -FDate::InfinityDate, last: +FDate::InfinityDate, exclude_end: false, between: false)
      first = FDate.build(first)
      last = FDate.build(last)

      # 期間オブジェクトが日付オブジェクトになるまで完全に丸める
      # @note 正確な情報は保持できないが、処理が非常に楽になる
      first = first.first if first.term?
      last = last.last if last.term?

      @rng = Range.new(first, last, exclude_end)
      @between = !!between
    end

    # @param [FDate::Term] other 比較対象の期間オブジェクト
    # @return [Boolean] `#rng`の各値と`#between?`が同じかどうか
    def eql?(other)
      self.class.equal?(other.class) && rng.eql?(other.rng) && between? == other.between?
    end

    # @return [Range<Integer>] ユリウス通日の範囲
    def jd_range
      Range.new(first_jd, last_jd, exclude_end?)
    end

    alias_method :jd, :jd_range

    %i(first last begin end min max).each do |direction|
      jd_method_name = "#{direction}_jd"

      # @return [Integer] 最初/最後のユリウス通日
      define_method(jd_method_name) do
        send(direction).send(jd_method_name)
      end

      # @return [FullDate] 最初/最後のfixedな日付
      define_method("fixed_#{direction}") do
        send(direction).send(direction)
      end
    end

    # @param [Boolean] fixed fixed化するかどうか
    # @return [FDate::Term] 期間オブジェクト
    # @todo オプション名`fixed`を動詞にする
    def to_dr(fixed: false, exclude_end: exclude_end?, between: between?)
      f = fixed ? fixed_first : first
      l = fixed ? fixed_last : last
      FDate::Term.new(first: f, last: l, exclude_end: exclude_end, between: between)
    end

    # @return [String] ユリウス通日範囲コード
    # @note `between?`の情報は含まない
    def global_code
      "(#{jd_range})"
    end

    # @return [String] 期間コード
    # @note `exclude_end`の情報は含まない
    def local_code
      [first, last].map(&:local_code).join(separator)
    end

    # @return [Boolean] 両端が誤差無しかどうか
    def fixed?
      first.fixed? && last.fixed?
    end

    # @return [Boolean] どちらかが誤差ありかどうか
    def fuzzy?
      first.fuzzy? || last.fuzzy?
    end

    # @return [Boolean] 最初から最後までずっとかどうか
    def through?
      !@between
    end

    # @return [Boolean] 最初から最後までのいずれかの時点かどうか
    def between?
      @between
    end

    private

    # @return [String] コードに使うセパレータ
    # @example 「ずっと」の場合
    #   data_range.through? #=> true
    #   data_range.send(:separator) #=> "~"
    # @example 「いずれか」の場合
    #   data_range.between? #=> true
    #   data_range.send(:separator) #=> "^"
    def separator
      between? ? BETWEEN_SEPARATORS.first : THROUGH_SEPARATORS.first
    end
  end
end
