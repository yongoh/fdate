module FDate
  class Part
    module Encodable
      include FDate::Encodable

      # @return [String] 日付パートコード
      def local_code
        # オーバーライド
        fuzzy_num_code
      end

      # コードの数値部分
      # @param [Integer] num 数値
      # @return [String] fuzzy指標付きグローバルコード
      # @example fuzzyの場合
      #   part.range #=> 1230..1239
      #   part.accuracy #=> 1
      #   part.send(:fuzzy_num_code) #=> "123*"
      def fuzzy_num_code(num = fuzzy_num_code_i)
        if nildate?
          FUZZY_CHAR
        else
          "#{num.abs / 10 ** accuracy}#{FUZZY_CHAR * accuracy}"
        end
      end

      private

      # @return [Integer] `#fuzzy_num_code`の作成に使う数値
      def fuzzy_num_code_i
        local_range.first.abs
      end
    end
  end
end
