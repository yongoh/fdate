module FDate
  class Part
    module Parseable
      include FDate::Parseable

      # 独自の書式のコードでパース
      # @param [String] code 日付パートコード
      # @return [Hash] インスタンス化用Hash
      def _local_parse(code)
        # オーバーライド
        _parse_fuzzy_num(code)
      end

      private

      # fuzzy指標付きグローバルコードでパース
      # @param [String] code グローバルコード
      # @return [Hash] インスタンス化用Hash
      # @raise [ParseError] コードの文法に間違いがあった場合に発生
      def _parse_fuzzy_num(code)
        if FUZZY_NUM_PATTERN =~ code
          index = code.index(FUZZY_CHAR)
          if index.nil?
            # fixed
            {num: code.to_i, accuracy: 0}
          elsif index == 0
            # nildate
            {num: nil, accuracy: 1}
          else
            # fuzzy
            accuracy = code.size - index
            {num: code.to_i * 10 ** accuracy, accuracy: accuracy}
          end
        else
          raise ParseError.new(code)
        end
      end
    end
  end
end
