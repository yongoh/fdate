module FDate
  class Part

    # 範囲エラー
    class RangeError < FDate::DateError

      # @!attribute [r] range
      #   @return [Range<Numeric>] 渡された範囲
      attr_reader :range

      def initialize(object, range)
        @range = range
        super(object, "#{range} deviates from #{object.max_range}")
      end
    end
  end
end
