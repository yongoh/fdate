module FDate
  class Part
    module RangeMethods

      # @!attribute [rw] max_range
      #   @return [Range<Numeric>] 許容される日付数値の範囲
      attr_accessor :max_range
      private :max_range=

      # @param [Range] range 絞り込み前の範囲
      # @return [Range<Numeric>] `#max_range`の範囲内に`range`を絞り込んで返す
      def trim_range(range)
        validate_range(range)

        a = max_range.include?(range.first)
        b = max_range.include?(range.last)
        if a && b
          range
        elsif a
          range.first..max_range.last
        elsif b
          max_range.first..range.last
        else
          max_range
        end
      end

      # @param [Range<Numeric>] range 判定する範囲
      # @return [Boolean] 渡した範囲が許容される範囲に一部でも引っかかるかどうか
      #
      # <pre>
      # [max_range] <range>
      #      [  ] <  > #=> false
      #      [  X  >   #=> true
      #      [ <] >    #=> true
      #     [< >]      #=> true
      #      {  }      #=> true
      #     <[ ]>      #=> true
      #    < [> ]      #=> true
      #   <  X  ]      #=> true
      # <  > [  ]      #=> false
      # </pre>
      def valid_range?(range)
        max_range.min <= range.max && max_range.max >= range.min
      end

      # @param [Range<Numeric>] range 判定する範囲
      # @raise [RangeError] 渡した範囲が許容される範囲に含まれない場合に発生
      def validate_range(range)
        raise RangeError.new(self, range) unless valid_range?(range)
      end
    end
  end
end
