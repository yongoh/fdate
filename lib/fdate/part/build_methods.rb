module FDate
  class Part
    module BuildMethods
      extend BuildMethodDefinable

      build_methods :build, :from_i, :nildate

      # 引数の型（クラス）ごとに方法を選んでハッシュ化
      # @param [Object] arg 引数
      # @param [FDate::About,String,nil] about 「頃」オブジェクトかそのキー
      # @return [Hash] インスタンス化用ハッシュ
      def _build(arg, about = nil)
        result = case arg
          when Hash then arg
          when String then _parse(arg)
          when Integer then _from_i(arg)
          when nil then _nildate(about)
          else raise TypeError, "can not convert #{arg.class} into Hash."
          end
        result[:about] ||= about if result.is_a?(Hash) && about
        result
      end

      # 数値から日付パートオブジェクトを作成する
      # @param [Integer] num 数値
      # @return [Hash] インスタンス化用ハッシュ
      def _from_i(num)
        {num: num}
      end

      # NilDateの日付パートオブジェクトを作成する
      # @param [FDate::About,String,Symbol,nil] about 「頃」オブジェクトかそのキー
      # @return [Hash] インスタンス化用ハッシュ
      def _nildate(about = nil)
        {num: nil, about: about}
      end

      %i(first last begin end min max).each do |direction|

        # @return [FDate::Part] 許容される範囲で最も過去/未来の日付パートインスタンス生成用ハッシュ
        define_method("_#{direction}") do
          {num: max_range.send(direction)}
        end

        build_methods direction
      end
    end
  end
end
