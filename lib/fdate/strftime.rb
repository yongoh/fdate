module FDate
  module Strftime

    # パブリックメソッドに移譲する処理記号
    DELEGATION_SIGN = "$".freeze

    # パブリックメソッド結果の`#strftime`に移譲する処理記号
    STRFTIME_SIGN = "@".freeze

    # ロケールから訳文を取得する処理記号
    TRANSLATION_SIGN = ":".freeze

    # フォーマット（整形）条件の記号
    FORMAT_SIGN = "%".freeze

    flag_pattern = '(?<flag>[^-+1-9a-zA-Z.])?'            # フラグ
    width_pattern = '(?<width>[-+]?[1-9]\d*)?'            # 幅
    precision_pattern = '(?:\.(?<precision>[-+]?\d+))?'   # 精度
    format_pattern = [FORMAT_SIGN, flag_pattern, width_pattern, precision_pattern].join

    sign_pattern = "(?<sign>#{Regexp.union(DELEGATION_SIGN, STRFTIME_SIGN, TRANSLATION_SIGN)})"
    name_pattern = '(?<name>\w+)'                         # メソッド名・訳文キー
    bracket_pattern = '(?<bracket>\((?:[^()]+|(\g<bracket>))*\))' # 引数を含む括弧
    replacement_pattern = [sign_pattern, name_pattern, bracket_pattern].join

    specifier_pattern = '(?<specifier>[a-zA-Z])'          # 指示子

    # 置換する独自記法の正規表現
    PATTERN = /(?:#{format_pattern})?#{replacement_pattern}/.freeze

    # %記法の正規表現
    FORMAT_TRANSLATION_PATTERN = Regexp.new([format_pattern, specifier_pattern].join).freeze

    # 引数を渡さない場合の訳文キー
    DEFAULT_TRANSLATION_KEY = :default

    # 代替する文字参照群
    CHARACTER_REFERENCES = {
      DELEGATION_SIGN => '&#36;',
      STRFTIME_SIGN => '&#64;',
      TRANSLATION_SIGN => '&#58;',
      FORMAT_SIGN => '&#37;',
      '(' => '&#40;',
      ')' => '&#41;',
    }.map{|key, value|
      ["\\#{key}".freeze, value.freeze]
    }.to_h.freeze


    # 記号や括弧をエスケープ（文字参照に置換）
    def self.escape(format)
      format.gsub(Regexp.union(CHARACTER_REFERENCES.keys), CHARACTER_REFERENCES)
    end

    # @param [String] str フォーマット文字列に含まれる引数文字列
    # @return [Array] 引数の配列
    # @todo `Kernel#eval`を使っているのでセキュリティーが心配だ
    def self.strftime_args_parse(str)
      eval(str.sub(/^\(/, "[").sub(/\)$/, "]"))
    end

    # @param [String] str 整形前の文字列
    # @param [Integer] width 整形後の文字列の幅（長さ）
    # @param [String] flag 余ったところを埋める文字
    # @return [String] 整形後の文字列
    def self.align(str, width, flag = " ")
      if flag == "0" && width > 0 && matched = str.match(/\A([-+])(.*)\z/)
        matched[1] + align(matched[2], width, flag)
      else
        flags = flag.to_s * (width.abs - str.length)
        width > 0 ? flags + str : str + flags
      end
    end

    # @param [String] str 切り捨て前の文字列
    # @param [Integer] width 切り捨て後の文字列の幅（長さ）
    # @return [String] 切り捨て後の文字列
    def self.omit(str, width)
      if width > 0
        str.slice(str.length - width, str.length)
      else
        str.slice(0, width.abs)
      end
    end

    # @overload strftime(format)
    #   @param [String] format フォーマット文字列
    #   @return [String] 置換済みの文字列
    # @overload strftime(key, locale: locale)
    #   @param [Symbol] key 訳文キー
    #   @param [FDate::Locale,String] locale ロケールオブジェクトかそのキー
    #   @return [String] ロケールからキーで取得した訳文
    # @overload strftime(locale: locale)
    #   @param [FDate::Locale,String] locale ロケールオブジェクトかそのキー
    #   @return [String] デフォルトの訳文
    # @overload strftime(name_set)
    #   @param [FDate::NameSet] name_set 名前セットオブジェクト
    #   @return [String] 得られた名前
    # @overload strftime(arg)
    #   @param [Object] arg それ以外のオブジェクト
    #   @return [String] `arg#to_s`で得られた文字列
    def strftime(arg = DEFAULT_TRANSLATION_KEY, locale: nil)
      locale = Locale.collection[locale]

      case arg
      when String then strftime_parse(arg, locale: locale)
      when Symbol then strftime(locale[self, arg], locale: locale)
      when NameSet then strftime(arg[self], locale: locale)
      else strftime_parse(arg.to_s, locale: locale)
      end
    end

    # @param [String] format フォーマット文字列
    # @param [FDate::Locale,String] locale ロケールオブジェクトかそのキー
    # @return [String] 置換済みの文字列
    # @example フォーマットが'$xxx()'を含む場合
    #   owner.foobar #=> "ふーばー"
    #   owner.strftime_parse("Foo$foobar()Bar") #=> "FooふーばーBar"
    # @example フォーマットが'@xxx()'を含む場合
    #   owner.foobar.strftime #=> "ふーばー"
    #   owner.strftime_parse("Foo@foobar()Bar") #=> "FooふーばーBar"
    # @example フォーマットが':xxx()'を含む場合
    #   locale[owner, "foobar"] #=> "ふーばー"
    #   owner.strftime_parse("Foo:foobar()Bar", locale: locale) #=> "FooふーばーBar"
    # @example フォーマットが'%x'を含む場合
    #   locale[owner, "a"] #=> "ふーばー"
    #   owner.strftime_parse("Foo%aBar", locale: locale) #=> "FooふーばーBar"
    def strftime_parse(format, locale: nil)
      locale = Locale.collection[locale]

      Strftime.escape(format).gsub(Regexp.union(PATTERN, FORMAT_TRANSLATION_PATTERN)){
        result = $~[:specifier] ? $~[:specifier].to_sym : strftime_sub($~[:sign], $~[:name], $~[:bracket], locale)
        str = strftime(result, locale: locale)
        str = Strftime.omit(str, $~[:precision].to_i) unless $~[:precision].nil?
        str = Strftime.align(str, $~[:width].to_i, $~[:flag] || " ") unless $~[:width].nil?
        str
      }
    end

    private

    # @param [String] sign 処理の種類を表す記号
    # @param [String] name メソッド名またはキー
    # @param [String] bracket 括弧と引数の文字列
    # @param [FDate::Locale] locale ロケールオブジェクト
    # @return [Object] 処理により得られたオブジェクト
    # @todo メソッド名は仮
    def strftime_sub(sign, name, bracket, locale)
      args = Strftime.strftime_args_parse(bracket)
      case sign
      when DELEGATION_SIGN then public_send(name, *args)
      when STRFTIME_SIGN then
        result = public_send(name)
        result.strftime(*args, locale: locale) unless result.nil?
      when TRANSLATION_SIGN then name.to_sym
      end
    end
  end
end
