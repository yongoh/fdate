module FDate
  module Comparable
    include ::Comparable

    # 同種のオブジェクトと比較
    # @return [-1] `self < other`の場合
    # @return [0] `self == other`の場合
    # @return [1] `self > other`の場合
    # @return [nil] 型が違うなど比較できないオブジェクトを渡した場合
    # 
    # <pre>
    # [self] <other>
    #      [  ] <  > | -1 | self < other
    #      [  X  >   |  0 | self == other
    #      [ <] >    |  0 | self == other
    #     [< >]      |  0 | self == other
    #      {  }      |  0 | self == other
    #     <[ ]>      |  0 | self == other
    #    < [> ]      |  0 | self == other
    #   <  X  ]      |  0 | self == other
    # <  > [  ]      |  1 | self < other
    # </pre>
    def <=>(other)
      case other
      when ::Range then
        if global_range.max < other.min    # self < other
          -1
        elsif global_range.min > other.max # self > other
          1
        else                        # self == other
          0
        end
      when Numeric then
        self <=> (other..other)
      end
    end

    alias_method :cover?, :==
  end
end
