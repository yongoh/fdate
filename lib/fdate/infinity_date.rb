module FDate

  # 無限過去/未来日付クラス
  # @note `FDate::Term`内で使う
  class InfinityDate
    extend BuildMethods
    include FullDate
    include Strftime

    # 無限過去/未来日付コードの正規表現
    CODE_PATTERN = /\A\(#{Parseable::INFINITY_PATTERN}\)\z/


    # @!attribute [r] jd
    #   @return [Float::INFINITY] 無限過去/未来のユリウス通日（正か負の無限数）
    attr_reader :jd

    # @param [Boolean] positive 真なら無限未来、偽なら無限過去オブジェクトを生成する
    def initialize(positive = true)
      @jd = Float::INFINITY
      @jd *= -1 unless positive
    end

    # @return [Boolean] 比較対象が無限過去/未来日付オブジェクトで正負が同じかどうか
    def eql?(other)
      self.class == other.class && jd == other.jd
    end

    # @return [String] 無限過去/未来日付コード
    # @example 無限未来の場合
    #   date.positive? #=> true
    #   date.global_code #=> "(Infinity)"
    # @example 無限過去の場合
    #   date.negative? #=> true
    #   date.global_code #=> "(-Infinity)"
    def global_code
      "(#{jd})"
    end

    # エイリアス
    [%i(local_code global_code)].each do |new_name, original_name|
      define_method(new_name){|*args, &block| send(original_name, *args, &block) }
    end

    %w(first last begin end min max center).each do |direction|

      # @return [FDate::InfinityDate] 自身
      define_method(direction){ self }

      alias_method "#{direction}_jd", :jd
    end

    # @return [Range<Float::INFINITY>] ユリウス通日の範囲
    def jd_range
      jd..jd
    end

    # @return [false] 誤差無しではないことを示す
    def fixed?
      false
    end

    # @return [true] 誤差ありであることを示す
    def fuzzy?
      true
    end

    # @return [Boolean] 無限未来であるかどうか
    def positive?
      jd > 0
    end

    # @return [Boolean] 無限過去であるかどうか
    def negative?
      jd < 0
    end
  end
end
