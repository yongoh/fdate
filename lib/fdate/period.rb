module FDate

  # 時代クラス
  class Period

    # @!attribute [r] key
    #   @return [String] この時代のキー
    #   @note `FDate::Period::Set`内で使う
    # @!attribute [r] term
    #   @return [FDate::Term] この時代の期間
    # @!attribute [r] context
    #   @return [Hash] おまけ情報群
    attr_reader :key, :term, :context

    # @param [String] key 時代キー
    # @param [FDate::Term] term 期間オブジェクト
    # @param [Hash] context 任意のおまけ情報群
    def initialize(key, term, context = {})
      @key = key
      @term = term
      @context = context
    end
  end
end
