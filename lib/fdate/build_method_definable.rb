module FDate
  module BuildMethodDefinable

    private

    # ハッシュ化メソッド`#_{method_name}`を使ってインスタンス化するメソッドを作成する
    # @param [Array<Symbol>] method_names メソッド名の配列
    def build_methods(*method_names)
      method_names.each do |method_name|
        define_method(method_name) do |*args, &block|
          klass = is_a?(Module) ? self : self.class
          klass.new(send("_#{method_name}", *args, &block))
        end
      end
    end
  end
end
