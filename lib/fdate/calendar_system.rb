# 暦体系クラス
module FDate
  class CalendarSystem < Calendar
    extend Forwardable
    extend CalendarsMethods
    extend BuildMethods

    # @!attribute [r] object
    #   @return [FDate::Calendar] 日付オブジェクト
    attr_reader :object

    def initialize(object)
      @object = object

      validate_calendar
      def_delegators
    end

    # @note コピー系メソッドは`#object`もコピーする
    def initialize_copy(obj)
      @object = obj.object.dup
    end

    # 状態変更系メソッド
    # @note `#object`も同様に変更する
    %i(taint untaint trust untrust freeze).each do |method_name|
      original_method = "original_#{method_name}"
      alias_method original_method, method_name

      define_method(method_name) do
        object.send(method_name)
        send(original_method)
      end
    end

    # @return [String] FDateコード
    # @example
    #   fdate.local_code #=> "adjl(Italy)_1234-10-2*_ab"
    def local_code
      [compound_calendar_key, local_code_date, about.key].join(FDate::SEPARATOR)
    end

    # @return [String] 自身と`#object`の暦キーを複合した暦キー
    def compound_calendar_key
      "#{object.calendar.key}(#{calendar.key})"
    end

    private

    # @raise [FDate::DateError] `#object.calendar`がこの日付に使える暦ではない場合に発生
    def validate_calendar
      unless calendar.valid_fdate?(object)
        raise FDate::DateError.new(self), "#{object.calendar} is not used in #{calendar} on #{object}"
      end
    end

    # 自身の特異クラスにデリゲートを定義
    def def_delegators(methods = delegate_methods)
      self.singleton_class.def_delegators :object, *methods
    end

    # @return [Array<Symbol>] `#object`にデリゲートするメソッド名の配列
    # @note 暦特有のメソッドや特異メソッドにも対応
    def delegate_methods
      object.public_methods
        .-(Object.instance_methods - %i(!~ <=> === =~ eql?))
        .-(%i(calendar))
        .-(FDate::Encodable.instance_methods)
    end
  end
end
