module FDate
  class Locale
    extend Forwardable
    include InheritableCollection::InstanceMember
    include Strftime

    # YAMLの読み込み処理
    AUTOLOAD_PROC = ->(key, path){
      build(key) unless has_key?(key)
      hash = YAML.load_file(path)
      if hash.has_key?("bases")
        hash["bases"].each do |base_key|
          self[key].reverse_merge!(self[base_key])
        end
      end
      self[key].merge!(hash["translations"]) if hash.has_key?("translations")
    }


    def_instance_collection

    # @!attribute [r] data
    #   @return [Hash] 訳文のハッシュのハッシュ
    attr_reader :data

    def_delegators :data, :each

    # @param [String] key コレクションから取得するためのキー
    # @param [Hash] data 訳文のハッシュのハッシュ
    def initialize(key, data = {})
      self.key = key
      @data = data
    end

    def initialize_copy(obj)
      @data = obj.data.dup
    end

    # 状態変更系メソッド
    %i(taint untaint trust untrust freeze).each do |method_name|
      original_method = "original_#{method_name}"
      alias_method original_method, method_name

      define_method(method_name) do
        data.send(method_name)
        data.each_key(&method_name)
        data.each_value(&method_name)
        send(original_method)
      end
    end

    # @param [Object,Module,String] obj_key オブジェクトキー
    # @overload [](obj)
    #   @return [Hash] 訳文のハッシュ
    # @overload [](obj, trs_key)
    #   @param [String] trs_key 訳文キー
    #   @return [String] 訳文
    #   @return [Symbol] 他の訳文キー
    #   @return [FDate::NameSet] 名前セットオブジェクト
    # @raise [KeyError] 遡っても訳文もしくは訳文ハッシュが見つからない場合に発生
    def [](obj, trs_key = nil)
      Locale.each_keys_by(obj) do |obj_key|
        return (trs_key ? data.fetch(obj_key).fetch(trs_key.to_s) : data.fetch(obj_key)) rescue KeyError
      end

      message = "key not found: #{obj.inspect}"
      message += ", #{trs_key.inspect}" if trs_key
      raise KeyError, message
    end

    # @param [Object,Module,String] obj_key オブジェクトキー
    # @param [String] trs_key 訳文キー
    # @param [String,Symbol,FDate::NameSet] translation 追加する訳文
    # @return [String,Symbol,FDate::NameSet] 追加した訳文
    def []=(obj_key, trs_key, translation)
      trs_hash = data.fetch(obj_key){ data[obj_key] = {} }
      trs_hash[trs_key.to_s] = build_translation(translation)
    end

    # @param [FDate::Locale,Hash] other 他のロケールオブジェクトもしくはハッシュ
    # @return [self] マージ後の自身
    def merge!(other)
      other.each do |obj_key, trs_hash|
        trs_hash.each do |trs_key, translation|
          self[obj_key, trs_key] = translation
        end
      end
      self
    end

    # @param [FDate::Locale,Hash] other 他のロケールオブジェクトもしくはハッシュ
    # @return [FDate::Locale] 自身と引数をマージした新しいロケールオブジェクト
    def merge(other)
      dup.merge!(other)
    end

    # 引数より自身の値を優先したマージ
    # @param [FDate::Locale,Hash] other 他のロケールオブジェクトもしくはハッシュ
    # @return [self] マージ後の自身
    def reverse_merge!(other)
      other.each do |obj_key, trs_hash|
        data.fetch(obj_key){ data[obj_key] = {} }
        trs_hash.each do |trs_key, translation|
          h = data[obj_key]
          h.fetch(trs_key){ h[trs_key] = translation }
        end
      end
      self
    end

    private

    # 訳文をオブジェクトに変換
    def build_translation(arg)
      if arg.is_a?(Hash) && arg.has_key?("names")
        NameSet.build(arg)
      else
        arg
      end
    end

    # @param [Object,Module] obj オブジェクト
    # @overload each_keys_by(obj){}
    #   @yield [key] 優先キー順
    #   @yieldparam [Object,Module,String] `obj`のキーとみなしうるオブジェクト
    # @overload each_keys_by(obj)
    #   @return [Array] `obj`のキーとみなしうるオブジェクトを優先順に格納した配列
    def self.each_keys_by(obj)
      if block_given?
        if obj.is_a?(Module)
          obj.ancestors.each do |mod|
            yield(mod)
            yield(mod.name_with_key) if mod.respond_to?(:name_with_key)
            yield(mod.key) if mod.respond_to?(:key) && mod.key
            yield(mod.name) if mod.name
          end
        else
          yield(obj)
          yield("#<#{obj.class_name_with_key}>") if obj.respond_to?(:class_name_with_key)
          each_keys_by(obj.class) do |obj_key|
            yield("#<#{obj_key}>") if obj_key.is_a?(String)
          end
        end
      else
        ary = []
        each_keys_by(obj){|obj_key| ary << obj_key }
        ary
      end
    end
  end
end
