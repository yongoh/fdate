module FDate
  class Period

    # 時代セットクラス
    # @note レーン無しバージョン
    # @todo クラス名が不明瞭
    class Set
      include Enumerable

      # @!attribute [r] periods
      #   @return [Array<FDate::Period>] 時代オブジェクトの配列
      #   @note 順不同
      attr_reader :periods

      # @param [FDate::Period] periods 時代オブジェクトの配列
      #   @note インターフェイスが揃っていれば`FDate::Period`でなくてもかまわない
      def initialize(periods)
        @periods = periods
      end

      def each(&block)
        periods.each(&block)
      end

      # 時代群を取得
      # @overload [](date)
      #   @param [Integer,Range,FDate::FullDate] arg 取得したい時代の期間に含まれる日付かその範囲
      # @overload [](key)
      #   @param [String] arg 時代のキー
      # @return [Array<FDate::Period>] 時代オブジェクトの配列
      def [](arg)
        case arg
        when Integer, Range, FDate::FullDate
          periods.select{|period| period.term.cover?(arg) }
        when String, Symbol
          periods.select{|period| period.key == arg }
        end
      end

      # 期間ハッシュの`:last`が無い場合、次の期間の`#first`をもって充てる
      # @param [Array] _terms 期間もしくはそのインスタンス化用引数を含む配列の配列
      # @yield [item, &to_term] 
      # @yieldparam [Object] item 配列の各メンバー
      # @yieldparam [Proc] to_term 終端を補った期間オブジェクト生成用Proc
      # @yieldreturn [Object] 返り値となる配列のメンバー
      # @return [Array] ブロックの結果の配列
      # @example
      #   FDate::Period::Set.fill_last([[arg, _term], ...]) do |arg, _term, &to_term|
      #
      #     # Procを使って終端を補った期間オブジェクトを生成する
      #     term = to_term.call(_term)
      #
      #     # ブロックの結果が返り値となる配列のメンバーとなる
      #     klass.new(arg, term)
      #   end
      #   #=> [#<klass>, ...]
      # @note 終端を補えるのはハッシュに限る
      def self.fill_last(array, &block)
        block ||= ->(_term, &to_term){ to_term.call(_term) }

        next_hash = {last: +FDate::InfinityDate}
        to_term = ->(_term){
          term = case _term
            when Hash then FDate::Term.new(next_hash.merge(_term))
            when FDate::Term then _term
            else FDate::Term.build(_term)
            end
          next_hash = {last: term.first, exclude_end: true}
          term
        }

        array.reverse.map do |*args|
          block.call(*args, &to_term)
        end.reverse
      end
    end
  end
end
