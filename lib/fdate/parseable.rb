module FDate
  module Parseable
    extend BuildMethodDefinable

    # 正負符号付き数値文字列の正規表現
    NUM_PATTERN = /[-+]?\d+/.freeze

    # 無限過去/未来の正規表現
    INFINITY_PATTERN = /[-+]?Infinity/.freeze

    # グローバルコードの正規表現
    GLOBAL_CODE_PATTERN = /^\((#{NUM_PATTERN})\)$/.freeze

    build_methods :parse, :decode, :local_parse, :global_parse

    # コードの形式ごとに振り分ける
    # @param [String] code コード
    # @return [Hash] インスタンス化用ハッシュ
    def _parse(code)
      if GLOBAL_CODE_PATTERN =~ code
        _global_parse(code)
      else
        _local_parse(code)
      end
    end

    # エイリアス
    [%i(_decode _parse)].each do |new_name, original_name|
      define_method(new_name){|*args, &block| send(original_name, *args, &block) }
    end

    # 独自の書式のコードでパース
    # @param [String] code コード
    # @return [Hash] インスタンス化用ハッシュ
    def _local_parse(code)
      # オーバーライド
    end

    # `#to_i`に対応するコードでパース
    # @param [String] code グローバルコード
    # @return [Hash] インスタンス化用ハッシュ
    def _global_parse(code)
      _from_i(Parseable.global_parse(code))
    end

    # @param [String] code グローバルコード
    # @return [Integer] グローバルコードから得られた数値
    # @raise [ParseError] `code`がグローバルコードではない場合に発生
    def self.global_parse(code)
      if GLOBAL_CODE_PATTERN =~ code
        code.slice(GLOBAL_CODE_PATTERN, 1).to_i
      else
        raise ParseError.new(code)
      end
    end
  end
end
