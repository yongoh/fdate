module FDate
  class InfinityDate
    module BuildMethods
      include Parseable

      # @param [String] code 判定する文字列
      # @return [Boolean] パース可能なコードかどうか
      def parseable?(code)
        !!CODE_PATTERN.match(code)
      end

      # @param [String] code 無限過去/未来日付コード
      # @return [FDate::InfinityDate] 無限過去/未来日付オブジェクト
      # @raise [FDate::ParseError] 不正なコードを渡した場合に発生
      def _parse(code)
        if CODE_PATTERN =~ code
          !negative_code?(code)
        else
          raise FDate::ParseError.new(code)
        end
      end

      # エイリアス
      [
        %i(_global_parse _parse),
        %i(_local_parse _parse),
      ].each do |new_name, original_name|
        define_method(new_name){|*args, &block| send(original_name, *args, &block) }
      end

      # @return [FDate::InfinityDate] 無限未来日付オブジェクト
      def positive
        new(true)
      end

      alias_method :+@, :positive

      # @return [FDate::InfinityDate] 無限過去日付オブジェクト
      def negative
        new(false)
      end

      alias_method :-@, :negative

      private

      # @param [String] code 判定するコード
      # @return [Boolean] 渡したコードが無限過去日付コードかどうか
      # @todo 正規表現が不十分。`CODE_PATTERN =~ code`で判定済みであることを前提にしている。
      def negative_code?(code)
        /\A\(-/ =~ code
      end
    end
  end
end
