module FDate
  module FullDate
    include Encodable
    include Calculatable
    include Comparable
    include Tarmizable

    # エイリアス
    [
      %i(inspect code),
      %i(jd_code global_code),
      %i(global_range jd_range),
      %i(to_i center_jd),
    ].each do |new_name, original_name|
      define_method(new_name){|*args, &block| send(original_name, *args, &block) }
    end

    # @return [Integer] 日付範囲のうち真ん中の日のユリウス通日
    def center_jd
      (last_jd - first_jd) / 2 + first_jd
    end

    # 同種のオブジェクトと比較
    # @param [FullDate] other 比較対象の日付オブジェクト
    # @return [-1] `self < other`の場合
    # @return [0] `self == other`の場合
    # @return [1] `self > other`の場合
    # @return [nil] 比較できないオブジェクトを渡した場合
    def <=>(other)
      if other.is_a?(FullDate)
        self <=> other.global_range
      else
        super(other)
      end
    end

    # @return [Boolean] 自身が日付オブジェクトであるかどうか
    def fdate?
      is_a?(FDate::Calendar)
    end

    # @return [Boolean] 自身が期間オブジェクトであるかどうか
    def term?
      is_a?(FDate::Term)
    end

    # @return [Boolean] 自身が無限日付オブジェクトであるかどうか
    def infinity?
      is_a?(FDate::InfinityDate)
    end
  end
end
