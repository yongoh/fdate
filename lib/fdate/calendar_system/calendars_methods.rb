# 暦体系クラス
module FDate
  class CalendarSystem < Calendar
    module CalendarsMethods

      # @!attribute [r] calendars
      #   @return [FDate::Period::Set] 暦時代セット
      attr_reader :calendars

      # 暦時代群をセット
      # @param [Array<FDate::Period>] periods 暦情報を持つ時代オブジェクトの配列
      # @note 時代キー＝暦キーであるか、`#context[:calendar]`に暦キーか暦オブジェクト（日付クラス）を持つこと
      # @note 時代の開始日順にソートされる
      def calendars=(periods)
        periods.each do |period|
          period.context[:calendar] = FDate::Calendar.collection[period.context[:calendar] || period.key]
        end
        @calendars = FDate::Period::Set.new(periods.sort_by{|period| period.term.first_jd })
      end

      # @param [arg] 日付に変換しうるオブジェクト
      #   @see FDate::Period::Set#[]
      # @return [Class<FDate::Calendar>] 見つかった中で使用開始日が最も遅い暦。見つからなかった場合はデフォルト暦。
      def calendar_get(arg)
        if period = calendars[arg].last
          period.context[:calendar]
        else
          default_calendar
        end
      end

      # @return [Class<FDate::Calendar>] デフォルト暦
      # @note 初期状態では`#calendars`のうち使用開始日が最も遅い暦
      def default_calendar
        @default_calendar ||= calendars.periods.last.context[:calendar]
      end

      # デフォルト暦をセット
      # @param [String,Class<FDate::Calendar>] arg 暦キーもしくは暦
      # @return [Class<FDate::Calendar>] デフォルト暦
      def default_calendar=(arg)
        @default_calendar = FDate::Calendar.collection[arg]
      end

      # @param [FDate::Calendar] fdate 判定対象の日付オブジェクト
      # @return [Boolean] 暦`fdate.calendar`が使われている期間の日付かどうか
      def valid_fdate?(fdate)
        periods = calendars[fdate]
        if periods.empty?
          default_calendar.equal?(fdate.calendar)
        else
          periods.any?{|period| period.context[:calendar] == fdate.calendar }
        end
      end
    end
  end
end
