# 暦体系クラス
module FDate
  class CalendarSystem < Calendar
    module BuildMethods

      # @param [String] code 日付コード（FDateコードの日付部分）
      # @return [self.class] 渡した日付の暦の日付オブジェクト
      def _local_parse(code)
        fdate = default_calendar.local_parse(code)
        calendar_get(fdate).local_parse(code)
      end

      # @param [Integer] jd ユリウス通日
      # @return [self.class] 渡した日付の暦の日付オブジェクト
      def from_jd(jd)
        calendar_get(jd).jd(jd)
      end
    end
  end
end
