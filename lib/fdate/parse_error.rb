module FDate

  # パースエラー
  #
  # コードに問題がありパースできない場合に発生
  class ParseError < StandardError

    # @!attribute [r] code
    #   @return [String] パースしようとしたコード
    attr_reader :code

    def initialize(code)
      @code = code
      super("It's wrong grammar. #{code.inspect}")
    end
  end
end
